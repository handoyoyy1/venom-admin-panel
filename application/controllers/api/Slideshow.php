<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Slideshow extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Slideshow_model');
    }

    

    function index_get(){
        $category = $this->get('category');

        $slide = $this->Slideshow_model->get_all($category);
                $this->response([
                            'kode' => 1,
                            'result' => $slide,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }
}