<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rating extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data_rating=$this->Model->GetRating("LEFT JOIN tb_community ON tb_community.id_community=tb_rating.community_id LEFT JOIN tb_user ON  tb_user.id_user=tb_rating.user_id order by tb_rating.create_on desc")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_rating' => $data_rating,
		);
		$this->load->view('rating/rating', $data);
	}
}