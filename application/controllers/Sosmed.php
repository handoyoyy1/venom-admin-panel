<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sosmed extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_sosmed' => $this->Model->GetSosmed("order by id_sosmed desc")->result_array(),
		);
		$this->load->view('sosmed/sosmed', $data);
	}

	function addsosmed()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('sosmed/add_sosmed', $data);
	}

	function savedata(){
		
		$id_sosmed = '';
		$nama = $_POST['nama'];
		$url = $_POST['url'];		
		$kategori = $_POST['kategori'];

		$data = array(	
			'id_sosmed'=> $id_sosmed,
			'nama_sosmed' => $nama,
			'url' => $url,
			'kategori' => $kategori
			);
		
		$result = $this->Model->Simpan('tb_sosmed', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}		
	}

	function editsosmed($kode = 0){
		$data_sosmed = $this->Model->GetSosmed("where id_sosmed = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_sosmed' => $data_sosmed[0]['id_sosmed'],
			'nama_sosmed' => $data_sosmed[0]['nama_sosmed'],
			'url' => $data_sosmed[0]['url'],
			'kategori' => $data_sosmed[0]['kategori'],
			);
		$this->load->view('sosmed/edit_sosmed', $data);
	}

	function updatesosmed(){

		$data = array(
			'id_sosmed' => $this->input->post('id'),
			'nama_sosmed' => $this->input->post('nama'),
			'url' => $this->input->post('url'),
			'kategori' => $this->input->post('kategori')
			        
			);

		$res = $this->Model->UpdateSosmed($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_sosmed', array('id_sosmed' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'sosmed');
		}
	}
}