<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diskon extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		$this->load->helper('currency_format_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama_user'),
			'data_diskon' => $this->Model->GetDiskon("order by id desc")->result_array(),
		);
		$this->load->view('diskon/diskon', $data);
	}

	function adddiskon()
	{
		$data = array(
			'nama' => $this->session->userdata('nama_user'),
		);
		
		$this->load->view('diskon/add_diskon', $data);
	}

	function savedata(){

		$id = '';
		$kode_voucher = $_POST['kode_voucher'];
		$nominal = $_POST['nominal'];
		if($_POST['nominal']<100){
		    $category = "diskon";
		}else{
		    $category = "voucher";
		}
		$expired = $_POST['expired'];

		$data = array(	
			'id'=> $id,
			'kode_voucher' => $kode_voucher,
			'nominal' => $nominal,
			'category' => $category,
			'expired' => $expired,
			);
		
		$result = $this->Model->Simpan('tb_diskon', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}		
	}

	function editdiskon($kode = 0){
		$data_slide = $this->Model->GetDiskon("where id = '$kode'")->result_array();

		$data = array(
			'nama' => $this->session->userdata('nama_user'),	
			'id' => $data_slide[0]['id'],
			'kode_voucher' => $data_slide[0]['kode_voucher'],
			'nominal' => $data_slide[0]['nominal'],
			'category' => $data_slide[0]['category'],
			'expired' => $data_slide[0]['expired'],
			);
		$this->load->view('diskon/edit_diskon', $data);
	}

	function updatediskon(){
			
		$data = array(
			'id' => $this->input->post('id'),
			'kode_voucher' => $this->input->post('kode_voucher'),
			'nominal' => $this->input->post('nominal'),
			'category' => $this->input->post('category'),
			'expired' => $this->input->post('expired')
			);

		$res = $this->Model->UpdateDiskon($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_diskon', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'diskon');
		}
	}
}