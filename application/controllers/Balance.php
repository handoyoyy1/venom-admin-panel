<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		date_default_timezone_set("Asia/Jakarta");

	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function search_user(){ 
		if (isset($_GET['term'])) {
		  	$result = $this->Model->search_user($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
		     		//'label'	=> $row->first_name,
		     		'label'	=> $row->nama_user,
		     		'id'	=> $row->id_user,
					
				);
		     	echo json_encode($arr_result);
		   	}
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_balance' => $this->Model->Getbalance("order by id desc")->result_array(),
		);
		$this->load->view('balance/balance', $data);
	}

	function addbalance()
	{
		$data = array(
			'data_album' => $this->Model->GetAlbum("order by id_album desc")->result_array(),
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('balance/add_balance', $data);
	}

	function savedata(){
		$id_user = $_POST['id_user'];
		$jumlah = $_POST['jumlah'];
		$member_input = $this->session->userdata('id_user');
		$keterangan = $_POST['keterangan'];

		$data = array(	
			'balance' => $jumlah,
			'user_id' => $id_user,
			'member_input' => $member_input,
			'keterangan' =>$keterangan		
			);
		
		$result = $this->Model->Simpan('tb_balance', $data);
		if($result == 1){

			$data_produk = $this->Model->GetUser("where id_user = '$id_user'")->result_array();
			$total = $jumlah+$data_produk[0]['balance'];
			$data = array(
    			'id_user' => $id_user,
    			'balance' => $total,   
			); 
			$res = $this->Model->UpdateUser($data);
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'balance');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'balance');
		}		
	}

	
	function hapus($kode = 1){
		$row=$this->Model->Getbalance("where id = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->nama_balance));
		$result = $this->Model->Hapus('tb_balance', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'balance');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'balance');
		}
	}
}