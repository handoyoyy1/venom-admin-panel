<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>TAMBAH DATA USER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT DATA PROFIL</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>user/updateuser" method="POST" enctype="multipart/form-data">
                     <input type="hidden" class="form-control" value="<?php echo $id_user ?>" id="" name="id" placeholder="Isikan Nama User"> 
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Nama User</label>
                        <input type="text" class="form-control" value="<?php echo $nama_user ?>" id="" name="nama_user" placeholder="Isikan Nama User" required>
                    </div>

                    <div class="form-group">
                      <label for="">Password</label>
                        <input type="password" class="form-control" value="<?php echo $pass_user ?>" id="" name="password" placeholder="Password" required>                        
                    </div>
                    
                  </div>
                  <div class="col-lg-6">
                    
                    <div class="form-group">
                      <label for="">Nama Depan</label>
                        <input type="text" class="form-control" value="<?php echo $dpn ?>" id="" name="namadpn" placeholder="Nama Depan" required>                        
                    </div>
                    <div class="form-group">
                      <label for="">Nama Belakang</label>
                        <input type="text" class="form-control" value="<?php echo $blk ?>" id="" name="namablk" placeholder="Nama Belakang" required>                        
                    </div>
                    
                    <div class="form-group">
                      <label for="">No. Hp</label>
                        <input type="text" class="form-control" value="<?php echo $no_hp ?>" id="" name="nohp" placeholder="Nomor Hp" required>                        
                    </div>

                    <div class="form-group">
                      <label for="">Alamat</label>
                        <textarea style="height:100px"  class="form-control" name="alamat" placeholder="Alamat"><?php echo $alamat ?></textarea>                
                    </div>
                    
                  </div>
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>user" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
</body>
</html>