<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="50">NO</th>
                      <th>Foto</th>
                      <th>Nama Lengkap</th>
                      <th>Nama User</th>
                      <th width="100">Alamat</th>
                      <th>No. Hp</th><?php if($this->session->userdata('level') == '1'){ ?>
                      <th>Tanggal Daftar</th>
                      <th width="100">Email</th>
                      <th width="100">Action</th><?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_user as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td>
                      <?php if($row['foto']!=null || $row['foto'] != ""){ ?>  
                    <img src="<?php echo $row['foto']; ?>" height="100" width="100" alt="user image" class="online"/>
                    <?php }else{ ?> 
                    <img src="http://venom.levanpedia.com/assets/dist/img/user2-160x160.jpg" height="100" width="100" alt="user image" class="online"/>
                    <?php } ?>
                      </td>
                      <td><?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?></td>
                       <td><?php echo $row['nama_user']; ?></td>
                      <td><?php echo $row['alamat']; ?></td>
                      <td><?php echo $row['no_hp']; ?></td>
                      <td><?php echo $row['create_on']; ?></td>
                      <td><?php echo $row['email']; ?></td>
                      <?php if($this->session->userdata('level') == '1'){ ?>
                      <td>
                      <!--<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>user/detail/<?php echo $row['id_user']; ?>"><i class="fa fa-eye"></i></a>-->
                     <!-- <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>user/editmember/<?php echo $row['id_user']; ?>"><i class="fa fa-pencil"></i></a>-->
                      <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>pesan/addpesan/<?php echo $row['id_user']; ?>">Pesan</i></a>
                      </td><?php } ?>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>