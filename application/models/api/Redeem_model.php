<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Redeem_model extends CI_Model
{
	public $table = 'tb_reedem';
    public $id = 'id';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }

    function get_all($user_id)
    {
        $this->db->where('user_id',$user_id);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    public function exist_row_check($table,$field,$data){
        $this->db->where($field,$data);
        $this->db->from($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    
    function get_by_id($table,$field,$data)
    {
        $this->db->where($field,$data);
        return $this->db->get($table)->row();
    }


}