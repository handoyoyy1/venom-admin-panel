<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_community' => $this->Model->GetCommunity("left join tb_kota on tb_kota.id_kota=tb_community.kota_id order by id_community desc")->result_array(),
		);
		$this->load->view('community/community', $data);
	}

	function addcommunity()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_kota' => $this->Model->GetKota()->result_array(),
			'data_user' => $this->Model->GetUser('where level=4')->result_array(),
		);
		
		$this->load->view('community/add_community', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/community',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');//
		$upload_data = $this->upload->data();

		$id_community = '';
		$userid = $_POST['userid'];
		$nama = $_POST['nama'];
		$pemilik = $_POST['pemilik'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$jenis = $_POST['jenis'];
		$lang = $_POST['lang'];
		$lat = $_POST['lat'];
		$ultah = $_POST['ultah'];
		$file_name = $upload_data['file_name'];
		$qr_code = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/upload/community/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name=$qr_code.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = $qr_code; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
 

		$data = array(	
			'id_community'=> $id_community,
			'user_id' => $userid,
			'nama' => $nama,
			'pemilik' => $pemilik,
			'hp' => $hp,
			'email' => $email,
			'alamat' => $alamat,
			'kota_id' => $kota,
			'jenis' => $jenis,
			'lat' => $lat,
			'lang' => $lang,
			'qr_code' => $qr_code,
			'ultah' => $ultah,
			'qr_img' => /*base_url('assets/upload/community/qrcode/'.*/$image_name,
			'images' => /*base_url()."assets/upload/community/".*/$file_name		
			);
		
		$result = $this->Model->Simpan('tb_community', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'community');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'community');
		}		
	}

	function editcommunity($kode = null){
	    if($kode == null){
	        $userid = $this->session->userdata('id_user');
	        if ($this->Model->exist_row_check('tb_community','user_id', $userid) > 0) {
	            $data_community = $this->Model->GetCommunity("where user_id = '$userid'")->result_array();
        		$data = array(
        			'nama' => $this->session->userdata('nama'),	
        			'data_user' => $this->Model->GetUser('where level not in ("8","NULL","1","2","3","4") order by id_user desc')->result_array(),
        			'id_community' => $data_community[0]['id_community'],
        			'user' => $data_community[0]['user_id'],
        			'nama' => $data_community[0]['nama'],
        			'pemilik' => $data_community[0]['pemilik'],
        			'hp' => $data_community[0]['hp'],
        			'email' => $data_community[0]['email'],
        			'alamat' => $data_community[0]['alamat'],
        			'kota' => $data_community[0]['kota_id'],
        			'jenis' => $data_community[0]['jenis'],
        			'images' => $data_community[0]['images'],
        			'lat' => $data_community[0]['lat'],
        			'lang' => $data_community[0]['lang'],
        			'qr_code' => $data_community[0]['qr_code'],
        			'qr_img' => $data_community[0]['qr_img'],
        			'ultah' => $data_community[0]['ultah'],
        			'data_kota' => $this->Model->GetKota()->result_array(),
        			);
        		$this->load->view('community/edit_community', $data);
	            
	        }else{
        		$data = array(
        			'nama' => $this->session->userdata('nama'),	
        			'id_community' => null,
                                'data_user' => null,
                                'user' => null,
        			'nama' => null,
        			'pemilik' => null,
        			'hp' => null,
        			'email' => null,
        			'alamat' => null,
        			'kota' => null,
        			'jenis' => null,
        			'images' => null,
        			'lat' => null,
        			'lang' => null,
        			'qr_code' => null,
        			'qr_img' => null,
        			'data_kota' => null,
        			'ultah' => null,
        			);
        		$this->load->view('community/edit_community', $data);
	            
	        }
	        
    		
	        
	    }else{
	    
		$data_community = $this->Model->GetCommunity("where id_community = '$kode'")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_community' => $data_community[0]['id_community'],
			'nama' => $data_community[0]['nama'],
                        'user' => $data_community[0]['user_id'],
			'pemilik' => $data_community[0]['pemilik'],
			'hp' => $data_community[0]['hp'],
			'email' => $data_community[0]['email'],
			'alamat' => $data_community[0]['alamat'],
			'kota' => $data_community[0]['kota_id'],
			'jenis' => $data_community[0]['jenis'],
			'images' => $data_community[0]['images'],
			'lat' => $data_community[0]['lat'],
			'lang' => $data_community[0]['lang'],
			'qr_code' => $data_community[0]['qr_code'],
			'ultah' => $data_community[0]['ultah'],
			'qr_img' => $data_community[0]['qr_img'],
            'data_user' => $this->Model->GetUser('where level not in ("8","NULL","1","2","3","4") order by id_user desc')->result_array(),
			'data_kota' => $this->Model->GetKota()->result_array(),
			);
		$this->load->view('community/edit_community', $data);
		
	    }
	}

	function updatecommunity(){
		$config = array(
				'upload_path' => './assets/upload/community',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();

		$qr_code=$this->input->post('qr_code');
		$qr_img=$this->input->post('qr_img');	
		if ($this->input->post('qr_code')=="" or $this->input->post('qr_img')=="") {
			$qr_code = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
	 
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './assets/'; //string, the default is application/cache/
	        $config['errorlog']     = './assets/'; //string, the default is application/logs/
	        $config['imagedir']     = './assets/upload/community/qrcode/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=$qr_code.'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $qr_code; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
	        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
	        $qr_img=/*base_url('assets/upload/community/qrcode/'.*/$image_name;
		}

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_community' => $this->input->post('id'),
				'nama' => $this->input->post('nama'),
				'pemilik' => $this->input->post('pemilik'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'kota_id' => $this->input->post('kota'),
				'jenis' => $this->input->post('jenis'),
				'lat' => $this->input->post('lat'),
				'lang' => $this->input->post('lang'),
				'ultah' => $this->input->post('ultah'),
				'qr_code' => $qr_code, 
				'qr_img' => $qr_img, 
				'images' => /*base_url()."assets/upload/community/".*/$upload_data['file_name']
				);
			$row=$this->Model->GetCommunity("where id_community = '$data[id_community]'")->row();
			unlink(str_replace(base_url(), "", $row->images));
			$res = $this->Model->Updatecommunity($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'community');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'community');
			}
		}else{
			
			$data = array(
				'id_community' => $this->input->post('id'),
				'nama' => $this->input->post('nama'),
				'pemilik' => $this->input->post('pemilik'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'kota_id' => $this->input->post('kota'),
				'jenis' => $this->input->post('jenis'),
				'lat' => $this->input->post('lat'),
				'lang' => $this->input->post('lang'),
				'ultah' => $this->input->post('ultah'),
				'qr_code' => $qr_code, 
				'qr_img' => $qr_img, 
				);

			$res = $this->Model->Updatecommunity($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'community');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'community');
			}
		}
	}

	function hapus($kode = 1){
		$row=$this->Model->GetCommunity("where id_community = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->images));
		$result = $this->Model->Hapus('tb_community', array('id_community' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'community');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'community');
		}
	}
}