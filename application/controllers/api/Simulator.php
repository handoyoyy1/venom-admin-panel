<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Simulator extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Simulator_model');
    }
    
    function index_get(){
      
    }

    function index_post(){
    $user_id = $this->post('iduser');
    $simulator = $this->post('simulator');

    $data_simpan = array(
      'simulator' => $simulator,
      'user_id' => $user_id
    );

    $this->Simulator_model->insert($data_simpan);
    $message = array("kode"=>1,"pesan"=>"Simulator berhasil disimpan!");
    $this->response($message, REST_Controller::HTTP_CREATED);
  }
}
