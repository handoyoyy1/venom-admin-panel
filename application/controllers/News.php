<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		$this->load->helper('site_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	} 

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_news' => $this->Model->GetNews("order by id_news desc")->result_array(),
		);
		$this->load->view('news/news', $data);
	}

	function addnews()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('news/add_news', $data);
	}

	function savedata(){

		$config = array(
			'upload_path' => './assets/upload/news',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		
		$tgl = date('Y-m-d');
		
		$id_news = '';
		$judul = $_POST['judul'];
		$isi = $_POST['isi'];
		/*if ($this->session->userdata('level')==1) {
			$status="publish";
		}else{
			$status="draft";
		}*/
		$file_name = $upload_data['file_name'];

		$image = base_url()."assets/upload/news/".$file_name;
		//$image = $file_name;

		$data = array(	
			'id_news'=> $id_news,
			'judul' => $judul,
			'isi' => $isi,
			'image' => $image,
			'tanggal' =>$tgl,
			'status' => 'publish',			
			);
		
		$result = $this->Model->Simpan('tb_news', $data);
		
        $to = "/topics/global";
        $values = array();
        $values['status'] = "News";
        $values['title'] = $judul;
        $values['message'] = $isi;
        $values['image'] = $image;
        $values['timestamp'] = $tgl;
        $values['address'] = "Indonesia";
        
        
        notification($to,$values);
            

		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan '$tanggal'</strong></div>");
			header('location:'.base_url().'news');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'news');
		}	
		
	}

	function editnews($kode = 0){
		$data_slide = $this->Model->GetNews("where id_news = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_news' => $data_slide[0]['id_news'],
			'judul' => $data_slide[0]['judul'],
			'isi' => $data_slide[0]['isi'],
			'image' => $data_slide[0]['image'],
			'tanggal' => $data_slide[0]['tanggal'],
			);
		$this->load->view('news/edit_news', $data);
	}

	function updatenews(){
		$config = array(
				'upload_path' => './assets/upload/news',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();
		if ($this->session->userdata('level')==1) {
			$status="publish";
		}else{
			$status="draft";
		}
		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_news' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'isi' => $this->input->post('isi'),
				'status' =>$status,
				'image' => base_url()."assets/upload/news/".$upload_data['file_name']
				//'image' => $upload_data['file_name']
				);

			$res = $this->Model->UpdateNews($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'news');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'news');
			}
		}else{
			
			$data = array(
				'id_news' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'status' =>$status,
				'isi' => $this->input->post('isi')
				);

			$res = $this->Model->UpdateNews($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'news');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'news');
			}
		}
	}

	function detailnews($kode = 0){
		$data_slide = $this->Model->GetNews("where id_news = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_news' => $data_slide[0]['id_news'],
			'judul' => $data_slide[0]['judul'],
			'isi' => $data_slide[0]['isi'],
			'image' => $data_slide[0]['image'],
			'tanggal' => $data_slide[0]['tanggal'],
			);
		$this->load->view('news/detail_news', $data);
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_news', array('id_news' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'news');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'news');
		}
	}


}