<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content-header">
        <h1>
          <b>TAMBAH DATA COMMUNITY</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH DATA COMMUNITY</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>community/savedata" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Nama COMMUNITY</label>
                        <input type="text" class="form-control" value="" id="" name="nama" placeholder="Isikan Nama Community" required>
                    </div>
                    <div class="form-group">
                      <label for="">Nama Pemilik</label>
                        <input type="text" class="form-control" value="" id="" name="pemilik" placeholder="Isikan Nama Pemilik" required>
                    </div>
                    <div class="form-group">
                      <label for="">No. Hp</label>
                        <input type="text" class="form-control" value="" id="" name="hp" placeholder="Isikan No. Hp" required>
                    </div>
                    <div class="form-group">
                      <label for="">Email</label>
                        <input type="email" class="form-control" value="" id="" name="email" placeholder="Isikan email" <!--required-->
                    </div>
                    <div class="form-group">
                      <label for="">Alamat</label>
                        <textarea class="form-control" required name="alamat" placeholder="Isikan alamat"></textarea>
                    </div>
					<div class="form-group">
                      <label for="">Birthdate</label>
                      <input type="file" class="form-control" value="<?php date('d-m-Y')?>" id="" name="ultah" placeholder="image" required>                        
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Latitute</label>
                        <input type="text" class="form-control" value="" id="" name="lat" placeholder="Isikan Langtitu" required>
                    </div>
                    <div class="form-group">
                      <label for="">Longitute </label>
                        <input type="text" class="form-control" value="" id="" name="lang" placeholder="Isikan Longitute" required>
                    </div>
                    <div class="form-group">
                      <label for="">User</label>
                      <select class="form-control select" style="width: 100%;" name="userid" >
                        <option value="">--Pilih User--</option>
                        <?php foreach ($data_user as $dtuser) { ?>
                          <option value="<?php echo $dtuser['id_user'] ?>"><?php echo $dtuser['first_name'] ?> <?php if($dtuser['last_name'] != null){ echo $dtuser['last_name'];} ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Kota</label>
                      <select class="form-control select2" style="width: 100%;" name="kota" required="">
                        <option value="">--Pilih Kota--</option>
                        <?php foreach ($data_kota as $dtkota) { ?>
                          <option value="<?php echo $dtkota['id_kota'] ?>"><?php echo $dtkota['namaKota'] ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="">Jenis Community</label>
                        <select class="form-control" name="jenis" required>
                          <option>--Pilih Jenis--</option>
                          <option  value="community">Community</option>
                          <option  value="store">Store</option>
                        </select>
                    </div>

                    <div class="form-group">
                      <label for="">Images</label>
                      <input type="file" class="form-control" value="" id="" name="file_upload" placeholder="image" required>                        
                    </div>
					
                    
                  </div>
                  
                    
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>community" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>assets/dist/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">

      $(function () {
    //Initialize Select2 Elements
        $('.select2').select2();
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>