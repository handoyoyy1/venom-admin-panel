<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class News extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/News_model');
    }

    

    function index_get(){
    	$data = $this->get('data');
        $news_id = $this->get('idnews');

        switch ($data) {
            case "all":
                $news = $this->News_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $news,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "detail":
                $news_detail = $this->News_model->get_by_id($news_id);
                $this->response([
                            'kode' => 1,
                            'id_news' => $news_detail->id_news,
                            'judul' => $news_detail->judul,
                            'isi' => $news_detail->isi,
                            'image' => $news_detail->image,
                            'tanggal' => $news_detail->tanggal,
                            
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }
}