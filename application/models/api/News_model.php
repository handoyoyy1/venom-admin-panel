<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
	public $table = 'tb_news';
    public $id = 'id_news';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all()
    {
        $this->db->where('status !=', 'draft');
        $this->db->order_by($this->id, $this->order);      
        return $this->db->get($this->table)->result();
    }
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }


}