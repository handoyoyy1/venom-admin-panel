<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Reward extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Reward_model');
    }

    
    function index_get(){
    	$Reward = $this->Reward_model->get_all();
                    $this->response([
                            'kode' => 1,
                            'result' => $Reward,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }
    
    function index_post(){
        $date = $this->post('date');
        $kode = $this->post('kode');
        $user_id = $this->post('userid');
        
        if ($this->Reward_model->exist_row_check('tb_reward','kode', $kode) > 0){  
            $Reward = $this->Reward_model->get_by_id($kode);
            if ($date != $Reward->expired) {
                if ($this->Reward_model->exist_row_check('tb_claim','user_id', $user_id) > 0){
                    $this->response(['kode' => 0,'pesan' =>'sorry your voucher failed to claim please contact venom admin'], REST_Controller::HTTP_OK);
                }else{
                    $data_reward = array(
                            'kode' => $kode,
                            'user_id' => $user_id
                        );
                        $this->Reward_model->insert($data_reward);
                    $this->response(['kode' => 0,'pesan' =>'thank your voucher have successfully redeem in '.$Reward->alamat], REST_Controller::HTTP_OK);
                }
                
            }else{ 
                $this->response(['kode' => 0,'pesan' =>'Sorry your voucher has expired until '.$Reward->expired], REST_Controller::HTTP_OK);
            }
        }else{ 
                $this->response(['kode' => 0,'pesan' =>'sorry your qrcode failed to claim please contact venom admin '], REST_Controller::HTTP_OK);
            }
        
    }
}