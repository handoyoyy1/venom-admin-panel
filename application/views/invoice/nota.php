<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div id="wrapper" class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Order Detail (<?php echo $kode;?>)</b>
        </h1>
        </section>
        
        <section class="content">
 
            
            <div class="col-md-12">
            <div class="box-body">
               

                <table class="table table-bordered" >
                  <tr>
                    <td colspan="6"><?php echo $kode;?>,&nbsp;Nama :&nbsp;<?php echo $nm;?>,&nbsp;Tipe User:&nbsp;<?php echo $nama1;?>,
                    &nbsp;Email:&nbsp;<?php echo $email;?></td>
                    <td colspan="1" align="right"><?php echo $tanggal;?></td>
                  </tr>
                  <tr>
                    <td colspan="6">HP:&nbsp;<?php echo $hp;?>,&nbsp;Alamat : <?php if ($alamat == ""){echo $alamat1;}
                    else {echo $alamat;} ?>
                    </td>
                    <td colspan="1" align="right"><?php if ($jenis == "credit") {echo "Tempo";} else {echo $jenis;}?></td>
                  </tr>
                  <tr>
                    <td width="20">No</td>
                    <td>Product</td>
                    <td width="50">Qty</td>
                    <td>Price</td>
                    <td>Cost</td>
                    <td>Diskon</td>
                    <td width="135">Total</td>
                  </tr>
                  <?php $tot = 0; $no=0; foreach($data_order as $row) { $no++ ?>
                  <tr>
                  	<td><?php echo $no; ?></td>
                    <td><?php echo $row['kode']; ?></td>
                    <td><?php echo $row['jumlah']; ?></td>
                    <td><?php echo number_format($row['harga'], 0, ',', '.'); ?></td>
                    <td><?php echo number_format($row['total'], 0, ',', '.'); ?></td>
                    <td><?php if($group == "4"){ echo "0";}else{  
                        switch($jenis) {
                            case "credit" : ?>
                                <?php echo $credit_a."+".$credit_b; ?>
                              <?php  break;
                            case "cash" : ?>
                                <?php echo $diskon_a."+".$diskon_b."+".$diskon_c."+".$diskon_d."+".$diskon_e; ?>
                            <?php break;
                            default :
                            break;
                        }
                       } ?></td>
                    <td><?php if($group == "4"){ echo number_format((($row['total'])), 0, ',', '.');}else{ 
                        switch($jenis) {
                            case "credit" : ?>
                                <?php 
                                    $HARGA  = $row['total'];
                                    $HARGA2 = $HARGA  - ($HARGA  * ($credit_a / 100) ); 
                                    $HARGA3 = $HARGA2 - ($HARGA2 * ($credit_b / 100) );
                                    $DISKON = $HARGA - $HARGA3;
                                    echo number_format(($HARGA3), 0, ',', '.');
                                ?>
                              <?php  break;
                            case "cash" : ?>
                                <?php 
                                      $HARGA  = $row['total'];
                                      $HARGA2 = $HARGA  - ($HARGA  * ($diskon_a / 100) ); 
                                      $HARGA3 = $HARGA2 - ($HARGA2 * ($diskon_b / 100) ); 
                                      $HARGA4 = $HARGA3 - ($HARGA3 * ($diskon_c / 100) ); 
                                      $HARGA5 = $HARGA4 - ($HARGA4 * ($diskon_d / 100) ); 
                                      $HARGA6 = $HARGA5 - ($HARGA5 * ($diskon_e / 100) );
                                      $DISKON = $HARGA - $HARGA6;
                                      echo number_format(($HARGA6), 0, ',', '.');
                                 ?>
                            <?php break;
                            default :
                            break;
                        } 

                      } ?></td>
                  </tr>
                  <?php $tot += $row['total'];  } ?>
                  <?php  
                    $hargaTotal = $harga_total;
                    $diskonTotal = $diskon_total;
                    $subTotal = $sub_total;
                    $diskonvoucer = $diskon_voucer;
                    $ppn = $ppn;
                    $grandTotal = $grand_total;

                    if($group == "4"){
                        switch($kategori) {
                            case "Express" : ?>
                                <tr><td colspan="5">&nbsp;</td><td>Harga Total</td>
                                <td><?php echo number_format($hargaTotal, 0, ',', '.'); ?></td></tr>
                                <tr><td colspan="5">&nbsp;</td><td>Ppn 10%</td>
                                <td><?php echo number_format($ppn, 0, ',', '.'); ?></td></tr>
                                <tr><td colspan="5">&nbsp;</td><td>Grand Total</td>
                                <td><?php echo number_format($grandTotal, 0, ',', '.'); ?></td></tr>
                          <?php break;
                            case "Product" : ?>
                                <tr><td colspan="5">&nbsp;</td><td>Harga Total</td>
                                <td><?php echo number_format($hargaTotal, 0, ',', '.'); ?></td></tr>
                                <?php if($diskonvoucer > 0){ ?>
                                <tr><td colspan="5">&nbsp;</td><td>Diskon Voucer</td>
                                <td><?php echo number_format($diskonvoucer, 0, ',', '.'); ?></td></tr>
                                <?php } ?>
                                <tr><td colspan="5">&nbsp;</td><td>Ppn 10%</td>
                                <td><?php echo number_format($ppn, 0, ',', '.'); ?></td></tr>
                                <tr><td colspan="5">&nbsp;</td><td>Grand Total</td>
                                <td><?php echo number_format($grandTotal, 0, ',', '.'); ?></td></tr> 

                    <?php   break; 
                            case "Simulator" :?>
                                <tr><td colspan="5">&nbsp;</td><td>Harga Total</td>
                                <td><?php echo number_format($hargaTotal, 0, ',', '.'); ?></td></tr>
                                <tr><td colspan="5">&nbsp;</td><td>Ppn 10%</td>
                                <td><?php echo number_format($ppn, 0, ',', '.'); ?></td></tr>
                                <tr><td colspan="5">&nbsp;</td><td>Grand Total</td>
                                <td><?php echo number_format($grandTotal, 0, ',', '.'); ?></td></tr>
                           <?php break; // optional
                            default :
                        }
                    }else {
                        switch($kategori) {
                            case "Express" : ?>
                                <tr><td colspan="5">&nbsp;</td><td>Harga Total</td>
                                <td><?php echo number_format($hargaTotal, 0, ',', '.'); ?></td></tr>
                              <?php  break; 
                            case "Product" : ?>
                                <tr><td colspan="5">&nbsp;</td><td>Harga Total</td>
                                <td><?php echo number_format($subTotal, 0, ',', '.'); ?></td></tr>
                                <?php if($diskonvoucer > 0){ ?>
                                <tr><td colspan="5">&nbsp;</td><td>Diskon Voucer</td>
                                <td><?php echo number_format($diskonvoucer, 0, ',', '.'); ?></td></tr>
                                <?php } ?>
                                <tr><td colspan="5">&nbsp;</td><td>Grand Total</td>
                                <td><b><?php echo number_format($grandTotal, 0, ',', '.'); ?></b></td></tr>
                            <?php break;
                            default :
                            break;
                        }
                    }

                    ?>
                  <tr>
                    <td colspan="2">Admin note </td>
                    <td colspan="5"><?php echo $admin_note; ?></td>
                  </tr>
                  <tr>
                    <td colspan="2">Sales note </td>
                    <td colspan="5"><?php echo $sales_note; ?></td>
                  </tr>
                </table>
                
              </div>
              </div>
            </div>
            
            
        </section>
        </div>
    </div>   

    <script>
      window.print();
    </script>
  </div><!-- ./wrapper -->
  <!-- page script -->
  
<?php $this->load->view('inc/footer'); ?>
    
    
   
</body>
</html>