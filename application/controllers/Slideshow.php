<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slideshow extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_slide' => $this->Model->GetSlide("order by id_slide desc")->result_array(),
		);
		$this->load->view('slideshow/slide', $data);
	}

	function addslide()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('slideshow/add_slide', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/slide',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();

		$id_slide = '';
		$nama = $_POST['nama'];
		$url = $_POST['url'];
		$status = $_POST['status'];
		$file_name = $upload_data['file_name'];

		$data = array(	
			'id_slide'=> $id_slide,
			'nama_slide' => $nama,
			'image' => base_url()."assets/upload/slide/".$file_name,
			'text' => $url,			
			'status' => $status			
			);
		
		$result = $this->Model->Simpan('tb_slideshow', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'slideshow');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'slideshow');
		}		
	}

	function editslide($kode = 0){
		$data_slide = $this->Model->GetSlide("where id_slide = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_slide' => $data_slide[0]['id_slide'],
			'nama_slide' => $data_slide[0]['nama_slide'],
			'foto' => $data_slide[0]['image'],
			'url' => $data_slide[0]['text'],
			'status' => $data_slide[0]['status'],
			);
		$this->load->view('slideshow/edit_slide', $data);
	}

	function updateslide(){
		$config = array(
			'upload_path' => './assets/upload/slide',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		if ($file_name != null) {
			$data = array(
				'id_slide' => $this->input->post('id'),
				'nama_slide' => $this->input->post('nama'),
				'text' => $this->input->post('url'),
				'status' => $this->input->post('status'),

				'image' => base_url()."assets/upload/slide/".$file_name,
				        
				);

			$res = $this->Model->UpdateSlide($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'slideshow');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'slideshow');
			}
		}else{
			$data = array(
				'id_slide' => $this->input->post('id'),
				'nama_slide' => $this->input->post('nama'),
				'text' => $this->input->post('text'),
				'status' => $this->input->post('status'),
				);

			$res = $this->Model->UpdateSlide($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'slideshow');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'slideshow');
			}
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_slideshow', array('id_slide' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'slideshow');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'slideshow');
		}
	}

}