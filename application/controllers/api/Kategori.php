<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Kategori extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/Produk_model');
    }

    

    function index_get(){
        $data = $this->get('data');
        $kategori_id = $this->get('idkategori');

        if($data == "kategori"){
        	$Kategori = $this->Produk_model->get_all_kategori();
                    $this->response([
                                'kode' => 1,
                                'result' => $Kategori,
                                'pesan' =>'Data tidak kosong!'
                            ], REST_Controller::HTTP_OK);
                             echo "Maaf data anda kosong..!"; 
        }else{
            $subkategori = $this->Produk_model->get_all_subkategori($kategori_id);
                    $this->response([
                                'kode' => 1,
                                'result' => $subkategori,
                                'pesan' =>'Data tidak kosong!'
                            ], REST_Controller::HTTP_OK);
                             echo "Maaf data anda kosong..!"; 

        }
    }	

    
}