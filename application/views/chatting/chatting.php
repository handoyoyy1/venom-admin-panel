<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('inc/head'); ?>
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<style>
#parent {
  height: 100px;
  width: 250px;
  border: 2px solid blue;  
}

#child {
  height:500px;   
}
</style>

<section class="content">
          <!-- Small boxes (Stat box) -->
        <div  class="row">
          <div  class="col-md-8">
              <!-- DIRECT CHAT PRIMARY -->
              <div  class="box box-primary direct-chat direct-chat-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Direct Chat</h3>
                </div>
                <!-- /.box-header -->
                <div  class="box-body">
                  <!-- Conversations are loaded here -->
                  <div id="child" class="direct-chat-messages">
                  </div>
                  <!--/.direct-chat-messages-->
                  <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <form action="#" method="post">
                    <div class="input-group">
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat">Send</button>
                          </span>
                    </div>
                  </form>
                </div>
                <!-- /.box-footer-->
              </div>
              <!--/.direct-chat -->
  
          </div><!-- /.col -->

          <div id="child"  class="col-md-4">
              <!-- DIRECT CHAT PRIMARY -->
              <div  class="box box-primary direct-chat direct-chat-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">List Customer</h3>
                </div>
                <!-- /.box-header -->
                <div  class="box-body">
                  <!-- Conversations are loaded here -->
                  <div id="child" class="direct-chat-messages">
                  <ul class="nav nav-pills nav-stacked">
                  <li id="show_custemer"></li>
                
              </ul>
                    

                  </div>
                  <!--/.direct-chat-messages-->
                  <!-- /.direct-chat-pane -->
                </div>
              </div>
              <!--/.direct-chat -->
  
          </div><!-- /.col -->

          

        </div><!-- /.row -->
        <!-- Main row -->
</section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  
  
<script type="text/javascript" src="<?php echo base_url().'assets/dist/js/jquery.js'?>"></script>

<script type="text/javascript">
    $(document).ready(function(){       
       setInterval(function(){ 
                tampil_data_custemer();
            }, 4000);
        tampil_data_custemer();
        
        function tampil_data_custemer(){
            $.ajax({
                type  : 'ajax',
                url   : '<?php echo base_url()?>Chatting/data_room',
                async : false,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html +=
                          '<li><a href="http://venom.levanpedia.com/Chatting/custemer/'+data[i].id+'">'+
                          '<i class="fa fa-user"></i> '+
                          data[i].first_name+' '+data[i].last_name+
                          ' <span class="label label-warning pull-right">'+
                          data[i].room+'</span></a></li>';
                      }
                    $('#show_custemer').html(html);
                }

            });
        }


    });

</script>

  

    
    <?php $this->load->view('inc/footer'); ?>


</body>
</html>