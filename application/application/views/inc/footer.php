
    <audio src="<?php echo base_url(); ?>assets/rintone_alert_venom.mp3" id="myAudio">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio>

    <script src="<?php echo base_url().'assets/bootstrap/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.js'?>" type="text/javascript"></script>
    
    
    <script src="<?php echo base_url().'assets/bootstrap/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- datepicker -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>
    
     <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>

    <!-- membuat efek animasi -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    
    <script>
        $(document).ready(function(){
            var base_url="<?php echo base_url(); ?>";
            var x = document.getElementById("myAudio");

            function enableAutoplay() { 
                x.autoplay = true;
                x.load();
            }
            function load_unseen_notification(view = ''){
                $.ajax({
                    url:base_url+"dashboard/notification",
                    method:"POST",
                    data:{baca:view},
                    dataType:"json",
                    success:function(data){
                        $('.notification').html(data.notification);
                        if(data.unseen_notification > 0){
                            $('.count').html(data.unseen_notification);
                            enableAutoplay();
                            $('.notification-popup').html(data.notificationpopup);
                        }
                    }
                });
            }
         
            load_unseen_notification();
             
            setInterval(function(){ 
                load_unseen_notification(); 
            }, 7000);
        });
    </script>
    
    