<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reward_model extends CI_Model
{
	public $table = 'tb_reward';
    public $id = 'kode';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }
    
    function insert($data)
    {
        $this->db->insert("tb_claim", $data);
    }

    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    public function exist_row_check_claim($table,$kode,$user_id){
        $this->db->where("kode",$kode);
        $this->db->where("user_id",$user_id);
        $this->db->from($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function exist_row_check($table,$field,$data){
        $this->db->where($field,$data);
        $this->db->from($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }


}