<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Community extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/community_model');
    }

    

    function index_get(){
        $data = $this->get('data');
        $idkota = $this->get('idkota'); 
        $lat = $this->get('lat'); 
        $lang = $this->get('lang');
        $qrcode = $this->get('qrcode');
        $nama = $this->get('nama');
        $user_id = $this->get('userid');
        switch ($data) {
            case "all":
                $community = $this->community_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "kat":
                $community = $this->community_model->get_all_kota();
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "kota":
                $community = $this->community_model->get_by_kota($idkota);
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "cari":
                $community = $this->community_model->get_by_cari($nama);
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "qrcode":
                $community = $this->community_model->get_by_qrcode($qrcode);
                $this->response([
                            'kode' => 1,
                            'id' => $community->id_community,
                            'nama' => $community->nama,
                            'img' => $community->images,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "edit":
                if ($this->community_model->exist_row_check('user_id', $user_id) > 0) {
                   $community =  $this->community_model->get_by_userid($user_id);
                   $this->response([
                            'kode' => 1,
                            'id' => $community->id_community,
                            'nama' => $community->nama,
                            'pemilik' => $community->pemilik,
                            'hp' => $community->hp,
                            'email' => $community->email,
                            'alamat' => $community->alamat,
                            'lat' => $community->lat,
                            'lang' => $community->lang,
                            'kota_id' => $community->kota_id,
                            'jenis' => $community->jenis,
                            'images' => $community->images,
                            'qrcode' => $community->qr_img,
                            'status' => "ada",
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                }else{
                    $this->response([
                            'kode' => 1,
                            'id' => "",
                            'nama' => "",
                            'pemilik' => "",
                            'hp' => "",
                            'email' => "",
                            'alamat' => "",
                            'lat' => "",
                            'lang' => "",
                            'kota_id' => "",
                            'jenis' => "",
                            'images' => "",
                            'qrcode' => "",
                            'status' => "kosong",
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                }
                
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         }
        

    }
}