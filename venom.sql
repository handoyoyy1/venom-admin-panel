-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2018 at 12:22 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `venom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_album`
--

CREATE TABLE IF NOT EXISTS `tb_album` (
  `id_album` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `foto` text,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `categori` enum('umum','community') NOT NULL DEFAULT 'umum'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_album`
--

INSERT INTO `tb_album` (`id_album`, `nama`, `foto`, `community_id`, `categori`) VALUES
(1, 'Cefiro To REVENTION', 'http://www.venom-audio.com/images/imageuploads/550_03b9133fd2edfe0516553be38fd54ae7.jpg', 0, 'umum'),
(2, 'Gallant', 'http://www.venom-audio.com/images/imageuploads/550_f47fec6c02ab18d9f7f59fc1501c9df3.jpg', 0, 'umum'),
(3, 'Alphard', 'http://www.venom-audio.com/images/imageuploads/550_85738133022302f169498b4de56dff74.jpg', 0, 'umum'),
(4, 'Tuminem projects', 'http://www.venom-audio.com/images/imageuploads/550_9cc337cf5fa387f56405150e49714f7c.jpg', 0, 'umum'),
(5, 'Panther To Hummer', 'http://www.venom-audio.com/images/imageuploads/550_f136974e3de227f6b1ed3ee4ae265649.jpg', 0, 'umum'),
(6, 'Hati Nurani', 'http://www.venom-audio.com/images/imageuploads/550_c369b10bd855ad9d1e20e3a5e02cd2d5.JPG', 0, 'umum'),
(7, 'Panoz Esperante', 'http://www.venom-audio.com/images/imageuploads/550_22de534c1c72cb4eaa83a3a20a5aaac0.jpg', 0, 'umum'),
(8, 'AUTO MAXIMUS', 'http://103.82.242.153/venom/assets/upload/community/venom_fighter_(no_picture).png', 13, 'community'),
(9, 'AWR AUDIO', 'http://103.82.242.153/venom/assets/upload/community/venom_fighter_(no_picture).png', 15, 'community');

-- --------------------------------------------------------

--
-- Table structure for table `tb_balance`
--

CREATE TABLE IF NOT EXISTS `tb_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `balance` int(15) NOT NULL,
  `status` enum('unapprove') NOT NULL DEFAULT 'unapprove',
  `member_input` int(11) NOT NULL,
  `keterangan` text,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_balance`
--

INSERT INTO `tb_balance` (`id`, `user_id`, `balance`, `status`, `member_input`, `keterangan`, `create_on`) VALUES
(4, 61, 10000000, 'unapprove', 61, 'venom', '2018-08-04 05:33:47'),
(5, 37, 10000, 'unapprove', 61, 'aji tambah saldo', '2018-08-05 10:45:18'),
(6, 86, 10000, 'unapprove', 37, 'pembayaran speaker', '2018-08-05 10:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE IF NOT EXISTS `tb_bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(55) NOT NULL,
  `atas_nama` varchar(60) NOT NULL,
  `no_rek` varchar(50) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`id_bank`, `nama_bank`, `atas_nama`, `no_rek`, `image`) VALUES
(4, 'BCA', 'PT. Sumber Sejahtera Audiotama', '2443023011', 'bca.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_chat_rooms`
--

CREATE TABLE IF NOT EXISTS `tb_chat_rooms` (
  `chat_room_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `baca` enum('yes','no') DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_chat_rooms`
--

INSERT INTO `tb_chat_rooms` (`chat_room_id`, `name`, `user_id`, `created_at`, `status`, `baca`) VALUES
(1, '998760', 20, '2018-04-13 07:40:23', 'active', 'yes'),
(2, '998790', 1, '2018-04-13 08:14:46', 'active', 'yes'),
(114, '431309', 17, '2018-04-16 23:19:12', 'active', 'yes'),
(115, '145991', 27, '2018-04-16 23:33:06', 'active', 'yes'),
(117, '734847', 48, '2018-04-17 07:06:51', 'inactive', 'yes'),
(118, '938038', 49, '2018-04-17 17:13:08', 'active', 'yes'),
(119, '462145', 54, '2018-04-17 17:19:36', 'active', 'yes'),
(120, '165226', 36, '2018-04-17 18:12:07', 'inactive', 'yes'),
(121, '159551', 57, '2018-04-17 18:53:29', 'inactive', 'yes'),
(122, '406004', 52, '2018-04-17 21:18:20', 'active', 'yes'),
(123, '896884', 63, '2018-04-17 21:58:00', 'active', 'yes'),
(124, '87768', 68, '2018-04-17 23:01:54', 'active', 'yes'),
(125, '580520', 69, '2018-04-18 00:06:51', 'active', 'yes'),
(126, '208791', 73, '2018-04-18 10:47:30', 'active', 'yes'),
(127, '763457', 78, '2018-04-18 23:36:23', 'inactive', 'yes'),
(128, '980569', 79, '2018-04-18 23:40:53', 'active', 'yes'),
(129, '230932', 81, '2018-04-19 03:44:45', 'active', 'yes'),
(130, '516456', 83, '2018-04-20 01:23:21', 'inactive', 'yes'),
(131, '832941', 86, '2018-04-20 01:28:35', 'active', 'yes'),
(132, '192767', 83, '2018-04-20 05:45:01', 'inactive', 'yes'),
(133, '717493', 92, '2018-04-20 08:27:08', 'active', 'yes'),
(134, '533780', 95, '2018-04-20 20:32:51', 'active', 'yes'),
(135, '998938', 97, '2018-04-20 22:10:52', 'active', 'yes'),
(136, '144128', 96, '2018-04-20 22:55:11', 'active', 'yes'),
(137, '15617', 104, '2018-04-21 03:08:20', 'active', 'yes'),
(138, '73925', 109, '2018-04-21 06:35:11', 'active', 'yes'),
(139, '438535', 113, '2018-04-21 18:55:17', 'active', 'yes'),
(140, '617634', 114, '2018-04-21 20:43:47', 'active', 'yes'),
(141, '77090', 115, '2018-04-21 20:51:23', 'active', 'yes'),
(142, '816345', 116, '2018-04-21 20:51:54', 'active', 'yes'),
(143, '930947', 117, '2018-04-21 20:53:21', 'active', 'yes'),
(144, '898576', 118, '2018-04-21 20:57:24', 'active', 'yes'),
(145, '377535', 110, '2018-04-21 21:15:10', 'active', 'yes'),
(146, '428382', 119, '2018-04-21 21:41:20', 'active', 'yes'),
(147, '805398', 120, '2018-04-21 21:48:20', 'active', 'yes'),
(148, '105961', 122, '2018-04-21 22:43:29', 'active', 'yes'),
(149, '367437', 123, '2018-04-21 23:33:29', 'inactive', 'yes'),
(150, '2622', 124, '2018-04-21 23:34:09', 'active', 'yes'),
(151, '187426', 125, '2018-04-22 00:26:28', 'active', 'yes'),
(152, '130160', 126, '2018-04-23 00:15:45', 'active', 'yes'),
(153, '593709', 127, '2018-04-23 00:35:56', 'active', 'yes'),
(154, '724186', 128, '2018-04-23 05:11:35', 'active', 'yes'),
(155, '407651', 72, '2018-04-23 18:19:55', 'active', 'yes'),
(156, '949564', 129, '2018-04-24 17:46:35', 'active', 'yes'),
(157, '618997', 101, '2018-04-24 20:19:51', 'active', 'yes'),
(158, '649518', 130, '2018-04-25 01:19:12', 'active', 'yes'),
(159, '653773', 131, '2018-04-25 06:26:20', 'active', 'yes'),
(160, '791153', 132, '2018-04-25 06:54:56', 'active', 'yes'),
(161, '370536', 135, '2018-04-26 02:11:36', 'active', 'yes'),
(162, '900305', 136, '2018-04-26 02:44:31', 'active', 'yes'),
(163, '624152', 137, '2018-04-26 08:39:07', 'active', 'yes'),
(164, '352231', 138, '2018-04-27 01:15:39', 'active', 'yes'),
(165, '299520', 139, '2018-04-28 03:06:26', 'active', 'yes'),
(166, '434275', 140, '2018-04-28 23:11:50', 'active', 'yes'),
(167, '177660', 141, '2018-04-29 00:02:04', 'active', 'yes'),
(168, '633039', 142, '2018-04-30 21:17:12', 'inactive', 'yes'),
(169, '500770', 143, '2018-05-02 01:34:33', 'active', 'yes'),
(170, '407731', 144, '2018-05-02 01:48:25', 'active', 'yes'),
(171, '226261', 145, '2018-05-03 01:01:40', 'active', 'yes'),
(172, '356443', 84, '2018-05-03 01:58:20', 'inactive', 'yes'),
(173, '673330', 133, '2018-05-03 05:06:34', 'active', 'yes'),
(174, '288249', 146, '2018-05-04 06:03:13', 'active', 'yes'),
(175, '398492', 147, '2018-05-04 14:54:28', 'active', 'yes'),
(176, '49288', 148, '2018-05-04 21:10:57', 'active', 'yes'),
(177, '662918', 149, '2018-05-05 16:12:11', 'active', 'yes'),
(178, '377865', 150, '2018-05-07 00:26:52', 'active', 'yes'),
(179, '59014', 151, '2018-05-07 11:06:11', 'active', 'yes'),
(180, '540038', 152, '2018-05-07 17:18:20', 'active', 'yes'),
(181, '152253', 153, '2018-05-07 23:11:20', 'active', 'yes'),
(182, '974594', 154, '2018-05-08 07:45:54', 'active', 'yes'),
(183, '343534', 155, '2018-05-08 17:24:06', 'active', 'yes'),
(184, '520469', 156, '2018-05-09 04:39:05', 'active', 'yes'),
(185, '510543', 157, '2018-05-09 06:13:49', 'active', 'yes'),
(186, '337232', 158, '2018-05-09 06:50:01', 'active', 'yes'),
(187, '439613', 159, '2018-05-09 09:15:36', 'active', 'yes'),
(188, '430501', 160, '2018-05-09 09:32:12', 'active', 'yes'),
(189, '861826', 161, '2018-05-09 11:35:40', 'active', 'yes'),
(190, '114007', 162, '2018-05-10 01:20:39', 'active', 'yes'),
(191, '182441', 163, '2018-05-10 08:47:40', 'active', 'yes'),
(192, '224634', 164, '2018-05-10 10:24:31', 'active', 'yes'),
(193, '762273', 165, '2018-05-11 10:24:23', 'active', 'yes'),
(194, '691651', 167, '2018-05-11 18:51:02', 'active', 'yes'),
(195, '483640', 168, '2018-05-11 19:47:35', 'active', 'yes'),
(196, '847300', 102, '2018-05-11 22:38:03', 'active', 'yes'),
(197, '1530', 171, '2018-05-11 22:40:03', 'active', 'yes'),
(198, '175367', 172, '2018-05-13 04:51:39', 'active', 'yes'),
(199, '436659', 173, '2018-05-13 22:52:03', 'active', 'yes'),
(200, '474271', 175, '2018-05-15 01:25:39', 'active', 'yes'),
(201, '710558', 176, '2018-05-15 03:01:10', 'active', 'yes'),
(202, '100598', 177, '2018-05-16 07:47:15', 'active', 'yes'),
(203, '263599', 178, '2018-05-20 19:28:51', 'active', 'yes'),
(204, '714813', 179, '2018-05-21 05:52:46', 'active', 'yes'),
(205, '488560', 180, '2018-05-22 01:36:45', 'active', 'yes'),
(206, '644467', 181, '2018-05-22 07:50:26', 'active', 'yes'),
(207, '125390', 182, '2018-05-22 09:24:25', 'active', 'yes'),
(208, '23122', 184, '2018-05-23 08:50:30', 'active', 'yes'),
(209, '944036', 185, '2018-05-24 15:22:45', 'active', 'yes'),
(210, '908315', 186, '2018-05-26 14:04:29', 'active', 'yes'),
(211, '978449', 187, '2018-05-29 20:01:14', 'active', 'yes'),
(212, '18263', 188, '2018-05-30 07:45:54', 'active', 'yes'),
(213, '137138', 189, '2018-05-31 22:46:14', 'active', 'yes'),
(214, '770088', 190, '2018-06-02 01:09:59', 'active', 'yes'),
(215, '905947', 191, '2018-06-05 00:44:08', 'active', 'yes'),
(216, '359456', 192, '2018-06-05 08:22:14', 'active', 'yes'),
(217, '783050', 193, '2018-06-05 21:51:01', 'active', 'yes'),
(218, '232648', 194, '2018-06-06 22:34:54', 'active', 'yes'),
(219, '746392', 195, '2018-06-10 03:51:09', 'active', 'yes'),
(220, '267600', 196, '2018-06-11 12:02:24', 'active', 'yes'),
(221, '916590', 197, '2018-06-12 06:22:33', 'active', 'yes'),
(222, '465271', 198, '2018-06-13 16:04:51', 'active', 'yes'),
(223, '1533', 199, '2018-06-14 12:12:26', 'active', 'yes'),
(224, '45427', 200, '2018-06-16 18:26:32', 'active', 'yes'),
(225, '551345', 201, '2018-06-21 03:14:48', 'active', 'yes'),
(226, '347129', 202, '2018-06-22 07:44:41', 'active', 'yes'),
(227, '245966', 203, '2018-06-22 23:31:35', 'active', 'yes'),
(228, '257665', 204, '2018-06-22 23:34:11', 'active', 'yes'),
(229, '746000', 205, '2018-06-25 06:35:29', 'active', 'yes'),
(230, '721013', 206, '2018-06-29 05:00:32', 'active', 'yes'),
(231, '398224', 207, '2018-06-29 08:22:25', 'active', 'yes'),
(232, '378128', 208, '2018-07-03 09:48:25', 'active', 'yes'),
(233, '688769', 209, '2018-07-05 08:23:58', 'active', 'yes'),
(234, '358010', 98, '2018-07-16 04:58:17', 'active', 'yes'),
(235, '206295', 61, '2018-07-30 04:30:49', 'active', 'yes'),
(236, '151794', 144, '2018-08-01 15:06:00', 'active', 'yes'),
(237, '348785', 145, '2018-08-01 15:44:09', 'active', 'yes'),
(238, '917145', 147, '2018-08-03 04:05:27', 'active', 'yes'),
(239, '892395', 149, '2018-08-03 07:26:08', 'active', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tb_claim`
--

CREATE TABLE IF NOT EXISTS `tb_claim` (
  `id` int(11) NOT NULL,
  `reward_kode` varchar(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('unapprove','approve') NOT NULL DEFAULT 'unapprove',
  `categori` enum('reward','hadiah','voucer') DEFAULT 'reward',
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_claim`
--

INSERT INTO `tb_claim` (`id`, `reward_kode`, `user_id`, `status`, `categori`, `create_on`) VALUES
(5, 'uu45', 61, 'unapprove', 'reward', '2018-07-27 08:40:16'),
(6, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-27 08:41:17'),
(7, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 01:36:20'),
(8, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 01:39:55'),
(9, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 03:27:48'),
(10, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 04:13:14'),
(11, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 04:21:31'),
(12, 'SIXTY3', 61, 'unapprove', 'reward', '2018-07-28 04:21:51'),
(13, 'v1', 61, 'unapprove', 'reward', '2018-07-28 04:24:14'),
(14, 'V8055', 17, 'unapprove', 'voucer', '2018-07-30 05:42:10'),
(15, 'V0001', 61, 'unapprove', 'voucer', '2018-07-30 13:19:10'),
(16, 'V0001', 61, 'unapprove', 'voucer', '2018-07-30 14:07:28'),
(17, 'V8055', 17, 'unapprove', 'voucer', '2018-07-31 05:44:11'),
(18, 'V0001', 61, 'unapprove', 'voucer', '2018-08-03 09:13:57'),
(19, 'V0001', 143, 'unapprove', 'voucer', '2018-08-03 09:18:48'),
(20, 'V8055', 143, 'unapprove', 'voucer', '2018-08-03 09:18:51'),
(21, 'V811', 143, 'unapprove', 'voucer', '2018-08-03 09:18:55'),
(22, 'V0001', 143, 'unapprove', 'voucer', '2018-08-03 09:18:57'),
(23, 'V0001', 61, 'unapprove', 'voucer', '2018-08-04 02:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_community`
--

CREATE TABLE IF NOT EXISTS `tb_community` (
  `id_community` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama` varchar(45) NOT NULL,
  `pemilik` varchar(25) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `alamat` text NOT NULL,
  `kota_id` int(11) NOT NULL,
  `lat` text,
  `lang` text,
  `images` text,
  `jenis` set('community','store') NOT NULL DEFAULT 'community',
  `qr_code` varchar(15) DEFAULT NULL,
  `qr_img` text,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ultah` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=410 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_community`
--

INSERT INTO `tb_community` (`id_community`, `user_id`, `nama`, `pemilik`, `hp`, `email`, `alamat`, `kota_id`, `lat`, `lang`, `images`, `jenis`, `qr_code`, `qr_img`, `create_on`, `exp_date`, `ultah`) VALUES
(2, NULL, 'CASTELLO', 'ROBERT', '08122349090', 'obecatello@yahoo.com', 'jl.kembang mas barat no.32 bandung', 7, '-8.4393276', '114.1849357', 'CASTELLO.jpg', 'community', 'NOMWF1ZD', 'NOMWF1ZD.png', '2018-03-25 15:40:12', '2019-03-01 09:13:15', NULL),
(3, NULL, 'AJM AUDIO WORKS', 'STEVEN JUNG', '081310983421', 'steven.ajm@gmail.com', 'JL. RADIN INTEN NO.102 DUREN SAWIT', 3, '-6.237889', '106.922306', 'venom_fighter_(no_picture).png', 'community', 'PT1MDAJF', 'PT1MDAJF.png', '2018-03-23 23:12:03', '2019-03-01 09:13:15', NULL),
(5, NULL, 'ALVIN AUDIO', 'Budijono Sarwono', '087700500835', 'alvinaudio@yahoo.com', 'JL.Anjasmoro Raya No.48 Semarang', 18, '-6.978470', '110.390925', 'venom_fighter_(no_picture).png', 'community', 'TZJ0NCK9', 'TZJ0NCK9.png', '2018-03-23 23:12:03', '2019-03-01 09:13:15', NULL),
(6, NULL, 'ASIA JAYA AUDIO', 'GO SUE GANG', '082233335859', 'asiajaya_40@yahoo.com', 'JLN GENTENG BESAR NO.40 SURABAYA', 5, NULL, NULL, 'venom_fighter_(no_picture).png', 'community', 'DJLVHXM7', 'DJLVHXM7.png', '2018-03-23 23:12:03', '2019-03-01 09:13:15', NULL),
(7, NULL, 'AUDIO TONES', 'HARTEN WIJAYA', '08161183083', 'c150nk@yahoo.com', 'JLN GENTENG BESAR NO.40 SURABAYA', 5, '-7.2584167', '112.7416667', 'venom_fighter_(no_picture).png', 'community', 'D5VLA8E4', 'D5VLA8E4.png', '2018-03-23 23:12:03', '2019-03-01 09:13:15', NULL),
(8, NULL, 'AGUNG MOTOR', 'Hendry Suwanto', '081361410677', 'agungmotoraudio@gmail.com', 'JL. krakatau no.111 A-B Medan', 2, '3.616397', '98.6782313', '13.png', 'community', 'I6VEZ2F8', 'I6VEZ2F8.png', '2018-03-23 23:12:03', '2019-03-01 09:13:15', NULL),
(9, NULL, 'AUDIO TRON', 'WILYS AGUS', '08126545178', 'wilysagus@yahoo.co.id', 'JL. GALANG NO. 78 - MEDAN', 2, '3.5955023', '98.6940695', 'venom_fighter_picture32.png', 'community', '6DBYOQLT', '6DBYOQLT.png', '2018-03-28 20:25:40', '2019-03-01 09:13:15', NULL),
(10, NULL, 'AUDIOPHILE', 'HAY GIE', '0811372600', 'hay_gee@hotmail.com', 'JL.NGAGEL JAYA UTARA NO 132 SURABAYA', 5, ' -7.289339', ' 112.761012', 'audiophile.jpg', 'community', 'O3NZI068', 'O3NZI068.png', '2018-03-28 20:27:31', '2019-03-01 09:13:15', NULL),
(11, NULL, '3 WARNA', 'KO BENY', '081311111869', 'admin@gmail.com', 'RUKO MITRA RAMAYANA BLOK B NO 26 - BEKASI', 3, '-6.2841796', '106.8332888', 'venom_fighter_picture8.png', 'community', '2FSN05KC', '2FSN05KC.png', '2018-04-08 20:27:31', '2019-03-01 09:13:15', NULL),
(12, NULL, 'Auto Land VID', 'Stefanus K & Peter H kurn', '0243552433', 'stefanusk78@gmail.com', 'Jl. Moch Suyudi No. 11 Semarang', 18, '-6.9824035', '110.4182922', 'venom_fighter_(no_picture).png', 'community', 'WX8H31TJ', 'WX8H31TJ.png', '2018-04-08 20:15:10', '2019-03-01 09:13:15', NULL),
(13, NULL, 'AUTO MAXIMUS', 'FRENDY', '08158811858', 'auto_maximus@yahoo.com', 'Taman Surya 2 C1/21', 1, '-6.1347737', '106.7129123', 'venom_fighter_(no_picture)1.png', 'community', '0KEAGSUV', '0KEAGSUV.png', '2018-04-08 20:17:15', '2019-03-01 09:13:15', NULL),
(14, NULL, 'AUTOMAXIMUS', 'JERRY / JOHN. L', '082111558871', 'mclaren_audioworkshop@yahoo.com', 'JL.TAMAN SURYA2 BLOK C1 NO.56', 1, '-6.133758', '106.7108403', 'venom_fighter_(no_picture)2.png', 'community', '4F9GPHV1', '4F9GPHV1.png', '2018-04-08 20:18:51', '2019-03-01 09:13:15', NULL),
(15, NULL, 'AWR AUDIO', 'Welly Then', '08128412001 / 0', 'afa.welly13@gmail.com', 'JL. HASYIM ASHARI RAYA NO.46 TANGERANG- CIPONDOH', 6, '-6.1866501', '106.6547383', 'venom_fighter_(no_picture)3.png', 'community', '2F13769Y', '2F13769Y.png', '2018-04-08 20:20:54', '2019-03-01 09:13:15', NULL),
(16, NULL, 'BERLIAN MOTOR', 'HOK LAI', '0811639010', 'phingberlian@yahoo.com', 'JL. GLUGUR NO.18i - MEDAN', 2, '3.5912355', '98.6667707', 'venom_fighter_(no_picture)4.png', 'community', 'ZE08B5ON', 'ZE08B5ON.png', '2018-04-08 20:22:16', '2019-03-01 09:13:15', NULL),
(17, NULL, 'BINA USAHA CEMERLANG', 'ROBERT', '08122349090', 'obecatello@yahoo.com', 'jl.kembang mas barat no.32 bandung', 7, '-6.9032739', '107.5731166', 'venom_fighter_(no_picture)5.png', 'community', 'EHIWZNLS', 'EHIWZNLS.png', '2018-04-08 20:23:52', '2019-03-01 09:13:15', NULL),
(18, NULL, 'BINTANG AUDIO', 'Sujianto BTA', '081332402626', 'bintang-audio@yahoo.com', 'Jl.Genteng Besar no.61 Surabaya', 5, '-7.2583697', '112.7383673', 'venom_fighter_(no_picture)6.png', 'community', 'QFRGTP1V', 'QFRGTP1V.png', '2018-04-08 20:32:08', '2019-03-01 09:13:15', NULL),
(19, NULL, 'CENTRAL VARIASI', 'FAHMY HUSEIN', '081803675737', 'fahny_mink@yahoo.co.id', 'JL. SANDUBAYA BLOK U/12A BARYAIS - CAKRA NEGARA SWETA LOMBOK', 24, '-8.5935837', '116.1526103', 'venom_fighter_(no_picture)7.png', 'community', 'PCX87NMV', 'PCX87NMV.png', '2018-04-08 20:37:12', '2019-03-01 09:13:15', NULL),
(20, NULL, 'CF AUDIO BATU', 'ROCKY', '081333842701', 'the_rocky70@yahoo.com', 'JLN IR SUKARNO NO.53 BATU MALANG', 13, '-7.9051412', '112.5685027', 'venom_fighter_(no_picture)8.png', 'community', 'CQPRVKJY', 'CQPRVKJY.png', '2018-04-08 20:38:52', '2019-03-01 09:13:15', NULL),
(21, NULL, 'CIS MOTOR', 'PONIRAN  ', '08126011876 ', 'jessllyynn@gmail.com', 'JLN GAGAK HITAM NO.78B RINGROUND', 2, '3.5829094', '98.6247332', 'venom_fighter_(no_picture)9.png', 'community', 'CU5H4PTQ', 'CU5H4PTQ.png', '2018-04-08 20:41:12', '2019-03-01 09:13:15', NULL),
(22, NULL, 'Citra Baru Motor', 'Toto Sutedja  ', '08129018899 ', 'toto_cbm@hotmail.com', 'Jl. Pusat Niaga Duta Mas Fatmawati Lt. Dasar No. 3 Jakarta Selatan', 1, '-6.2637493', '106.7957153', 'venom_fighter_(no_picture)10.png', 'community', 'B9Z16TAK', 'B9Z16TAK.png', '2018-04-08 20:42:36', '2019-03-01 09:13:15', NULL),
(23, NULL, 'CLINIK CAR''S', 'WILIAM''S APRILLIUS SOEYAN', ' 081279889898  ', 'livia221205@yahoo.co.id', 'PERUMAHAN THE ROMAN DHIKA BLOK A NO.3A SIDOARJO', 32, '-7.4271962', '112.7086976', 'venom_fighter_(no_picture)11.png', 'community', 'VF9NL2M3', 'VF9NL2M3.png', '2018-04-08 20:47:46', '2019-03-01 09:13:15', NULL),
(24, NULL, 'DARMO PERMAI CAR AUDIO', 'VINCENTIUS STEFANTO  ', '085100635553  ', 'vincentiusstefanio@gmail.com', 'JL. SIMPANG DARMO PERMAI UTARA NO.20 SURABAYA', 5, '-7.2776217', '112.6808486', 'venom_fighter_(no_picture)12.png', 'community', 'OWZV3UYI', 'OWZV3UYI.png', '2018-04-08 20:49:31', '2019-03-01 09:13:15', NULL),
(25, NULL, 'DARREN SOUND', 'LEONARDO  ', '082185534747  ', 'leeyo_nardo@yahoo.com', 'JL. JEND. SUDIRMAN 223 B GANJAR AGUNG METRO LAMPUNG', 8, '-5.1329398', '105.2796546', 'LEO_NARDO.jpg', 'community', '78IMZU01', '78IMZU01.png', '2018-04-08 20:51:44', '2019-03-01 09:13:15', NULL),
(26, NULL, 'DBEST / AHWA', 'AHWA / JIMMY JAMSA  ', '081916191212  ', 'dbest_jimmy@yahoo.com', 'JL.TEGAL DUKUH VII NO.12 (BELAKANG MITRA 10) PENAMPARAN GATSU BARAT DENPSAR - BALI', 9, '-8.6376057', '115.182515', 'venom_fighter_picture3.png', 'community', 'CB2DI1MV', 'CB2DI1MV.png', '2018-04-08 20:55:51', '2019-03-01 09:13:15', NULL),
(27, NULL, 'DC AUDIO', 'Dendy Permana  ', '085210065001 ', 'dendi.dcaudio@gmail.com', 'Jl. Raya Tajur No.248A Bogor', 10, '-6.6387963', '106.8305963', 'venom_fighter_picture4.png', 'community', 'KJ6Z8GDY', 'KJ6Z8GDY.png', '2018-04-08 20:57:27', '2019-03-01 09:13:15', NULL),
(28, NULL, 'DIANTARA', 'Eddy Pramono / Julim  ', '0816664484  ', 'dt_autoworks@yahoo.com', 'Jl. Tentara Pelajar No. 12Semarang', 18, '-7.0089747', '110.4312913', 'venom_fighter_picture6.png', 'community', 'OH6AC94I', 'OH6AC94I.png', '2018-04-08 20:59:12', '2019-03-01 09:13:15', NULL),
(29, NULL, 'DYNASTY', 'INDRAWAN  ', '085261100077  ', 'kingdynasty78@gmail.com', 'TALAUD NO 11', 2, '3.5900784', '98.6828925', '369_m249_c8de307f9a19a51008fc71d5e79acc0b.jpg', 'community', '5R2Y3M4E', '5R2Y3M4E.png', '2018-04-08 21:01:57', '2019-03-01 09:13:15', NULL),
(30, NULL, 'SOMESOUND ', 'FILIPUS', '08174838833', 'venom.mania@gmail.com', 'JL. PLUIT MURNI II NO 11A, Pluit, Jakarta Utara ', 1, '-6.114469', '106.7830615', 'venom_fighter_picture7.png', 'community', 'HYZ4FXLQ', 'HYZ4FXLQ.png', '2018-04-08 22:33:18', '2019-03-01 09:13:15', NULL),
(31, NULL, 'VIN''S VARIASI (VLOUD)', 'ELFIN', '08156040499', 'elvinsam@yahoo.com', 'KH. ABDULLAH BIN NUH NO 72 CIANJUR\r\n', 15, '-6.8224862', '107.1263217', 'venom_fighter_picture9.png', 'community', 'XU4KZJSG', 'XU4KZJSG.png', '2018-04-08 22:37:54', '2019-03-01 09:13:15', NULL),
(32, NULL, 'TRAN AUDIO (VLOUD)', 'ACONK', '0818693899', 'aconkjimmy@gmail.com', 'BINTARO TRADE CENTER LT 1 BLOK G4 NO. 25-26 SEKTOR VII JAK-SEL\r\n', 1, '-6.2565788', '106.6789555', 'venom_fighter_picture10.png', 'community', '9YSPVDQ5', '9YSPVDQ5.png', '2018-04-08 22:39:17', '2019-03-01 09:13:15', NULL),
(33, NULL, 'SS AUDIO BOUTIQUE', 'SANDRI SEFIADI', '087871053787', 'nadakalih@gmail.com', 'JL Puri Serang Hijau A6-5 Ciponcok Jaya SERANG\r\n', 21, '-6.1462846', '106.1774721', 'venom_fighter_picture11.png', 'community', 'USR6BFOQ', 'USR6BFOQ.png', '2018-04-08 22:40:43', '2019-03-01 09:13:15', NULL),
(270, NULL, 'SURYA JAYA MOTOR', 'YULIUS CHANDRA', '085218081978', '', 'JL RE MARTADINATA NO. 279 LOS 20 CIPUTAT PONDOK CABE UP: BP. CHANDRA', 6, '-6.3502537', '106.7479553', 'venom_fighter_picture6.png', 'community', 'PM4Y5186', 'PM4Y5186.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(271, NULL, 'DARWIN SURYA JAYA', 'DARWIN ', '081315638489', 'darwin_zhen@yahoo.com', 'JL.raya boulevard ( RUKO BATAVIA BLOK AA2 NO.18 ) gading serpong', 6, '6.2308407', '106.6309323', 'venom_fighter_picture6.png', 'community', 'CYM9R2FP', 'CYM9R2FP.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(273, NULL, 'SINAR JAYA HARMONI', 'WELLY', '081282656511', 'sjh.audio@gmail.com', 'BSD AUTO PART BLOK.L NO.11-12 BSD TANGERANG', 6, '-6.2958927', '106.6772103', 'venom_fighter_picture13.png', 'community', 'GC0ORDFZ', 'GC0ORDFZ.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(274, NULL, 'NUSA JAYA VARIASI ', 'KUSNANTO', '0811845320', 'kusnanto_jn@yahoo.com', 'BSD AUTO PART SEKTOR 17 NO. 30-31', 6, '-6.2450225', '106.6321668', 'venom_fighter_picture6.png', 'community', '0EV3LAXS', '0EV3LAXS.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(278, NULL, 'NURMAN AUDIO', 'SULAIMAN', '081808015805', '', 'MGK Lt. 6 Blok C7 No. 7', 1, '-6.1549562', '106.8467696', 'nurman.jpg', 'community', 'YB96UPQS', 'YB96UPQS.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(279, NULL, 'TWINS CAR AUDIO', 'YOSEF', '082299075288', '', 'MGK LT 6 BLOK D8 NO 1', 1, '-6.1543864', '106.8463936', 'venom_fighter_picture6.png', 'community', 'EQ724B3N', 'EQ724B3N.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(280, NULL, 'STYLING VARIASI', 'JULIUS SUHARDI', '081315832199', '', 'JL. BOULEVARD HIJAU RUKO SENTRA NIAGA 3 BLOK B 6 NO. 32 HARAPAN INDAH - BEKASI', 3, '-6.1850028', '106.9810347', 'venom_fighter_picture6.png', 'community', 'LKO38YZ7', 'LKO38YZ7.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(282, NULL, 'PLATINUM AUTO CENTER', 'HENDRIKO  08170692529    ', '08170692529', 'enriccoplatinum@gmail.com', 'JL. TUPAREV NO. 91 CIREBON', 12, '-6.7112348', '108.5393854', '369_ce36709e5475caa06977926f4863fecc.jpg', 'community', 'KDJNOWF6', 'KDJNOWF6.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(284, NULL, 'MENARA AUTO ACCECORIES', 'SARTONO', '08127405656', 'j500let@yahoo.com', 'JL. HAYAM WURUK NO. 43 JELUTUNG JAMBI', 4, '-1.6062611', '103.6131374', 'venom_fighter_picture6.png', 'community', 'J3MO5VBW', 'J3MO5VBW.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(288, NULL, 'CARROZZA', 'WIELLIAM PRATIKO', '085242025566', 'Carrozza.Makassar@gmail.Com', 'Jl.Sultan Alauddin Pa''Baeng-Baeng no.85', 19, '-5.1706139', '119.4231463', 'venom_fighter_picture6.png', 'community', 'I6HAFM7N', 'I6HAFM7N.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(289, NULL, 'SURYA VARIASI', 'REZKY', '085656171133', 'reskymarchell@rocketmail.com', 'JL. USMAN SALENGKE NO.11 SUNGGUMINASA GOWA, MAKASSAR', 19, '-5.210247', '119.4527759', 'venom_fighter_picture6.png', 'community', 'Q5TEN60S', 'Q5TEN60S.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(291, NULL, 'ISTANA MOBIL', 'OKY ALEXANDER', '085252212214', '', 'JL. KH. A. DAHLAN NO. 71 PONTIANAK', 11, '-0.030407', '109.3277623', 'venom_fighter_picture6.png', 'community', 'YT2A583O', 'YT2A583O.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(292, NULL, 'VIRGO - PORSCKIT AUTO GARAGE', 'AGUS MULIADI Adi sofyan', '081332888882', 'porsckitautogarage@gmail.com', 'JLN HIDAYATULLAH NO.62SAMARINDA', 14, '-0.5011073', '117.1521361', 'venom_fighter_picture6.png', 'community', '7LNVKO4J', '7LNVKO4J.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(296, NULL, 'XTREME / PD. CABE', 'RYAN ZHANG', '087774708777', 'xtreme.audio@yahoo.com', 'JL. RE MARTADINATA NO 279 CIPUTAT los 81(KOMP ONDERDIL) CIPUTAT - PONDOK CABE', 6, '-6.3497717', '106.7484013', 'venom_fighter_picture6.png', 'community', 'UGYZI4VH', 'UGYZI4VH.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(299, NULL, 'KINTAMANI AUDIO', 'MAKRAM ABDULLAH', '08115009495', '', 'JL. B RAYA NO. 4 KARANG ANYAR JAK-PUS', 1, '-6.1613702', '106.8699122', 'venom_fighter_picture21.png', 'community', '584M9PI6', '584M9PI6.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(300, NULL, 'SIMPATI AUDIO', 'AMIN ', '081584914169', '', 'ATRIUM SENEN BLOK E NO. 50-51', 1, '-6.1765343', '106.8388223', 'venom_fighter_picture6.png', 'community', 'JTVLBPNW', 'JTVLBPNW.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(303, NULL, 'MENTARI MOTOR', 'HALEX', '082343202222', '', 'JLN. PENGAYUMAN BLOK B NO. 1 (KOMP. MIRAH) PANAKUKANG - MAKASAR', 19, '-5.161343', '119.4476839', 'venom_fighter_picture6.png', 'community', 'SZPWAD26', 'SZPWAD26.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(306, NULL, 'MANSION AUDIO', 'WILLY PRATIKO', '089674043555', 'csmakassar@gmail.com', 'jl. Andi Tonro No.36 FG', 19, '-5.1724674', '119.4176718', 'venom_fighter_picture31.png', 'community', 'IPXW5T7J', 'IPXW5T7J.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(308, NULL, 'MINIMALIS AUDIO', 'DENNY NAWAWI', '081381268731', '', 'sunter kirana 7 blok ab no.7 perumahan villa danau indah,,SUNTER', 1, '-6.1560847', '106.8778457', 'venom_fighter_picture18.png', 'community', 'RDE5OULF', 'RDE5OULF.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(311, NULL, 'TRIJAYA MOTOR', 'SUDIR', '087877840018', 'senosudiro@gmail.com', 'RUKO AXC SUMMARECON BEKASI BLOK VE NO. 28 BEKASI', 3, '-6.2261944', '106.9995172', 'venom_fighter_picture6.png', 'community', 'ZIOSF6QX', 'ZIOSF6QX.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(314, NULL, 'JG AUDIO VARIASI', 'JOHAN JONATHAN', '083856299568', 'jevariasi@gmail.com', 'Raya Indah Wetan No.20A Surabaya', 5, '-7.2538459', '112.7339589', 'venom_fighter_picture6.png', 'community', 'MK2PJWX8', 'MK2PJWX8.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(318, NULL, 'NICE CAR AUDIO', 'EDDY HALIM/AWIE', '08116125721', 'wie@venommedan.info', 'JL. AIP. KS. TUBUN II D/D SUMATRA NO. 9 MEDAN', 2, '3.6426183', '98.5294051', 'venom_fighter_picture6.png', 'community', 'IEZCNQO6', 'IEZCNQO6.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(321, NULL, 'VITARA AUDIO ', 'WILLY SALIM', '0811616989', 'willysalim@yahoo.co.id', 'JLN GURU PATIMPUS NO.11 G-F MEDAN', 2, '3.5934464', '98.6684023', 'venom_fighter_picture6.png', 'community', 'V51O4KR3', 'V51O4KR3.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(322, NULL, 'MANDIRI AUDIO ', 'SUSANTO', '081361124567', 'pinpinjep@hotmail.com', 'JLN.GLUGU NO.18F-18G MEDAN', 2, '3.5914212', '98.6667432', 'venom_fighter_picture6.png', 'community', '9ZQSJOL5', '9ZQSJOL5.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(323, NULL, 'VIN AUDIOWORKS ', 'KEVIN KLISE', '08116119591', 'klise0501@gmail.com', 'JLN SEI BATANG HARI NO 111AMEDAN', 2, '3.5848929', '98.6423393', 'VIN_AUDIO.jpg', 'community', 'ICZ10JT9', 'ICZ10JT9.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(325, NULL, 'ZONA AUDIO', 'FREDY WIRANATA', '087867265315', 'fredywira@gmail.com', 'JLN. KAPTEN MUSLIM 1B/173 MEDAN', 2, '3.6021782', '98.6429159', 'venom_fighter_picture6.png', 'community', 'DS47O2YB', 'DS47O2YB.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(326, NULL, 'PAHLAWAN AUDIO', 'ERICKKO SHEN', '081360008565', 'pahlawanaudio@gmail.com', 'JLN IMAM BONJOL NO.262 Binjai', 31, '3.6079634', '98.4894928', 'venom_fighter_picture6.png', 'community', 'ZOEWY027', 'ZOEWY027.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(331, NULL, 'MERLIN JAYA SERVICE', 'MARIANA SADINI', '081361729788', '', 'JL SETIA BUDI NO. 125 TJ REJO - MEDAN', 2, '3.565674', '98.6313269', 'venom_fighter_picture19.png', 'community', '8X7ZPFE1', '8X7ZPFE1.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(333, NULL, 'JAYA AUDIO', 'STEVEN', '082366969493', 'flink_18@yahoo.com', 'JL GLUGUR NO.33B - MEDAN', 2, '3.590703', '98.6665723', 'venom_fighter_picture6.png', 'community', 'BF83XKJY', 'BF83XKJY.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(334, NULL, 'VARIA MOTOR', 'AAN', '08116832580', '', 'JL. TWK 14 DAUT SYAH NO.51 - BANDA ACEH', 38, '5.5500053', '95.314478', 'venom_fighter_picture6.png', 'community', '06W9DKVO', '06W9DKVO.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(340, NULL, 'PT.EKA SEJAHTERA AUDIOTAMA ', 'ERWIN KOSASIH / Yuliana  ', '08122160358  ', 'dolphine20@yahoo.com', 'JL. WASTU KENCANA NO.11 BANDUNG', 7, '-6.9120724', '107.6067489', 'venom_fighter_picture30.png', 'community', 'NL1Y8CAK', 'NL1Y8CAK.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(344, NULL, 'HAYATO VARIASI', 'ANSORI', '082230605151', '', 'JLN RAYA DEMAK NO.167 SURABAYA', 5, '-7.2478087', '112.7030884', 'hayato_variasi.png', 'community', '41QMAXRV', '41QMAXRV.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(345, NULL, 'GRAND PRIX 88', 'HERI GUNAWAN', '085276667005', 'herigunawan2609@gmail.com', 'JLN PADMO SUSASTRO NO 1-2A SURABAYA', 5, '-7.2920709', '112.726715', 'venom_fighter_picture6.png', 'community', 'I5E108DG', 'I5E108DG.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(352, NULL, 'SJA KLAMPIS', 'THOMAS LIANG ', '081703681010', 'liangsja@yahoo.com', 'RUKO KLAMPIS SQUARE A-1 SURABAYA', 5, '-7.2853087', '112.7763172', 'venom_fighter_picture29.png', 'community', '7ZSCDRVA', '7ZSCDRVA.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(356, NULL, 'SSA-YK VENOM DIY & JATENG', 'MICHAEL ARIYANTO/AKIAT', '08568880996', 'co_sportage@yahoo.co.id', 'JLN RING ROAD BARAT NO.250(THUNDER)YOGYAKARTA', 17, '-7.7622149', '110.3341013', 'venom_fighter_picture6.png', 'community', 'A7SEFZGW', 'A7SEFZGW.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(357, NULL, 'KOWLOON AUDIO', 'HERU NUGROHO', '087839938000', '', 'Jl. Veteran no. 18', 17, '-7.8043137', '110.3924963', 'venom_fighter_picture20.png', 'community', '7U6NLIXB', '7U6NLIXB.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(358, NULL, 'DIAN TARA ', 'EDDY PRAMONO/JIULIM ', '0816664484', '', 'Jl. Tentara Pelajar No. 12Semarang', 18, '-7.0089747', '110.4312913', 'venom_fighter_picture6.png', 'community', 'KFQ3ZWPX', 'KFQ3ZWPX.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(359, NULL, 'DREAM SOUND CAR AUDIO', 'ANTON', '0816670135', '', 'JLN KAPTEN MULYADI NO.50 BSLONG SOLO', 23, '-7.5695839', '110.8310531', 'venom_fighter_picture6.png', 'community', 'TJ4LFXDK', 'TJ4LFXDK.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(363, NULL, 'OTO VARIASI', 'CHANDRA RIYANTO', '0811268913', 'c_riyanto@yahoo.com', 'MAYJEN BAMBANG SOEGENG NO.7', 25, '-7.5077469', '110.2071093', 'venom_fighter_picture6.png', 'community', 'M4YRX12B', 'M4YRX12B.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(365, NULL, 'XPOSE CAR AUDIO & VARIASI', 'TONI YULIANTO', '08157639137', '', 'jl. Barito 12A Semarang', 18, '-6.9834883', '110.4313359', 'venom_fighter_picture6.png', 'community', '9Y2LZTCJ', '9Y2LZTCJ.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(367, NULL, 'G&S AUDIO', 'SOEGITO', '085101688480', 'sugitogns@yahoo.com', 'JLN. GOJAH RAYA NO.22SEMARANG', 18, '-6.9785762', '110.4468608', 'venom_fighter_picture6.png', 'community', 'UQ57VP0W', 'UQ57VP0W.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(370, NULL, 'SUBUR AUDIO', 'EDWIN EFENDI', '08122660808', 'suburaudioworkshoppwt@gmail.com', 'JL. Gerilya Timur no.1124 Purwokerto Jawa Tengah', 29, '-7.4312585', '109.2263801', 'venom_fighter_picture6.png', 'community', '56JFK8R4', '56JFK8R4.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(371, NULL, 'RX AUDIO', 'ERIC SETIOPRABOWO', '087761008006', '', 'Jl.gatot Subroto Timur no.22 Tohpati denpasar bali 80237', 9, '-8.6358602', '115.2497378', 'venom_fighter_picture6.png', 'community', 'TSMNUV24', 'TSMNUV24.png', '2018-04-09 00:54:36', '2019-03-01 09:13:15', NULL),
(375, NULL, 'MARVIN AUTO GARAGE', 'MARVIN', '087865097111', '', 'JLN. GATSU TIMUR NO.296 DENPASAR BALI', 9, '-8.6359469', '115.231803', 'venom_fighter_picture6.png', 'community', 'RJKDLMXE', 'RJKDLMXE.png', '2018-04-09 00:54:37', '2019-03-01 09:13:15', NULL),
(376, NULL, 'FOCUS FARIASI LOMBOK', 'HAMPTON ROY', '0818362272', 'focus_lombok@yahoo.com', 'PERTOKOAN SELAPARANG NO:5 JALAN:SELAPARANG CAKRANEGARA', 24, '-8.5898296', '116.1378993', 'venom_fighter_picture6.png', 'community', 'UN87DAI9', 'UN87DAI9.png', '2018-04-09 00:54:37', '2019-03-01 09:13:15', NULL),
(377, NULL, 'SL AUDIOWORK', 'EDWARD STANLEY', '081333223335', 'sumberlancar3@gmail.com', 'JL. TUMENGGUNG SURYA NO 96 MALANG', 13, '-7.9596115', '112.6407725', 'venom_fighter_picture12.png', 'community', 'XPRE7FI8', 'XPRE7FI8.png', '2018-04-09 00:54:37', '2019-03-01 09:13:15', NULL),
(378, NULL, 'PRIMA AC KEDIRI', 'ASNAWI', '081335002123', '', 'JLN AIR LANGGA NO 86 KATANG KEDIRI', 27, '-7.7951243', '112.0054198', 'venom_fighter_picture22.png', 'community', 'ONS2EPLK', 'ONS2EPLK.png', '2018-04-09 00:54:37', '2019-03-01 09:13:15', NULL),
(381, NULL, 'SUMBER AGUNG VARIASI', 'WILI MUJIONO', '081353328469', '', 'JL. GAJAH MADA NO. 21 A PAGESANGAN - LOMBOK', 24, '-8.5986057', '116.0996013', 'venom_fighter_picture6.png', 'community', '1B7CR9XW', '1B7CR9XW.png', '2018-04-09 00:54:37', '2019-03-01 09:13:15', NULL),
(382, NULL, 'SS AUDIO VID', 'Sendri Sefiadi ', '087871053787 ', 'nadakalih@gmail.com', 'JL Puri Serang Hijau A6-5 Ciponcok Jaya SERANG', 21, '-6.1462846', '106.1774721', 'venom_fighter_picture6.png', 'community', 'A62IMF7K', 'A62IMF7K.png', '2018-04-09 02:01:01', '2019-03-01 09:13:15', NULL),
(383, NULL, 'SMART & CARE (HSL SOUNDCRAFT)', 'EDY  ', '08116119789  ', 'hslsoundcraft@yahoo.com', 'JL. SEI BATANG HARI NO.11 - MEDAN', 2, '3.5849331', '98.6499586', 'venom_fighter_picture6.png', 'community', 'J4EFAUYR', 'J4EFAUYR.png', '2018-04-09 02:03:00', '2019-03-01 09:13:15', NULL),
(384, NULL, 'SJA CAR AUDIO', 'RONALD  ', '08123508828 ', 'ronald5ja@yahoo.com', 'JL. GENTONG BESAR NO.27 SURABAYA', 5, '-7.2591022', '112.7393921', 'venom_fighter_picture6.png', 'community', 'NWFSCI5K', 'NWFSCI5K.png', '2018-04-09 02:37:48', '2019-03-01 09:13:15', NULL),
(385, NULL, 'SERASI AUDIO', 'BUDI', ' 082122836721', '', 'MEGA GLODOK KEMAYORAN LT.5 BLOK A9 NO.2', 1, '-6.1545207', '106.846198', 'venom_fighter_picture6.png', 'community', 'ZU39SBW2', 'ZU39SBW2.png', '2018-04-09 02:37:48', '2019-03-01 09:13:15', NULL),
(386, NULL, 'Sand’s Modification’s', 'SANDY SETIAWAN', '085298007888', '', 'JL. KELAPA NO. 77 KOTA PALOPO', 42, '-3.0072057', '120.1875419', 'venom_fighter_picture6.png', 'community', '74OPA3B1', '74OPA3B1.png', '2018-04-09 02:37:48', '2019-03-01 09:13:15', NULL),
(387, NULL, 'RPM AUTO GARAGE', 'SONNY ABADI ', ' 085343665888 ', 'rajawalibn@gmail.com', 'JENDRAL SUDIRMAN NO.57 BONE SULAWESI SELATAN', 40, '-6.2246157', '106.8042863', 'venom_fighter_picture6.png', 'community', 'RAN6VYBI', 'RAN6VYBI.png', '2018-04-09 02:49:02', '2019-03-01 09:13:15', NULL),
(388, NULL, 'R GARAGE MODIFICATION ', 'RAMA ADY PUTRA RANTETONDO', ' 082188751888 ', 'rgarageauto@gmail.com', 'JL. Janggo paropo no.8 Makassar 90233', 19, '-5.1490907', '119.4560813', 'venom_fighter_picture14.png', 'community', 'XIG65N4K', 'XIG65N4K.png', '2018-04-09 02:51:23', '2019-03-01 09:13:15', NULL),
(389, NULL, 'MODIF AUDIO', 'PANJI IRAWAN', '081362319909', '', 'JL. AH NASUTION NO. 100 MEDAN', 2, '3.5419664', '98.6628133', 'venom_fighter_picture6.png', 'community', '6IY8ERN3', '6IY8ERN3.png', '2018-04-09 02:37:48', '2019-03-01 09:13:15', NULL),
(390, NULL, 'MM AUDIOWORKS', 'HENDRO SETIAWAN  ', '081805123457  ', 'hendro76@gmail.com', 'JL. SUNANDAR PRIYO SUDARMO NO.45 MALANG - JATIM 65123', 13, '-7.9535003', '112.6434419', 'venom_fighter_picture6.png', 'community', '5QPVRJMU', '5QPVRJMU.png', '2018-04-09 03:12:39', '2019-03-01 09:13:15', NULL),
(391, NULL, 'MJM - BEKASI', 'ABAS  ', '081311177003 ', 'admin@gmail.com', 'JL.RAYA KOTA LEGENDA, KALIJAMBE WARENG NO.2 BEKASI TIMUR', 3, '-6.2746403', '107.0422019', 'venom_fighter_picture16.png', 'community', 'MT36PV1D', 'MT36PV1D.png', '2018-04-09 03:15:36', '2019-03-01 09:13:15', NULL),
(392, NULL, 'MITRA VARIASI', 'AYUNG', '08158862569', 'ayong_mitra22@yahoo.com', 'PASAR MOBIL TAHAP 2 BLOK F NO. 8-9 JAK-PUS', 1, '-6.1631619', '106.856853', 'venom_fighter_picture17.png', 'community', '8XOEPV7G', '8XOEPV7G.png', '2018-04-09 03:17:45', '2019-03-01 09:13:15', NULL),
(393, NULL, 'MILANO CAR AUDIO', 'HARTONO SETIAWAN', '081285066574', 'rafael.hartono.setiawan95@gmail.com', 'JL. YOS SURDASO NO. 121 KRATON SOLO', 23, '-7.5825068', '110.8113756', 'venom_fighter_picture6.png', 'community', 'TBP1938G', 'TBP1938G.png', '2018-04-09 17:09:05', '2019-03-01 09:13:15', NULL),
(394, NULL, 'MANDIRI AUDIO JAYA', 'RUDY LIM    ', '08126009853', '', 'JL.TAPIAN NAULI PS III KOMPLEK BAKUL INDAH TAHAP II NO.C - MEDAN', 2, '3.5914212', '98.6667432', 'venom_fighter_picture6.png', 'community', 'HSP0M7Y9', 'HSP0M7Y9.png', '2018-04-09 23:38:21', '2019-03-01 09:13:15', NULL),
(395, NULL, 'KONSEP AUTO ACCESSORIES', 'HUANDI CHANDRA    ', '081266301889', 'huandi_88@yahoo.com', 'JL. LEMBAGA PERMASYARAKATAN 24F TANGKERANG UTARA BUKIT RAYA ', 36, '0.5061726', '101.4598288', 'venom_fighter_picture6.png', 'community', 'LNTWEISM', 'LNTWEISM.png', '2018-04-09 23:52:02', '2019-03-01 09:13:15', NULL),
(396, NULL, 'JEJ AUTOTUNE', 'JACKY TANIMENA', '081232222922', 'jacky.autotune@gmail.com', 'JL.CENDRAWASIH NO.12 (NIA) AMBON', 35, '-3.691079', '128.1849871', 'venom_fighter_picture6.png', 'community', 'DLS1YXUF', 'DLS1YXUF.png', '2018-04-10 00:19:01', '2019-03-01 09:13:15', NULL),
(397, NULL, 'HN JAYA VARIASI', 'RECKSY  ', '082333392666 ', 'RECKSYARMA@GMAIL.COM', 'jl. lasingran no 102 rappang Kota Makassar, Sulawesi Selatan 90114', 43, '-3.8400642', '119.8056701', 'venom_fighter_picture6.png', 'community', '35RCMDEH', '35RCMDEH.png', '2018-04-10 02:17:24', '2019-03-01 09:13:15', NULL),
(398, NULL, 'GEMILANG AUDIO', 'RUSMIN    ', '085296561653', 'alingiphone@icloud.com', ' JL. AMAL NO 1T GRAHA KASUARI MEDAN', 2, '3.5857174', '98.6195593', 'venom_fighter_picture25.png', 'community', '6DVHYRJB', '6DVHYRJB.png', '2018-04-10 02:46:16', '2019-03-01 09:13:15', NULL),
(399, NULL, 'Surabaya motor variasi', 'Wong Yong  Chiang', '081934443888', 'wong.justin388@gmail.com', 'Jalan veteran Utara no 337 ', 19, '-5.1426127', '119.4224733', 'venom_fighter_picture6.png', 'community', 'TLX19ZJ5', 'TLX19ZJ5.png', '2018-04-11 02:57:48', '2019-03-01 09:13:15', NULL),
(400, NULL, 'Sinar Jaya Audio', 'Richard Gerard', '085298074835', 'ichal.tribe@gmail.com', ' Jl. achmad A Wahab no 7', 41, '0.5788793', '123.0421117', 'venom_fighter_picture23.png', 'community', '1WT74A0N', '1WT74A0N.png', '2018-04-11 03:03:20', '2019-03-01 09:13:15', NULL),
(401, NULL, 'Winz Auto Manado', 'Edwin Koilam', '0816231207', 'edy_sekring@yahoo.com', 'Jl.Balaikota 1 no.25', 33, '1.4863072', '124.8467797', 'venom_fighter_picture24.png', 'community', '17CRJ2G8', '17CRJ2G8.png', '2018-04-11 03:06:38', '2019-03-01 09:13:15', NULL),
(402, NULL, 'Classic Audio', 'Fuk  Po', '08127253363', 'classic198@ymail.com', 'JL. Pemuda no.15 Tanjung Karang', 8, '-5.4110882', '105.2559961', 'venom_fighter_picture6.png', 'community', 'P0OI62RG', 'P0OI62RG.png', '2018-04-11 03:11:07', '2019-03-01 09:13:15', NULL),
(403, NULL, 'mister king otoproject', 'Andi ruslan  palewai', '085351377711', 'andiruslanpalewai@gmail.com', 'jalan MT.Haryono No 1 Watampone ', 40, '-4.5399157', '120.2986402', 'venom_fighter_picture26.png', 'community', 'GPNTBKA0', 'GPNTBKA0.png', '2018-04-11 03:17:00', '2019-03-01 09:13:15', NULL),
(404, NULL, 'Pratama variasi', 'Jimmy  Tanujaya', '081341112226', 'Intersis_Makassar1@yahoo.com', 'Jalan veteran selatan no 296', 19, '-5.1624524', '119.4191119', 'venom_fighter_picture27.png', 'community', 'UTD1LHQB', 'UTD1LHQB.png', '2018-04-11 03:25:16', '2019-03-01 09:13:15', NULL),
(405, NULL, 'X FILE audio', 'Jimmy Yohanes', '08563002775', 'Jimzcaraudio@yahoo.com', 'Keputih timur jaya no 7', 5, '-7.2920436', '112.8073161', 'venom_fighter_picture28.png', 'community', 'H1QGDK63', 'H1QGDK63.png', '2018-04-11 03:29:17', '2019-03-01 09:13:15', NULL),
(406, NULL, 'SUMBER JAYA AUDIO', 'Tan Kian Ming', '0811318080', 'tanotoming@gmail.com', 'Kedung Doro 21', 5, '-7.2594277', '112.7322588', 'venom_fighter_picture6.png', 'community', 'ANQH1PUZ', 'ANQH1PUZ.png', '2018-04-11 03:35:35', '2019-03-01 09:13:15', NULL),
(407, NULL, 'Cemara Abadi', 'Lucky Gunawan', '0811228181', 'lucky@cemaraabadi.com', 'BANDUNG Peta 60', 7, '-6.9372936', '107.5974289', 'venom_fighter_picture15.png', 'community', '3ZOVAG5X', '3ZOVAG5X.png', '2018-04-11 03:38:48', '2019-03-01 09:13:15', NULL),
(408, NULL, 'Makassar Variasi', 'Rizal Haris ', '085394000024', 'yhulie24yl@gmail.com', 'Kumala Poros No.3', 19, '-5.172871', '119.4089533', 'venom_fighter_picture6.png', 'community', 'D17MJOZ2', 'D17MJOZ2.png', '2018-04-11 03:45:31', '2019-03-01 09:13:15', NULL),
(409, 69, 'Azizal', 'azizal', '097986', 'azizal@gmail.com', 'Jogya', 17, '7.00999809988', '110.098998', 'venom_fighter_picture6.png', 'community', 'JGXLQD2E', 'JGXLQD2E.png', '2018-04-27 03:09:54', '2018-04-27 03:09:54', '2018-08-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskon`
--

CREATE TABLE IF NOT EXISTS `tb_diskon` (
  `id` int(11) NOT NULL,
  `kode_voucher` varchar(40) NOT NULL,
  `nominal` decimal(10,0) NOT NULL,
  `category` set('voucher','diskon') DEFAULT 'voucher',
  `expired` date NOT NULL,
  `create_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_diskon`
--

INSERT INTO `tb_diskon` (`id`, `kode_voucher`, `nominal`, `category`, `expired`, `create_on`) VALUES
(2, 'VNOM78', '10', 'diskon', '2018-08-30', '2018-03-29 07:50:36'),
(4, 'hh1123', '100000', 'voucher', '2018-03-30', '2018-03-29 09:20:28'),
(10, 'VENOM50RB', '50000', 'voucher', '2018-08-30', '2018-05-03 17:48:37'),
(11, 'abn', '50000', 'voucher', '2018-08-24', '2018-05-12 10:40:47'),
(12, '6HGZ05O4', '100000', 'diskon', '2018-07-30', '2018-07-28 16:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dislike`
--

CREATE TABLE IF NOT EXISTS `tb_dislike` (
  `id` int(11) NOT NULL,
  `foto_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` set('like','dislike') NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dislike`
--

INSERT INTO `tb_dislike` (`id`, `foto_id`, `user_id`, `name`, `create_on`) VALUES
(48, 5, 69, 'dislike', '2018-04-25 19:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_event`
--

CREATE TABLE IF NOT EXISTS `tb_event` (
  `id_event` int(11) NOT NULL,
  `nama_event` varchar(80) NOT NULL,
  `detail_event` varchar(300) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `image` text,
  `baca` set('yes','no') NOT NULL DEFAULT 'no',
  `lat` text,
  `lang` text,
  `status` set('publish','draft') NOT NULL DEFAULT 'draft',
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_event`
--

INSERT INTO `tb_event` (`id_event`, `nama_event`, `detail_event`, `lokasi`, `tanggal`, `image`, `baca`, `lat`, `lang`, `status`, `create_on`) VALUES
(1, 'IIMS 2018', '<p>Come and Join Di Booth Venom IIMS 2018</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'JIEXPO KEMAYORAN', '2018-04-25', 'http://103.82.242.153/venom/assets/upload/event/panr_1.png', 'yes', '-6.1464037', '106.8408621', 'publish', '2018-03-25 01:03:04'),
(2, 'Promo IIMS 2018 19 -29 April 2018', '<p>Headphone Venom Elemento hanya Rp. 100.000 selama pameran IIMS</p>\r\n', 'JIEXPO KEMAYORAN Kemayoran', '2018-04-19', 'http://103.82.242.153/venom/assets/upload/event/headphone_100_ribu.png', 'yes', '-6.1464037', '106.8408621', 'publish', '2018-04-21 04:35:25'),
(3, 'Promo IIMS 2018 19 -29 April 2018', '<p>Setiap pembelian dengan harga di atas free hadiah langsung</p>\r\n', 'JIEXPO KEMAYORAN', '2018-04-19', 'http://103.82.242.153/venom/assets/upload/event/promo_21.png', 'yes', '-6.1464037', '106.8408621', 'publish', '2018-04-21 04:57:57'),
(4, 'VENOM GIIAS 2018', '<p>Venom Photo Contest.<br />\r\nVenom mengadakan photo contest selama penyelenggaraan GIIAS 2018 dan terbuka untuk siapa saja tanpa biaya pendaftaran.&nbsp;<br />\r\nRegulasi :<br />\r\n- Setiap peserta wajib me-repost flyer ini di akun instagram nya masing-masing.<br />\r\n- Bebas menggunakan kamera apa s', 'Jalan BSD Grand Boulevard Raya No. 1, BSD City, Pagedangan, Tangerang, Banten 15339', '2018-08-02', 'http://103.82.242.153/venom/assets/upload/event/INSTAGRAM_PHOTO_CONTEST.jpg', 'yes', '', '', 'publish', '2018-08-03 03:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_foto`
--

CREATE TABLE IF NOT EXISTS `tb_foto` (
  `id` int(11) NOT NULL,
  `nama_foto` text NOT NULL,
  `album_id` int(11) NOT NULL,
  `status_foto` enum('publish','draft') NOT NULL DEFAULT 'draft',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_foto`
--

INSERT INTO `tb_foto` (`id`, `nama_foto`, `album_id`, `status_foto`, `user_id`) VALUES
(1, 'http://www.venom-audio.com/images/imageuploads/550_03b9133fd2edfe0516553be38fd54ae7.jpg', 1, 'publish', 15),
(2, 'http://www.venom-audio.com/images/imageuploads/550_684c81706add07164054dab2705b0ffe.jpg', 1, 'publish', 15),
(3, 'http://www.venom-audio.com/images/imageuploads/550_bbfd26093ac5afedeffe30b14ca0bf28.jpg', 1, 'publish', 15),
(4, 'http://www.venom-audio.com/images/imageuploads/550_e084657d95802adb2a3033539c2e5edb.jpg', 1, 'publish', 15),
(5, 'http://www.venom-audio.com/images/imageuploads/550_f47fec6c02ab18d9f7f59fc1501c9df3.jpg', 2, 'publish', 15),
(6, 'http://www.venom-audio.com/images/imageuploads/550_c86c14f3c36b9c16ad8e3daf17f92660.jpg', 2, 'publish', 15),
(7, 'http://www.venom-audio.com/images/imageuploads/550_ebfb455f8717cca37d59d9d29dfb8e2c.jpg', 2, 'publish', 15),
(8, 'http://www.venom-audio.com/images/imageuploads/550_053a3b6532979633cb2db2556ccb9ba7.jpg', 2, 'publish', 15),
(9, 'http://www.venom-audio.com/images/imageuploads/550_85738133022302f169498b4de56dff74.jpg', 3, 'publish', 15),
(10, 'http://www.venom-audio.com/images/imageuploads/550_93d17ad6c81ef6e9595a21be688b7291.jpg', 3, 'publish', 15),
(11, 'http://www.venom-audio.com/images/imageuploads/550_d68fe1d6646129e1c7093424ea4f2b8c.jpg', 3, 'publish', 15),
(13, 'http://www.venom-audio.com/images/imageuploads/550_9cc337cf5fa387f56405150e49714f7c.jpg', 4, 'publish', 15),
(14, 'http://www.venom-audio.com/images/imageuploads/550_6bbc280df5cb78dcf7e4c02866961575.jpg', 4, 'publish', 15),
(15, 'http://www.venom-audio.com/images/imageuploads/550_728b0bed5bae513d7b7aa3b5f3ef5db5.jpg', 4, 'publish', 15),
(16, 'http://www.venom-audio.com/images/imageuploads/550_5ef4d2fae539f502665ee3a1796cc36c.jpg', 4, 'publish', 15),
(17, 'http://www.venom-audio.com/images/imageuploads/550_f136974e3de227f6b1ed3ee4ae265649.jpg', 5, 'publish', 15),
(18, 'http://www.venom-audio.com/images/imageuploads/550_e73a3fe7a63fcbb8b1afb8fef7dd7fd2.jpg', 5, 'publish', 15),
(19, 'http://www.venom-audio.com/images/imageuploads/550_a0fec6b31d9d1ae715c9128b91b591ca.jpg', 5, 'publish', 15),
(20, 'http://www.venom-audio.com/images/imageuploads/550_fc44f260f9e1e61aac2552da8502a141.jpg', 5, 'publish', 15),
(21, 'http://www.venom-audio.com/images/imageuploads/550_8c864dfc194bc9aea012402634831363.jpg', 5, 'publish', 15),
(22, 'http://www.venom-audio.com/images/imageuploads/550_c369b10bd855ad9d1e20e3a5e02cd2d5.JPG', 6, 'publish', 15),
(23, 'http://www.venom-audio.com/images/imageuploads/550_fb3b81342acab39b0a9294ae0aa0d816.JPG', 6, 'publish', 15),
(24, 'http://www.venom-audio.com/images/imageuploads/550_9d7817ae6f4fd1d989708d779f8094b5.JPG', 6, 'publish', 15),
(26, 'http://www.venom-audio.com/images/imageuploads/550_f29cea5c3a30fda3e235ec9cdd2fc54e.JPG', 6, 'publish', 15),
(27, 'http://www.venom-audio.com/images/imageuploads/550_494d4e8bb547d6f83ce1e16946993eac.JPG', 6, 'publish', 15),
(28, 'http://www.venom-audio.com/images/imageuploads/550_c03ac366689d26dfade07a1e468219b0.JPG', 6, 'publish', 15),
(29, 'http://www.venom-audio.com/images/imageuploads/550_22de534c1c72cb4eaa83a3a20a5aaac0.jpg', 7, 'publish', 15),
(30, 'http://www.venom-audio.com/images/imageuploads/550_0ead2b472e86db54714595382f8169a6.jpg', 7, 'publish', 15),
(31, 'http://www.venom-audio.com/images/imageuploads/550_f25e119922bad520218883e730c6fa96.jpg', 7, 'publish', 15),
(32, 'http://www.venom-audio.com/images/imageuploads/550_4a3925e9a84d68fa08ec3f099143b643.jpg', 7, 'publish', 15),
(33, 'http://www.venom-audio.com/images/imageuploads/550_22de534c1c72cb4eaa83a3a20a5aaac0.jpg', 9, 'publish', 61),
(34, 'http://www.venom-audio.com/images/imageuploads/550_22de534c1c72cb4eaa83a3a20a5aaac0.jpg', 9, 'publish', 61),
(35, 'http://www.venom-audio.com/images/imageuploads/550_c369b10bd855ad9d1e20e3a5e02cd2d5.JPG', 8, 'publish', 15),
(36, 'http://www.venom-audio.com/images/imageuploads/550_fb3b81342acab39b0a9294ae0aa0d816.JPG', 8, 'publish', 15);

-- --------------------------------------------------------

--
-- Table structure for table `tb_halaman`
--

CREATE TABLE IF NOT EXISTS `tb_halaman` (
  `id_halaman` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` tinytext NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice`
--

CREATE TABLE IF NOT EXISTS `tb_invoice` (
  `kode_invoice` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` set('paid','unpaid') NOT NULL,
  `alamat` text,
  `nama` varchar(50) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `cart` text,
  `baca` set('yes','no') NOT NULL DEFAULT 'no',
  `jenis` varchar(15) DEFAULT NULL,
  `comunity_id` int(11) DEFAULT NULL,
  `barang_status` set('accept','unaccept','delivery') NOT NULL DEFAULT 'unaccept',
  `kode_voucher` varchar(40) DEFAULT NULL,
  `kategori` varchar(15) DEFAULT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_invoice`
--

INSERT INTO `tb_invoice` (`kode_invoice`, `id_user`, `tanggal`, `status`, `alamat`, `nama`, `no_hp`, `cart`, `baca`, `jenis`, `comunity_id`, `barang_status`, `kode_voucher`, `kategori`, `create_on`) VALUES
(100001, 109, '2018-04-28 08:03:47', 'unpaid', 'malang', 'Handoyo', '081333260292', '[{"id_product":"48","jml_barang":"2"},{"id_product":"41","jml_barang":"2"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Simulator', '2018-04-28 08:03:47'),
(100002, 141, '2018-04-29 22:06:35', 'unpaid', '', 'teguh andria nugraha', '', '[{"id_product":"83","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-04-29 22:06:35'),
(100003, 109, '2018-04-30 23:56:22', 'unpaid', 'malang', 'Handoyo', '081333260292', '[{"id_product":"102","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Express', '2018-04-30 23:56:22'),
(100004, 17, '2018-05-01 07:31:26', 'paid', '', 'lukman marlin', '', '[{"id_product":"83","jml_barang":"6"}]', 'yes', 'Credit', NULL, 'accept', NULL, 'Product', '2018-05-01 07:31:26'),
(100005, 17, '2018-05-01 08:57:14', 'paid', '', 'lukman marlin', '', '[{"id_product":"88","jml_barang":"3"}]', 'yes', 'Credit', NULL, 'accept', NULL, 'Product', '2018-05-01 08:57:14'),
(100006, 17, '2018-05-01 09:07:10', 'unpaid', '', 'lukman marlin', '', '[{"id_product":"87","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-01 09:07:10'),
(100007, 144, '2018-05-02 02:24:21', 'paid', '', 'bababubu bababubu', '', '[{"id_product":"87","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-02 02:24:21'),
(100008, 144, '2018-05-02 02:26:16', 'unpaid', 'bababubu', 'bababubu', '123456789', '[{"id_product":"87","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-02 02:26:16'),
(100009, 109, '2018-05-02 02:44:21', 'unpaid', '', 'Handoyo Yanuar', '', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-02 02:44:21'),
(100010, 144, '2018-05-02 02:58:15', 'unpaid', 'bababubu', 'bababubu', '123456789', '[{"id_product":"11","jml_barang":"2"},{"id_product":"25","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-02 02:58:15'),
(100011, 150, '2018-05-07 00:27:11', 'unpaid', '', 'Disa Annisa', '', '[{"id_product":"18","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-07 00:27:11'),
(100012, 150, '2018-05-07 00:28:05', 'unpaid', 'bandung', 'Disa Annisa', '08112206411', '[{"id_product":"18","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-07 00:28:05'),
(100013, 150, '2018-05-07 00:43:59', 'unpaid', '', 'Disa Annisa', '', '[{"id_product":"11","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-07 00:43:59'),
(100014, 0, '2018-05-08 11:28:50', 'unpaid', '', '', '', '', 'yes', '', NULL, 'unaccept', NULL, '', '2018-05-08 11:28:50'),
(100015, 84, '2018-05-11 21:36:32', 'unpaid', '', 'Denny Laila', '', '[{"id_product":"58","jml_barang":"4"},{"id_product":"85","jml_barang":"2"},{"id_product":"48","jml_barang":"2"},{"id_product":"42","jml_barang":"2"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Simulator', '2018-05-11 21:36:32'),
(100016, 79, '2018-05-11 23:44:13', 'unpaid', 'bbbzb', 'Dee Irawan', '0844961', '[{"id_product":"18","jml_barang":"1"},{"id_product":"20","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-11 23:44:13'),
(100017, 171, '2018-05-12 02:35:38', 'unpaid', '', 'Andhika Aji', '', '[{"id_product":"88","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-12 02:35:38'),
(100018, 79, '2018-05-15 01:53:21', 'unpaid', '', 'Dee Irawan ', '', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-15 01:53:21'),
(100019, 79, '2018-05-15 01:53:31', 'unpaid', '', 'Dee Irawan ', '', '[{"id_product":"18","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-15 01:53:31'),
(100020, 79, '2018-05-15 01:54:49', 'unpaid', '', 'Dee Irawan ', '', '[{"id_product":"18","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-15 01:54:49'),
(100021, 79, '2018-05-15 07:30:12', 'unpaid', '', 'Dee Irawan ', '', '[{"id_product":"11","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-15 07:30:12'),
(100022, 109, '2018-05-15 07:31:44', 'unpaid', '', 'Handoyo yanuar', '', '[{"id_product":"6","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-15 07:31:44'),
(100023, 144, '2018-05-20 05:34:23', 'unpaid', '', 'bababubu bababubu', '', '[{"id_product":"3","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-20 05:34:23'),
(100024, 144, '2018-05-20 05:41:53', 'unpaid', 'bababubu addr', 'bababubu rc', '123456789', '[{"id_product":"7","jml_barang":"2"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-20 05:41:53'),
(100025, 144, '2018-05-20 05:48:25', 'unpaid', '', 'bababubu bababubu', '', '[{"id_product":"7","jml_barang":"4"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-20 05:48:25'),
(100026, 144, '2018-05-20 06:10:30', 'unpaid', '', 'bababubu bababubu', '', '[{"id_product":"7","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-05-20 06:10:30'),
(100027, 17, '2018-05-21 01:19:35', 'unpaid', 'desa buduk mengwi', 'simulator iOS recipient', '123456789', '[\n  {\n    "id" : "6",\n    "harga" : 1510000\n  },\n  {\n    "id" : "7",\n    "harga" : 4050000\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 01:19:35'),
(100028, 17, '2018-05-21 01:26:07', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 01:26:07'),
(100029, 17, '2018-05-21 04:58:25', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 04:58:25'),
(100030, 17, '2018-05-21 05:05:32', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:05:32'),
(100031, 17, '2018-05-21 05:12:00', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:12:00'),
(100032, 17, '2018-05-21 05:15:33', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:15:33'),
(100033, 17, '2018-05-21 05:15:56', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:15:56'),
(100034, 17, '2018-05-21 05:18:08', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:18:08'),
(100035, 17, '2018-05-21 05:27:10', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:27:10'),
(100036, 17, '2018-05-21 05:36:23', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:36:23'),
(100037, 17, '2018-05-21 05:37:08', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:37:08'),
(100038, 17, '2018-05-21 05:37:31', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:37:31'),
(100039, 17, '2018-05-21 05:40:07', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:40:07'),
(100040, 17, '2018-05-21 05:41:49', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:41:49'),
(100041, 17, '2018-05-21 05:48:56', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:48:56'),
(100042, 17, '2018-05-21 05:49:24', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:49:24'),
(100043, 17, '2018-05-21 05:55:45', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:55:45'),
(100044, 17, '2018-05-21 05:56:30', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:56:30'),
(100045, 17, '2018-05-21 05:57:24', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 05:57:24'),
(100046, 17, '2018-05-21 05:57:56', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 05:57:56'),
(100047, 17, '2018-05-21 06:06:26', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 06:06:26'),
(100048, 17, '2018-05-21 06:06:51', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:06:51'),
(100049, 17, '2018-05-21 06:11:05', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  },\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 06:11:05'),
(100050, 17, '2018-05-21 06:11:28', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:11:28'),
(100051, 17, '2018-05-21 06:22:28', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:22:28'),
(100052, 17, '2018-05-21 06:25:38', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:25:38'),
(100053, 17, '2018-05-21 06:29:47', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:29:47'),
(100054, 17, '2018-05-21 06:30:30', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:30:30'),
(100055, 17, '2018-05-21 06:32:10', 'unpaid', NULL, 'ada', NULL, NULL, 'yes', NULL, NULL, 'unaccept', NULL, NULL, '2018-05-21 06:32:10'),
(100056, 17, '2018-05-21 06:54:40', 'unpaid', 'ada', 'ada', 'ada', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "7"\n  },\n  {\n    "jml_barang" : 4,\n    "id_product" : "6"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-05-21 06:54:40'),
(100057, 193, '2018-06-18 03:51:57', 'unpaid', 'ada', 'ada', '123456', '[\n  {\n    "jml_barang" : 2,\n    "id_product" : "94"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-06-18 03:51:57'),
(100058, 193, '2018-06-20 10:54:14', 'unpaid', 'ada', 'Ada', '123456', '[\n  {\n    "jml_barang" : 5,\n    "id_product" : "20"\n  }\n]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-06-20 10:54:14'),
(100059, 84, '2018-07-07 00:12:44', 'unpaid', 'pluit', 'test', '08953265865', '[{"id_product":"88","jml_barang":"2"},{"id_product":"17","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-07-07 00:12:44'),
(100060, 110, '2018-07-07 00:15:47', 'unpaid', '', 'cs cs', '', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-07-07 00:15:47'),
(100061, 84, '2018-07-07 00:16:30', 'unpaid', '', 'Denny Laila ', '', '[{"id_product":"17","jml_barang":"2"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-07-07 00:16:30'),
(100062, 98, '2018-07-16 05:09:29', 'unpaid', '', 'Muhammad Abdul Hamid ', '', '[{"jml_barang":"2","id_product":"11"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-07-16 05:09:29'),
(100063, 61, '2018-07-28 07:24:12', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"111","jml_barang":"1"},{"id_product":"61","jml_barang":"2"},{"id_product":"38","jml_barang":"1"},{"id_product":"11","jml_barang":"2"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-07-28 07:24:12'),
(100064, 143, '2018-07-31 04:39:23', 'unpaid', '', '0', '', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-07-31 04:39:23'),
(100065, 147, '2018-08-03 04:07:08', 'unpaid', 'jakarta', 'indah', '0813303699487', '[{"jml_barang":"2","id_product":"17"},{"jml_barang":"1","id_product":"11"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:08'),
(100066, 147, '2018-08-03 04:07:43', 'paid', 'jakarta', 'indah', '081366948554', '[{"jml_barang":"2","id_product":"17"},{"jml_barang":"1","id_product":"11"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:43'),
(100067, 147, '2018-08-03 04:07:45', 'unpaid', '', '', '', '[{"jml_barang":"2","id_product":"17"},{"jml_barang":"1","id_product":"11"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:45'),
(100068, 147, '2018-08-03 04:07:46', 'unpaid', '', '', '', '[{"jml_barang":"2","id_product":"17"},{"jml_barang":"1","id_product":"11"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:46'),
(100069, 147, '2018-08-03 04:07:47', 'unpaid', '', '', '', '[{"jml_barang":"2","id_product":"17"},{"jml_barang":"1","id_product":"11"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:47'),
(100070, 147, '2018-08-03 04:07:48', 'unpaid', '', '', '', '[]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:07:48'),
(100071, 17, '2018-08-03 04:12:15', 'unpaid', 'Banyu', 'lukman', '089876574', '[{"jml_barang":"2","id_product":"20"},{"jml_barang":"1","id_product":"15"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-03 04:12:15'),
(100072, 146, '2018-08-03 10:57:49', 'unpaid', '', '0', '', '[{"id_product":"59","jml_barang":"1"},{"id_product":"22","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-08-03 10:57:49'),
(100073, 61, '2018-08-04 06:09:36', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-04 06:09:36'),
(100074, 61, '2018-08-04 06:26:15', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', NULL, 'yes', 'Balance', NULL, 'unaccept', NULL, 'Balance', '2018-08-04 06:26:15'),
(100075, 61, '2018-08-04 06:26:27', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', NULL, 'yes', 'Balance', NULL, 'unaccept', NULL, 'Balance', '2018-08-04 06:26:27'),
(100076, 61, '2018-08-04 06:28:55', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"19","jml_barang":"1"},{"id_product":"3","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-04 06:28:55'),
(100077, 61, '2018-08-04 06:33:56', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"17","jml_barang":"1"}]', 'yes', 'Balance', NULL, 'unaccept', NULL, 'Balance', '2018-08-04 06:33:56'),
(100078, 61, '2018-08-04 06:37:18', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"11","jml_barang":"2"}]', 'yes', 'Balance', NULL, 'unaccept', NULL, 'Product', '2018-08-04 06:37:18'),
(100079, 37, '2018-08-05 09:15:56', 'unpaid', '', '0', '', '[{"id_product":"19","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-08-05 09:15:56'),
(100080, 61, '2018-08-05 11:58:25', 'paid', 'Jalan ciliwung VI no 5A', 'yesCS', '081333260292', '[{"id_product":"6","jml_barang":"1"},{"id_product":"19","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-05 11:58:25'),
(100081, 61, '2018-08-05 12:32:56', 'unpaid', '', '10000000', '', '[{"id_product":"112","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:32:56'),
(100082, 61, '2018-08-05 12:34:00', 'unpaid', '', '10000000', '', '[{"id_product":"112","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:34:00'),
(100083, 61, '2018-08-05 12:34:25', 'unpaid', '', '10000000', '', '[{"id_product":"112","jml_barang":"1"}]', 'yes', 'Credit', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:34:25'),
(100084, 61, '2018-08-05 12:34:51', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"112","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:34:51'),
(100085, 61, '2018-08-05 12:35:30', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"112","jml_barang":"2"}]', 'yes', 'Balance', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:35:30'),
(100086, 61, '2018-08-05 12:37:18', 'unpaid', 'Jalan ciliwung VI no 5A', 'CS', '081333260292', '[{"id_product":"15","jml_barang":"1"},{"id_product":"22","jml_barang":"1"}]', 'yes', 'Balance', NULL, 'unaccept', NULL, 'Product', '2018-08-05 12:37:18'),
(100087, 146, '2018-08-06 03:45:46', 'unpaid', 'yyy', 'xxz', '123545', '[{"id_product":"163","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Express', '2018-08-06 03:45:46'),
(100088, 146, '2018-08-06 04:15:56', 'unpaid', 'jakarta', '', '0000', '[{"id_product":"163","jml_barang":"1"}]', 'yes', 'Cash', NULL, 'unaccept', NULL, 'Express', '2018-08-06 04:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `id_kat` int(2) NOT NULL,
  `subkat_id` int(2) NOT NULL DEFAULT '0',
  `kategori` varchar(50) NOT NULL,
  `images` text,
  `tgl_input_kat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kat`, `subkat_id`, `kategori`, `images`, `tgl_input_kat`) VALUES
(1, 1, 'SPEAKER DEPAN', 'http://www.venom-audio.com/img/icon-spks.png', '2018-03-07 12:00:22'),
(2, 2, 'SUBWOOFER', 'http://www.venom-audio.com/img/icon-subs.png', '2018-03-07 12:00:59'),
(3, 3, 'AMPLIFIER MONOBLOCK', 'http://www.venom-audio.com/img/icon-amps.png', '2018-03-07 12:03:33'),
(4, 4, 'PROCESSOR', 'http://www.venom-audio.com/img/icon-procs.png', '2018-03-07 12:04:05'),
(5, 5, 'MIDRANGE', 'http://www.venom-audio.com/img/icon-spks.png', '2018-03-27 03:12:52'),
(6, 0, 'Hot Items', NULL, '2018-03-13 14:09:22'),
(7, 0, 'VENOM EXPRESS', NULL, '2018-03-21 08:13:03'),
(10, 1, 'COAXIAL', 'http://www.venom-audio.com/img/icon-spks.png', '2018-03-27 03:13:08'),
(13, 3, 'AMPLIFIER 4 CHANNEL', 'http://www.venom-audio.com/img/icon-amps.png', '2018-04-04 03:38:54'),
(14, 0, '2 nd', NULL, '2018-05-03 04:16:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kat_user`
--

CREATE TABLE IF NOT EXISTS `tb_kat_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `diskon` int(15) DEFAULT '0',
  `diskon_a` int(15) NOT NULL DEFAULT '0',
  `diskon_b` int(15) NOT NULL DEFAULT '0',
  `diskon_c` int(15) NOT NULL DEFAULT '0',
  `diskon_d` int(15) NOT NULL DEFAULT '0',
  `diskon_e` int(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kat_user`
--

INSERT INTO `tb_kat_user` (`id`, `nama`, `diskon`, `diskon_a`, `diskon_b`, `diskon_c`, `diskon_d`, `diskon_e`) VALUES
(1, 'Super admin', 10, 10, 5, 5, 85, 0),
(2, 'Wartawan', 0, 0, 0, 0, 0, 0),
(3, 'Admin', 0, 0, 0, 0, 0, 0),
(4, 'Member', 0, 0, 0, 0, 0, 0),
(5, 'Distributor', 10, 10, 0, 0, 0, 0),
(6, 'Sub Distributor', 5, 5, 0, 0, 0, 0),
(7, 'VFC', 5, 0, 0, 0, 0, 0),
(8, 'Owner', 0, 0, 0, 0, 0, 0),
(9, 'Dealer A', 10, 10, 0, 0, 0, 0),
(10, 'NULL', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_komentar`
--

CREATE TABLE IF NOT EXISTS `tb_komentar` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `komentar` varchar(500) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi`
--

CREATE TABLE IF NOT EXISTS `tb_konfirmasi` (
  `id_konfirmasi` int(11) NOT NULL,
  `kode_invoice` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `bank` varchar(30) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `images` tinytext NOT NULL,
  `read` enum('yes','no') NOT NULL DEFAULT 'no',
  `tanggal` date NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_konfirmasi`
--

INSERT INTO `tb_konfirmasi` (`id_konfirmasi`, `kode_invoice`, `id_user`, `atas_nama`, `bank`, `no_rek`, `images`, `read`, `tanggal`, `create_on`) VALUES
(1, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acde8b32f386.png', 'no', '2018-04-11', '2018-04-11 03:51:31'),
(2, 84977, 35, 'bbg', 'bni', 28949, 'http://venom.levanpedia.com/assets/upload/konfirm/5acde9e8935da.png', 'no', '2018-04-12', '2018-04-11 03:56:40'),
(3, 176160, 35, 'bambang', 'bni', 2994849, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdea75f33fb.png', 'no', '2018-04-11', '2018-04-11 03:59:01'),
(4, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdeaa89a17f.png', 'no', '2018-04-11', '2018-04-11 03:59:52'),
(5, 137622, 1, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdee33c29a9.png', 'no', '2018-04-11', '2018-04-11 04:14:59'),
(6, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdefd7be0b4.png', 'no', '2018-04-11', '2018-04-11 04:21:59'),
(7, 448821, 35, 'bambang', 'bni', 99378, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf0c1287dc.png', 'no', '2018-04-11', '2018-04-11 04:25:53'),
(8, 448821, 35, 'bambang', '', 99378, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf0fe00fc1.png', 'no', '2018-04-11', '2018-04-11 04:26:54'),
(9, 448821, 35, 'bambang', '', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf109cd95c.png', 'no', '2018-04-11', '2018-04-11 04:27:05'),
(10, 448821, 35, '', '', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf112e28cf.png', 'no', '0000-00-00', '2018-04-11 04:27:14'),
(11, 0, 35, 'bwnjsn', 'bni', 39938, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf25be7be1.png', 'no', '2018-04-11', '2018-04-11 04:32:43'),
(12, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf8985de83.png', 'no', '2018-04-11', '2018-04-11 04:59:20'),
(13, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdf9dd790e5.png', 'no', '2018-04-11', '2018-04-11 05:04:45'),
(14, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdfada33078.png', 'no', '2018-04-11', '2018-04-11 05:08:58'),
(15, 84977, 35, 'bambang', 'bni', 918883, 'http://venom.levanpedia.com/assets/upload/konfirm/5acdfbe5ef143.png', 'no', '2018-04-11', '2018-04-11 05:13:25'),
(16, 167998, 27, 'aku', 'bca', 12345577, 'http://venom.levanpedia.com/assets/upload/konfirm/5ad40cac59aac.png', 'no', '2018-04-17', '2018-04-15 19:38:36'),
(17, 167998, 27, '', '', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ad40ffadb73a.png', 'no', '0000-00-00', '2018-04-15 19:52:42'),
(18, 188064, 50, 'test', 'test', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ad5ae09738dc.png', 'no', '2018-04-17', '2018-04-17 01:19:21'),
(19, 949348, 27, 'a', 'a', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ad5af4151c3f.png', 'no', '0000-00-00', '2018-04-17 01:24:33'),
(20, 100004, 101, '', '', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5adf1b21104fa.png', 'no', '0000-00-00', '2018-04-24 04:55:13'),
(22, 100006, 17, 'nm', 'MN', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae101965b313.png', 'no', '2018-04-25', '2018-04-25 15:30:46'),
(23, 100007, 69, 'testing app by @developer', 'bni', 987264749, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae143c705996.png', 'no', '2018-04-26', '2018-04-25 20:13:11'),
(24, 100011, 109, 'test', 'test', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae1c3cc71782.png', 'no', '2018-04-26', '2018-04-26 05:19:24'),
(25, 100014, 109, 'test', 'test', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae2ad0950d84.png', 'no', '2018-04-27', '2018-04-26 21:54:33'),
(26, 100017, 109, 'test', 'test', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae2ed0573765.png', 'no', '2018-04-27', '2018-04-27 02:27:33'),
(27, 100001, 109, 'yyy', 'tey', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5ae48ddad7fc8.png', 'no', '2018-04-28', '2018-04-28 08:06:02'),
(28, 137622, 20, 'budi', 'bi', 1289128, 'http://venom.levanpedia.com/assets/upload/konfirm/5afaa9a33e6c0.png', 'no', '2018-04-11', '2018-05-15 02:34:27'),
(29, 100036, 17, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b02be1220a61.png', 'no', '2018-05-21', '2018-05-21 05:39:46'),
(30, 100036, 17, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b02be4b71c09.png', 'no', '2018-05-21', '2018-05-21 05:40:43'),
(31, 100004, 17, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b02cabedc32a.png', 'no', '2018-05-21', '2018-05-21 06:33:50'),
(32, 100005, 17, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b02cf17d5545.png', 'no', '2018-05-08', '2018-05-21 06:52:23'),
(33, 100056, 17, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b02cfb5aa4a0.png', 'no', '2018-05-20', '2018-05-21 06:55:01'),
(34, 100007, 144, 'ada', 'ada', 0, 'http://venom.levanpedia.com/assets/upload/konfirm/5b03f86759835.png', 'no', '2018-05-15', '2018-05-22 04:00:55'),
(35, 100057, 193, 'Eka', 'BCA', 123456, 'http://venom.levanpedia.com/assets/upload/konfirm/5b278eeecb03c.png', 'no', '2018-06-18', '2018-06-18 03:52:30'),
(36, 100058, 193, 'Parama Dharmika', 'BCA', 123456, 'http://venom.levanpedia.com/assets/upload/konfirm/5b2a94e3e8fb6.png', 'no', '2018-06-22', '2018-06-20 10:54:43'),
(37, 100058, 193, 'Parama Dharmika', 'BCA', 123456, 'http://venom.levanpedia.com/assets/upload/konfirm/5b2a9511236c4.png', 'no', '2018-06-22', '2018-06-20 10:55:29'),
(38, 100064, 143, 'denyv', 'bca', 12344567, 'http://103.82.242.153/venom/assets/upload/konfirm/5b641c375cabd.png', 'no', '0000-00-00', '2018-08-03 09:11:19'),
(39, 100080, 61, 'test', 'tesg', 0, 'http://103.82.242.153/venom/assets/upload/konfirm/5b66e8605c112.png', 'no', '2018-08-05', '2018-08-05 12:06:56'),
(40, 100085, 61, 'test', 'tesg', 0, 'http://103.82.242.153/venom/assets/upload/konfirm/5b66ef2eba761.png', 'no', '0000-00-00', '2018-08-05 12:35:58');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kota`
--

CREATE TABLE IF NOT EXISTS `tb_kota` (
  `id_kota` int(11) NOT NULL,
  `namaKota` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kota`
--

INSERT INTO `tb_kota` (`id_kota`, `namaKota`) VALUES
(1, 'JAKARTA'),
(2, 'MEDAN'),
(3, 'BEKASI'),
(4, 'JAMBI'),
(5, 'SURABAYA'),
(6, 'TANGERANG'),
(7, 'BANDUNG'),
(8, 'LAMPUNG'),
(9, 'BALI'),
(10, 'BOGOR'),
(11, 'PONTIANAK'),
(12, 'CIREBON'),
(13, 'MALANG'),
(14, 'SAMARINDA'),
(15, 'CIANJUR'),
(16, 'PALEMBANG'),
(17, 'YOGYAKARTA'),
(18, 'SEMARANG'),
(19, 'MAKASSAR'),
(20, 'TARAKAN KALIMANTAN'),
(21, 'SERANG '),
(22, 'MATARAM'),
(23, 'SOLO'),
(24, 'LOMBOK'),
(25, 'MAGELANG'),
(26, 'PEKALONGAN'),
(27, 'KEDIRI'),
(28, 'TEGAL'),
(29, 'PURWOKERTO'),
(30, 'MARTAPURA'),
(31, 'BINJAI'),
(32, 'SIDOARJO'),
(33, 'MANADO'),
(34, 'BANJARMASIN'),
(35, 'AMBON'),
(36, 'RIAU'),
(37, 'KALIMANTAN'),
(38, 'BANDA ACEH'),
(39, 'SULAWESI'),
(40, 'BONE'),
(41, 'GORONTALO'),
(42, 'PALOPO'),
(43, 'SIDRAP');

-- --------------------------------------------------------

--
-- Table structure for table `tb_like`
--

CREATE TABLE IF NOT EXISTS `tb_like` (
  `id` int(11) NOT NULL,
  `foto_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` set('like','dislike') NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_like`
--

INSERT INTO `tb_like` (`id`, `foto_id`, `user_id`, `name`, `create_on`) VALUES
(105, 1, 69, 'like', '2018-04-25 20:21:23'),
(104, 4, 69, 'like', '2018-04-25 20:21:15'),
(103, 7, 69, 'like', '2018-04-25 20:17:26'),
(102, 8, 69, 'like', '2018-04-25 19:50:48'),
(101, 6, 69, 'like', '2018-04-25 19:45:29'),
(100, 5, 69, 'like', '2018-04-25 19:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_merk`
--

CREATE TABLE IF NOT EXISTS `tb_merk` (
  `id_merk` int(2) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `tgl_input_merk` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_merk`
--

INSERT INTO `tb_merk` (`id_merk`, `merk`, `tgl_input_merk`) VALUES
(1, 'Venom', '2018-03-07 12:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_messages`
--

CREATE TABLE IF NOT EXISTS `tb_messages` (
  `message_id` int(11) NOT NULL,
  `chat_room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_messages`
--

INSERT INTO `tb_messages` (`message_id`, `chat_room_id`, `user_id`, `message`, `created_at`) VALUES
(184, 125, 69, 'test', '2018-04-21 18:10:02'),
(185, 135, 61, 'Selamat Pagi ada yang bisa kami bantu', '2018-04-21 18:54:08'),
(186, 139, 61, 'Selamat Pagi', '2018-04-21 18:56:12'),
(187, 139, 61, 'ada yang bisa kami bantu', '2018-04-21 18:56:17'),
(188, 125, 69, 'chat', '2018-04-21 20:54:54'),
(189, 125, 17, 'chat from admin', '2018-04-21 21:09:28'),
(190, 128, 61, 'ada yang bisa dibantu', '2018-04-22 00:35:41'),
(191, 129, 81, 'chat', '2018-04-22 05:06:34'),
(192, 129, 17, 'iya', '2018-04-22 05:06:51'),
(193, 129, 17, 'cb', '2018-04-22 05:07:27'),
(194, 129, 81, 'sudah ', '2018-04-22 05:09:40'),
(195, 129, 17, 'piye', '2018-04-22 05:10:12'),
(196, 129, 17, 'piye', '2018-04-22 05:10:12'),
(197, 125, 17, '', '2018-04-22 21:32:22'),
(198, 125, 17, 'kit', '2018-04-22 21:40:26'),
(199, 125, 69, '', '2018-04-22 22:24:38'),
(200, 125, 17, 'opo?', '2018-04-22 22:30:26'),
(201, 125, 69, 'bn', '2018-04-22 22:42:54'),
(202, 125, 17, 'asdjfnaksjf asjfkasjdf isjdf asdjf ', '2018-04-22 22:56:23'),
(203, 125, 69, 'xnndn', '2018-04-22 23:22:46'),
(204, 125, 69, 'bcncnf', '2018-04-22 23:24:18'),
(205, 125, 69, 'coba', '2018-04-22 23:27:32'),
(206, 125, 69, 'djjjd', '2018-04-22 23:49:41'),
(207, 129, 81, 'aku', '2018-04-23 00:47:45'),
(208, 129, 17, 'helo', '2018-04-23 00:48:05'),
(209, 129, 17, 'aku', '2018-04-24 00:36:42'),
(210, 135, 97, 'minta lagu untuk sound quality dong min, makasih', '2018-04-24 20:35:26'),
(211, 135, 61, 'sound quality yang mana kak ?', '2018-04-24 23:35:30'),
(212, 125, 61, 'yo', '2018-04-25 07:33:25'),
(213, 128, 79, 'hallo', '2018-04-25 21:48:11'),
(214, 149, 15, 'selamat sore', '2018-04-26 00:52:24'),
(215, 149, 15, 'ada yang bisa kami bantu', '2018-04-26 00:52:31'),
(216, 138, 109, 'test', '2018-04-26 01:31:30'),
(217, 138, 61, 'Test juga', '2018-04-26 01:31:59'),
(218, 138, 61, 'Test lagi', '2018-04-26 01:32:21'),
(219, 128, 15, 'iya pak bro', '2018-04-26 02:12:04'),
(220, 128, 79, 'cb cek lagi ', '2018-04-26 02:12:11'),
(221, 128, 79, 'kalau aku pencet lonceng notif ', '2018-04-26 02:12:20'),
(222, 128, 79, 'dia langsung out ', '2018-04-26 02:12:24'),
(223, 161, 15, 'Selamat sore ', '2018-04-26 02:14:25'),
(224, 161, 15, 'ada yang bisa kami bantu', '2018-04-26 02:14:31'),
(225, 128, 15, 'ud update ke 1.2.3', '2018-04-26 02:14:50'),
(226, 128, 15, 'barusan q update', '2018-04-26 02:14:57'),
(227, 128, 79, 'ok ', '2018-04-26 02:16:32'),
(228, 128, 79, 'cek ', '2018-04-26 02:16:33'),
(229, 128, 15, 'ditempatku aman pak bro', '2018-04-26 02:26:36'),
(230, 128, 15, 'di hpne siapa yang gak bisa', '2018-04-26 02:26:46'),
(231, 128, 15, 'bisa jadi sinyal juga bos', '2018-04-26 02:31:56'),
(232, 128, 15, 'kalau lambat', '2018-04-26 02:32:05'),
(233, 135, 97, 'Semacam lagu Yang santai, relax gitu kyk di live music', '2018-04-26 02:42:52'),
(234, 131, 15, 'iya pak din', '2018-04-26 03:27:36'),
(235, 128, 79, 'test chat', '2018-04-28 00:50:56'),
(236, 128, 61, 'monggo', '2018-04-28 00:51:23'),
(237, 128, 61, 'jam piro launching bos', '2018-04-28 00:51:36'),
(238, 131, 61, 'Siap pak din', '2018-04-28 03:58:32'),
(239, 166, 140, '', '2018-04-28 23:12:32'),
(240, 166, 61, 'selamat siang', '2018-04-28 23:12:56'),
(241, 166, 61, 'ada yang bisa kami bantu', '2018-04-28 23:15:35'),
(242, 138, 109, 'test', '2018-04-29 05:32:12'),
(243, 114, 17, 'test', '2018-05-01 07:30:14'),
(244, 114, 17, 'selamat tengah malam', '2018-05-01 08:58:39'),
(245, 169, 143, 'test', '2018-05-02 01:42:51'),
(246, 146, 119, 'selamat pagi', '2018-05-02 19:08:12'),
(247, 146, 15, 'selamat pagi pak', '2018-05-02 21:06:51'),
(248, 146, 15, 'ada yang bisa kami bantu', '2018-05-02 21:06:57'),
(249, 138, 85, 'sore pak handoyo', '2018-05-03 01:08:58'),
(250, 172, 84, 'test', '2018-05-03 01:58:25'),
(251, 172, 84, 'minn', '2018-05-03 01:58:31'),
(252, 172, 85, 'iya bagaimana', '2018-05-03 01:58:48'),
(253, 172, 85, 'apa yang bisa dibantu', '2018-05-03 01:59:23'),
(254, 172, 85, 'test test', '2018-05-03 02:01:19'),
(255, 172, 85, 'selamat sore', '2018-05-03 02:01:32'),
(256, 172, 85, 'pakk', '2018-05-03 02:01:38'),
(257, 172, 85, 'bagaimana', '2018-05-03 02:01:42'),
(258, 138, 85, 'pagi pak', '2018-05-03 20:25:01'),
(259, 138, 85, 'test', '2018-05-03 20:25:02'),
(260, 172, 84, 'tessttttt', '2018-05-03 20:25:37'),
(261, 172, 61, 'Iya pak', '2018-05-03 20:25:39'),
(262, 172, 61, 'Siang pak', '2018-05-03 20:26:42'),
(263, 138, 61, 'Test', '2018-05-03 20:26:53'),
(264, 138, 109, 'ada pak', '2018-05-03 20:27:11'),
(265, 138, 61, 'Test', '2018-05-03 20:27:30'),
(266, 138, 109, 'test', '2018-05-03 20:28:04'),
(267, 138, 61, 'Tesy', '2018-05-03 20:28:12'),
(268, 138, 109, 'siang pak', '2018-05-03 20:30:44'),
(269, 178, 150, 'hai', '2018-05-07 00:30:57'),
(270, 178, 150, 'admin', '2018-05-07 00:31:04'),
(271, 184, 156, 'saya membeli mobil bekas dan  sudah terpasang venom vpr 4.4 apa password defult wifinya?', '2018-05-09 04:40:26'),
(272, 128, 166, 'Test', '2018-05-11 20:46:45'),
(273, 128, 79, 'test', '2018-05-11 20:47:13'),
(274, 128, 166, 'Test 2', '2018-05-11 20:47:37'),
(275, 172, 84, 'dicoba', '2018-05-11 21:44:48'),
(276, 172, 166, 'Wokeh', '2018-05-11 21:44:58'),
(277, 197, 171, 'Min mhon pencerahan, untuk upgrade audio brio btuh produk venom apa aja ya.. Mkasih min', '2018-05-12 00:47:13'),
(278, 184, 166, 'selamat malam, mohon maaf atas keterlambatan respon dikarenakan adanya proses maintenance. ', '2018-05-14 04:02:06'),
(279, 184, 166, 'password wifi VPR 4.4 adalah : 87654321', '2018-05-14 04:02:40'),
(280, 199, 166, 'selamat malam ada yang bisa kami bantu ?', '2018-05-14 04:07:49'),
(281, 199, 173, 'mau liat2 pak bozz??', '2018-05-14 04:09:55'),
(282, 199, 173, 'baju kaos venom ada jual kak', '2018-05-14 04:10:12'),
(283, 199, 174, 'baju model yang mana pak ??', '2018-05-14 04:12:02'),
(284, 125, 174, 'masuk pak', '2018-05-14 04:12:30'),
(285, 135, 174, 'selamat malam pak, coming soon yah untuk CD lagu nya... ', '2018-05-14 04:13:36'),
(286, 128, 79, 'malam ', '2018-05-14 04:14:34'),
(287, 128, 79, 'bales woyy ', '2018-05-14 04:15:03'),
(288, 128, 79, 'orang tanya ga di jawab ', '2018-05-14 04:15:18'),
(289, 199, 173, 'kaos2 gtu ada jual pak', '2018-05-14 04:15:31'),
(290, 172, 84, 'crottt', '2018-05-14 04:15:41'),
(291, 199, 174, 'coming soon yah pak...', '2018-05-14 04:16:09'),
(292, 199, 173, 'oke pak....makaseeh????????', '2018-05-14 04:26:06'),
(293, 206, 181, 'mau tanya om .... Pandora 4.6 lagi kosong ya om ?.... kemarin saya ke Venom Fighther MJM Bekasi', '2018-05-22 07:52:30'),
(294, 206, 181, 'ada lagi kapan ya om ?... tx.Ady  Permana', '2018-05-22 07:54:08'),
(295, 206, 183, 'Siang mas.....iya 4.6 kita sedang kosong mas', '2018-05-22 20:38:13'),
(296, 206, 183, 'Sehabis Idul Fitri pak kita ada untuk stock nya', '2018-05-22 20:42:18'),
(297, 210, 186, 'pagi pak,  saya baru beli venom pandora VPR-3.4 + diablo baby VX 6 BD speaker + venom vx8.1A. sudah diinstal di bengkel.  hy masih belum pas sesuai keinginan saya.  Boleh gak kita download software untuk setting ampli nya sendiri?  atau hanya bengkel aja yg boleh ngesetting? coz saya coba download di venom dsb yang di playstore pake bahasa chinese.. ', '2018-05-26 14:43:49'),
(298, 210, 186, 'ngerubah bahasanya ke bahasa indonesia atw inggris ada gak pak? ', '2018-05-26 22:00:54'),
(299, 210, 186, 'thx', '2018-05-26 22:00:57'),
(300, 210, 110, 'siang pak', '2018-06-01 21:40:23'),
(301, 210, 110, 'untuk merubah bahasanya dari setingan hp pak', '2018-06-01 21:40:35'),
(302, 210, 186, 'Trimakasih infonya pak... Mohon bantuannya kembali ni pak, aplikasinya ud terhubung dgn amplinya, untuk setting volume bisa, namun ketika akan masuk ke setting equalizer n delay diminta password.. paswordnya apa ya pak? ', '2018-06-02 04:51:14'),
(303, 210, 183, 'passwordnya 666666', '2018-06-07 00:25:10'),
(304, 199, 173, 'allow', '2018-06-08 22:47:47'),
(305, 206, 181, 'ok terima kasih om', '2018-06-10 09:30:34'),
(306, 221, 197, 'baru install venom pandora 3.4. di vemom dsp setting EQ minta passwords..mohon info', '2018-06-12 06:23:32'),
(307, 227, 203, 'tolong kirim aplikasi ny dong', '2018-06-22 23:32:21'),
(308, 131, 183, 'test', '2018-06-29 21:18:37'),
(309, 234, 110, 'opo mid', '2018-07-16 05:01:49'),
(310, 234, 110, 'ping', '2018-07-16 06:38:18'),
(311, 234, 110, 'ping', '2018-07-16 06:38:18'),
(312, 166, 140, 'testt', '2018-07-28 08:22:41'),
(313, 235, 61, 'tes', '2018-07-30 13:21:00'),
(314, 168, 142, 'test', '2018-07-31 03:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `tb_news`
--

CREATE TABLE IF NOT EXISTS `tb_news` (
  `id_news` int(11) NOT NULL,
  `judul` varchar(80) NOT NULL,
  `isi` text NOT NULL,
  `status` set('publish','draft') NOT NULL DEFAULT 'draft',
  `image` varchar(200) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `baca` set('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_news`
--

INSERT INTO `tb_news` (`id_news`, `judul`, `isi`, `status`, `image`, `tanggal`, `baca`) VALUES
(1, 'KUNJUNGI BOOTH KAMI', '<p>KUNJUNGI BOOTH KAMI PADA &nbsp;IIMS 2018 DI JIEXPO KEMAYORAN 19 - 28 APRIL 2018</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'draft', 'http://103.82.242.153/venom/assets/upload/news/panr_1.png', '2018-08-05 01:04:01', 'yes'),
(2, 'Yuk buruan Register Dan Dapatkan Reward di Aplikasi Venom Indonesia', '<p>Silahkan register dahulu menggunakan login google kemudian ikuti 4 Cara untuk klaim reward &nbsp;:</p>\r\n\r\n<p>1. Klik menu reward :</p>\r\n\r\n<p><img alt="" src="http://venom.levanpedia.com/assets/reward/1.png" style="height:533px; width:300px" /></p>\r\n\r\n<p>2.&nbsp; Pilih Item</p>\r\n\r\n<p><img alt="" src="http://venom.levanpedia.com/assets/reward/2.png" style="height:533px; width:300px" /></p>\r\n\r\n<p>3. Klik Claim</p>\r\n\r\n<p><img alt="" src="http://venom.levanpedia.com/assets/reward/3.png" /></p>\r\n\r\n<p>4. Kode reward&nbsp;akan diisi oleh petugas dari SIXTYNINE</p>\r\n\r\n<p><img alt="" src="http://venom.levanpedia.com/assets/reward/4.png" /></p>\r\n\r\n<p>Terima kasih selamat bergabung</p>\r\n', 'publish', 'http://103.82.242.153/venom/assets/upload/news/sixtynine.png', '2018-08-05 01:03:56', 'yes'),
(3, 'Talkshow Terkini IIMS 2018', '<p>Talkshow dengan youtuber Ridwan Hanif dari Autonetmagz dan Jodiemotovlog HD</p>\r\n\r\n<p><img alt="" src="http://venom.levanpedia.com/assets/upload/news/2.png" style="height:300px; width:300px" /></p>\r\n', 'publish', 'http://103.82.242.153/venom/assets/upload/news/2.png', '2018-08-05 01:03:47', 'yes'),
(4, 'IIMS 2018 : Tiga Produk Baru Venom Bikin BerAudio Mobil Makin Mudah dan Murah', '<p style="text-align:justify">Jakarta, ModifiSexy.com &ndash; Menjadi pemegang brand Venom tidaklah mudah. Karena harus mengerti kebutuhan pasar, teknologi harus up to date, dan tetap berkulitas. Setidaknya itulah yang terus dila<a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i0.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-2.jpg?resize=800%2C514" style="float:left; height:193px; width:300px" /></a>kukan Venom Indonesia, selalu menghadirkan inovasi tiada henti. Singkat cerita, Venom tak pernah berhenti melahirkan dan melakukan regenerasi produk baru.&nbsp;</p>\r\n\r\n<p style="text-align:justify">Pesan moral yang selalu disampaikan pada produk Venom masa kini adalah BerAudio kini makin mudah dan murah. &ldquo;Jadi sekarang jangan berfikir pasang audio itu rumit dan mahal serta berhari hari, No!, sekarang teknologi sudah canggih pasang audio Venom tidak lebih dari 2 jam,&rdquo; ucap Irwan Kusuma, President Director PT. Sumber Sejahtera Audiotama.</p>\r\n\r\n<p style="text-align:justify">Nah, untuk momen Indonesia Internasional Motor Show (IIMS) 2018 kali ini, sekaligus bertepatan dengan hari Kartini 21 April. Venom meluncurkan tiga produk sekaligus, yakni Digital Signal Processor (DSP) Venom Pandora VPR-4.6MKII , DSP Baby Pandora, dan Subwoofer Ban Serep VX Series. Ketiga produk ini adalah plug n play yang pemasangannya mudah dan cepat.</p>\r\n\r\n<h4>Venom Pandora VPR-4.6MKII</h4>\r\n\r\n<p style="text-align:justify">Venom Pandora merupakan Processor <a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i0.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-4.jpg?resize=800%2C514" style="float:left; height:193px; width:300px" /></a>audio mobil yang sudah terintegrasi power amplifier. Pandora adalah produk plug n play yang waktu pemasangan nya cepat dan tanpa merusak garansi mobil namun kualitas suara speaker naik signifikan. Pandora juga bisa diatur sesuai selera dengan smartphone.</p>\r\n\r\n<p style="text-align:justify">&ldquo;Venom Pandora VPR 4.6 MK2 softwarenya terbaru dan sudah kita register sendiri, jadi tidak ada software seperti kami. IC nya sudah terbaru, amplifier wattnya dua kali lipat lebih besar. Bluetooth nya sudah hi-res, jadi setel musik dari smartphone tinggal pencet saja lewat aplikasi Pandora, nggak usah pakai CD lagi. Sehingga kekuatan dan kualitas suara jauh lebih baik,&rdquo; terang Abay, panggilan akrab Irwan Kusuma.</p>\r\n\r\n<p style="text-align:justify">Venom Pandora VPR-4.6MKII memiliki respon frekuensi 10 Hz &ndash; 18 KHz serta dimensi lebar 122 mm x panjang 200 mm x tinggi 41 mm. Output power tersedia tiga varian 40 ohm, 20 ohm, dan 10 ohm. Aplikasi Pandora bisa di download lewat Android atau Apple store. &ldquo;Produk ini baru akan tersedia dipasaran setelah lebaran atau paling lambat bulan 7,&rdquo; kata Abay.</p>\r\n\r\n<p style="text-align:justify">Venom Baby Pandora VPR-1B</p>\r\n\r\n<p style="text-align:justify">Venom Baby Pandora <a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i0.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-5.jpg?resize=800%2C514" style="float:left; height:193px; width:300px" /></a>VPR-1B merupakan Processor audio yang dikeluarkan untuk segmen menengah dengan harga yang terjangkau. &ldquo;Sekarang market audio ada yang hi-end dan middle, nah kelas middle ini yang banyak sekali, hanya saja untuk konsumsi pasar ini tidak ada Processor murah, maka dari itu kita hadirkan Babby Pandora,&rdquo; ujar Abay.</p>\r\n\r\n<p style="text-align:justify">Processor Venom Pandora VPR-1B dikemas dengan dimensi 173,5 x 114 x 41 mm dengan respon frekuensi direntang 10 Hz &ndash; 20 KHz.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">Sabwoofer Ban Serep Venom VX15.1 dan VX15.1A</p>\r\n\r\n<p style="text-align:justify">Untuk memanfaatkan ruang bagasi agar tetep luas biasanya installer audio memanfaatkan ruang ban serep untuk dibenamkan subwoofer. Sejauh ini instalasinya bersifat custom dan paten jadi tidak bisa dibongkar pasang. &ldquo;Zaman dulu itu bikin boks dengan waktu yang sangat lama dan <a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i2.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-3.jpg?resize=800%2C514" style="float:right; height:193px; width:300px" /></a>biaya yang cukup tinggi, sekarang lebih mudah tinggal pasang ini di ban serep lalu tutup dan bass itu akan keluar secara baik dan rendah,&rdquo; ucap Abay.</p>\r\n\r\n<p style="text-align:justify">Subwoofer aktif ini punya dua seri yang satu VX15.1 dan VX15.1A apa perbedaan nya. VX 15.1 tidak mempunyai amplifier jadi ini bisa di kombinasi dengan Pandora 4.6 MK2 yang sudah ada soubwoofer output. Sementara VX 15.1A sudah dilengkapi dengan power amplifier. &ldquo;Pemasangan untuk subwoofer ini paling lama sekitar 1 jam, tinggal tarik kabel dari depan, sementara Pandora sekitar 30 menit, kalau installer nya jago lebih dari itu,&rdquo; jelas Abay.</p>\r\n\r\n<p style="text-align:justify">Abay menambahkan, &ldquo;kami tetap memikirkan safety, kalau suatu saat tetap mau pasang ban serep. Subwoofer ini hanya tinggal buka bautnya, angkat selesai, sekitar 1 menitan,&rdquo;.</p>\r\n\r\n<p style="text-align:justify">Keduanya memiliki spesifikasi sama, impedance 2.0 ohm + 2.0 ohm (dual speaker), respons frekuensi 30 &ndash; 200 Hz, speaker and RCA level inputs, peak power 800 watts dan daya arus 14.4 Volt DC serta mampu menyemburkan suara bass hingga 92 dB. Subwoofer ini dibanderol berkisar Rp 4,8 dan 5,8 jutaan.</p>\r\n\r\n<h4>Talk Show Bersama YouTubers</h4>\r\n\r\n<p style="text-align:justify">Saat press conference bersama media, yang terselenggara di PAHAMI Village, IIMS 2018, Venom juga menggelar sesi talk show bersama YouTubers otomotif Ridwan Ha<a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i1.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-6.jpg?resize=800%2C461" style="float:right; height:173px; width:300px" /></a>nif dan vlogger Jodie Motovlog yang telah mengaplikasi perangkat audio Venom. Mereka berdua sepakat kalau Venom memiliki kualitas produk dan suara yang bagus.</p>\r\n\r\n<p><a href="http://www.modifisexy.com/tiga-produk-baru-venom-iims-2018/#0"><img alt="" src="https://i0.wp.com/www.modifisexy.com/wp-content/uploads/2018/04/PRODUK-VENOM-IIMS-2018-7.jpg?resize=800%2C514" style="float:left; height:193px; width:300px" /></a></p>\r\n', 'draft', 'http://103.82.242.153/venom/assets/upload/news/PRODUK-VENOM-IIMS-2018-1.png', '2018-08-05 01:03:41', 'yes'),
(5, 'Bersiap, Sejumlah Perangkat Anyar Venom Segera Meluncur di Pasar Car Audio Tanah', '<p style="text-align:justify"><strong>Amoplus Magazine &ndash;&nbsp;</strong>Sebagai salah satu brand audio mobil kenamaan, Venom kembali menunjukan eksistensinya dengan ikut serta pada salah satu event otomotif bergengsi di tanah air, Indonesia International Motor Show (IIMS) 2018. Pada ajang yang bertempat di JIExpo, Kemayoran, Jakarta Pusat, 19-29 April 2018 tersebut Venom Menempati Hall B, berada dalam area PAHAMI Village.</p>\r\n\r\n<p style="text-align:justify"><a href="https://www.amoplusmagz.com/bersiap-sejumlah-perangkat-anyar-venom/"><img alt="" src="https://www.amoplusmagz.com/wp-content/uploads/2018/04/Presentasi-Produk-Terbaru-Venom-oleh-Irwan-Kusuma-CEO-VENOM-1024x683.jpg" style="float:left; height:200px; width:300px" /></a>Tahun ini Venom kembali hadir dengan konsep booth yang mewah. Dalam area booth tersedia display demo car yang salah satunya merupakan Mitsubishi Xpander. Mobil ini telah diupgrade audionya menggunakan processor Venom Pandora VPR 3.4, subwoofer aktif VX 6.8 PB, dan tweeter Vin 2. Jadi secara suara tentunya ia memiliki kualitas yang jauh lebih mumpuni. Sebagai informasi tambahan, Paket upgrade audio seperti yang diaplikasikan pada Mitsubishi Xpander ini bisa juga digunakan untuk semua kendaraan Jepang (Nissan, Honda, Mitsubishi, Suzuki, Toyota) dan China (Wuling).</p>\r\n\r\n<p style="text-align:justify">Moment IIMS 2018 dimanfaatkan Venom untuk memperkenalkan sejumlah produk terbarunya yang bersiap meluncur di 2018. Produk-produk tersebut diantaranya adalah processor Venom Pandora VPR 4.6 MK2, processor Venom Pandora VPR 1 B, subwoofer pasif&nbsp; Venom VX 15.1, dan subwoofer aktif&nbsp; Venom VX 15.1A.</p>\r\n\r\n<p style="text-align:justify">Venom Pandora VPR 4.6 MK2 merupakan pembaharuan dari seri sebelumnya yang telah terlebih dahulu melejit di pasar car audio tanah air. Pada seri terbaru ini sejumlah fitur terbaru turut ditambahkan, seperti diantaranya adalah koneksi Bluetooth (untuk pemutaran lagu via smartphone), sudah mengadopsi software original Venom (untuk melakukan pengaturan suara), serta improvement IC yang lebih baik.</p>\r\n\r\n<p style="text-align:justify">Produk kedua yang diperkenalkan adalah processor Venom Pandora VPR 1 B. Produk ini lebih ditujukan untuk segmentasi market kelas ekonomis, karenanya harganya pun terbilang sangat terjangkau. Dimensinya tak terlalu besar, sehingga mudah ditempatkan di area manapun dalam mobil. Meski begitu dengan fitur yang dimiliki ia mampu merubah tatanan audio pada mobil menjadi lebih baik.</p>\r\n\r\n<p style="text-align:justify">Selanjutnya adalah subwoofer pasif&nbsp; Venom VX 15.1 dan subwoofer aktif&nbsp; Venom 15.1A. Keduanya memiliki spesifikasi sama, yang membedakan hanyalah pasif dan aktifnya saja. Jika Venom 15.1A (aktif) ia didalamnya sudah terintegrasi dengan amplifier, jadi tidak perlu amplifier eksternal lagi untuk membunyikannya. Sementara untuk Venom VX 15.1 (pasif) masih memerlukan amplifier eksternal untuk membuatnya berbunyi. Karena keduanya memiliki spesifikasi yang sama, jadi bisa dipilih sesuai dengan kebutuhan saja.</p>\r\n\r\n<p style="text-align:justify"><a href="https://www.amoplusmagz.com/bersiap-sejumlah-perangkat-anyar-venom/"><img alt="" src="https://www.amoplusmagz.com/wp-content/uploads/2018/04/Ridwan-dan-Jodie-sharing-pengalaman-tentang-produk-venom-1024x683.jpg" style="float:left; height:200px; width:300px" /></a>Desain subwoofer buatan Venom ini terbilang inovatif, dimana kotak reproduksi suara dikemas mirip bentuk driver sub-nya itu sendiri, sehingga sepintas orang akan mengira bahwa subwoofer ini seolah telanjang tanpa kotak. Dengan bentuknya yang sangat kompak, tentunya juga memungkinkan posisi penempatan fleksibel. Kotak resonansinya dibentuk oleh konstruksi aluminium cor padat yang memiliki bobot tidak terlalu berat namun kokoh, dimana saluran udara tersalur secara omni directional yang memungkinkan efek bas rendah merata ke segala arah, dan untuk nafas driver juga terdapat port udara terpisah di bagian belakang driver. Bagi yang ingin upgrade sistem audio mobil secara instan, cara pasang subwoofer Venom VX15.1 atau VX15.1A tentunya menjadi pilihan yang patut dipertimbangkan. Selain kemudahan instalasi, biaya yang dikeluarkan pun tidak sampai menguras kocek.</p>\r\n\r\n<p style="text-align:justify">Selain memperkenalkan produk baru, pada IIMS 2018 ini Venom juga turut menawarkan sejumlah program menarik selama pameran berlangsung. Untuk pembelian paket Venom senilai Rp 5 &ndash; 8 juta akan mendapatkan free instalasi audio, e-money serta kaos bola. Pengunjung yang berbelanja diatas Rp 9 juta berhak mendapatkan Aria Snorkeling Mask dan gratis jasa instalasi audio ditambah kaos bola. Pengunjung bisa meraih discount tambahan 5% setiap pembelian melalui aplikasi Venom.</p>\r\n\r\n<p style="text-align:justify"><a href="https://www.amoplusmagz.com/bersiap-sejumlah-perangkat-anyar-venom/"><img alt="" src="https://www.amoplusmagz.com/wp-content/uploads/2018/04/Ridwan-Hanif-1024x683.jpg" style="height:200px; width:300px" /></a></p>\r\n\r\n<p style="text-align:justify">Saat press conference, Venom mengadakan Talk Show bersama Ridwan Hanif, perwakilan dari media dan Jodie Motovlog yang telah mengaplikasi perangkat audio Venom di mobilnya masing-masing. Menurut keduanya, aplikasi audio Venom terbilang gampang dan mudah, tidak makan tempat dan tatanan suara audio mobil lebih jernih ketimbang versi standarnya.</p>\r\n\r\n<p style="text-align:justify"><a href="https://www.amoplusmagz.com/bersiap-sejumlah-perangkat-anyar-venom/"><img alt="" src="https://www.amoplusmagz.com/wp-content/uploads/2018/04/Jodiemotovlog-1024x683.jpg" style="height:200px; width:300px" /></a></p>\r\n\r\n<p style="text-align:justify">Bagi Anda penikmat sekaligus pecinta audio yang penasaran terhadap produk terbaru Venom, silakan kunjungi pameran IIMS 2018 yang berlokasi di Jakarta International Expo (JIExpo), Kemayoran, Jakarta Pusat, hingga 29 April 2018 mendatang. Dapatkan promo menarik hanya selama pameran dan dapatkan edukasi seputar audio dalam booth Venom.</p>\r\n\r\n<p style="text-align:justify"><a href="https://www.amoplusmagz.com/bersiap-sejumlah-perangkat-anyar-venom/"><img alt="" src="https://www.amoplusmagz.com/wp-content/uploads/2018/04/Special-Thanks-for-Guest-Star-1024x683.jpg" style="height:200px; width:300px" /></a></p>\r\n', 'draft', 'http://103.82.242.153/venom/assets/upload/news/Ridwan-dan-Jodie-sharing-pengalaman-tentang-produk-venom-640x300.png', '2018-08-05 01:03:35', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE IF NOT EXISTS `tb_order` (
  `id_order` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `kode_invoice` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id_order`, `id_barang`, `jumlah`, `kode_invoice`) VALUES
(1, 48, 2, 100001),
(2, 41, 2, 100001),
(3, 83, 1, 100002),
(4, 102, 1, 100003),
(5, 83, 6, 100004),
(6, 88, 3, 100005),
(7, 87, 1, 100006),
(8, 87, 1, 100007),
(9, 87, 1, 100008),
(10, 17, 1, 100009),
(11, 11, 2, 100010),
(12, 25, 1, 100010),
(13, 18, 1, 100011),
(14, 18, 1, 100012),
(15, 11, 1, 100013),
(16, 58, 4, 100015),
(17, 85, 2, 100015),
(18, 48, 2, 100015),
(19, 42, 2, 100015),
(20, 18, 1, 100016),
(21, 20, 1, 100016),
(22, 88, 1, 100017),
(23, 17, 1, 100018),
(24, 18, 1, 100019),
(25, 18, 1, 100020),
(26, 11, 1, 100021),
(27, 6, 1, 100022),
(28, 3, 1, 100023),
(29, 7, 2, 100024),
(30, 7, 4, 100025),
(31, 7, 1, 100026),
(32, 7, 2, 100028),
(33, 6, 4, 100028),
(34, 7, 2, 100029),
(35, 6, 4, 100029),
(36, 6, 4, 100030),
(37, 7, 2, 100030),
(38, 7, 2, 100031),
(39, 6, 4, 100031),
(40, 6, 4, 100032),
(41, 7, 2, 100032),
(42, 6, 4, 100033),
(43, 7, 2, 100033),
(44, 7, 2, 100034),
(45, 6, 4, 100034),
(46, 7, 2, 100035),
(47, 6, 4, 100035),
(48, 6, 4, 100036),
(49, 7, 2, 100036),
(50, 7, 2, 100041),
(51, 6, 4, 100041),
(52, 7, 2, 100043),
(53, 6, 4, 100043),
(54, 7, 2, 100045),
(55, 6, 4, 100045),
(56, 6, 4, 100047),
(57, 7, 2, 100047),
(58, 6, 4, 100049),
(59, 7, 2, 100049),
(60, 7, 2, 100056),
(61, 6, 4, 100056),
(62, 94, 2, 100057),
(63, 20, 5, 100058),
(64, 88, 2, 100059),
(65, 17, 1, 100059),
(66, 17, 1, 100060),
(67, 17, 2, 100061),
(68, 11, 2, 100062),
(69, 111, 1, 100063),
(70, 61, 2, 100063),
(71, 38, 1, 100063),
(72, 11, 2, 100063),
(73, 17, 1, 100064),
(74, 17, 2, 100065),
(75, 11, 1, 100065),
(76, 17, 2, 100066),
(77, 11, 1, 100066),
(78, 17, 2, 100067),
(79, 11, 1, 100067),
(80, 17, 2, 100068),
(81, 11, 1, 100068),
(82, 17, 2, 100069),
(83, 11, 1, 100069),
(84, 20, 2, 100071),
(85, 15, 1, 100071),
(86, 59, 1, 100072),
(87, 22, 1, 100072),
(88, 17, 1, 100073),
(89, 19, 1, 100076),
(90, 3, 1, 100076),
(91, 17, 1, 100077),
(92, 11, 2, 100078),
(93, 19, 1, 100079),
(94, 6, 1, 100080),
(95, 19, 1, 100080),
(96, 112, 1, 100081),
(97, 112, 1, 100082),
(98, 112, 1, 100083),
(99, 112, 1, 100084),
(100, 112, 2, 100085),
(101, 15, 1, 100086),
(102, 22, 1, 100086),
(103, 163, 1, 100087),
(104, 163, 1, 100088);

-- --------------------------------------------------------

--
-- Table structure for table `tb_otp`
--

CREATE TABLE IF NOT EXISTS `tb_otp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `kode` int(5) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_otp`
--

INSERT INTO `tb_otp` (`id`, `user_id`, `kode`, `create_on`) VALUES
(2, 61, 9345, '2018-07-27 08:05:00'),
(3, 61, 4460, '2018-07-27 08:18:39'),
(4, 61, 6526, '2018-07-27 08:24:06'),
(5, 61, 2872, '2018-07-27 08:31:19'),
(6, 61, 7279, '2018-07-27 08:34:20'),
(7, 61, 4421, '2018-07-27 08:41:02'),
(8, 61, 9646, '2018-07-27 13:04:13'),
(9, 61, 792, '2018-07-28 03:26:34'),
(10, 61, 2249, '2018-07-28 03:27:29'),
(11, 61, 4105, '2018-07-28 04:12:55'),
(12, 140, 1077, '2018-07-28 08:23:30'),
(13, 61, 7097, '2018-07-29 05:54:32'),
(14, 61, 3646, '2018-07-30 04:31:39'),
(15, 17, 5460, '2018-07-30 04:37:44'),
(16, 61, 5252, '2018-07-30 06:32:12'),
(17, 61, 6051, '2018-07-30 13:21:27'),
(18, 17, 3554, '2018-07-30 13:21:38'),
(19, 17, 7028, '2018-07-30 13:33:47'),
(20, 61, 4068, '2018-07-30 14:05:54'),
(21, 61, 4000, '2018-08-05 09:20:04'),
(22, 17, 224, '2018-08-05 09:28:20'),
(23, 17, 709, '2018-08-05 09:38:52'),
(24, 17, 7528, '2018-08-05 09:41:57'),
(25, 61, 2659, '2018-08-06 10:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian`
--

CREATE TABLE IF NOT EXISTS `tb_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jml` int(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  `keterangan` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembelian`
--

INSERT INTO `tb_pembelian` (`id_pembelian`, `produk_id`, `jml`, `user_id`, `keterangan`, `tanggal`) VALUES
(1, 60, 6, 17, 'baru', '2018-07-18 23:54:34'),
(2, 3, 5, 17, '', '2018-07-19 00:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesan`
--

CREATE TABLE IF NOT EXISTS `tb_pesan` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `baca` set('yes','no') DEFAULT 'no',
  `categori` set('single','multi') DEFAULT 'multi',
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_point`
--

CREATE TABLE IF NOT EXISTS `tb_point` (
  `id` int(11) NOT NULL,
  `point` int(15) DEFAULT NULL,
  `harga` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_point`
--

INSERT INTO `tb_point` (`id`, `point`, `harga`) VALUES
(0, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_present`
--

CREATE TABLE IF NOT EXISTS `tb_present` (
  `id_present` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_present`
--

INSERT INTO `tb_present` (`id_present`, `user_id`, `event_id`, `name`, `create_on`) VALUES
(1, 1, 1, 'hadir', '2018-04-11 05:16:22'),
(2, 1, 1, 'hadir', '2018-04-11 05:16:29'),
(3, 1, 0, 'kjdkjd', '2018-04-11 05:30:55'),
(4, 35, 0, 'hadir', '2018-04-11 05:34:44'),
(5, 35, 1, 'hadir', '2018-04-11 05:57:50'),
(6, 35, 1, 'hadir', '2018-04-11 22:55:51'),
(7, 35, 1, 'hadir', '2018-04-12 02:11:25'),
(8, 27, 1, 'hadir', '2018-04-13 21:56:41'),
(9, 38, 1, 'hadir', '2018-04-14 04:34:53'),
(10, 38, 1, 'hadir', '2018-04-14 04:35:42'),
(11, 38, 2, 'hadir', '2018-04-14 04:46:50'),
(12, 63, 1, 'hadir', '2018-04-17 22:12:45'),
(13, 63, 1, 'hadir', '2018-04-17 22:18:29'),
(14, 83, 1, 'hadir', '2018-04-20 07:49:12'),
(15, 81, 2, 'hadir', '2018-04-21 04:50:15'),
(16, 69, 3, 'hadir', '2018-04-22 19:30:53'),
(17, 1, 1, 'hadir', '2018-05-02 02:14:39'),
(18, 1, 1, 'hadir', '2018-05-08 03:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE IF NOT EXISTS `tb_produk` (
  `id_produk` int(2) NOT NULL,
  `judul` varchar(220) NOT NULL,
  `harga` int(20) DEFAULT NULL,
  `point` int(15) NOT NULL DEFAULT '0',
  `jumlah` varchar(50) NOT NULL,
  `id_merk` int(2) NOT NULL,
  `id_kat` int(2) NOT NULL,
  `url` text,
  `ket` text NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `counter` text,
  `tgl_input_pro` datetime NOT NULL,
  `foto` varchar(100) NOT NULL,
  `id_subkat` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `judul`, `harga`, `point`, `jumlah`, `id_merk`, `id_kat`, `url`, `ket`, `status`, `counter`, `tgl_input_pro`, `foto`, `id_subkat`) VALUES
(3, 'Venom X SERIES V480XII Amplifier [4 Channel] ', 2127500, 0, '10', 1, 13, NULL, '<p>Venom X SERIES V480XII Amplifier [4 Channel] merupakan amplifier yang dirancang dengan 4 Channel. Ideall untuk memberikan sound mendetail pada kabin mobil Anda.</p>\r\n\r\n<p><strong>Detail</strong></p>\r\n\r\n<ul>\r\n	<li>Amplifier chanel Venom X 5 memberikan banyak kekuatan untuk sistem hiburan mobil.</li>\r\n	<li>Demension : 38.4 x 23.8 x 5 cm</li>\r\n	<li>Output Power in 4 ohm : 4 x 50W</li>\r\n	<li>Output Power in 2 ohm : 4 x 110W</li>\r\n	<li>Type : 4 Channel Crossover : HP/LP/FULL</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-12 02:30:09', 'V480X2.png V480X2.png ', 3),
(6, 'Venom VT 12 Vertigo Series Subwoofer Pasif ', 1445500, 0, '10', 1, 2, NULL, '<p>Venom VT 12 Vertigo Series Subwoofer Pasif merupakan produk terbaru dari subwoofer audio mobil dengan performa terbaik untuk menghadirkan suara Bass 40 Hz. Didesain elegan dengan ukuran 12 Inch, dan frekuensi respons sebesar 25- 1500 Hz. Daya maksimum power 400 Watt RMS, impedance sebesar 4 Ohm, dan sensity sebesar 86 dB. Subwoofer ini sangat cocok untuk pengguna audio mobil harian.</p>\r\n\r\n<p><strong>Spesifikasi :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 12 Inch</li>\r\n	<li>Resistance : 4 ohm</li>\r\n	<li>Frequency Response : 25 - 1500HZ</li>\r\n	<li>Sensitivity : 86 db/W/M</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-12 07:54:23', 'VT12.png VT12.png ', 2),
(7, 'Venom Illuminator Series VL1000.1D ', 3863500, 0, '10', 1, 3, NULL, '<p>Venom Illuminator Series VL1000.1D Power Monoblock merupakan power monoblock yang memiliki output power in 4 ohm 4 x 250W, output power in 2 ohm 1 x 500W serta output power in 1 ohm 1 x 1000W. Sudah sangat tidak diragukan lagi bagi para pecinta musik yang sedang mencari power monoblock dengan kualitas sinyal suaranya yang akan menunjukkan kehebatan suara pada bass.<br />\r\n<br />\r\n<strong>Spesifikasi :</strong></p>\r\n\r\n<ul>\r\n	<li>Demension : 360 x 54 x 231 mm</li>\r\n	<li>Output Power in 4 ohm : 1 x 250 W</li>\r\n	<li>Output Power in 2 ohm : 1 x 500 W</li>\r\n	<li>Output Power in 1 ohm : 1 x 1000 W</li>\r\n	<li>Type : Monoblock</li>\r\n	<li>Frequensi Response : 0Hz - 150Hz</li>\r\n	<li>Low Pass Cross Freq : 40Hz - 150Hz</li>\r\n	<li>Bass EQ : 0-12 dB @45Hz</li>\r\n</ul>\r\n\r\n<p><strong>Isi Dalam Paket :</strong></p>\r\n\r\n<ul>\r\n	<li>Venom Illuminator Series VL1000.1D - Power Monoblock</li>\r\n	<li>Manual Book</li>\r\n	<li>Kartu Garansi</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-12 07:56:46', 'VL70_4.png VL70_4.png ', 3),
(9, 'Venom Baby Diablo series VO400.1 BD ', 2127500, 0, '10', 1, 3, NULL, '<p>Venom Baby Diablo Series VO 400.1BD Monoblock Amplifier merupakan monoblock amplifier yang difungsikan untuk memberikan daya lebih pada semua perangkat audio mobil terutama speaker dan subwoofer. Monoblock amplifier ini memiliki frekuensi respon 10 Hz-250 Hz, bassEQ 0 dB-12 dB @ 45 Hz, Comments fuse size 2 x 25 A dan speaker impedance 1-16 ohm. Dengan output power in 4 ohm : 300 W x1, output power in 2 ohm : 450 W x1, output power in 1 ohm : 500 W x1.</p>\r\n', 'publish', '0', '2018-03-20 22:46:38', 'DSC_7511_1.png ', 3),
(11, 'Venom Baby Diablo Series VX12BD  Subwoofer [12 Inch]  ', 1154500, 0, '10', 1, 2, NULL, '<p>Venom Baby Diablo Series VX12BD SVC Subwoofer mobil [10 Inch] merupakan subwoofer mobil berukuran 10 Inch yang dirancang dengan frequency response 25 - 500 Hz&nbsp;serta power handling 200 W. Ideal digunakan untuk meningkatkan kualitas audio pada mobil Anda.</p>\r\n\r\n<p><strong>Specification :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 12 inch</li>\r\n	<li>Resistance : 4 ohm</li>\r\n	<li>Frequency Response : 25 - 500 Hz</li>\r\n	<li>Power Handling : 200 W</li>\r\n	<li>Sensitivity : 88 dB</li>\r\n	<li>Comments : Magnet: 38 Oz</li>\r\n	<li>Voice Coil: 50mm ASV</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-20 23:08:54', 'vbd.png ', 2),
(15, 'PANDORA VPR 3.4', 2764000, 0, '10', 1, 4, NULL, '<p style="text-align:justify">Venom Pandora VPR 3.4 Processor Audio Mobil adalah processor dan amplifier audio mobil yang menghadirkan kualitas suara yang jernih. Pandora VPR 3.4&nbsp; ini sangat cocok buat Anda yang ingin mempunyai&nbsp; audio simple untuk penggunaan sehari-hari di mobil anda, Pandora VPR 3.4 ini menghadirkan untuk kamu yang tidak ingin ribet, ingin Cepat, Praktis, dan Aman. Pandora VPR 3.4 ini sangat mudah pemasangannya hanya 30 menit pemasangan, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.</p>\r\n\r\n<p><strong>Tipe Mobil Yang Dapat Pasangkan Paket ini :</strong></p>\r\n\r\n<ul>\r\n	<li>Nissan : Evalia, Grand Livina, Juke, March, Serena, Teana, X-Trail</li>\r\n	<li>Toyota : Alphard, Altis, Avanza, Camry, Fortuner, Innova, Etios, Agya, Rush, Vios, Yaris</li>\r\n	<li>Honda : Accord, Brio, City, Civic, CRV, HRV, BRV, Jazz, Mobiloio, Freed, CRZ</li>\r\n	<li>Mitsubishi : Mirage, Outlander, dan Pajero</li>\r\n	<li>Suzuki : APV, Ertiga, Karimun, Splash, Swift, Grand Vitara</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-26 01:31:49', 'pandora_3_4.png pandora_3_4.png', 4),
(16, 'PANDORA VPR 4.6', 4445500, 0, '10', 1, 4, NULL, '<p style="text-align:justify">Venom Pandora VPR 4.6 Amplifier merupakan processor dan amplifier audio mobil yang menghadirkan kualitas suara yang jernih dan berkualitas tinggi. Pandora VPR 4.6 ini sangat cocok buat Anda yang ingin mempunyai&nbsp; audio simple dan tetap memiliki suara yang power full untuk penggunaan sehari-hari di mobil anda, Pandora VPR 4.6 ini menghadirkan untuk kamu yang tidak ingin ribet, ingin Cepat, Praktis, dan Aman. Pandora VPR 4.6 ini sangat mudah pemasangannya hanya 30 menit pemasangan, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Tersedia layanan pasang di rumah.<br />\r\n<br />\r\n<strong>Tipe Mobil Yang Dapat Pasangkan Paket ini :</strong></p>\r\n\r\n<ul>\r\n	<li>Nissan : Evalia, Grand Livina, Juke, March, Serena, Teana, X-Trail</li>\r\n	<li>Toyota : Alphard, Altis, Avanza, Camry, Fortuner, Innova, Etios, Agya, Rush, Vios, Yaris</li>\r\n	<li>Honda : Accord, Brio, City, Civic, CRV, HRV, BRV, Jazz, Mobiloio, Freed, CRZ</li>\r\n	<li>Mitsubishi : Mirage, Outlander, dan Pajero</li>\r\n	<li>Suzuki : APV, Ertiga, Karimun, Splash, Swift, Grand Vitara</li>\r\n</ul>\r\n', 'publish', '0', '2018-03-26 01:35:04', 'vpr-4_6.png vpr-4_61.png ', 4),
(17, 'Venom X Series VX6XII Component Speaker Mobil [6 Inch]', 1536500, 0, '10', 1, 1, NULL, '<p>Venom X Series VX6XII Component Speaker Mobil [6 Inch] merupakan speaker mobil ni terdiri dari tweeter 28mm flush-mount, woofer dan crossover terpisah. Bagian berkualitas tinggi digunakan di seluruh pengemudi. Permukaannya terbentuk dari kehilangan berat namun karet ringan untuk memperlancar respons frekuensi dan menjaga sensitivitas setinggi mungkin. Lingkaran dilekatkan pada cetakan injeksi, mineral berisi kerucut polipropilena. Hal ini membuat kerucut kaku dengan redaman internal yang baik. Struktur motor mesin membantu menjaga toleransi tetap ketat sambil memberikan kekuatan magnet energi tinggi yang dibutuhkan untuk tuntutan produksi saat ini yang tinggi. Perada mengarah melewati laba-laba untuk menghilangkan potensi dengungan dan meningkatkan daya tahan.</p>\r\n\r\n<p><strong>Specification</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 6 Inch 2 way</li>\r\n	<li>Frequency Response : 65 - 20 kHz</li>\r\n	<li>Power Handling : 60 W</li>\r\n	<li>Sensitivity : 88 dB</li>\r\n	<li>Comments :<br />\r\n	- Magnet : 13 oz<br />\r\n	- Voice Coil : 30mm<br />\r\n	- TSV Woofer : Fiberglass + paper composite pots</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 12:16:05', 'VX6XII-1.png ', 1),
(19, 'Venom Intelligent Series VI6S 2 Way Component LTD Speaker [6 Inch]', 7127300, 0, '10', 1, 1, NULL, '<p>Venom Intelligent Series VI6S 2 Way Component LTD Speaker [6 Inch] merupakan loudspeaker otomatis yang menampilkan beberapa teknologi terbaik Venom. Kerucut woofernya sangat kaku, namun ringan untuk meningkatkan efisiensi, kejernihan midrange yang luar biasa dan distorsi yang lebih rendah pada tingkat output tinggi. Kumparan suara kebesarannya meningkatkan penanganan daya sambil meminimalkan distorsi. Keandalan laba-labanya dan menghilangkan suara tak diinginkan. Woofer kerucut dengan luas permukaan lebih dari model bersaing dengan ukuran yang sama, menawarkan peningkatan output bass dan peningkatan efisiensi. Crossover pasif yang dioptimalkan untuk komputer telah disesuaikan dengan suara untuk memastikan pencitraan, kejernihan dan detail optimal. Tweeter kubah tekstil berbahan lebar yang berbahan dasar pita tepi tepi menghasilkan distorsi yang rendah pada tingkat output tinggi.</p>\r\n\r\n<p><strong>Specification :</strong></p>\r\n\r\n<ul>\r\n	<li>Frequency Response : 50 - 28 kHz +/- 3 dB</li>\r\n	<li>Sensitivity : 93 dB / 0.5 m / 2.83 V</li>\r\n	<li>Comments : Continuous</li>\r\n	<li>Power : 80 W</li>\r\n	<li>Ultimate Power Handling : 160 W</li>\r\n	<li>Nominal Impedance : 4 Ohm</li>\r\n	<li>Crossover : 3 kHz</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 12:49:27', 'VI_6_S.png ', 1),
(21, 'Venom Vertigo Series VT 65B Component 2 Way Speaker Mobil [6 Inch] ', 1218182, 0, '10', 1, 1, NULL, '<p>Venom Vertigo Series VT 65B Component 2 Way Speaker Mobil [6 Inch], sebuah produk stylish dengan desain minimalis modern yang akan menyenangkan telinga Anda dengan kualitas suara prima, bentuk kompak dan pemasangan yang sederhana. Untuk pengalaman perjalanan yang paling menyenangkan, biarkan seri Venom Vertigo menjadi teman Anda.</p>\r\n', 'publish', NULL, '2018-04-08 13:00:03', 'venom_venom-vertigo-series-vt-65b-component-6-inch-2-way-speaker_full02.jpg ', 1),
(22, 'Venom Intelligent Series VI3M Midrange Speaker Mobil ', 1927500, 0, '10', 1, 5, NULL, '<p>Venom Intelligent Series VI3M Midrange Speaker Mobil, kerucut woofernya ringan untuk meningkatkan efisiensi, kejernihan midrange yang luar biasa dan distorsi yang lebih rendah pada tingkat output tinggi. Kumparan suara kebesarannya meningkatkan penanganan daya sambil meminimalkan distorsi. Dapat meredam suara fals pada audio. Woofer kerucut dengan luas permukaan luas, output bass dan peningkatan efisiensi. Crossover pasif yang dioptimalkan untuk komputer telah disesuaikan dengan suara untuk memastikan pencitraan, kejernihan dan detail optimal. Dan tweeter kubah tekstil berbahan lebar yang berbahan dasar pita tepi tepi menghasilkan distorsi yang rendah pada tingkat output tinggi.</p>\r\n\r\n<p><strong>Specification :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 3 inch</li>\r\n	<li>Resistance : 4 Ohm</li>\r\n	<li>Frequency Response : 100Hz - 20KHz</li>\r\n	<li>Sensitivity : 87 + 2 db</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 13:48:05', 'venom_venom-intelligent-series-vi3m-midrange-speaker_full0210.jpg ', 5),
(24, 'Venom VX6012 Subwoofer ', 5963500, 0, '10', 1, 2, NULL, '<p>Venom VX6012 Subwoofer Pasif adalah subwoofer pasif yang dapat menghasilkan Low Frequency Response sebesar 40Hz hingga 700Hz. Subwoofer dilengkapi dengan penguat amplifier pada bagian dalam.</p>\r\n', 'publish', NULL, '2018-04-08 14:16:51', 'DSC_3659.png DSC_3663.png DSC_3666.png ', 2),
(25, 'Venom DBX 10.1  VEM-00054-01032', 2150000, 0, '10', 1, 2, NULL, '<p style="text-align:justify">Venom DBX 10.1 Subwoofer Speaker merupakan subwoofer yang dapat membuat emosi Anda bergetar, yang menawarkan suara extra bass yang mendalam, Powerfull dan Dinamis serta jelas dan tidak ada duanya. Dengan ketukan akurat dan tepat. Subwoofer ini dapat memuaskan telinga Anda dengan suara tajam dan jelas menonjol untuk suara bass. Subwoofer Pasif DBX 10.1 menghasilkan suara bass yang powerfull dan dinamis.</p>\r\n', 'publish', NULL, '2018-04-08 14:24:50', 'DSC_7572_1.png DSC_7578.png ', 2),
(26, 'Venom VX 1112W Subwoofer Pasif ', 2118200, 0, '10', 1, 2, NULL, '<p>Venom VX 1112W merupakan produk terbaru dari Venom Audio dengan performa terbaik untuk menghadirkan suara Bass yang bertenaga. Didesain dengan elegan yang berwarna hitam, serta kualitas dan performa yang cukup baik. Subwoofer pasif ini berukuran 12&quot; single voice coil yang memiliki frekuensi Respone sebesar 30- 1KHz. Daya maksimum power 100 - 250 Watt RMS. Sangat cocok untuk Anda sebagai pecinta audio mobil harian.</p>\r\n', 'publish', NULL, '2018-04-08 14:40:52', 'DSC_3435.png DSC_3441.png DSC_3444.png ', 2),
(27, 'Venom VX 2112W Subwoofer Pasif ', 1809000, 0, '10', 1, 2, NULL, '<p>Venom VX 2112W Subwoofer Pasif merupakan subwoofer Pasif yang berukuran 12 Inch dengan Frequency Response yang mencapai 34Hz hingga 600KHz serta memiliki Power Handling sebesar 300 hingga 600W. Subwoofer ini sangatlah cocok untuk dijadikan bagian dalam komponen audio mobil Anda.</p>\r\n', 'publish', NULL, '2018-04-08 14:45:17', 'DSC_3573.png DSC_3577.png DSC_3585.png ', 2),
(28, 'VENOM VX 1110W - SUBWOOFER PASIF ', 1891000, 0, '10', 1, 2, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:12pt"><span style="font-size:16px">Subwoofer Venom VX-1110W ini merupakan subwoofer Pasif berukuran 10 inch, yang dirancang khusus untuk menghasilkan frekuensi suara menengah. Frekuensi yang dimiliki oleh Subwoofer pasif ini sebesar 30Hz - 1KHz.</span></span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt; text-align:center">\r\n<hr /></div>\r\n\r\n<p style="margin-left:0px; margin-right:0px; text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="color:#333333"><span style="font-size:16px"><strong>SPESIFIKASI :</strong></span></span></span></span></span></span></p>\r\n\r\n<p style="margin-left:0px; margin-right:0px; text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="color:#333333"><span style="font-size:16px">1. Dimension : 10 inch</span></span><br />\r\n<span style="color:#333333"><span style="font-size:16px">2. Resistance : 2 x 4 ohm</span></span><br />\r\n<span style="color:#333333"><span style="font-size:16px">3. Frequency Response : 30Hz - 1KHz</span></span><br />\r\n<span style="color:#333333"><span style="font-size:16px">4. Power Handling : 100 - 250W<br />\r\n5. Sensitivity : 87 dB</span></span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 14:48:56', 'DSC_3401.png DSC_3405.png DSC_3407.png ', 2),
(29, 'Venom VI12 DC Intellegent Subwoofer ', 2127500, 0, '10', 1, 2, NULL, '<p>Venom VI12 DC Intellegent Subwoofer Pasif merupakan Subwoofer berukuan 12 inch yang memiliki power handling kisaran 450 - 600 watt. Venom intellegent ini dapat memberikan suara bass yang dahsyat dengan volume box yang tepat. Subwoofer venom intellegent ini juga memiliki double voice coil dengan frekuensi respon 28 Hz hingga 1 kHz.</p>\r\n', 'publish', NULL, '2018-04-08 14:53:05', 'VI_12_DC_-_Copy_-_Copy.png VI_12_DC_-_Copy.png VI_12_DC.png ', 2),
(30, 'Venom VX 6.8 PB Subwoofer VEM-00054-01031', 2327500, 0, '10', 1, 2, NULL, '<p>Subwoofer VX 6.8 PB merupakan speaker mobil dengan ampliflier 6x8 Inch berdesain slim yang sangat cocok untuk diletakan di bawah jok mobil Anda. Subwoofer VX 6.8 PB ini memiliki bass yang dinamis dan pressure yang powerfull.&nbsp; Bass level terasa bertenaga, meskipun tidak di setup dengan volume yang tinggi. Anda dapat mengatur volume bass melalui remote bass bawaan dari subwoofer ini. Cocok buat kamu yang suka audio minimalis dimobil.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'publish', NULL, '2018-04-08 15:01:53', 'vx6m-2.png vx6m-4_1.png vx6m-14_1.png ', 2),
(31, 'Venom VX8.1 Subwoofer Mobil [8 Inch] 	VEM-00054-01130', 1936500, 0, '10', 1, 2, NULL, '<p>Venom VX8.1 Subwoofer Mobil [8 Inch] merupakan subwoofer dengan frekuensi respon 10 Hz hingga 150 kHz. Subwoofer ini memiliki radio 90 dB dengan fuse size 21 A dan speaker impedance 4 Ohm.</p>\r\n\r\n<p><strong>Specification :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 36 x 25 x 7 cm</li>\r\n	<li>Frequency Response : 10 Hz - 150 kHz</li>\r\n	<li>Comments : Signal to Noise</li>\r\n	<li>Radio : 90 dB</li>\r\n	<li>Fuse Size : 21 A</li>\r\n	<li>Speaker Impedance : 4 Ohm</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 15:05:33', 'VENOM_PRODUC-8re_1.png VENOM_PRODUC-9.png VENOM_PRODUC-10.png ', 2),
(32, 'Venom DBX Series Diablo Subwoofer Aktif [10.1A] ', 3850000, 0, '10', 1, 2, NULL, '<p>Venom DBX Series Diablo Subwoofer Aktif [10.1A] merupakan subwoofer yang dapat membuat emosi Anda bergetar, yang menawarkan suara extra bass yang mendalam, Powerfull dan Dinamis serta jelas dan tidak ada duanya. Dengan ketukan akurat dan tepat. Subwoofer ini dapat memuaskan telinga Anda dengan suara tajam dan jelas menonjol untuk suara bass. Subwoofer Pasif DBX 10.1 menghasilkan suara bass yang powerfull dan dinamis.<br />\r\n<br />\r\n<strong>Spesifikasi :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 10 Inch</li>\r\n	<li>Resistance : 4 ohm</li>\r\n	<li>Frequency Response : 10Hz - 150Hz</li>\r\n	<li>Power Handling : N/A</li>\r\n	<li>Sensitivity : 86 db</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 15:09:46', 'DSC_7572_11.png DSC_7576.png DSC_75781.png ', 2),
(33, 'Venom VX7012 DVC Subwoofer Mobil [12 Inch] ', 9345500, 0, '10', 1, 2, NULL, '<p>Venom VX7012 DVC Subwoofer Mobil [12 Inch] adalah subwoofer mobil berukuran 12 Inch dalam bentuk black paper cone dengan foam edge 3 Inch (76.2 mm). Memiliki rated power 5000 W dan maximal power 10000 W untuk memberikan kualitas sound yang jernih dan mendetail di dalam mobil Anda.</p>\r\n', 'publish', NULL, '2018-04-08 15:35:52', 'DOE_1967.png DOE_1975.png DOE_1979.png ', 2),
(34, 'VENOM VX 2002W - SUBWOOFER PASIF 12 INCH ', 1582000, 0, '10', 1, 2, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Subwoofer Venom VX 2002W ini merupakan subwoofer Pasif yang berukuran 12 Inch. Dengan desain warna hitam menunjukan kesederhana pada subwoofer pasif ini.</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt; text-align:center">\r\n<hr /></div>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px"><strong>SPESIFIKASI :</strong></span></span></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Dimension : 12 inch</span><br />\r\n<span style="font-size:16px">2. Resistance : 2 x 4 ohm</span><br />\r\n<span style="font-size:16px">3. Frequency Response : 40Hz &ndash; 1KHz</span><br />\r\n<span style="font-size:16px">4. Power Handling : 200 &ndash; 400W<br />\r\n5. Sensitivity : 87 dB</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 15:45:55', 'DSC_3562.png DSC_3567.png DSC_3569.png ', 2),
(35, 'VENOM BASS TUBE VX 10PB - SUBWOOFER AKTIF ', 3300000, 0, '10', 1, 2, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Venom Bass Tube VX 10PB ini merupakan Subwoofer Aktif yang berbentuk cukup simple dengan ukuran 10 inch. Venom ini memiliki daya yang cukup kuat untuk menghasilkan nada - nada rendah di kabin mobil anda. Power outputnya cukup besar serta kualitas suara bass yang cukup solid .</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><strong><span style="font-size:16px">SPESIFIKASI :</span></strong></span></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Dimension : 10 inch</span><br />\r\n<span style="font-size:16px">2. Resistance : 1 x 4 ohm</span><br />\r\n<span style="font-size:16px">3. Frequency Response : 300Hz - 100KHz</span><br />\r\n<span style="font-size:16px">4. Power Handling : -<br />\r\n5.&nbsp;</span><span style="font-size:16px">Sensitivity : 91 db</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 15:54:32', 'DSCF9233.JPG DSCF9239_1.png ', 2),
(36, 'INTELLEGENT SERIES VI10DC ', 1973000, 0, '10', 1, 2, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Venom Intellegent VI10DC merupakan Subwoofer berukuan 10 inch yang memiliki power handling kisaran 350 - 600 watt. Venom intellegent ini dapat memberikan suara bass yang dahsyat dengan volume box yang tepat. subwoofer venom intellegent ini juga memiliki double voice coil dengan frekuensi respon 28 Hz hingga 1 kHz.</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><strong><span style="font-size:16px">SPESIFIKASI :</span></strong></span></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Dimension : 10 inch</span><br />\r\n<span style="font-size:16px">2. Resistance : 2 x 2 ohm</span><br />\r\n<span style="font-size:16px">3. Frequency Response : 28Hz - 1Khz</span><br />\r\n<span style="font-size:16px">4. Power Handling : 350 - 600W<br />\r\n5.&nbsp;</span><span style="font-size:16px">Sensitivity : 84 db</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 16:00:54', 'VI_12_DC_-_Copy_-_Copy1.png VI_12_DC_-_Copy1.png VI_12_DC1.png ', 2),
(37, 'VENOM DIABLO SERIES VX603D - SPEAKER COAXIAL ', 600000, 0, '10', 1, 10, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Venom Diablo VX603D memberi Anda solusi untuk menyediakan kualitas suara yang jernih dengan memaksimalkan tenaga sebesar 150W dan mempunyai sensitivitas sebesar 92 dB dengan ukuran 6,5 Inch . Tunggu apalagi jadikan speaker coaxial ini menjadi komponen audio dimobil kamu</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><strong><span style="font-size:16px">SPESIFIKASI :</span></strong></span></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Dimension : 6.5 inch</span><br />\r\n<span style="font-size:16px">2. Resistance : 4 ohm</span><br />\r\n<span style="font-size:16px">3. Frequency Response : 50Hz - 20KHz</span><br />\r\n<span style="font-size:16px">4. Power Handling : 50 - 150W<br />\r\n5.&nbsp;</span><span style="font-size:16px">Sensitivity : 92 db</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 16:13:56', '603D_-_Copy_(2).jpg 603D_-_Copy.jpg 603D.jpg ', 1),
(38, 'Venom Turbo Series VX603TO Coaxial Speaker [6 Inch]', 636364, 0, '10', 1, 10, NULL, '<p>Venom Turbo Series VX603TO Coaxial Car Speaker [6 Inch] adalah speaker mobil&nbsp;koaksial&nbsp;berukuran 6 Inch yang dibuat dengan 6 variable color head sink, sehingga sangat cocok untuk Anda pecinta mobil. Speaker ini dapat memberikan kualitas sound yang powerful dan mendetail di dalam mobil Anda.</p>\r\n\r\n<p><strong>Specification :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 6 Inch 2-WAY</li>\r\n	<li>Resistance : 4 Ohm</li>\r\n	<li>Frequency Response : 60 - 20KHz</li>\r\n	<li>Sensitivity : 88 db/W/M</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 16:23:12', 'VX603TO-620x264_-_Copy_(2).png VX603TO-620x264_-_Copy.png VX603TO-620x264.png ', 1),
(40, 'Venom Black Series V475BII Amplifier Mobil [4 Channel] ', 2454500, 0, '10', 1, 13, NULL, '<p>Venom Black Series V475BII Amplifier Mobil [4 Channel] adalah amplifier mobil dengan penguat Kelas D, dua channel dan empat saluran untuk memberikan pengguna sejumlah fitur yang pasti akan disukai oleh pecinta musik. Dirancang untuk mensupply tenaga elektrikal yang dihubungkan pada komponen audio speaker maupun subwoofer mobil, sehingga membuat speaker dapat menghasilkan kualitas sound yang jernih &amp; powerful.</p>\r\n\r\n<ul>\r\n	<li>Demension : 29.5 x 5.4 x 23.5 cm</li>\r\n	<li>Output Power in 4 ohm : 50 W x 4</li>\r\n	<li>Output Power in 2 ohm : 75 W x 4</li>\r\n	<li>Frequency Response : 10 Hz - 55 Hz</li>\r\n	<li>Low Pass Cross Freq : 50 Hz - 250 Hz</li>\r\n	<li>High Pass Cross Freq : 50 Hz - 250 Hz</li>\r\n	<li>Comments : Speaker Impedance: 2 - 16 Ohm</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 16:31:25', '475BII_-_Copy.png 475BII_-_Copy_(2).png 475BII.png ', 3),
(41, 'VENOM ILLUMINATOR SERIES VL 70.4 - POWER 4 CHANNEL ', 2490909, 0, '10', 1, 13, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Power 4 channel ini memiliki fitur utama yang dilengkapi dengan Iluminasi lampu LED dengan beberapa warna yang berganti secara otomatis.&nbsp; Amplifier ini sangat Cocok untuk memperlengkapi sistem audio mobil anda. Dengan Output Power in 4 ohm 1 x 70W dan Output Power in 2 ohm 1 x 140W .</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><strong><span style="font-size:16px">SPESIFIKASI :</span></strong></span></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Demension : 360 x 54 x 231 mm</span><br />\r\n<span style="font-size:16px">2. Output Power in 4 ohm : 1 x 70W</span><br />\r\n<span style="font-size:16px">3. Output Power in 2 ohm : 1 x 140<br />\r\n4.&nbsp;</span><span style="font-size:16px">Frequency Response : 10Hz - 50KHz</span><br />\r\n<span style="font-size:16px">5. Low Pass Cross Freq : 40Hz - 150Hz</span><br />\r\n<span style="font-size:16px">6. High Pass Cross Freq : 60Hz - 1.2KHz<br />\r\n7. Bass EQ : 0 - 12 dB @45Hz</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 16:37:55', 'DSC_1739.png DSC_1740.png DSC_1744.png ', 3),
(42, 'Venom Vi150.4 Intellegent Series Power Amplifier [4 Channel] ', 7673000, 0, '10', 1, 13, NULL, '<p>Venom Vi150.4 Intellegent Series Power Amplifier [4 Channel] merupakan power amplifier 4 channel yang memiliki Output Power in 4 ohm 4 x 150W, Output Power in 2 ohm 4 x 250W . Power amplifier ini cocok untuk jenis musik Sound Quality. Ideal untuk mengeluarkan daya suara yang lebih detail.</p>\r\n', 'publish', NULL, '2018-04-08 16:39:53', 'DSC_2101.png DSC_2103.png DSC_2106.png ', 3),
(43, 'Venom Vi2Tb Intellegent Series Power Amplifier [4 Channel]', 7027300, 0, '10', 1, 13, NULL, '<p>Venom Vi2Tb Intellegent Series Power Amplifier [4 Channel] merupakan power amplifier 4 channel ini yang dirancang untuk meningkatkan berbagai bidang utama, seperti kinerja termal radikal yang ditingkatkan, desain casing stackable baru yang indah dan puluhan tweak yang bertujuan untuk meningkatkan performa dan fleksibilitas. Ideal untuk berikan sound yang lebih mendetail.</p>\r\n', 'publish', NULL, '2018-04-08 16:45:43', 'DSC_2089.png DSC_2089.png DSC_2099.png ', 3),
(44, 'Venom Turbo Series V406To 4 Channel Power Amplifier Mobil 	VEM-00054-01118', 1991000, 0, '10', 1, 13, NULL, '<p>Venom Turbo Series V406To 4 Channel Power Amplifier Mobil, Venom series terbaru yang tentunya memiliki performa dan kualitas yang lebih lengkap. Dengan kualitas power yang menghasilkan suara bass yang sangat baik. Kualitas suara bass yang cukup kuat untuk menggetarkan kabin mobil, maka power 4 channel Venom ini, dapat mendrive speaker split dan subwoofer 12 Inch dengan tenaga yang cukup besar.</p>\r\n', 'publish', NULL, '2018-04-08 16:51:23', 'DSC_2292.png DSC_2300.png DSC_2301.png ', 3),
(45, 'Venom Vertigo Series VT 800.3 Power Amplifier [3 Channel] ', 3736500, 0, '10', 1, 13, NULL, '<p>Venom Vertigo Series Vt 800.3 Power Amplifier [3 Channel] merupakan power amplifier yang dirancang dengan kualitas suara yang clean. Amplifier ini didesain dengan bentuk yang minimalis dan instalasi yang sederhana. Ideal untuk sound yang lebih mendetail.</p>\r\n', 'publish', NULL, '2018-04-08 16:55:53', 'DSC_2382.png DSC_2383.png DSC_2384.png ', 3),
(46, 'Venom Vertigo Series VT 475 4 Channel Power Amplifier Mobil 	', 1973000, 0, '10', 1, 13, NULL, '<p>Venom Vertigo Series Vt 475 4 Channel Power Amplifier Mobil, dengan kualitas suara bass yang cukup kuat untuk menggetarkan kabin mobil Anda. Power 4 channel ini dapat mendrive speaker split dan subwoofer 12 Inch dengan tenaga yang cukup besar. Produk power Amplifier ini sangat baik untuk bermain di Sound Pressure Level. Dengan kualitas yang handal, kiranya produk dari brand audio Venom ini bisa dijadikan alternatif untuk Sound Pressure Level di mobil Anda.</p>\r\n', 'publish', NULL, '2018-04-08 17:01:33', '800_3-1.jpg 800-1.png ', 3),
(47, 'Venom V500.1BII Black Series Monoblock Amplifier ', 2873000, 0, '10', 1, 3, NULL, '<p>Venom V500.1BII Black Series Monoblock Amplifier merupakan monoblock amplifier dengan Frequency Response : 15 Hz - 250 Hz dan Impedance : 1 - 16 ohm. Ideal untuk meberikan sound medetail pada kabin mobik Anda.</p>\r\n\r\n<p><strong>Detail</strong></p>\r\n\r\n<ul>\r\n	<li>Demension : 29.5 x 5.4 x 23.5 cm</li>\r\n	<li>Output Power in 4 ohm : 300 W x 1</li>\r\n	<li>Output Power in 2 ohm : 450 W x 1</li>\r\n	<li>Output Power in 1 ohm : 500 W x 1</li>\r\n	<li>Frequency Response : 15 Hz - 250 Hz</li>\r\n	<li>Low Pass Cross Freq : 40 Hz - 150 Hz</li>\r\n	<li>Comments : Speaker</li>\r\n	<li>Impedance : 1 - 16 ohm</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 17:06:34', 'venom_venom-v500-1bii-black-series-monoblock-amplifier_full02.png venom_venom-v500-1bii-black-series', 3),
(48, 'VENOM DIABLO SERIES VD500.1 - POWER MONOBLOCK', 2600000, 0, '10', 1, 3, NULL, '<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Venom Diablo VD500.1 - Power Amplifier Monoblock merupakan sebuah alat yang berfungsi memperkuat sinyal audio speaker dengan level tertentu sesuai kebutuhan. Sebuah performa generasi baru yang diarahkan dan cocok untuk penggemar audio mobil pemula.</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="margin-left:0px; margin-right:0px; text-align:start"><span style="font-size:12px"><span style="background-color:#ffffff"><span style="color:#333333"><strong><span style="font-size:16px"><span style="color:#000000">SPESIFIKASI :</span></span></strong></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1. Output Power in 4 ohm : 1 x 250W</span><br />\r\n<span style="font-size:16px">2. Output Power in 2 ohm : 1 x 500W</span><br />\r\n<span style="font-size:16px">3. Output Power in 1 ohm : 1 x 1000W</span><br />\r\n<span style="font-size:16px">4. Type : Monoblock</span><br />\r\n<span style="font-size:16px">5. Demension : 38.5 x 24 x 5 cm</span><br />\r\n<span style="font-size:16px">6. Remote Level : Included</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-08 17:10:22', 'VO500_1.png VO500_1_-_Copy_(2).png VO500_1_-_Copy.png ', 3),
(49, 'Venom X Series V1800XDII Monoblock Amplifier Mobil ', 3700000, 0, '10', 1, 3, NULL, '<p style="text-align:justify">Venom X Series V1800XDII Monoblock Amplifier Mobil merupakan dua model Venom Extreme amplifier series Class D dan 5 channel amplifier yang memberikan keandalan. Amplifier monoblock Venom X menggunakan daya Class D yang hemat energi. Amplifier channel Venom X memberikan banyak kekuatan untuk sistem hiburan mobil.</p>\r\n\r\n<p><strong>Spesifikasi :</strong></p>\r\n\r\n<ul>\r\n	<li>Dimension : 44.2 x 23.8 x 5 cm</li>\r\n	<li>Output Power in 4 ohm : 4 x 250 W</li>\r\n	<li>Output Power in 2 ohm : 1 x 500 W</li>\r\n	<li>Output Power in 1 ohm : 1 x 1000 W</li>\r\n	<li>Type : Monoblock</li>\r\n	<li>Remote Level : Included</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 17:13:49', 'venom_2017_product-60_1.png venom_2017_product-58.png ', 3),
(50, 'Venom Intellegent Series VI100.4D Power 4 Channel ', 5255000, 0, '10', 1, 3, NULL, '<p>Venom Intellegent Series VI500.1D Power Monoblock merupakan power monoblock yang telah mereproduksi kekuatan suara yang mengesankan. Dengan kekuatan suara yang dimiliki oleh Venom Power Monoblock ini, dapat menggentarkan suasana dalam mobil Anda menjadi lebih menyenangkan. Power monoblock ini memiliki Output Power in 4 ohm 1 x 1500W, Output Power in 2 ohm 1 x 3000W, serta Output Power in 1 ohm sebesar 1 x 4500W. Cocok untuk dijadikan bagian dalam komponen Audio mobil Anda.</p>\r\n\r\n<p><strong>Spesifikasi :</strong></p>\r\n\r\n<ul>\r\n	<li>Demension : 65.5 x 21.3 x 5.5 cm</li>\r\n	<li>Output Power in 4 ohm : 1 x 1500 W</li>\r\n	<li>Output Power in 2 ohm : 1 x 3000 W</li>\r\n	<li>Output Power in 1 ohm : 1 x 4500 W</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 17:18:17', 'DSC_1797.png DSC_1800.png DSC_1802.png ', 3),
(51, 'Venom Silver Series V500.2SII 2 Channel Power Amplifier Mobil', 2936500, 0, '10', 1, 3, NULL, '<p>Venom Silver Series V500.2Sii 2 Channel Power Amplifier Mobil merupakan power 2 Channel yang didedikasikan sebagai penguat sinyal suara. Dengan output power in 4 ohm 2 x 80W dan output power in 2 ohm 2 x 160W.</p>\r\n', 'publish', NULL, '2018-04-08 17:23:03', 'DSC_2643.png DSC_2645.png DSC_2646.png ', 3),
(52, 'Venom Silver Series VD2000SII Power Monoblock', 5573000, 0, '10', 1, 3, NULL, '<p style="text-align:justify">Venom Silver Series Vd2000Sii Power Monoblock merupakan power monoblock memiliki Output Power in 4 ohm 1 x 400W, Output Power in 2 ohm 1 x 800W, serta Output Power in 1 ohm sebesar 1 x 1600W. Ideal untuk memberikan sound lebih mendetail.</p>\r\n', 'publish', NULL, '2018-04-08 17:25:45', 'DSC_2618.png DSC_2621.png DSC_2622.png ', 3),
(53, 'Venom Silver Series VD2600SII Power Monoblock ', 7800000, 0, '10', 1, 3, NULL, '<p style="text-align:justify">Venom Silver Series Vd2600Sii Power Monoblock merupakan power monoblock yang memiliki Output Power in 4 ohm 1 x 600W, Output Power in 2 ohm 1 x 1200W, serta Output Power in 1 ohm sebesar 1 x 2400W sehingga memiliki kualitas suara bass yang sangat baik. Ideal untuk memberikan sound lebih mendetail.</p>\r\n', 'publish', NULL, '2018-04-08 17:28:06', 'DSC_2131.png DSC_2130.png DSC_2126.png ', 3),
(54, 'Venom Vertigo Series VT600.1 Monoblock Amplifier', 2727500, 0, '10', 1, 3, NULL, '<p>Venom Vertigo Series VT600.1 Monoblock Amplifier, amplifier dengan desain minimalis modern yang akan menyenangkan telinga Anda dengan kualitas suara yang prima, bentuk kompak dan pemasangan yang sederhana. Untuk pengalaman perjalanan yang paling menyenangkan.</p>\r\n\r\n<p>Spesifikasi :</p>\r\n\r\n<ul>\r\n	<li>Demension : 39.2 x 22.2 x 5.2 cm</li>\r\n	<li>Output Power in 4 ohm : 1 x 300 W</li>\r\n	<li>Output Power in 2 ohm : 1 x 450 W</li>\r\n	<li>Output Power in 1 ohm : 1 x 500 W</li>\r\n	<li>Type : Monoblock Remote</li>\r\n	<li>Level : Included</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-08 17:31:32', 'DSC_2367.png DSC_2370.png DSC_2371.png ', 3),
(55, 'VENOM DIABLO SERIES VD 406 - POWER 4 CHANNEL', 1863500, 0, '10', 1, 13, NULL, '<p style="text-align:justify"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">Power Venom Diablo VD406 ini merupakan power amplifier 4 channel. Power ini difungsikan untuk mensupply elektrikal ke komponen speaker maupun subwoofer. Power ini juga mampu mengangkat kualitas suara musik. Cocok dijadikan komponen tambahan audio di mobil kamu.</span></span></span></span></span></p>\r\n\r\n<div style="-webkit-text-stroke-width:0px; margin-bottom:0.0001pt">\r\n<hr /></div>\r\n\r\n<p style="margin-left:0px; margin-right:0px; text-align:start"><span style="font-size:12px"><span style="background-color:#ffffff"><span style="color:#333333"><strong><span style="font-size:16px"><span style="color:#000000">SPESIFIKASI :</span></span></strong></span></span></span></p>\r\n\r\n<p style="text-align:start"><span style="font-size:13px"><span style="color:#282828"><span style="font-family:Lato,sans-serif"><span style="background-color:#ffffff"><span style="font-size:16px">1.&nbsp; &nbsp;4 Channel Power Amplifier Diablo</span><br />\r\n<span style="font-size:16px">2.&nbsp; &nbsp;Output Power Rating @ 4 Ohms : 60W x 4&nbsp;</span><br />\r\n<span style="font-size:16px">3.&nbsp; &nbsp;Output Power Rating @ 2 Ohms : 120W x 4</span><br />\r\n<span style="font-size:16px">4.&nbsp; &nbsp;Bridged @ 4 Ohms : 240W x 2</span><br />\r\n<span style="font-size:16px">5.&nbsp; &nbsp;Frequency Response : 10Hz - 50KHz</span><br />\r\n<span style="font-size:16px">6.&nbsp; &nbsp;Input Sensitivity : 0.2V ~ 4.0V</span><br />\r\n<span style="font-size:16px">7.&nbsp; &nbsp;Signal to Noise Ratio : &gt; 104dB</span><br />\r\n<span style="font-size:16px">8.&nbsp; &nbsp;THD &amp; Noise : 0&nbsp;</span><br />\r\n<span style="font-size:16px">9.&nbsp; &nbsp;Low Pass Cross Frequency : 40Hz - 150Hz</span><br />\r\n<span style="font-size:16px">10. High Pass Cross Frequency : 60Hz - 1.2kHz</span><br />\r\n<span style="font-size:16px">11. Bass EQ : 0dB - 12dB @ 45Hz</span><br />\r\n<span style="font-size:16px">12. Speaker Impedance : 2 ~ 16 Ohm<br />\r\n13.&nbsp;</span><span style="font-size:16px">Fuse Size : 2 x 25A<br />\r\n14.&nbsp;</span><span style="font-size:16px">Dimensions (L x H x W) mm : 320 x 47 x 239</span></span></span></span></span></p>\r\n', 'publish', NULL, '2018-04-11 02:57:04', 'vd_406_-_Copy.png vd_406.png vd_406_-_Copy_(2).png ', 3),
(56, 'PANDORA VPR 2 MKII', 4518500, 0, '10', 1, 4, NULL, '<p>Like the legendary name :</p>\r\n\r\n<p>pandora is the mystical box that absorbs all the problems in your car audio system. from Venom lab and assisted by the venom Fighter we try to make you realize, that this was an actual car audio.</p>\r\n\r\n<p>pandora VPR-1 is the birth of new begining from digital sound processor era. so.......</p>\r\n\r\n<p>why should you hesitate ?</p>\r\n\r\n<p>now we have the way.</p>\r\n\r\n<h4>Specification :</h4>\r\n\r\n<p>Frequency Response : 10 - 20KHz</p>\r\n\r\n<p>Phase : 0 - 180 degrees</p>\r\n\r\n<p>Comments :</p>\r\n\r\n<p>1. POWER SUPPLY :</p>\r\n\r\n<ul>\r\n	<li>Voltage&nbsp;&nbsp;&nbsp; 11 - 15 VDC</li>\r\n	<li>Idling current&nbsp;&nbsp; 0.4 A</li>\r\n	<li>Switched off with DRC&nbsp; 4 mA</li>\r\n	<li>Switched off without DRC&nbsp; 2.5 mA</li>\r\n	<li>Remote IN voltage&nbsp; 7 - 15 VDC (1.3 mA)</li>\r\n	<li>Remote OUT voltage&nbsp; 12 VDC (130 mA)</li>\r\n</ul>\r\n\r\n<p>2. SIGNAL STAGE :</p>\r\n\r\n<ul>\r\n	<li>Distortion - THD @1kHz, 1V RMS Output&nbsp; = 0.005%</li>\r\n	<li>Bandwith @-3dB&nbsp;&nbsp; = 10 - 22kHz</li>\r\n	<li>Channel Separation @ 1 kHz&nbsp; = 88 dB</li>\r\n	<li>Input Sensitivity [ High level ]&nbsp; = 2 - 15 V RMS</li>\r\n	<li>Input Sensitivity [ Low level ]&nbsp;&nbsp; = 0.6 - 5 V RMS</li>\r\n	<li>Max Output Level (RMS) @0.1% THD&nbsp; = 4 V RMS</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-11 08:33:14', 'be829391afa7194d6f55b862a34bba96.png ', 4),
(57, 'PANDORA VPR 2 MKII L', 6200000, 0, '10', 1, 4, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Frequency Response : 10 - 20KHz</p>\r\n\r\n<p>Phase : 0 - 180 degrees</p>\r\n\r\n<p>Comments :</p>\r\n\r\n<p>1. POWER SUPPLY :</p>\r\n\r\n<ul>\r\n	<li>Voltage&nbsp;&nbsp;&nbsp; 11 - 15 VDC</li>\r\n	<li>Idling current&nbsp;&nbsp; 0.4 A</li>\r\n	<li>Switched off with DRC&nbsp; 4 mA</li>\r\n	<li>Switched off without DRC&nbsp; 2.5 mA</li>\r\n	<li>Remote IN voltage&nbsp; 7 - 15 VDC (1.3 mA)</li>\r\n	<li>Remote OUT voltage&nbsp; 12 VDC (130 mA)</li>\r\n</ul>\r\n\r\n<p>2. SIGNAL STAGE :</p>\r\n\r\n<ul>\r\n	<li>Distortion - THD @1kHz, 1V RMS Output&nbsp; = 0.005%</li>\r\n	<li>Bandwith @-3dB&nbsp;&nbsp; = 10 - 22kHz</li>\r\n	<li>Channel Separation @ 1 kHz&nbsp; = 88 dB</li>\r\n	<li>Input Sensitivity [ High level ]&nbsp; = 2 - 15 V RMS</li>\r\n	<li>Input Sensitivity [ Low level ]&nbsp;&nbsp; = 0.6 - 5 V RMS</li>\r\n	<li>Max Output Level (RMS) @0.1% THD&nbsp; = 4 V RMS</li>\r\n</ul>\r\n', 'publish', NULL, '2018-04-11 08:37:39', 'f2cb0e7c3b5537b7bba8a717e815e68f.png ', 4),
(58, 'INFERNO VIN 6', 5145500, 0, '10', 1, 1, NULL, '', 'publish', NULL, '2018-04-11 09:20:51', '704470f64530530309e3397cc77d05ad.png ', 1),
(59, 'INFERNO VIN2', 2845500, 0, '10', 1, 5, NULL, '', 'publish', NULL, '2018-04-11 09:22:09', 'venom_product_vin_pandora_limited-32.jpg ', 5),
(60, 'DIABLO VX 6 D', 1182000, 0, '10', 1, 1, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Dimension : 6.5 inch</p>\r\n\r\n<p>Resistance : 4 ohm</p>\r\n\r\n<p>Frequency Response : 60Hz - 20KHz</p>\r\n\r\n<p>Power Handling : 50 - 150W</p>\r\n\r\n<p>Sensitivity : 90 db</p>\r\n\r\n<p>Tweater : 22 mm</p>\r\n', 'publish', NULL, '2018-04-11 09:43:48', 'DSC_3178.JPG ', 1),
(61, 'ILLUMINATOR VL6', 1990909, 0, '10', 1, 1, NULL, '', 'publish', NULL, '2018-04-11 09:45:30', 'VL_6.png ', 1),
(62, 'INTELLIGENT VI 6.4', 4354500, 0, '10', 1, 1, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Dimension : 6 inch</p>\r\n\r\n<p>Resistance : 4 ohm</p>\r\n\r\n<p>Frequency Response : 30Hz - 21KHz</p>\r\n\r\n<p>Power Handling : 60W</p>\r\n\r\n<p>Sensitivity : 88 db</p>\r\n', 'publish', NULL, '2018-04-11 09:53:42', 'VI_6_4.png ', 1),
(63, 'VX 3 FR', 1745500, 0, '10', 1, 5, NULL, '', 'publish', NULL, '2018-04-12 02:44:04', 'DSC_3012.JPG DSC_3013.JPG DSC_3016.JPG ', 5),
(65, 'FULLRANGE VX 8 FR', 1055000, 0, '10', 1, 2, NULL, '<p><br />\r\nSPECIFICATIONS<br />\r\nRms.Power(W)&nbsp;250 &nbsp; &nbsp;<br />\r\nMax.Power(W) 500<br />\r\nFo (Hz) 100<br />\r\nNominal lmpedance(&Omega;)&nbsp;8<br />\r\nSensitivity@1w/1m(dB)&nbsp;95<br />\r\nFrequency Range&nbsp;100Hz~8KHz<br />\r\nVoice Coil&nbsp;2?<br />\r\nCone&nbsp;Cloth Edge Paper Cone<br />\r\nMounting Depth(mm)&nbsp;94<br />\r\nMounting Diameter(mm) 183</p>\r\n', 'publish', NULL, '2018-04-12 02:58:25', 'vx8fr-1.jpg DSC_6915.png vx8fr-2.jpg ', 2),
(66, 'DIABLO VO 406 4CHANNEL', 1863500, 0, '10', 1, 13, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Demension : 35.5 x 24 x 5 cm</p>\r\n\r\n<p>Output Power in 4 ohm : 4 x 60 W</p>\r\n\r\n<p>Output Power in 2 ohm : 4 x 120 W</p>\r\n\r\n<p>Type : 4 Channel</p>\r\n\r\n<p>Crossover : HP/LP/FULL</p>\r\n', 'publish', NULL, '2018-04-12 03:08:01', 'DSC_2311_1.png DSC_2313_1.png DSC_2314_1.png ', 3),
(70, 'INTELLIGENT VI 500.1D MONOBLOK', 4518200, 0, '10', 1, 3, NULL, '<p>Demension : 21 x 4.7 x 15.8 cm</p>\r\n\r\n<p>Output Power in 4 ohm : 1 x 200 W</p>\r\n\r\n<p>Output Power in 2 ohm : 1 x 350 W</p>\r\n\r\n<p>Output Power in 1 ohm : 1 x 500 W</p>\r\n\r\n<p>Type : Power Monoblok</p>\r\n\r\n<p>Frequency Response&nbsp; &nbsp;: 10Hz - 150 Hz</p>\r\n', 'publish', NULL, '2018-04-12 03:23:07', 'DSC_1802.JPG DSC_1797.JPG DSC_1800.JPG ', 3),
(73, 'PURPLE STROM PS 1501.1', 3654500, 0, '10', 1, 3, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Demension : 28.5 x 20 x 4.8 cm</p>\r\n\r\n<p>Output Power in 4 ohm : 1 x 500W</p>\r\n\r\n<p>Output Power in 2 ohm : 1 x 800 W</p>\r\n\r\n<p>Output Power in 1 ohm : 1 x 1400W</p>\r\n\r\n<p>Type : Monoblock</p>\r\n\r\n<p>Remote Level : Included</p>\r\n', 'publish', NULL, '2018-04-12 03:34:31', 'DSC_1782.JPG DSC_1780.JPG DSC_1784.JPG ', 3),
(80, 'INTELLIGENT VI 8 W', 2982000, 0, '10', 1, 2, NULL, '<h4>Specification :</h4>\r\n\r\n<p>Dimension : 8 inch</p>\r\n\r\n<p>Resistance : 4 ohm</p>\r\n\r\n<p>Frequency Response : 32Hz &ndash; 800KHz</p>\r\n\r\n<p>Sensitivity : 83 db</p>\r\n', 'publish', NULL, '2018-04-12 03:46:16', 'vi_8wjpg.jpg ', 2),
(84, 'PURPLE STROM PS 10', 2354500, 0, '10', 1, 2, NULL, '<p>Dimension : 10&nbsp;inch</p>\r\n\r\n<p>Resistance : 4 ohm</p>\r\n\r\n<p>Frequency Response : 20 Hz - 160 Hz</p>\r\n\r\n<p>Power Handling : 80 - 175 W</p>\r\n\r\n<p>Sensitivity : 970 db</p>\r\n', 'publish', NULL, '2018-04-12 03:50:44', 'DSC_3491.png DSC_3499.png DSC_3501.png ', 2),
(85, 'PURPLE STROM PS 12', 2600000, 0, '10', 1, 2, NULL, '<p>Dimension : 12&nbsp;inch</p>\r\n\r\n<p>Resistance : 4 ohm</p>\r\n\r\n<p>Frequency Response : 20 Hz - 160 Hz</p>\r\n\r\n<p>Power Handling : 80 - 175 W</p>\r\n\r\n<p>Sensitivity : 970 db</p>\r\n', 'publish', NULL, '2018-04-12 03:52:30', 'DSC_3328_1.png DSC_3331_1.png DSC_3333_1.png ', 2),
(89, 'SUBWOOFER VX 8.1A', 2545500, 0, '10', 1, 2, NULL, '<p>Output Power in4 Ohm&nbsp; : 200 W</p>\r\n\r\n<p>Frequency Response&nbsp; &nbsp; : 10 Hz - 150 Khz</p>\r\n\r\n<p>input Sensitivity&nbsp; : 0.2 V - 5 V</p>\r\n\r\n<p>Signal to Noise Radio&nbsp; : 90dB</p>\r\n\r\n<p>Low Pass&nbsp; : 30 Hz - 150 Hz</p>\r\n\r\n<p>Fuse Size&nbsp; : 21 A</p>\r\n\r\n<p>Speaker Impedance 4 Ohm</p>\r\n\r\n<p>Dimension&nbsp; : 36 x 25 x 7 Cm</p>\r\n', 'publish', NULL, '2018-04-12 04:04:11', 'venom_photo_vpr_4_6-50.jpg venom_photo_vpr_4_6-54.jpg venom_photo_vpr_4_6-55.jpg ', 2),
(92, 'SUBWOOFER VX 15.1', 3781900, 0, '10', 1, 2, NULL, '', 'publish', NULL, '2018-04-12 04:25:39', 'SAM_2432_1.png SAM_2433_1.png SAM_2434_1.png ', 2),
(93, 'SUBWOOFER VX 15.1A', 4627500, 0, '10', 1, 2, NULL, '', 'publish', NULL, '2018-04-12 04:27:10', 'SAM_2435_.jpg SAM_2437.png ', 2),
(111, 'Venom V406TO', 2190000, 0, '10', 1, 14, NULL, '', 'publish', NULL, '2018-05-03 04:17:16', '2.jpg 1.jpg 4.jpg 3.jpg ', 3),
(112, 'Subwoofer diablo 12', 750000, 0, '10', 1, 14, NULL, '', 'publish', NULL, '2018-05-03 04:26:15', 'sub.png sub2.png ', 8),
(114, 'Paket Venom Voucher [Rp 10.000.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-10-000-000/ps--VEM-00054-01050', '', 'publish', NULL, '2018-08-04 06:02:39', '10juta.jpg ', 9),
(115, 'Paket Venom Voucher [Rp 5.000.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-5-000-000/ps--VEM-00054-01049', '', 'publish', NULL, '2018-08-04 06:11:38', '5juta.jpg ', 9),
(116, 'Paket Venom Voucher [Rp 1.000.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-1-000-000/ps--VEM-00054-01048', '', 'publish', NULL, '2018-08-04 06:13:47', '1juta.jpg ', 9),
(117, 'Paket Venom Voucher [Rp 500.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-500-000/ps--VEM-00054-01047', '', 'publish', NULL, '2018-08-04 06:14:47', '500.jpg ', 9),
(118, 'Paket Venom Voucher [Rp 100.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-100-000/ps--VEM-00054-01046', '', 'publish', NULL, '2018-08-04 06:15:22', '100.jpg ', 9),
(119, 'Paket Venom Voucher [Rp 50.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-50-000/ps--VEM-00054-01045', '', 'publish', NULL, '2018-08-04 06:16:08', '50.jpg ', 9),
(120, 'Paket Venom Voucher [Rp 10.000]', 0, 0, '', 0, 0, 'https://www.blibli.com/p/paket-venom-voucher-rp-10-000/ps--VEM-00054-01044', '', 'publish', NULL, '2018-08-04 06:16:42', '10.jpg ', 9),
(122, 'VX6TO', 1254545, 0, '10', 1, 1, NULL, '', 'publish', NULL, '2018-08-04 08:36:07', 'vx6to.png ', 1),
(123, 'VX403TO', 554545, 0, '10', 1, 10, '', '', 'publish', NULL, '2018-08-04 08:38:36', 'VX403TO.jpg ', 1),
(124, 'Venom Baby Diablo Series VX12BD SVC Subwoofer mobil [12 Inch]', 1463636, 0, '10', 1, 2, '', '', 'publish', NULL, '2018-08-04 08:39:56', 'VX12TO.jpg ', 2),
(125, 'Vx6T', 3063636, 0, '10', 1, 1, NULL, '', 'publish', NULL, '2018-08-04 08:43:26', 'vx6to1.png', 1),
(126, 'Venom Pandora VPR 4.6MKII', 4809000, 0, '10', 1, 4, NULL, '', 'publish', NULL, '2018-08-04 09:21:14', 'VPR_4_6_MKII.jpg VPR_4_6_MKII.jpg VPR_4_6_MKII.jpg ', 4),
(127, 'Venom Pandora VPR 3.5', 3400000, 0, '10', 1, 4, '', '', 'publish', NULL, '2018-08-04 09:23:03', 'VPR_3_51.jpg VPR_3_51.jpg ', 4);
INSERT INTO `tb_produk` (`id_produk`, `judul`, `harga`, `point`, `jumlah`, `id_merk`, `id_kat`, `url`, `ket`, `status`, `counter`, `tgl_input_pro`, `foto`, `id_subkat`) VALUES
(129, 'PAKET AUDIO MOBIL VENOM MAZDA RX7', 19145000, 0, '10', 1, 6, NULL, '<p>PAKET AUDIO MOBIL VENOM MAZDA RX7<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + c + Subwoofer VX1112W dan Free VENOM Watch Limied Edition<br />\r\nPaket Mobil Demo Venom Mazda RX7 digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih dan power full</p>\r\n\r\n<p>PAKET AUDIO MOBIL VENOM MAZDA RX7<br />\r\n&#39;&#39; Processor pandora VPR 4.6MKII<br />\r\n&#39;&#39; Speaker Inferno VIN 6<br />\r\n&#39;&#39; Fullrange VIN 2<br />\r\n&#39;&#39; Grill Fullrange VIN 2G<br />\r\n&quot; Illuminator VL 1000.1D<br />\r\n&#39;&#39; Subwoofer VX 1112W<br />\r\n&#39;&#39; Free VENOM Watch Limied Edition</p>\r\n\r\n<p>Tersedia untuk : Mitsubishi, Nissan, Toyota</p>\r\n', 'publish', NULL, '2018-08-04 10:13:05', 'mobil_demo.jpg inferno_vin6_vin_2.jpg VENOM_WATCH.jpg VL_1000_1D.jpg VPR_4_6_MKII1.jpg VX_1112W.jpg ', 6),
(130, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 1', 11500000, 0, '10', 1, 6, NULL, '<p>Paket audio mobil&nbsp;VENOM PAKET EXPRESS BASIC PLUS 1</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer VX 8.1 +&nbsp;Venom Fullrange Inferno VIN 2 + VIN 2G dan&nbsp;Free VENOM Watch Limied Edition<br />\r\nIdeal&nbsp;digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p><br />\r\nPaket audio mobil&nbsp;VENOM PAKET EXPRESS BASIC PLUS 1</p>\r\n\r\n<p>Processor pandora VPR 4.6MKII<br />\r\nVenom Fullrange Inferno VIN 2<br />\r\nVenom Grill Fullrange Inferno VIN 2G<br />\r\nVenom Subwoofer VX 8.1<br />\r\nFree VENOM Watch Limied Edition</p>\r\n', 'publish', NULL, '2018-08-04 10:16:29', 'VPR_4_6_MKII_+_VX_8_1_+_VIN2.jpg VENOM_WATCH1.jpg VIN_2.jpg VIN_2G.jpg VPR_4_6_MKII2.jpg VX_8_1.jpg ', 6),
(131, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 2', 11740000, 0, '10', 1, 6, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer DBX 10.1 + Venom Fullrange Inferno VIN 2 + VIN 2G dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer DBX 10.1 Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Fullrange Inferno VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Grill Fullrange Inferno VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:19:48', 'VPR_4_6_MKII_+_DBX_10_1_+_VIN2.jpg DBX_10_1.jpg VENOM_WATCH2.jpg VIN_21.jpg VIN_2G1.jpg VPR_4_6_MKII', 6),
(132, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3', 13530000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer VX 15.1 + Venom Fullrange Inferno VIN 2 + VIN 2G dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 15.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu..Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Fullrange Inferno VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Grill Fullrange Inferno VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 15.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:24:10', 'VPR_4_6_MKII_+_VX_15_1_+_VIN2.jpg VENOM_WATCH3.jpg VIN_22.jpg VIN_2G2.jpg VPR_4_6_MKII4.jpg VX_15_1.', 6),
(133, 'Paket audio mobil VENOM Paket Express Custom Pro 1', 17830000, 0, '10', 1, 6, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + Venom Subwoofer VX 8.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1A Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Custom Pro 1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Inferno VIN 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Fullrange VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Grill Fullrange VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 8.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:28:22', 'VPR_4_6_MKII_+_VX_8_1A_+_VIN2_+VIN6.jpg VX_8_1A.jpg VPR_4_6_MKII5.jpg VENOM_WATCH4.jpg inferno_vin6_', 6),
(134, 'Paket audio mobil VENOM Paket Express Custom Pro 2', 19270000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM Paket Express Custom Pro 2<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + Venom Subwoofer DBX 10.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1A Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Custom Pro 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Inferno VIN 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Fullrange VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Grill Fullrange VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:30:04', 'VPR_4_6_MKII_+_DBX_10_1A_+_VIN2_+_VIN6.jpg VPR_4_6_MKII6.jpg VENOM_WATCH5.jpg inferno_vin6_vin_22.jp', 6),
(135, 'Paket audio mobil VENOM Paket Express Custom Pro 3', 20120000, 0, '10', 1, 6, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII +&nbsp;Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G +&nbsp;Venom Subwoofer VX 15.1A dan Free VENOM Watch Limied Edition</p>\r\n\r\n<p>Ideal&nbsp;digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.&nbsp;Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G&nbsp;series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil.&nbsp;Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Paket audio mobil&nbsp;VENOM Paket Express Custom Pro 3</p>\r\n\r\n<p>Processor pandora VPR 4.6MKII<br />\r\nSpeaker Inferno VIN 6<br />\r\nFullrange VIN 2<br />\r\nGrill Fullrange VIN 2G<br />\r\nVenom Subwoofer&nbsp;VX 15.1A<br />\r\nFree VENOM Watch Limied Edition</p>\r\n\r\n<ul>\r\n</ul>\r\n', 'publish', NULL, '2018-08-04 10:31:50', 'VPR_4_6_MKII_+_VX_15_1A_+_VIN2_+_VIN6.jpg VX_15_1A.jpg VPR_4_6_MKII7.jpg VENOM_WATCH6.jpg inferno_vi', 6),
(136, 'Paket audio mobil VENOM PAKET EXPRESS BASIC A1', 6780000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A1</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 6.8 PB dan Free Tweeter VL 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 6.8 PB Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 6X8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 6.8 PB<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:33:18', 'VPR_3_5_+_VX_6_8_PB1.jpg VL_1_T1.jpg  VPR_3_52.jpg VX_6_8_PB1.jpg ', 6),
(137, 'Paket audio mobil VENOM PAKET EXPRESS BASIC A2', 8460000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A2</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer DBX 10.1A dan Free Tweeter Venom VL 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer Pasive DBX 10.1A Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter Venom VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:35:09', 'VPR_3_5_+_VX_15_1A_.jpg VL_1_T2.jpg VPR_3_53.jpg VX_15_1A1.jpg ', 6),
(138, 'Paket audio mobil VENOM Paket Express  Basic A3', 9300000, 0, '10', 1, 6, NULL, '<p>Paket audio mobil VENOM Paket Express &nbsp;Basic A3</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 15.1A dan Free Tweeter Venom Vl 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express &nbsp;Basic A3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free Tweeter Venom Vl 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:36:45', 'VPR_3_5_+_VX_15_1A_1.jpg  VL_1_T3.jpg VPR_3_54.jpg VX_15_1A2.jpg ', 6),
(139, 'Paket audio mobil VENOM Paket Express Basic B1', 7970000, 0, '10', 1, 6, NULL, '<p>&bull;&nbsp;&nbsp; &nbsp;Terdiri dari Venom Processor pandora VPR 4.6MKII dan Venom Subwoofer VX 8.1 dan Free Tweeter VL 1T<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Ideal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Basic B1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 8.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:40:26', 'Vpr_4_6_MKII_+_VX_8_1.jpg VL_1_T4.jpg VPR_4_6_MKII8.jpg VX_8_11.jpg ', 6),
(140, 'Paket audio mobil VENOM Paket Express  Basic B2', 8210000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM Paket Express &nbsp;Basic B2</p>\r\n\r\n<p>&bull;&nbsp;&nbsp; &nbsp;Terdiri dari Venom Processor pandora VPR 4.6MKII dan Venom Subwoofer DBX 10.1 dan Free Tweeter Venom VL 1T<br />\r\n&nbsp; &nbsp; &nbsp;Ideal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\n&nbsp; &nbsp; &nbsp;menghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Subwoofer Pasive DBX 10.1 Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Basic B2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp; Free Tweeter Venom VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:43:23', 'VPR_4_6_MKII_+_DBX_10_1.jpg  DBX_10_11.jpg VL_1_T5.jpg VPR_4_6_MKII9.jpg ', 6),
(141, 'Paket audio mobil VENOM PAKET EXTRA A1 ', 8970000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXTRA A1&nbsp;<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer VX 6.8 PB dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM PAKET EXTRA A1&nbsp;<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Processor pandora VPR 3.5<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Speaker Illuminator VL 6<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Venom Subwoofer VX 6.8 PB<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:52:27', 'VPR_3_5_+_VX_6_8PB_+_VL_6.jpg  VENOM_WATCH7.jpg VL_6.jpg VPR_3_55.jpg VX_6_8_PB2.jpg ', 6),
(142, 'Paket audio mobil VENOM PAKET EXTRA A2', 10650000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXTRA A2<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer DBX 10.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Extra A2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Illuminator VL 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer BDX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:54:46', 'VPR_3_5_+_DBX_10_1A_+_VL_6_.jpg VPR_3_56.jpg DBX_10_1A1.jpg VL_61.jpg VENOM_WATCH8.jpg DBX_10_1A2.jp', 6),
(143, 'Paket audio mobil VENOM PAKET EXTRA A3', 11500000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM PAKET EXTRA A3<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer VX 15.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM PAKET EXTRA A3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Speaker Illuminator VL 6<br />\r\n&bull;Venom Subwoofer VX 15.1A<br />\r\n&bull;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:56:54', 'VPR_3_5_+_VX_15_1A_+_VL_6_.jpg  VX_15_1A3.jpg VPR_3_57.jpg VL_62.jpg VENOM_WATCH9.jpg ', 6),
(144, 'Paket audio mobil VENOM Paket Express Extra B1', 10220000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM Paket Express Extra B1<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer VX 8.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Extra B1<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Processor pandora VPR 4.6MKII<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Speaker Illuminator VL 6.2<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Venom Subwoofer VX 8.1<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Free VENOM Watch Limied Edition</p>\r\n', 'publish', NULL, '2018-08-04 14:00:22', 'VPR_4_6_MKII_+_VX_8_1_+_VL_6_2.jpg  VENOM_WATCH10.jpg VL_6_2.jpg VPR_4_6_MKII10.jpg VX_8_12.jpg ', 6),
(145, 'Paket audio mobil VENOM Paket Express Extra B2', 10460000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM Paket Express Extra B2<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer DBX 10.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Extra B2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Illuminator VL 6.2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer BDX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:02:58', 'VPR_4_6_MKII_+_DBX_10_1_+_VL_6_2.jpg _10_12.jpg VENOM_WATCH11.jpg VL_6_21.jpg VPR_4_6_MKII11.jpg ', 6),
(146, 'Paket audio mobil VENOM Paket Express Extra B3', 12260000, 0, '10', 1, 6, '', '<p>Paket audio mobil VENOM Paket Express Extra B3<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer VX 15.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Extra B3<br />\r\n&bull;Processor pandora VPR 4.6MKII<br />\r\n&bull;Speaker Illuminator VL 6.2<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:05:44', 'VPR_4_6_MKII_+_VX_15_1_+_VL_6_2_.jpg  VENOM_WATCH12.jpg VL_6_22.jpg VPR_4_6_MKII12.jpg VX_15_11.jpg ', 6),
(147, 'Paket audio mobil VENOM Paket Express  Basic B3', 10000000, 0, '10', 1, 6, NULL, '<p style="margin-left:0in; margin-right:0in; text-align:justify">Paket audio mobil VENOM Paket Express &nbsp;Basic B3<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 15.1A dan Free Tweeter Venom Vl 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express &nbsp;Basic B3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free Tweeter Venom Vl 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:09:09', 'VPR_3_5_+_VX_15_1A_2.jpg  VL_1_T6.jpg VPR_3_58.jpg VX_15_1A4.jpg ', 6),
(148, 'PAKET AUDIO MOBIL VENOM MAZDA RX7', 19145000, 0, '10', 1, 7, NULL, '<p>PAKET AUDIO MOBIL VENOM MAZDA RX7<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + c + Subwoofer VX1112W dan Free VENOM Watch Limied Edition<br />\r\nPaket Mobil Demo Venom Mazda RX7 digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih dan power full</p>\r\n\r\n<p>PAKET AUDIO MOBIL VENOM MAZDA RX7<br />\r\n&#39;&#39; Processor pandora VPR 4.6MKII<br />\r\n&#39;&#39; Speaker Inferno VIN 6<br />\r\n&#39;&#39; Fullrange VIN 2<br />\r\n&#39;&#39; Grill Fullrange VIN 2G<br />\r\n&quot; Illuminator VL 1000.1D<br />\r\n&#39;&#39; Subwoofer VX 1112W<br />\r\n&#39;&#39; Free VENOM Watch Limied Edition</p>\r\n\r\n<p>Tersedia untuk : Mitsubishi, Nissan, Toyota</p>\r\n', 'publish', NULL, '2018-08-04 10:13:05', 'mobil_demo.jpg VL_1000_1D.jpg inferno_vin6_vin_2.jpg VPR_4_6_MKII1.jpg VENOM_WATCH.jpg VX_1112W.jpg ', 7),
(149, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 1', 11500000, 0, '10', 1, 7, NULL, '<p>Paket audio mobil&nbsp;VENOM PAKET EXPRESS BASIC PLUS 1</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer VX 8.1 +&nbsp;Venom Fullrange Inferno VIN 2 + VIN 2G dan&nbsp;Free VENOM Watch Limied Edition<br />\r\nIdeal&nbsp;digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p><br />\r\nPaket audio mobil&nbsp;VENOM PAKET EXPRESS BASIC PLUS 1</p>\r\n\r\n<p>Processor pandora VPR 4.6MKII<br />\r\nVenom Fullrange Inferno VIN 2<br />\r\nVenom Grill Fullrange Inferno VIN 2G<br />\r\nVenom Subwoofer VX 8.1<br />\r\nFree VENOM Watch Limied Edition</p>\r\n', 'publish', NULL, '2018-08-04 10:16:29', 'VPR_4_6_MKII_+_VX_8_1_+_VIN2.jpg VENOM_WATCH1.jpg VIN_2.jpg VIN_2G.jpg VPR_4_6_MKII2.jpg VX_8_1.jpg ', 7),
(150, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 2', 11740000, 0, '10', 1, 7, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer DBX 10.1 + Venom Fullrange Inferno VIN 2 + VIN 2G dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer DBX 10.1 Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Fullrange Inferno VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Grill Fullrange Inferno VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:19:48', 'VPR_4_6_MKII_+_DBX_10_1_+_VIN2.jpg DBX_10_1.jpg VENOM_WATCH2.jpg VIN_21.jpg VIN_2G1.jpg VPR_4_6_MKII', 7),
(151, 'Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3', 13530000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Venom Subwoofer VX 15.1 + Venom Fullrange Inferno VIN 2 + VIN 2G dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 15.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu..Venom Fullrange Inferno VIN 2 menghadirkan kualitas suara yang vokal yang sangat jernih dan bekualitas yang memanjakan telinga anda.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC PLUS 3<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Fullrange Inferno VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Grill Fullrange Inferno VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 15.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:24:10', 'VPR_4_6_MKII_+_VX_15_1_+_VIN2.jpg VENOM_WATCH3.jpg VIN_22.jpg VIN_2G2.jpg VPR_4_6_MKII4.jpg VX_15_1.', 7),
(152, 'Paket audio mobil VENOM Paket Express Custom Pro 1', 17830000, 0, '10', 1, 7, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + Venom Subwoofer VX 8.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1A Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Custom Pro 1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Inferno VIN 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Fullrange VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Grill Fullrange VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 8.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:28:22', 'VPR_4_6_MKII_+_VX_8_1A_+_VIN2_+VIN6.jpg VX_8_1A.jpg VPR_4_6_MKII5.jpg VENOM_WATCH4.jpg inferno_vin6_', 7),
(153, 'Paket audio mobil VENOM Paket Express Custom Pro 2', 19270000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM Paket Express Custom Pro 2<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G + Venom Subwoofer DBX 10.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1A Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Custom Pro 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Inferno VIN 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Fullrange VIN 2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Grill Fullrange VIN 2G<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:30:04', 'VPR_4_6_MKII_+_DBX_10_1A_+_VIN2_+_VIN6.jpg VPR_4_6_MKII6.jpg VENOM_WATCH5.jpg inferno_vin6_vin_22.jp', 7),
(154, 'Paket audio mobil VENOM Paket Express Custom Pro 3', 20120000, 0, '10', 1, 7, '', '<p>Terdiri dari Venom Processor pandora VPR 4.6MKII +&nbsp;Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G +&nbsp;Venom Subwoofer VX 15.1A dan Free VENOM Watch Limied Edition</p>\r\n\r\n<p>Ideal&nbsp;digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.&nbsp;Speaker Inferno VIN 6 + Fullrange VIN 2 + Grill Fullrange VIN 2G&nbsp;series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil.&nbsp;Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Paket audio mobil&nbsp;VENOM Paket Express Custom Pro 3</p>\r\n\r\n<p>Processor pandora VPR 4.6MKII<br />\r\nSpeaker Inferno VIN 6<br />\r\nFullrange VIN 2<br />\r\nGrill Fullrange VIN 2G<br />\r\nVenom Subwoofer&nbsp;VX 15.1A<br />\r\nFree VENOM Watch Limied Edition</p>\r\n\r\n<ul>\r\n</ul>\r\n', 'publish', NULL, '2018-08-04 10:31:50', 'VPR_4_6_MKII_+_VX_15_1A_+_VIN2_+_VIN6.jpg VX_15_1A.jpg VPR_4_6_MKII7.jpg VENOM_WATCH6.jpg inferno_vi', 7),
(155, 'Paket audio mobil VENOM PAKET EXPRESS BASIC A1', 6780000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A1</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 6.8 PB dan Free Tweeter VL 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 6.8 PB Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 6X8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 6.8 PB<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:33:18', 'VPR_3_5_+_VX_6_8_PB1.jpg VL_1_T1.jpg VPR_3_52.jpg VX_6_8_PB1.jpg ', 7),
(156, 'Paket audio mobil VENOM PAKET EXPRESS BASIC A2', 8460000, 0, '10', 1, 7, NULL, '<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A2</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer DBX 10.1A dan Free Tweeter Venom VL 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer Pasive DBX 10.1A Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.</p>\r\n\r\n<p>Paket audio mobil VENOM PAKET EXPRESS BASIC A2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter Venom VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:35:09', 'VPR_3_5_+_DBX_10_1A_.jpg VL_1_T2.jpg DBX_10_1A3.jpg VPR_3_53.jpg ', 7),
(157, 'Paket audio mobil VENOM Paket Express  Basic A3', 9300000, 0, '10', 1, 7, NULL, '<p>Paket audio mobil VENOM Paket Express &nbsp;Basic A3</p>\r\n\r\n<p>Terdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 15.1A dan Free Tweeter Venom Vl 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express &nbsp;Basic A3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free Tweeter Venom Vl 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:36:45', 'VPR_3_5_+_VX_15_1A_1.jpg VL_1_T3.jpg VPR_3_54.jpg VX_15_1A2.jpg ', 7),
(158, 'Paket audio mobil VENOM Paket Express Basic B1', 7970000, 0, '10', 1, 7, NULL, '<p>&bull;&nbsp;&nbsp; &nbsp;Terdiri dari Venom Processor pandora VPR 4.6MKII dan Venom Subwoofer VX 8.1 dan Free Tweeter VL 1T<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Ideal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Basic B1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer VX 8.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free Tweeter VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:40:26', 'Vpr_4_6_MKII_+_VX_8_11.jpg VL_1_T7.jpg VPR_4_6_MKII13.jpg VX_8_11.jpg ', 7),
(159, 'Paket audio mobil VENOM Paket Express  Basic B2', 8210000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM Paket Express &nbsp;Basic B2</p>\r\n\r\n<p>&bull;&nbsp;&nbsp; &nbsp;Terdiri dari Venom Processor pandora VPR 4.6MKII dan Venom Subwoofer DBX 10.1 dan Free Tweeter Venom VL 1T<br />\r\n&nbsp; &nbsp; &nbsp;Ideal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\n&nbsp; &nbsp; &nbsp;menghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Subwoofer Pasive DBX 10.1 Subwoofer dengan suara bass yang Powerfull dan Dinamis. suara bass yang dihasilkan lebih bertenaga di kelasnya.</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Basic B2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer DBX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp; Free Tweeter Venom VL 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 10:43:23', 'VPR_4_6_MKII_+_DBX_10_1.jpg DBX_10_11.jpg VL_1_T5.jpg VPR_4_6_MKII9.jpg ', 7),
(160, 'Paket audio mobil VENOM PAKET EXTRA A1 ', 8970000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM PAKET EXTRA A1&nbsp;<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer VX 6.8 PB dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM PAKET EXTRA A1&nbsp;<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Processor pandora VPR 3.5<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Speaker Illuminator VL 6<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Venom Subwoofer VX 6.8 PB<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:52:27', 'VPR_3_5_+_VX_6_8PB_+_VL_6.jpg  VENOM_WATCH7.jpg VL_6.jpg VPR_3_55.jpg VX_6_8_PB2.jpg ', 7),
(161, 'Paket audio mobil VENOM PAKET EXTRA A2', 10650000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM PAKET EXTRA A2<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer DBX 10.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Extra A2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 3.5<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Illuminator VL 6<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer BDX 10.1A<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:54:46', 'VPR_3_5_+_DBX_10_1A_+_VL_6_.jpg VPR_3_56.jpg DBX_10_1A1.jpg VL_61.jpg VENOM_WATCH8.jpg DBX_10_1A2.jp', 7),
(162, 'Paket audio mobil VENOM PAKET EXTRA A3', 11500000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM PAKET EXTRA A3<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Speaker Illuminator VL 6 + Venom Subwoofer VX 15.1A dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6 series dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM PAKET EXTRA A3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Speaker Illuminator VL 6<br />\r\n&bull;Venom Subwoofer VX 15.1A<br />\r\n&bull;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 13:56:54', 'VPR_3_5_+_VX_15_1A_+_VL_6_.jpg VX_15_1A3.jpg VPR_3_57.jpg  VL_62.jpg VENOM_WATCH9.jpg ', 7),
(163, 'Paket audio mobil VENOM Paket Express Extra B1', 10220000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM Paket Express Extra B1<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer VX 8.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 8.1 Subwoofer Kolong jok dengan suara bass yang Powerfull dan Dinamis. Meskipun ukuran subwoofer berukuran 8 inch, namun suara bass yang dihasilkan lebih bertenaga di kelasnya. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Extra B1<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Processor pandora VPR 4.6MKII<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Speaker Illuminator VL 6.2<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Venom Subwoofer VX 8.1<br />\r\n&bull; &nbsp; &nbsp; &nbsp; &nbsp; Free VENOM Watch Limied Edition</p>\r\n', 'publish', NULL, '2018-08-04 14:00:22', 'VPR_4_6_MKII_+_VX_8_1_+_VL_6_2.jpg VENOM_WATCH10.jpg VL_6_2.jpg VPR_4_6_MKII10.jpg VX_8_12.jpg ', 7),
(164, 'Paket audio mobil VENOM Paket Express Extra B2', 10460000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM Paket Express Extra B2<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer DBX 10.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer DBX 10.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya.&nbsp;</p>\r\n\r\n<p>Paket audio mobil VENOM Paket Express Extra B2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Processor pandora VPR 4.6MKII<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Speaker Illuminator VL 6.2<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Venom Subwoofer BDX 10.1<br />\r\n&bull;&nbsp;&nbsp; &nbsp;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:02:58', 'VPR_4_6_MKII_+_DBX_10_1_+_VL_6_2.jpg DBX_10_12.jpg VENOM_WATCH11.jpg VL_6_21.jpg VPR_4_6_MKII11.jpg ', 7),
(165, 'Paket audio mobil VENOM Paket Express Extra B3', 12260000, 0, '10', 1, 7, '', '<p>Paket audio mobil VENOM Paket Express Extra B3<br />\r\nTerdiri dari Venom Processor pandora VPR 4.6MKII + Speaker Illuminator VL 6.2 + Venom Subwoofer VX 15.1 dan Free VENOM Watch Limied Edition<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda. Speaker Illuminator VL 6.2 series terbaru dari Venom membuat suara audio dalam mobil anda yang dihasilkan lebih jernih dari sebelumya.Subwoofer VX 15.1 Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express Extra B3<br />\r\n&bull;Processor pandora VPR 4.6MKII<br />\r\n&bull;Speaker Illuminator VL 6.2<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free VENOM Watch Limied Edition<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:05:44', 'VPR_4_6_MKII_+_VX_15_1_+_VL_6_2_.jpg VENOM_WATCH12.jpg VL_6_22.jpg VPR_4_6_MKII12.jpg VX_15_11.jpg ', 7),
(166, 'Paket audio mobil VENOM Paket Express  Basic B3', 10000000, 0, '10', 1, 7, NULL, '<p style="margin-left:0in; margin-right:0in; text-align:justify">Paket audio mobil VENOM Paket Express &nbsp;Basic B3<br />\r\nTerdiri dari Venom Processor pandora VPR 3.5 + Venom Subwoofer VX 15.1A dan Free Tweeter Venom Vl 1T<br />\r\nIdeal digunakan untuk meningkatkan kualitas audio pada mobil Anda<br />\r\nmenghadirkan kualitas suara yang sangat jernih, pemasangan pun tidak akan merusak garansi mobil dan tidak ada potong kabel, jadi aman garansi untuk mobil anda.Subwoofer VX 15.1A Subwoofer suara bass yang Powerfull dan Dinamis. Suara bass yang dihasilkan lebih bertenaga di kelasnya. Penempatan Subwoofer ini di bagian Ban Serep pada Mobil. Dengan ukuran yang minimalis dan compact, kamu tidak perlu mengorbankan space di bagasi mobil kamu.<br />\r\n&nbsp;<br />\r\nPaket audio mobil VENOM Paket Express &nbsp;Basic B3<br />\r\n&bull;Processor pandora VPR 3.5<br />\r\n&bull;Venom Subwoofer VX 15.1<br />\r\n&bull;Free Tweeter Venom Vl 1T<br />\r\n&nbsp;</p>\r\n', 'publish', NULL, '2018-08-04 14:09:09', 'VPR_3_5_+_VX_15_1A_2.jpg VL_1_T6.jpg VPR_3_58.jpg VX_15_1A4.jpg ', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tb_rating`
--

CREATE TABLE IF NOT EXISTS `tb_rating` (
  `id` int(11) NOT NULL,
  `community_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `img` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `create_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rating`
--

INSERT INTO `tb_rating` (`id`, `community_id`, `user_id`, `jumlah`, `img`, `comment`, `create_on`) VALUES
(1, 15, 7, 5, '[{"img":"nm.png"},{"img":"nm.png"},{"img":"nm.png"},{"img":"nm.png"}]', 'test', '2018-03-30 22:03:33'),
(2, 15, 7, 5, 'kih', 'kol', '2018-04-12 16:53:38'),
(3, 408, 35, 4, '[{"im1":"\\/9j\\/4AAQSkZJRgABAQAAAQABAAD\\/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkz\\nODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P\\/2wBDARESEhgVGC8aGi9jQjhCY2NjY2Nj\\nY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2N', 'good', '2018-04-12 17:34:00'),
(4, 408, 27, 0, '[{"im1":"","im2":"","im3":"","im4":""}]', 'test', '2018-04-14 11:55:49'),
(5, 408, 35, 4, '[{"im1":"\\/9j\\/4AAQSkZJRgABAQAAAQABAAD\\/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkz\\nODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P\\/2wBDARESEhgVGC8aGi9jQjhCY2NjY2Nj\\nY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2N', 'coba...', '2018-04-14 14:26:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reedem`
--

CREATE TABLE IF NOT EXISTS `tb_reedem` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `point` int(15) NOT NULL,
  `status` enum('unapprove','approve') NOT NULL DEFAULT 'unapprove',
  `keterangan` text,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_reedem`
--

INSERT INTO `tb_reedem` (`id`, `user_id`, `point`, `status`, `keterangan`, `create_on`) VALUES
(1, 17, 100, 'unapprove', NULL, '2018-08-05 09:39:04'),
(2, 17, 200, 'unapprove', NULL, '2018-08-05 09:42:07'),
(3, 61, 100, 'unapprove', NULL, '2018-08-06 10:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reward`
--

CREATE TABLE IF NOT EXISTS `tb_reward` (
  `kode` varchar(15) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `point` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `deskripsi` text,
  `redeem` text,
  `term` text,
  `category` enum('reward','hadiah') NOT NULL DEFAULT 'reward',
  `images` text NOT NULL,
  `img_qrcode` text NOT NULL,
  `expired` date NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_reward`
--

INSERT INTO `tb_reward` (`kode`, `nama`, `point`, `stock`, `alamat`, `deskripsi`, `redeem`, `term`, `category`, `images`, `img_qrcode`, `expired`, `create_on`) VALUES
('SIXTY1', 'Hot/Ice Americano', 10, 50, 'Jakarta', '', '', '', 'reward', 'http://venom.baguswebmaster.com/assets/upload/reward/kopi1.jpg', 'http://venom.baguswebmaster.com/assets/upload/reward/qrcode/SIXTY1.png', '2018-08-23', '2018-07-28 01:44:41'),
('v1', 'kopi', 0, 0, 'venom', '<p>kopi</p>\r\n', '<p>kopi</p>\r\n', '<p>kopi</p>\r\n', 'hadiah', 'http://venom.baguswebmaster.com/assets/upload/reward/gg.jpg', 'http://venom.baguswebmaster.com/assets/upload/reward/qrcode/v1.png', '0000-00-00', '2018-07-28 04:23:28'),
('SIXTY2', 'Hot/Ice Latte', 10, 100, 'alamat', '', '', '', 'reward', 'http://venom.baguswebmaster.com/assets/upload/reward/caffe_latte1.jpg', 'http://venom.baguswebmaster.com/assets/upload/reward/qrcode/SIXTY2.png', '2018-08-10', '2018-07-28 01:42:04'),
('SIXTY3', 'Fizzy Lime', 10, 0, 'jakarta', '', '', '', 'hadiah', 'http://venom.baguswebmaster.com/assets/upload/reward/glaz2.jpg', 'http://venom.baguswebmaster.com/assets/upload/reward/qrcode/SIXTY3.png', '0000-00-00', '2018-07-28 01:34:28'),
('ktm001', 'koppi', 10, 50, 'jakarta', '', '', '', 'reward', 'http://localhost/venom/assets/upload/reward/book.png', 'http://localhost/venom/assets/upload/reward/qrcode/ktm001.png', '2018-07-28', '2018-07-19 12:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_simulator`
--

CREATE TABLE IF NOT EXISTS `tb_simulator` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `simulator` text NOT NULL,
  `status` set('yes','no') NOT NULL DEFAULT 'no',
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_simulator`
--

INSERT INTO `tb_simulator` (`id`, `user_id`, `simulator`, `status`, `create_on`) VALUES
(3, 27, '[{"id_product":"1","nama_brg_simulator":"Speaker Kecil Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kanan"},{"id_product":"2","nama_brg_simulator":"Speaker Kecil Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kiri"},{"id_product":"3","nama_brg_simulator":"Midrange Kiri","harga_brg_simulator":"0","nm_kategori":"Midrange Kiri"},{"id_product":"15","nama_brg_simulator":"VPR 3.4","harga_brg_simulator":"3510000","nm_kategori":"Midrange Kanan"},{"id_product":"5","nama_brg_simulator":"Speaker Besar Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kiri"},{"id_product":"6","nama_brg_simulator":"Processor","harga_brg_simulator":"0","nm_kategori":"Processor"},{"id_product":"7","nama_brg_simulator":"Speaker Besar Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kanan"},{"id_product":"8","nama_brg_simulator":"Speaker Coaxial Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kiri"},{"id_product":"9","nama_brg_simulator":"Aplifier Kiri","harga_brg_simulator":"0","nm_kategori":"Aplifier Kiri"},{"id_product":"10","nama_brg_simulator":"Aplifier Kanan","harga_brg_simulator":"0","nm_kategori":"Aplifier Kanan"},{"id_product":"11","nama_brg_simulator":"Speaker Coaxial Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kanan"},{"id_product":"12","nama_brg_simulator":"Subwofer Kiri","harga_brg_simulator":"0","nm_kategori":"Subwofer Kiri"},{"id_product":"13","nama_brg_simulator":"Subwofer Kanan","harga_brg_simulator":"0","nm_kategori":"Subwofer Kanan"}]', 'no', '2018-04-05 06:44:56'),
(2, 17, '[{"id_product":"1","nama_brg_simulator":"subBelakangKananA","harga_brg_simulator":"0","nm_kategori":"subBelakangKananA"},{"id_product":"2","nama_brg_simulator":"subBelakangKananB","harga_brg_simulator":"0","nm_kategori":"subBelakangKananB"},{"id_product":"7","nama_brg_simulator":"ILLUMINATOR SERIES","harga_brg_simulator":"7500000","nm_kategori":"subDepanKanan"},{"id_product":"4","nama_brg_simulator":"vpr","harga_brg_simulator":"0","nm_kategori":"vpr"},{"id_product":"5","nama_brg_simulator":"subDepanKiri","harga_brg_simulator":"0","nm_kategori":"subDepanKiri"},{"id_product":"6","nama_brg_simulator":"subBelakangKiriB","harga_brg_simulator":"0","nm_kategori":"subBelakangKiriB"},{"id_product":"7","nama_brg_simulator":"subBelakangKiriA","harga_brg_simulator":"0","nm_kategori":"subBelakangKiriA"},{"id_product":"8","nama_brg_simulator":"subTengahKiri","harga_brg_simulator":"0","nm_kategori":"subTengahKiri"},{"id_product":"9","nama_brg_simulator":"veb2","harga_brg_simulator":"0","nm_kategori":"veb2"},{"id_product":"10","nama_brg_simulator":"veb","harga_brg_simulator":"0","nm_kategori":"veb"},{"id_product":"11","nama_brg_simulator":"subTengahKanan","harga_brg_simulator":"0","nm_kategori":"subTengahKanan"},{"id_product":"12","nama_brg_simulator":"subBelakangKanan","harga_brg_simulator":"0","nm_kategori":"subBelakangKanan"},{"id_product":"13","nama_brg_simulator":"subBelakangKiri","harga_brg_simulator":"0","nm_kategori":"subBelakangKiri"}]', 'no', '2018-03-30 08:09:29'),
(4, 20, '[{"id_product":"1","nama_brg_simulator":"Speaker Kecil Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kanan"},{"id_product":"2","nama_brg_simulator":"Speaker Kecil Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kiri"},{"id_product":"3","nama_brg_simulator":"Midrange Kiri","harga_brg_simulator":"0","nm_kategori":"Midrange Kiri"},{"id_product":"8","nama_brg_simulator":"VPR 2 MKII L","harga_brg_simulator":"890000","nm_kategori":"Midrange Kanan"},{"id_product":"5","nama_brg_simulator":"Speaker Besar Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kiri"},{"id_product":"6","nama_brg_simulator":"Processor","harga_brg_simulator":"0","nm_kategori":"Processor"},{"id_product":"7","nama_brg_simulator":"Speaker Besar Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kanan"},{"id_product":"8","nama_brg_simulator":"Speaker Coaxial Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kiri"},{"id_product":"9","nama_brg_simulator":"Aplifier Kiri","harga_brg_simulator":"0","nm_kategori":"Aplifier Kiri"},{"id_product":"10","nama_brg_simulator":"Aplifier Kanan","harga_brg_simulator":"0","nm_kategori":"Aplifier Kanan"},{"id_product":"11","nama_brg_simulator":"Speaker Coaxial Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kanan"},{"id_product":"12","nama_brg_simulator":"Subwofer Kiri","harga_brg_simulator":"0","nm_kategori":"Subwofer Kiri"},{"id_product":"13","nama_brg_simulator":"Subwofer Kanan","harga_brg_simulator":"0","nm_kategori":"Subwofer Kanan"}]', 'no', '2018-04-08 22:35:20'),
(5, 20, '[{"id_product":"1","nama_brg_simulator":"Speaker Kecil Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kanan"},{"id_product":"2","nama_brg_simulator":"Speaker Kecil Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Kecil Kiri"},{"id_product":"3","nama_brg_simulator":"Midrange Kiri","harga_brg_simulator":"0","nm_kategori":"Midrange Kiri"},{"id_product":"4","nama_brg_simulator":"Midrange Kanan","harga_brg_simulator":"0","nm_kategori":"Midrange Kanan"},{"id_product":"5","nama_brg_simulator":"Speaker Besar Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kiri"},{"id_product":"6","nama_brg_simulator":"Processor","harga_brg_simulator":"0","nm_kategori":"Processor"},{"id_product":"7","nama_brg_simulator":"Speaker Besar Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Besar Kanan"},{"id_product":"8","nama_brg_simulator":"Speaker Coaxial Kiri","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kiri"},{"id_product":"9","nama_brg_simulator":"Aplifier Kiri","harga_brg_simulator":"0","nm_kategori":"Aplifier Kiri"},{"id_product":"39","nama_brg_simulator":"Venom VT 603 Vertigo Series Coaxial Speaker Mobil [6 Inch] VEM-00054-01100","harga_brg_simulator":"660000","nm_kategori":"Aplifier Kanan"},{"id_product":"11","nama_brg_simulator":"Speaker Coaxial Kanan","harga_brg_simulator":"0","nm_kategori":"Speaker Coaxial Kanan"},{"id_product":"12","nama_brg_simulator":"Subwofer Kiri","harga_brg_simulator":"0","nm_kategori":"Subwofer Kiri"},{"id_product":"13","nama_brg_simulator":"Subwofer Kanan","harga_brg_simulator":"0","nm_kategori":"Subwofer Kanan"}]', 'no', '2018-04-09 00:25:56'),
(6, 20, '[{"id_product":"0","nama_brg_simulator":"Twitter","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Midrange","harga_brg_simulator":"0"},{"id_product":"39","nama_brg_simulator":"Venom VT 603 Vertigo Series Coaxial Speaker Mobil [6 Inch] VEM-00054-01100","harga_brg_simulator":"1320000"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-09 02:42:26'),
(7, 20, '[{"id_product":"0","nama_brg_simulator":"Twitter","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Midrange","harga_brg_simulator":"0"},{"id_product":"38","nama_brg_simulator":"Venom Turbo Series VX603TO Coaxial Speaker [6 Inch]  \\tVEM-00054-01102","harga_brg_simulator":"1320000"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-09 02:46:05'),
(8, 20, '[{"id_product":"10","nama_brg_simulator":"Venom Baby Diablo Series VX6BD Component Speaker Mobil [6 Inch]  VEM-00054-01114","harga_brg_simulator":"800000"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Midrange","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Coaxial Belakang","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-09 02:51:48'),
(9, 20, '[{"id_product":"18","nama_brg_simulator":"VENOM BLACK SERIES VX6.3B (NEW) - SPEAKER 3 WAY BRM-48581-00438","harga_brg_simulator":"2710000"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Midrange","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Coaxial Belakang","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-09 03:04:28'),
(10, 35, '[{"id_product":"0","nama_brg_simulator":"Twitter","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Midrange","harga_brg_simulator":"0"},{"id_product":"37","nama_brg_simulator":"VENOM DIABLO SERIES VX603D - SPEAKER COAXIAL \\tBRM-48581-00442","harga_brg_simulator":"575000"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-10 00:42:35'),
(11, 35, '[{"id_product":"10","nama_brg_simulator":"Venom Baby Diablo Series VX6BD Component Speaker Mobil [6 Inch]  VEM-00054-01114","harga_brg_simulator":"800000"},{"id_product":"25","nama_brg_simulator":"Venom DBX 10.1  VEM-00054-01032","harga_brg_simulator":"2135000"},{"id_product":"7","nama_brg_simulator":"Venom Illuminator Series VL1000.1D Power  \\tBRM-48581-00779","harga_brg_simulator":"3749000"},{"id_product":"8","nama_brg_simulator":"VPR 2 MKII L","harga_brg_simulator":"890000"},{"id_product":"22","nama_brg_simulator":"Venom Intelligent Series VI3M Midrange Speaker Mobil VEM-00054-01105","harga_brg_simulator":"2020000"},{"id_product":"37","nama_brg_simulator":"VENOM DIABLO SERIES VX603D - SPEAKER COAXIAL \\tBRM-48581-00442","harga_brg_simulator":"575000"},{"id_product":"42","nama_brg_simulator":"Venom Vi150.4 Intellegent Series Power Amplifier [4 Channel]  VEM-00054-01126","harga_brg_simulator":"8030000"}]', 'no', '2018-04-10 01:08:50'),
(12, 33, '[{"id_product":"21","nama_brg_simulator":"Venom Vertigo Series VT 65B Component 2 Way Speaker Mobil [6 Inch]  \\tVEM-00054-01099","harga_brg_simulator":"1265000"},{"id_product":"0","nama_brg_simulator":"Subwofer","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Amplifier Monoblock","harga_brg_simulator":"0"},{"id_product":"0","nama_brg_simulator":"Processor","harga_brg_simulator":"0"},{"id_product":"22","nama_brg_simulator":"Venom Intelligent Series VI3M Midrange Speaker Mobil VEM-00054-01105","harga_brg_simulator":"2020000"},{"id_product":"39","nama_brg_simulator":"Venom VT 603 Vertigo Series Coaxial Speaker Mobil [6 Inch] VEM-00054-01100","harga_brg_simulator":"660000"},{"id_product":"0","nama_brg_simulator":"Amplifier 4 chenel","harga_brg_simulator":"0"}]', 'no', '2018-04-10 01:34:20'),
(13, 35, '[{"id_product":"8","nama_brg_simulator":"VPR 2 MKII L","harga_brg_simulator":"890000"}]', 'no', '2018-04-10 20:17:53'),
(14, 35, '[{"id_product":"39","nama_brg_simulator":"Venom VT 603 Vertigo Series Coaxial Speaker Mobil [6 Inch] VEM-00054-01100","harga_brg_simulator":"660000"}]', 'no', '2018-04-10 20:42:25'),
(20, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:05:14'),
(21, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:14:04'),
(22, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:14:27'),
(23, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:14:34'),
(24, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:15:50'),
(25, 17, '[\n  {\n    "nama_brg_simulator" : "PANDORA VPR MKII ",\n    "harga_brg_simulator" : 3850000,\n    "id_product" : "8"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  }\n]', 'no', '2018-05-14 04:19:13'),
(26, 17, '[\n  {\n    "nama_brg_simulator" : "Venom X Series VX6XII Component Speaker Mobil [6 Inch] VEM-00054-01108",\n    "harga_brg_simulator" : 1595000,\n    "id_product" : "17"\n  },\n  {\n    "nama_brg_simulator" : "Venom VX6012 Subwoofer Pasif \\tCAO-60023-00007",\n    "harga_brg_simulator" : 6240000,\n    "id_product" : "24"\n  },\n  {\n    "nama_brg_simulator" : "Venom Baby Diablo series VO400.1 BD  VEM-00054-01075",\n    "harga_brg_simulator" : 2225000,\n    "id_product" : "9"\n  },\n  {\n    "nama_brg_simulator" : "PANDORA VPR 3.4",\n    "harga_brg_simulator" : 3510000,\n    "id_product" : "15"\n  },\n  {\n    "nama_brg_simulator" : "INFERNO VIN2",\n    "harga_brg_simulator" : 2970000,\n    "id_product" : "59"\n  }\n]', 'no', '2018-05-14 05:20:39'),
(27, 193, '[\n  {\n    "nama_brg_simulator" : "Venom Turbo Series VX6TO Component Speaker Mobil [6 Inch] VEM-00054-01101",\n    "harga_brg_simulator" : 1310000,\n    "id_product" : "20"\n  }\n]', 'no', '2018-06-18 03:50:06'),
(28, 193, '[\n  {\n    "nama_brg_simulator" : "Venom X Series VX6XII Component Speaker Mobil [6 Inch] VEM-00054-01108",\n    "harga_brg_simulator" : 1595000,\n    "id_product" : "17"\n  },\n  {\n    "nama_brg_simulator" : "VX 3 FR",\n    "harga_brg_simulator" : 1815000,\n    "id_product" : "63"\n  },\n  {\n    "nama_brg_simulator" : "Venom Turbo Series VX603TO Coaxial Speaker [6 Inch]  \\tVEM-00054-01102",\n    "harga_brg_simulator" : 660000,\n    "id_product" : "38"\n  }\n]', 'no', '2018-06-20 10:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_slideshow`
--

CREATE TABLE IF NOT EXISTS `tb_slideshow` (
  `id_slide` int(11) NOT NULL,
  `nama_slide` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `text` text,
  `status` set('all','member','dealer') NOT NULL DEFAULT 'all'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slideshow`
--

INSERT INTO `tb_slideshow` (`id_slide`, `nama_slide`, `image`, `text`, `status`) VALUES
(3, 'Web Venom', 'http://venom.levanpedia.com/assets/upload/slide/slide_apsweb.png', 'http://www.venom-audio.com', 'all'),
(6, 'VPR 3.5', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Aps15.jpg', 'http://www.venom-audio.com', 'all'),
(10, 'new Product', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Apsd5.jpg', 'http://www.venom-audio.com', 'all'),
(11, 'Photo Contest', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Apsd71.jpg', 'http://www.venom-audio.com', 'all'),
(12, 'DJ Performance', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Apsd82.jpg', 'http://www.venom-audio.com', 'all'),
(14, 'Giias 2018', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Apsd91.jpg', 'http://www.venom-audio.com', 'all'),
(15, 'Online Shop', 'http://103.82.242.153/venom/assets/upload/slide/Slide_Apsd102.jpg', 'http://www.venom-audio.com', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sosmed`
--

CREATE TABLE IF NOT EXISTS `tb_sosmed` (
  `id_sosmed` int(11) NOT NULL,
  `nama_sosmed` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `kategori` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sosmed`
--

INSERT INTO `tb_sosmed` (`id_sosmed`, `nama_sosmed`, `url`, `kategori`) VALUES
(1, 'Venom Audio', 'https://www.facebook.com/venomindonesia', 'Facebook'),
(2, 'VENOM INDONESIA', 'https://www.instagram.com/venomindonesia/', 'Instagram'),
(3, 'Venom Indonesia', 'www.youtube.com/user/VenomMania2012', 'Youtube');

-- --------------------------------------------------------

--
-- Table structure for table `tb_subkategori`
--

CREATE TABLE IF NOT EXISTS `tb_subkategori` (
  `id_kat` int(2) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `images` text,
  `tgl_input_kat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_subkategori`
--

INSERT INTO `tb_subkategori` (`id_kat`, `kategori`, `images`, `tgl_input_kat`) VALUES
(1, 'SPEAKER', 'http://www.venom-audio.com/img/icon-spks.png', '2018-03-06 22:00:22'),
(2, 'SUBWOOFER', 'http://www.venom-audio.com/img/icon-subs.png', '2018-03-06 22:00:59'),
(3, 'AMPLIFIER', 'http://www.venom-audio.com/img/icon-amps.png', '2018-03-06 22:03:33'),
(4, 'PROCESSOR', 'http://www.venom-audio.com/img/icon-procs.png', '2018-03-06 22:04:05'),
(5, 'MIDRANGE', 'http://www.venom-audio.com/img/icon-spks.png', '2018-03-26 13:12:52'),
(6, 'Hot Items', '', '2018-03-26 13:12:52'),
(7, 'VENOM EXPRESS', '', '2018-03-26 13:12:52'),
(8, 'VENOM etc', 'http://www.venom-audio.com/img/icon-amps.png', '2018-05-02 14:07:19'),
(9, 'KODE VOUCHER', NULL, '2018-07-28 03:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_testimoni`
--

CREATE TABLE IF NOT EXISTS `tb_testimoni` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `komentar` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user` int(2) NOT NULL,
  `point` int(15) DEFAULT '0',
  `balance` int(15) DEFAULT '0',
  `nama_user` varchar(30) NOT NULL,
  `pass_user` text NOT NULL,
  `first_name` varchar(55) DEFAULT NULL,
  `last_name` varchar(55) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `foto` varchar(200) DEFAULT NULL,
  `alamat` tinytext,
  `no_hp` varchar(30) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `token` tinytext,
  `code_verifikasi` text,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `point`, `balance`, `nama_user`, `pass_user`, `first_name`, `last_name`, `level`, `status`, `foto`, `alamat`, `no_hp`, `email`, `token`, `code_verifikasi`, `create_on`) VALUES
(15, 0, 1050000, 'admin', 'HKwU3sqrhTlBNw5xerH6l8bXlD9ECD2C6Rsxqc7iUK5G6Y8jZyEzvcpH+tN+q2xE6ACo4B4z+sSTgBdSOcVpag==', 'admin', 'mn', 3, '1', 'http://levanpedia.com/vibe/assets/upload/slide/Desert.jpg', 'Jajag', '0998765', NULL, NULL, NULL, '2018-03-19 13:52:11'),
(17, 1000, 500000, 'admin990', 'opKvp5jKAnqVAEfQJqNpt0TG5VRWq8oWSdgR2FB6geZ9g0omHZgUzoCrAXsy55LhV6Ky+F8xdVOvIxV5BfS/JQ==', 'lukman', 'marlin', 1, '1', 'http://103.82.242.153/venom/assets/upload/user/5b5dd859bfb0a.png', 'Banyu', '089876574', 'hamidbanyuwangi@gmail.com', '', NULL, '2018-03-19 13:52:11'),
(18, 0, 0, 'admin110', 'l8gULy8HIhzn8ok9Rw+bJF180FkVDQIyDZwGMXkGmwis1nQep3Z5zda61gIQHDtISnUI5ZFsLw7+9XAiVVFu6g==', 'admin990', 'admin990', 1, '1', NULL, '', '09888777777', NULL, NULL, NULL, '2018-03-19 13:52:11'),
(19, 0, 0, 'wartawan01', '3PBgvkiTQ2tosV15uRzWg+GoXM/rGKL1Tix/algElVxou7njtixhfpOqrJ+HL2oHk/ivyuQ8vJevBPilubgZkA==', 'wartawan01', 'wartawan', 4, '1', NULL, 'malang', '00000000000', NULL, NULL, NULL, '2018-03-19 13:52:11'),
(37, 0, 10000, 'Aji', 'gDxCOS4qylbxyg+BreEuMaAyOyKPkMiuyV/bR75EOWttf7FYZ+Ktv7bZxRm2IWDvU4mbOQCDiPtSLqv7jHb+AQ==', 'Aji', 'aji', 1, '1', NULL, 'Jakarta', '000000000000000000000', NULL, '', NULL, '2018-04-11 03:10:26'),
(61, 15000, 10000000, 'CS', 'VVsjHZK+tt42quyNlGs2yjOBW71XctPXXdLTO4Q1NVM0Slp6Ojt8WmiY1sqdnMf5hhvcVvyNgLDN5y3ZeKOITA==', 'CS', 'Admin', 1, '1', 'http://103.82.242.153/venom/assets/upload/user/5b619f5a75e35.png', 'Jalan ciliwung VI no 5A', '081333260292', 'handoyoyy1@gmail.com', '', NULL, '2018-04-18 03:51:23'),
(71, 0, 0, 'Eko Setiyono', '', 'Eko Setiyono', '', 5, '1', 'http://venom.levanpedia.com/assets/upload/user/5ad9b726e5ad8.png', '', '', 'setiyonoeko1978@gmail.com', NULL, NULL, '2018-04-18 07:28:07'),
(72, 0, 0, 'Liauw Sonny', '', 'Liauw Sonny', NULL, 5, '1', 'https://lh5.googleusercontent.com/-LR1zi9p21g4/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWBmybPOqnsaoaWRDDhR8NL1q_Y0LA/s96-c/photo.jpg', NULL, NULL, 'liauwsonny@gmail.com', 'etdHZ5e-fDM:APA91bHzpJhqRIES7VXnGT4NX34lN4kRbocZ0lVXdO1KkhOBKuwBDDD27ksCFu9Jrj5XGMu66bgJ1fZGbu7PK6-hJN1XhSQplsFt2Ac3WFsWXsx0llpl12BpKoNW97S0Qp2gXjroo98D', NULL, '2018-04-18 11:03:08'),
(73, 0, 0, 'berto berhitu', 'myJFEeQ7XcmpaH2C0zzSjjEuz5UQi86ECRN3gcS7EhLPnfSdnCUO71l8GqO8fErqUoqyfQkMx2ix9IXZXz4vzw==', 'berto', 'ambon', 5, '1', 'http://venom.levanpedia.com/assets/upload/user/5ad7852e16ad1.png', 'jl .setia jatiwaringin', '085695390129', 'nyongberhitu@gmail.com', NULL, '738205', '2018-04-18 17:25:56'),
(74, 0, 0, 'linda990', 'kCkVw+KbYr8J1wjd/0uWpbfRnigthmqHlkZpqPXWHVp5c58/8nwpHvxF/JW2mAkRGROfnehq7ZNtzOXX2kSVww==', 'linda', 'marla', 5, '1', NULL, 'jakarta', '08465787978', 'linda@gmail.com', 'cFRs5V_WWr8:APA91bEg7hYT0QhU-4I9OQCJ6wUL2NKhuiDIrcMoJfvhAgnLNEeB4XCKRJUGUBkSJZqMJa5x-k6ZyhH--3dD4sI9eRsA-iv5Z-Fa4BpiHadGZGcUDMdsoUP7RffLX6w67EsUShp58gsU', '578573', '2018-04-19 03:31:43'),
(80, 0, 0, 'widivenom', 'DQOsDP/C4CwGbovdNyai5hcywpXJmxl69CJjjdnMCAyxPloHIfo9O9Q4FNF+v2vyzMap4dhWiY3H+70DcICNhQ==', 'Widi', 'Kutilang', 5, '1', NULL, 'Teluk Gong', '087882092633', 'widi_kutilang@yahoo.co.id', NULL, '430605', '2018-04-19 08:09:53'),
(81, 0, 0, 'Levan Panca', '', 'Levan Panca', NULL, 5, '1', 'https://lh4.googleusercontent.com/-n2idAkhJYlE/AAAAAAAAAAI/AAAAAAAABPM/iPdE8A10r5E/s96-c/photo.jpg', NULL, NULL, 'panca990@gmail.com', 'c0LUIR2B2bo:APA91bE8c6G7Ofj0Q-miiLE_AOjDelcaP4q3KNAlXa5ZjD26tQpnKlIVkZMaIwkBVtIBa9_kGzXl0m1MQXuag_K8H-GBCDkuevWlsY9hudjTa0wnlh0JlPclMGixzzXftSZmqi83ohc1', NULL, '2018-04-19 10:41:15'),
(86, 0, 10000, 'Din Afriansyah', '', 'Din Afriansyah', NULL, 5, '1', 'https://lh4.googleusercontent.com/-LUuzQcvs6Tw/AAAAAAAAAAI/AAAAAAAAFjc/wAAiPSvTyVA/s96-c/photo.jpg', NULL, NULL, 'dinneno@gmail.com', 'dNAjqlp4IMM:APA91bGWIm7xhrHJMN4KVNRC5JWtce9SmHv29t_ajKyox5FqxO9kehURcHr4C9NRRwgzqJZq0S2Dz3_UxGm7p2Nt8tvAqPt_SuNXINSooHFublGklG9Or55-LTyRKhvooMGbty47Ck0zH5yx8aaNaSIk5LS2GTo-EA', NULL, '2018-04-20 08:27:45'),
(87, 0, 0, 'laurence herman', '', 'laurence herman', NULL, 5, '1', 'https://lh3.googleusercontent.com/-8NASCuxkqvw/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWAuUd4wtesyPg-UfknMCNKdEh8RKQ/s96-c/photo.jpg', NULL, NULL, 'laurence6678@gmail.com', NULL, NULL, '2018-04-20 10:15:08'),
(88, 0, 0, 'Budi Umay', '', 'Budi Umay', NULL, 5, '1', 'https://lh5.googleusercontent.com/-WgakcYCGNRs/AAAAAAAAAAI/AAAAAAAACpE/Ultq-Y4Yg0w/s96-c/photo.jpg', NULL, NULL, 'makroniumay@gmail.com', 'd2qW5yEB8G0:APA91bFw4PgwkHJbpea_GyG1027VCOW4PEvDya0oTPNRGlJzH7GCkQRzDIx5ObDB-eUtUlnAli5UghCeBpCI-K3R4ejFRAsecx_6DJJJJiLT5A15E4Bmmdk26JWLqzICKz8pQxb4kkVT', NULL, '2018-04-20 12:55:41'),
(89, 0, 0, 'Zhafran Akmal', '', 'Zhafran Akmal', NULL, 5, '1', 'https://lh6.googleusercontent.com/-ThpWld-ep3o/AAAAAAAAAAI/AAAAAAAAAAg/A5_uC492tvE/s96-c/photo.jpg', NULL, NULL, 'zhafran39@gmail.com', 'cg1oXY2LlfE:APA91bFAgJ_3sR4NcuVb-9czREHDyqH8eoNr1ek3fTipWpBPWjD_4Sp9WZFlIHbLdvEopDl1CZBeSOYyKks3pBGCzHi-6uVNgIUmpWY3yYqPi-MgZf56saDgYQdGBF1vAWjra0XjQ8li', NULL, '2018-04-20 14:35:45'),
(93, 0, 0, 'Umay', 'tT6LV+o0Og0DIPfUKZpbQZBUbEL0bFTxx1T2+w23c+H+GndTtZTd/nODoUerN79jF9Azrbq3E9YVc2ST8VBqkQ==', 'Budi', 'Umay', 5, '1', NULL, 'Perum Baitul Jannah Blok O-7 Kel Bantarsari Kec Bungursari Kota Tasikmalaya 46151', '082129084749', 'budi_2790@yahoo.co.id', 'd2qW5yEB8G0:APA91bFw4PgwkHJbpea_GyG1027VCOW4PEvDya0oTPNRGlJzH7GCkQRzDIx5ObDB-eUtUlnAli5UghCeBpCI-K3R4ejFRAsecx_6DJJJJiLT5A15E4Bmmdk26JWLqzICKz8pQxb4kkVT', '540469', '2018-04-21 00:02:31'),
(94, 0, 0, 'lika990', '030WqkvNWnw8STIgfkL7Da/h9zOLjqKUndGGXhUtb9DDBMTuZtCoCbCQSnpmkdJNyhi0Qy2D/l2yKVVhyw9xkQ==', 'lika', 'marlin', 5, '1', NULL, 'jakarta', '08848788778', 'lika@gmail.com', 'ear8hFrNd8Y:APA91bHp2dD9HGuMyGfAgnM3K09g0DbCsF9laA33YCOXVoU5hIn7ybuFWSZ4STAeaNMteCSp4tBOCo1NpBg3zQW6xgnDg5oJukkUcA7HH12q0Hg-vStqU0R6YsEh8rnbeB-Puw84L2VC', '737288', '2018-04-21 02:31:32'),
(97, 0, 0, 'Michael Tjandra', '', 'Michael Tjandra', NULL, 5, '1', 'https://lh4.googleusercontent.com/-Yc09bB5da4g/AAAAAAAAAAI/AAAAAAAAAGY/LtDdyrcyX5Q/s96-c/photo.jpg', NULL, NULL, 'rafael.michael.tjandra@gmail.com', NULL, NULL, '2018-04-21 04:51:51'),
(98, 0, 0, 'Muhammad Abdul Hamid', '', 'Muhammad Abdul Hamid', NULL, 5, '1', 'https://lh4.googleusercontent.com/-AiLmLLicOrQ/AAAAAAAAAAI/AAAAAAAAAWY/MwaYKBTngLc/s96-c/photo.jpg', NULL, NULL, 'hamidbanyuwangi@gmail.com', 'c_rClJH976g:APA91bFxBi0AAXOBnQIC0CWIK7R55HSY7yoQB2P07bvN5Fc5CUya6SanNnzGDv7gdU2CkfrQQ-glAFh-3RqSmn3zXUnS4HsYj_XNy1VdLJCUYDY6YY7om7_8BoEM8ZGsdsqPit2GMwz6', NULL, '2018-04-21 05:27:59'),
(99, 0, 0, 'Januari Sakti', '', 'Januari Sakti', NULL, 5, '1', 'https://lh3.googleusercontent.com/-qztacdqdyPw/AAAAAAAAAAI/AAAAAAAAACA/g-zr22YlW_c/s96-c/photo.jpg', NULL, NULL, 'rikie1106@gmail.com', 'cqvOjpeLuXM:APA91bFy6tMot2Au0tgXJsx_s7q-Xplbf1sCfRbR71CbAW96V2zLWXG4ehj_hfAoW5Cao8SgV6PgvlhDSdgVh-ot9b4qRm4eA2UQDf74Aysc4Mt3DzAp6PqCTsrkfW938Nz9vORgbCxb', NULL, '2018-04-21 08:29:11'),
(100, 0, 0, 'Rina', 'AJH/ZAFicw5pkvwq604GKy0ICH2u6Zp1ts/SU7PeDMl/0vLs7riCEIHTc5nMN/EnKPS5ZagJi28pY+IlXOYOqg==', 'Rina', 'Rina', 1, '1', NULL, 'Jakarta', '000000000', NULL, NULL, NULL, '2018-04-21 08:37:28'),
(101, 0, 0, 'Aldi Siahaan', '', 'Aldi Siahaan', NULL, 5, '1', 'https://lh3.googleusercontent.com/-l8jeyLXD3v0/AAAAAAAAAAI/AAAAAAAAAGY/Sd44uncUNNg/s96-c/photo.jpg', NULL, NULL, 'renaldyfederik@gmail.com', NULL, NULL, '2018-04-21 08:57:11'),
(102, 0, 0, 'putu wisnu', '', 'putu wisnu', NULL, 5, '1', 'https://lh3.googleusercontent.com/-OKXJzhXAF5c/AAAAAAAAAAI/AAAAAAAAABc/tb1jRcV-lig/s96-c/photo.jpg', NULL, NULL, 'vrtroppers@gmail.com', NULL, NULL, '2018-04-21 09:49:51'),
(104, 0, 0, 'Lucas Casper', '', 'Lucas Casper', NULL, 5, '1', 'https://lh4.googleusercontent.com/-KIvVzRG4-rI/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWD0LZalucjyppbhL883z3nRWJsWYA/s96-c/photo.jpg', NULL, NULL, 'lucascasper841@gmail.com', 'feKJoFY47RI:APA91bGrXNdZde1qUF4v8KZtCx5qYE-hpcm0s1lKbCA_vyqgZsNwc_J5PRcZhkrywedBPsZ0Ox2ERC_n98Uh37M71r8n-oZmrrv5Kll-k4omTl5s_CT4bVnP4vvNk-LO8MaGQBRasQEX', NULL, '2018-04-21 10:07:34'),
(105, 0, 0, 'Hari Yulianto', '', 'Hari Yulianto', NULL, 5, '1', 'https://lh4.googleusercontent.com/-Gk15KamME6w/AAAAAAAAAAI/AAAAAAAAAEg/R2JUJonqZVk/s96-c/photo.jpg', NULL, NULL, 'hari.yulianto.sdj@gmail.com', 'cA1V4240CMs:APA91bFncGa3KZzRHvNOGZCs-lVRayZMzCEpcnktjFU40NX3geJBDdAyA2WJ23ORS3Bd_A6XpBld95JM5VSnr5lpDbsf6uRFyNsOvLPUkuishtcV7KMZ_uEWNUXPIsTAAi3_5H5VeAO4', NULL, '2018-04-21 11:07:13'),
(106, 0, 0, 'Martiono Insentius', '', 'Martiono Insentius', NULL, 5, '1', 'https://lh3.googleusercontent.com/--MZHt7myDXY/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWBW0Y36ElHriyoUf8LlC1JlAVHqgw/s96-c/photo.jpg', NULL, NULL, 'martionoinsentius01@gmail.com', 'cwkyfTI4Cfo:APA91bEszziGz9stSdgggq5Pk2fwBkqG4BLY46Z-92uFtlG7Onz2k0LiQsPi_4HqPRFHv5WlmaQ0WTfBYacu4FJv2l-SLtUNf3X1V5na6TcCs8bTd89KfHAUzU7u20Thi2kq4Bc40EGe', NULL, '2018-04-21 12:06:26'),
(108, 0, 0, 'Acakadut', '', 'Acakadut', NULL, 5, '1', 'https://lh3.googleusercontent.com/-yHDMXm943F4/AAAAAAAAAAI/AAAAAAAAAIs/7-fIwGl1H8E/s96-c/photo.jpg', NULL, NULL, 'kholickavengers12@gmail.com', 'dlIyv5ySK44:APA91bEUCLduN_ds_Al8iw0WksvdvCLI2rdLVDrttvGA4Y26NNHlbATgH97sDEOS6OJlE4-5QYuguSb_twdsOJ22m4b36c9k43Vz0KfzvY5S4Mxj3KZeG_uvRMG3FjjjLT3xd_q1pUxg', NULL, '2018-04-21 12:39:58'),
(109, 0, 0, 'Handoyo Yanuar', 'VHoFKlkbucg+PmYdCV4juG+RJx21Ir6hDxIrDmauA58KqPI+LuC0xEio/SeN2jifLXKu1oH+eG95VyCOyYYXnA==', 'Handoyo', 'Yanuar', 5, '1', 'http://venom.levanpedia.com/assets/upload/user/5ae3f04b1508f.png', 'malang', '081333260292', 'handoyoyy1@gmail.com', 'fzwtjdLD1iU:APA91bFbu23J4a-rlP1WSW1drE3z8Qfcc4-0KrLI_N3rSDnQLPw8SnHa96PBSUxSM2lqgN0JXce4umusUokVQ7w_hU4lkQzbUMnSfmIwqoXgEhdneF1VIQfvh8Q3_S7kGC38r0xhZj9-', NULL, '2018-04-21 12:53:51'),
(110, 0, 0, 'Miftada Handoyo Yanuar', '', 'Miftada Handoyo Yanuar', NULL, 5, '1', 'https://lh6.googleusercontent.com/-x_yoPggkI7k/AAAAAAAAAAI/AAAAAAAAAlQ/ssak_3KC2W0/s96-c/photo.jpg', NULL, NULL, 'miftadayanuar1@gmail.com', 'ematiqLeIkg:APA91bESjnG4fzApIO4OTCxT4wFislCk3KHar3JJYv8Gj83wnuJAy-SG9Yop9Ml8kiUhpvehcy-BLrkUVwi7PAR_9rKCk7K_VJvmTDU8FAn-O0iakAlRoDtBCieyilBPCJtuF9S-ayht', NULL, '2018-04-22 01:26:57'),
(111, 0, 0, 'Koko Fly123', '', 'Koko Fly123', NULL, 5, '1', 'https://lh3.googleusercontent.com/-CoHqtQbn8ak/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWC5997GcZpL16EMdM_CgskvK7gf5g/s96-c/photo.jpg', NULL, NULL, 'kokofly123@gmail.com', 'fOPqGz0nFtw:APA91bE4YGw4pD92YX7rDraEhtPyO26VT4I0rKBk6N5Q9wgMtbze0i7R0hr2N5PfchQLj5Sbhvy3C7fMNyNkE6or-MVEwFdiYq2cVoCr83aq-DNUDkxFT_a8K7gqvJmMlH3VK3AznRNC', NULL, '2018-04-22 01:30:45'),
(112, 0, 0, 'lukaz990', '', 'malika', NULL, 5, '1', NULL, NULL, NULL, 'lukal@gmail.com', 'oihui5665t68kijyuf678i', NULL, '2018-04-22 01:53:49'),
(113, 0, 0, 'lukali990', '', 'malika', NULL, 5, '1', NULL, NULL, NULL, 'milkanan@gmail.com', 'oihui5665t68kijyuf678i', NULL, '2018-04-22 01:55:17'),
(114, 0, 0, 'Keshi key', '', 'Keshi key', NULL, 5, '1', 'https://lh4.googleusercontent.com/-faltYF4BAEo/AAAAAAAAAAI/AAAAAAAAAAo/DWCy8PmvWTo/s96-c/photo.jpg', NULL, NULL, 'keykeshi@gmail.com', 'cU0xHH9X0HY:APA91bGC6-_65TLw8KuONEzOPcI2MPBEP70ShbCl_dOttLqbjajRRZjW0o5Aj2Hp5aeER8wVQD4wue4nN8AZUepKjJGeA0Y7wT_D-Up1iURxjlVuNhbpLp354VdU5zFPFNfUfW8qoIh54jYGE9vw-lqR6vBjRNmZNw', NULL, '2018-04-22 03:43:47'),
(115, 0, 0, 'Kodir Venom', '', 'Kodir Venom', NULL, 5, '1', 'https://lh5.googleusercontent.com/-ULzA28yFi_Y/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWC3R6rjqOD0IHi-4qeQDV7FucRfpg/s96-c/photo.jpg', NULL, NULL, 'kodir.venom7@gmail.com', 'dgffXKLyGDg:APA91bGlQi7qoQ-Lpi2MzrACMp1hjcKVjNXzi09GA3qlvPzYiEM2n-4zOJ2rdmHDlg3rQAypm4n8wNA6bCcSFP4FR7urUky-trnEt_69z7yV6dp7Ichng3Ff5jPiCYiBlZluwesVZ-HP', NULL, '2018-04-22 03:51:23'),
(116, 0, 0, 'Yudi Setiawan', '', 'Yudi Setiawan', NULL, 5, '1', 'https://lh4.googleusercontent.com/-5ahDnIAckkA/AAAAAAAAAAI/AAAAAAAAOf8/4O5D6ko98LA/s96-c/photo.jpg', NULL, NULL, 'yudicoconuttreez@gmail.com', 'dXBLWBuglhM:APA91bEiqZPl9zW8t32p_saWL8O9_eSIQWloA9XtsApP2jlGChPFuDLXVrb8W1mPQ1phuqRDdiPw4w8goGX8DKJkZpQ38ZL1FsXrCBkCgjOV2X0xF30E8Yja-7h3kBhtsMKSj1zvLMQO', NULL, '2018-04-22 03:51:54'),
(117, 0, 0, 'Iqbal Jimmy', '', 'Iqbal Jimmy', NULL, 5, '1', 'https://lh6.googleusercontent.com/-3nXCiTzZOP8/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWBn-6uGevEjkuQEhzSzrBp3DSU6Xg/s96-c/photo.jpg', NULL, NULL, 'iqbaljimmy71@gmail.com', 'eavcKYfnvpU:APA91bFjZHOG8F-4TLPjudP6PkFi_tyzmkkaBLmOcj2Rpgo6S6uVrEQeX4-GfrnvrGmZBnELVFYoirJsi1fZF9GndQ_X6KeCHlhD1qq6VwCoFrWfFKu6yCAseAuf_155CX5MxzSzI4YF', NULL, '2018-04-22 03:53:21'),
(118, 0, 0, 'nanang reliana', '', 'nanang reliana', NULL, 5, '1', 'https://lh4.googleusercontent.com/-zE8n2dAqYkA/AAAAAAAAAAI/AAAAAAAAAAc/5wB4MRqPM0E/s96-c/photo.jpg', NULL, NULL, 'nanangreliana1980@gmail.com', 'f9dvg3FZuw4:APA91bEbyhc-owajHC5LHa0wkreWpwRsiyCRNZRshMX6ihGIItR1PDyEPyRe7JKSqYifGYMFiaDR-O-ILqd3FTjAqz2bKeHsCEKH06T8EQFYxeLmiodfje8kthktge1c29Aa38OwLOcU', NULL, '2018-04-22 03:57:24'),
(119, 0, 0, 'Bobypalvenom Palvenom', '', 'Bobypalvenom Palvenom', NULL, 5, '1', 'https://lh6.googleusercontent.com/-pMijky8FqLc/AAAAAAAAAAI/AAAAAAAAAA4/jcDavCVYw-w/s96-c/photo.jpg', NULL, NULL, 'bobypalvenom@gmail.com', 'e-KOKASHa30:APA91bGchKWkPBzDMNHoCuBBimFrGU2y1eHPEd58Dtubp7RtMJSr_-lPe2hlJGyn_yGPsW1AUgYE8VvREB_fbB6Wt7eMSC67ZxWmJRZmfTiDEIRm_OR1UxymwMi4TtY9i6z1-TvNX421', NULL, '2018-04-22 04:41:20'),
(120, 0, 0, 'Kevin Tang', '', 'Kevin Tang', NULL, 5, '1', 'https://lh4.googleusercontent.com/-sPEB17H_pyA/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCNkhsPGqA8PwgcwlHOiZ4APg70Gg/s96-c/photo.jpg', NULL, NULL, 'kevintangd@gmail.com', 'dam1vkTg-is:APA91bG6KicgQ6TAcSSUPWoDmT_EJCGbMFkzh2xhb7YE3Ms5EEySOaEOfuruPI2slaz8Gq-I8_i3C-lvtj2PjPCxTUxjPi5Zw9Vm3fQPGCb9K8aBgPWitJExEgci1JozKbSI9LaCc_Fc', NULL, '2018-04-22 04:48:20'),
(121, 0, 0, 'Widi', 'D6yBz+NdXO/cZB9656Bl4PSg3YIFumZnaCnIYTNdDOj1t2/JBFmoAScUZrtQmgxzFvzK6Kh4EyuKrjmlvV3NUQ==', 'Widi', 'Widi', 1, '1', NULL, 'Jakarta', '087882092633', NULL, NULL, NULL, '2018-04-22 05:07:47'),
(122, 0, 0, 'zaka venom', '', 'zaka venom', NULL, 5, '1', 'https://lh3.googleusercontent.com/-6sJCG7yJ5pY/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWA71FaTQXIudxQ_9HNSQ89BW3x1pw/s96-c/photo.jpg', NULL, NULL, 'zakavenom85@gmail.com', 'dBcO7gJ-ZbQ:APA91bHa9Qo5JrFeYHQqxmeEs38h0jLpnRwR1QMITaO66Dx5ak8QU1ruH5RAwd5x119SF-w4nxbU9NYCBCiG5BtwBWaAVoA9KGTEV_iv7v-UjImmrzoXs_Y9WMKLYHH4Z7Ob04_GWAlF', NULL, '2018-04-22 05:43:29'),
(123, 0, 0, 'Muhammad Samsudin', '', 'Muhammad Samsudin', NULL, 5, '1', 'https://lh6.googleusercontent.com/-7_6roTyCsVs/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCHIjVv7jbkSnSTH4L1UhZ2J2gqmA/s96-c/photo.jpg', NULL, NULL, 'msamsudin797@gmail.com', 'feastK347io:APA91bEAGVSQH5EhfsiISftrS6eudeefCf-N6pclW1WKFS05adnBm_3sc54gMZqKlrRvcjs2ee0FOuQDPbjwYCK_UR5DeElSIoiPxjkVfFb8kZ6mXlOJCxPkI2HyHVdIG42DGeOnYwpO', NULL, '2018-04-22 06:33:29'),
(124, 0, 0, 'george aryo', '', 'george aryo', NULL, 5, '1', 'https://lh5.googleusercontent.com/-wmWGoL-wqpU/AAAAAAAAAAI/AAAAAAAAACE/ZzK_f3SZ6uw/s96-c/photo.jpg', NULL, NULL, 'aryotombeng@gmail.com', 'f2M6c1cyR5I:APA91bGDLA_9yoLV1Aderoyl_PATHQN5KX0t5KbWPqnKoBgmPbE00FJk355f24zxGtHodFp-YZ2mQ651xPhGhmqElKQlA5szMgF3KAuHjq85PdWd7v4fPsodaGGYzZ6AD9fctRHkKJV5', NULL, '2018-04-22 06:34:09'),
(125, 0, 0, 'Muhamad Tohir', '', 'Muhamad Tohir', NULL, 5, '1', 'https://lh4.googleusercontent.com/-NbezThAqIwI/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWAfv4iqI0RD24PDs26to1ANFc_5NQ/s96-c/photo.jpg', NULL, NULL, 'muhamadtohir040212.mt41@gmail.com', 'ej1Afn6u9mk:APA91bETrhSTpiwtB6SQ-yH7lZjB1y5nrF6G3XY0Q4Vhzb1F5CTfurz12OX7oodOFp5jtLvfKzgLj4_rjyvQQ_wWvGfuSNzwZKE9MSuZd7-pmsIcDOfMq07iSv1TnEMpgD5COwb0gHvY', NULL, '2018-04-22 07:26:28'),
(127, 0, 0, 'deni suharta', '', 'deni suharta', NULL, 5, '1', 'https://lh5.googleusercontent.com/-U8m9vrPtPyk/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWDMg7O5ghgdqloH8mDfx5qdj-A_rA/s96-c/photo.jpg', NULL, NULL, 'denisuharta@gmail.com', 'dxBAXfL36jU:APA91bEHlZWJzKP4te2VEA-9fBuybSVfhCj_Spd9z2L_iczYp274qF7m6WQKt_0SGxpTApHVsngEFY_-ItmJNyE7yDPJ7H3_T1ykmDjOS513QlmuN2NEYaRb5E0U8EEDHEhfH9iV7fxL', NULL, '2018-04-23 07:35:56'),
(129, 0, 0, 'winda windol', '', 'winda windol', NULL, 4, '1', 'https://lh4.googleusercontent.com/-INCR1ddaGTI/AAAAAAAAAAI/AAAAAAAAADE/QV70QOL_pxg/s96-c/photo.jpg', NULL, NULL, 'winda705@gmail.com', 'crz0QD7gcQM:APA91bFj4kLjavCqzS5hL2aslC7jgHIedsWbziaXCADAuJN_S6SisLIvivIExZ_c5S7OaBBXDnny9I4EMSPfeXJl9xcGv9ZzWoP4vCP3opYus_Ac5JjSrR_BCAk4EubPls579uND0rSF', NULL, '2018-04-25 00:46:35'),
(130, 0, 0, 'Dhimas M F N', '', 'Dhimas M F N', NULL, 5, '1', 'https://lh5.googleusercontent.com/-zkJmNTjjG0c/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWAydoUsFhJC77ku7HuJfgit591BTQ/s96-c/photo.jpg', NULL, NULL, 'dhimasmfn2.27@gmail.com', NULL, NULL, '2018-04-25 08:19:12'),
(131, 0, 0, 'lukman990', '1PyDP1bwb/aCsKL+CuW01C79EXCh5/JRDHTvu0XYvVA6UUU9lC6OmoVw2QH/OjgCeUGkzicTTQxviz3dUwu8Kg==', 'lukman', 'marlin', 5, '1', NULL, 'jakarta', '08136697997', 'lukman@gmail.com', '', '653773', '2018-04-25 13:26:20'),
(132, 0, 0, 'Joni Setiawan', '', 'Joni Setiawan', NULL, 4, '1', 'https://lh4.googleusercontent.com/-fhA1bD3797c/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWAsWWXnce2dz6EK2bX752AVYcruJA/s96-c/photo.jpg', NULL, NULL, 'setiawanjonni22@gmail.com', 'cFm64isdU6w:APA91bEIKqF1c1OQEQL1cpiTv51-WVPpl3FXzKaQBGSerikP6aN3Bc2dO6Z55t8d1ChpsVvdOgNGennLsF9wnMPa7imKVRNIDjqU1ZV2mGSm2GWOHgx276F3IuBEbrRE9_emT9Bg2epG', NULL, '2018-04-25 13:54:56'),
(133, 0, 0, 'dealer', 'wlwOaDSx5mm7IgXyjWY0zv44EcIeQzsp4axDxvXK4QG2L7QM8DvA84dfBYHCOMkbFEhCu4+V9znMwqTY5PEy3g==', 'test dealer', 'deal', 4, '1', NULL, 'test', '00000000', NULL, NULL, NULL, '2018-04-26 00:52:46'),
(134, 0, 0, 'wartawan02', '8G9BPzfVChWzt0Gwp2hjviZ/C3QdSdrJRrgI5yp8V2PSQCLSFq7Wzx/UND7PPLkRqOys78AiiW/GNhX9gAJ1cA==', 'wartawan02', '02', 2, '1', NULL, 'wartawan jakarta\r\n', '0000000', NULL, NULL, NULL, '2018-04-26 01:19:12'),
(135, 0, 0, 'eka lestari', '', 'eka lestari', NULL, 5, '1', 'https://lh4.googleusercontent.com/-P1VXV2UO1tg/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWDmJT06-4e_PRp1jCdpBdv3xQ9-Ww/s96-c/photo.jpg', NULL, NULL, 'lestarieka2009@gmail.com', 'e1GySQruyK0:APA91bEkP7YCofTl4Q7ColoCskLrYLncIDOoGeXdgo4P-WVENnsHYgDFtuhhMoivx70oFeQKMe7gr0n2lRHPzVbkA14iZBKR7MoT6Gq28XE7MRy-cWatLicC2aDYcY3jFtpvMEJAa8G_', NULL, '2018-04-26 09:11:36'),
(136, 0, 0, 'Christian Pandis', '', 'Christian Pandis', NULL, 5, '1', 'https://lh6.googleusercontent.com/-3Myr9cI-64Y/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCTCamhbgxsd1rth5yPElLyygyMZQ/s96-c/photo.jpg', NULL, NULL, 'kuncirman@gmail.com', 'ceMtd0RaYjc:APA91bGJ68YWcougF8PXd0KT1CQf5tK2VGZzEZ_UXn5aghOMBL52R3tJz6ndPeq-0yO4QajyWGH2B13rGIgDfHVXKwvUHzvm0v0rNX29tUfUshn1YMJzN8fz3QlcszQT3ROsua1fMn_O', NULL, '2018-04-26 09:44:31'),
(137, 0, 0, '', '', NULL, NULL, 4, '1', 'https://lh4.googleusercontent.com/-QcAYtwbcjMk/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCDga58HVmz4e5oOXEHH7CCdPnrFw/s96-c/photo.jpg', NULL, NULL, 'himawancahyoeddy0305@gmail.com', 'dbCQJ2abneE:APA91bEZytcfNqXv0RQazB4V24bz6_vkH3kexzJw4pni7g3il4xBw8aHg9oz5Y-1wASly10m7kPJdly6Gh8Imj4_CNI4TOJCfhL9HNQ2MPrO7jrIX3IrTnrD8TCtfwSsAVUBdyiEsG9b', NULL, '2018-04-26 15:39:07'),
(138, 0, 0, '', '/3CJjBrUiwfszxflgt2xaIkrT2LGuUhQz4PCrv6XyYI6oUUCmI8cqcCURVi+12pD2kZ++/Pv1LnqNJjNt0rkkg==', NULL, NULL, 4, '1', 'https://lh3.googleusercontent.com/-ITIjTVXwor0/AAAAAAAAAAI/AAAAAAAAACg/L-qc_nNAAwY/s96-c/photo.jpg', NULL, NULL, 'sbmibanyuwangi@gmail.com', 'fsNsrOpqcOE:APA91bFNWNv_1sO-IM241hk9de_ZiKv-WXPphhSqVhViVdHH98hiE7ummAF35nmrSnJA7yclO89RVAqZ3yx3CwxID5RrOMopZynbso9t5n7dX4rsE8SAqppjpFvPEOVOWdYOy-rPfjrg', NULL, '2018-04-27 08:15:39'),
(139, 0, 0, '', 'uGGECQ3DeeIT6ORU5nL4mrR0GGypKUMrnQ7FVFU2Q88E/O4BfTFcCouR9g2kgM1qY1hQY+6XxZNinMI58L+rIA==', NULL, NULL, 4, '1', 'https://lh5.googleusercontent.com/-gMxdOk3wqFE/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCVTtn-Q_0Y86RPdRzSXVWNRjSggg/s96-c/photo.jpg', NULL, NULL, 'zhuly212@gmail.com', 'fhXR87t6YG4:APA91bHsZMDGhq_5cTcAuE9PWtkQCvQpjGTmDnhITVdgXBaay1xtga2Tqow5Cz6_EiRwwEyoCGKaN39iszgpVnXgOR0ToVxIFv3eXLh4t491og9OpQyHpp_P-vxxrwm5dHKtqegiP-2o', NULL, '2018-04-28 10:06:26'),
(140, 0, 0, 'deny', '8riT0geMjjwhiu90a4H90Z6JgqQpE6f2Gh/+qBhoQrnaLM/dxC1kRxBOh3BLoWLE6eZDIvuEohj4lGWuG/Ebtg==', 'Deny', 'Venom', 3, '1', NULL, 'Jalan Taman Pluit Murni 2 no 11 A\r\nJakarta Utara', '087888783666', NULL, '', NULL, '2018-07-28 06:47:26'),
(141, 0, 0, 'admin4', 'g6ur38kso7pclSG30a37MO8Tq6uK2zmf4/fmYcBLYwBmtMvO5c0/gZwZhZdXCSctlvJOfakfIPhxoxz+MR7XzQ==', 'y', 'y', 4, '1', NULL, 't', '000', NULL, '', NULL, '2018-07-28 10:39:00'),
(142, 0, 0, 'deny_d', 'XXBX+bde8+i22gsLTRGzKkxbNlKlTW7Xa6eZGR861DGUNE9wqTdR+2lpVxxbVAGxabBRjKGJ7tBMLDTjFBrcwQ==', 'Deny', 'Dealer', 5, '1', NULL, 'jakarta', '000000', NULL, '', NULL, '2018-07-31 02:25:47'),
(143, 0, 0, 'denyv', 'EKL40ekWnyjFie10WDEEPOYtf9BbRrP7YG3Oo3uwA+65XpLgmJowr3qpl+CSksPbZv6afIJZ09u2RImwmShJfQ==', 'deny', 'VFC', 7, '1', NULL, 'jakarta', '0000', NULL, '', NULL, '2018-07-31 02:27:46'),
(144, 0, 0, 'Bluecore Suite', '', 'Bluecore Suite', NULL, 4, '1', 'https://lh3.googleusercontent.com/-Mk60LjeC_O8/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWA4EiovCYIly97_2Z7oOFOspjdOBw/s96-c/photo.jpg', NULL, '', 'bluecoresuite@gmail.com', 'eGQz5V24r2A:APA91bG7PprHX6LsuK9zYa8R_cEDM7Scbqu5zDoQQCp86DEjx_t84oAR34Zzy4kcdkH0rUKa3q6j8p5cUYpCS1wPATukAYldoZ8kiXP0QNudVLy7-pu4sgo059Q7KT-HrVzPuVmBjzBkrogTcfi4GqDYRaS16-QtbQ', NULL, '2018-08-01 15:06:00'),
(145, 0, 0, 'andini990', 'g4VTq6Uj7OYcxJmyD6GNKHqUD6gwzYPqP52jJVu9Qiw/b/8YocME28vEZUZyJRFCAGZUamXlnVz5UKE5qaPUxA==', 'andini', 'andini', 4, '1', NULL, 'jakarta', '081330669855', 'andini990', '', '348785', '2018-08-01 15:44:09'),
(146, 0, 0, 'denyuser', 'UJ6do/H5ImbWLt1Ez1EnmjsBxMsSJ6srmoyurrYuVrI7Nsgkg5Cxb+ideO3P2orewtdtQs0zxnffP6jir2C5Hg==', 'deny', 'user', 4, '1', 'http://103.82.242.153/venom/assets/upload/user/5b6428c5dd6f4.png', 'jakarta', '0000', 'yokonazo@yahoo.com', '', NULL, '2018-08-02 06:01:13'),
(147, 0, 0, 'indah990', 'fzORm53h6XA+2uvRuPgGRrqZa5oxHlK/WAVwqx6ccL9ljL3EQyZf8IBn/APm1WE4yk7MtpaRi8Ugz39F4zS/LA==', 'indah', 'permatasari', 4, '1', NULL, 'jakarta', '0813303699487', 'indah@gmail.com', '', '917145', '2018-08-03 04:05:27'),
(148, 0, 0, 'Din', 'V+HFFQ0mF/JMg0A1NpNCz/lBMBC+8mOqrocU/WxieswZOOTE6FGAcij5S1Z3OZubJ5g08Tpr6JionHqSOAXX/w==', 'Din', 'Venom', 5, '1', NULL, 'Jakarta', '00000', NULL, '', NULL, '2018-08-03 07:17:33'),
(149, 0, 0, 'haha', 'vkw5WOg8ZOU3DAT7sg4cA0bH4whRHfIzxCmeooNRoTHbxU87S3XoJru+A0JNldXw/p0scRkSVk6DRhkn3AkEnw==', 'hahaa', 'hahaha', 4, '1', NULL, 'jakarta', '000000', 'hahaha', 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjM2OTExYjU1NjU5YTI5YmU3NTYyMDYzYmIxNzc2NWI1NDk4ZTgwZDYifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdmVub20taWQiLCJuYW1lIjoiS2VzaGkga2V5IiwicGljdHVyZSI6Imh0dHBzOi8vbGg0Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tZmFsdFlGNEJBRW8v', '892395', '2018-08-03 07:26:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_video`
--

CREATE TABLE IF NOT EXISTS `tb_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `judul` varchar(75) NOT NULL,
  `deskripsi` text,
  `foto` text,
  `url` varchar(55) NOT NULL,
  `status` enum('draft','publish') NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_video`
--

INSERT INTO `tb_video` (`id`, `user_id`, `judul`, `deskripsi`, `foto`, `url`, `status`) VALUES
(1, 0, 'Komunitas Daihatsu Ayla Indonesia (DAI) [Venom Audio]', '<p>Komunitas Daihatsu Ayla Indonesia (DAI) [Venom Audio]</p>\r\n', NULL, 'hovX7cpPiUs', 'draft'),
(2, 0, 'Venom Audio Demo Song', '<p>Venom Audio Demo Song</p>\r\n', NULL, 'DN0xzbQON48', 'draft'),
(3, 0, 'Venom Audio express On Blibli.com', '<p style="text-align:justify">Venom Audio sudah tersedia di Blibli.com, termasuk produk terbarunya yang bisa membuat audio mobil kamu semakin baik cukup dengan 30 menit! Nah semuanya itu bisa kamu beli di blib.li/venomaudio-yt</p>\r\n', NULL, 'mvzYcc1Pd8k', 'draft'),
(4, 0, 'Gilaaak upgrade Audio murah dengan Venom pandora 3.4 plug n play instalasi', '<p>Gilaaak upgrade Audio murah dengan Venom pandora 3.4 plug n play instalasi</p>\r\n', NULL, 'OtY3nOMJfT0', 'draft'),
(5, 0, 'Venom Talent RX Audio Project Denpasar', '<p>Venom Talent RX Audio Project Denpasar</p>\r\n', NULL, 'IOUUgYKz6ns', 'draft'),
(6, 0, 'Autorave Bali 2014', '<p style="text-align:justify">Venom present... the Automotive and Rave party in Bali with performences by DJ Yasmin, DJ Mbonk, DJ Denyz Andrew, DJ Hanna Wo, sponsored by Oxone, Cozmo, SL plastick, Amore, HID Provision, Hurricane, Drive</p>\r\n', 'http://www.venom-audio.com/images/imageuploads/6fb239f3d743f3ccf8daab31f3b27bd7.png', 'it5REff5HQQ', 'draft'),
(7, 0, 'Slideshow Beijing 2013', '<p>Venom tour with dealers and distributors, this in compilation photo, please enjoy.</p>\r\n', 'http://www.venom-audio.com/images/imageuploads/d6de33f4c71d3c64b51c0a330807d71d.png', 'vRpCWNIhFLU', 'draft'),
(8, 0, 'VMCG Rekor Muri', '<p>The Venom Modificartion Community Gathering 2013</p>\r\n', 'http://www.venom-audio.com/images/imageuploads/5a9ff937a945afdc483e42f30d5d78f2.png', 'y2C28U5M84A', 'draft'),
(9, 0, 'Venom Product', '<p><a href="https://www.blibli.com/brand/venom">https://www.blibli.com/brand/venom</a></p>\r\n', NULL, 'NGxnS_hyeYE', 'draft'),
(10, 0, 'Cara Menghubungkan Venom Pandora ke Gadget Melalui Apps', '<p>ini adalah cara menyambungkan gadet ke venom apps bagi pengguna Venom Pandora VPR 3.4 Feel the 3D sound stage with extra bass</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'tFcF93jRjAg', 'draft'),
(11, 0, 'Modifikasi Xpander By Venom', '<p>Xpander pertama dengan Venom Pandora. Feel the 3D sound stage with extra bass. dengan pemasangan Plug and play.</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'x6Spr0HoGEE', 'draft'),
(12, 0, 'Atasi Macet Tanpa Bete Dengan Venom VPR 3.4', '<p style="text-align:justify">Kejebak macet di jalanan ibu kota? Atasi stress karena macet dengan mendengar musik favorit kamu. Didukung dengan perangkat audio yang berkualitas, penyanyi idola kamu se-akan bernyanyi tepat di depan kamu. Solusinya adalah Venom Pandora VPR 3.4. Feel the music become real.</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'hP5tI6MBsao', 'draft'),
(13, 0, 'Venom Pandora VPR 3.4 Video Commercial', '<p style="text-align:justify">Dengan prosesor Venom Pandora VPR 3.4 memungkin kamu melakukan penyetingan audio melalui smartphone. Untuk kamu yang mobilnya masih standar, Venom Pandora VPR 3.4 adalah pilihan yang tepat untuk mobil kesayangan kamu. Karena dengan system plug n play, tidak ada pemotongan kabel, sehingga garansi mobil kamu masih tetap aman. Dan yang paling penting, kamu tidak perlu melakukan pergantian speaker. Venom Pandora VPR 3.4 solusi cerdas untuk kamu yang ingin menikmati musik selama perjalanan, tanpa keluar budget yang besar.</p>\r\n\r\n<p>Follow sosial media Venom :</p>\r\n\r\n<p>Facebook : <a href="https://www.youtube.com/redirect?v=MlV6ngN8YTo&amp;redir_token=H5jn_T0_1kIb9BjymIrfyW0j2T18MTUyMzMzNTE3OEAxNTIzMjQ4Nzc4&amp;event=video_description&amp;q=http%3A%2F%2Ffacebook.com%2Fvenomindonesia">http://facebook.com/venomindonesia</a></p>\r\n\r\n<p>Instagram : <a href="https://www.youtube.com/redirect?v=MlV6ngN8YTo&amp;redir_token=H5jn_T0_1kIb9BjymIrfyW0j2T18MTUyMzMzNTE3OEAxNTIzMjQ4Nzc4&amp;event=video_description&amp;q=http%3A%2F%2Finstagram.com%2Fvenomindonesia">http://instagram.com/venomindonesia</a></p>\r\n\r\n<p>Website : <a href="https://www.youtube.com/redirect?v=MlV6ngN8YTo&amp;redir_token=H5jn_T0_1kIb9BjymIrfyW0j2T18MTUyMzMzNTE3OEAxNTIzMjQ4Nzc4&amp;event=video_description&amp;q=http%3A%2F%2Fvenom-audio.com">http://venom-audio.com</a></p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'MlV6ngN8YTo', 'draft'),
(14, 0, 'Launching New Product Venom VPR 3.4, VPR 4.6 and VPR 2L (LIMITED EDITION) #', '<p style="text-align:justify">New Product Venom VPR 3.4 30 Menit Plug n Play - Done Clear Launching New Product Venom VPR 3.4, VPR 4.6 and VPR 2L (LIMITED EDITION) <a href="https://www.youtube.com/results?search_query=%23IIMS2017">#IIMS2017</a> ) Series VPR 2L (LIMITED EDITION) - 300 Unit di dunia warna emas, ketersediaan 2 Bulan kedepan CP Phone : (021) 6693936 EMail: venom.mania@gmail.com</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'PApbGcOFDwg', 'draft'),
(15, 0, 'Venomous Star : Aurel Hermansyah', '<p style="text-align:justify">Venom celebrity, Aurel Hermansyah use Venom products pandora for his car entertainment <a href="https://www.youtube.com/results?search_query=%23venomindonesia">#venomindonesia</a> <a href="https://www.youtube.com/results?search_query=%23venomania">#venomania</a> <a href="https://www.youtube.com/results?search_query=%23venomcaraudio">#venomcaraudio</a> <a href="https://www.youtube.com/results?search_query=%23venom">#venom</a> <a href="https://www.youtube.com/results?search_query=%23venomenal">#venomenal</a> <a href="https://www.youtube.com/results?search_query=%23venomoustar">#venomoustar</a> <a href="https://www.youtube.com/results?search_query=%23venomcelebrity">#venomcelebrity</a> <a href="https://www.youtube.com/results?search_query=%23audiosystem">#audiosystem</a> <a href="https://www.youtube.com/results?search_query=%23venompandora">#venompandora</a></p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', '1ZaF7X08dGc', 'draft'),
(16, 0, 'Venomous Star : Venom pandora Vj Daniel', '<p style="text-align:justify">Venom celebrity, VJ Daniel use Venom products pandora for his car entertainment <a href="https://www.youtube.com/results?search_query=%23venomindonesia">#venomindonesia</a> <a href="https://www.youtube.com/results?search_query=%23venomania">#venomania</a> <a href="https://www.youtube.com/results?search_query=%23venomcaraudio">#venomcaraudio</a> <a href="https://www.youtube.com/results?search_query=%23venom">#venom</a> <a href="https://www.youtube.com/results?search_query=%23venomenal">#venomenal</a> <a href="https://www.youtube.com/results?search_query=%23venomoustar">#venomoustar</a> <a href="https://www.youtube.com/results?search_query=%23venomcelebrity">#venomcelebrity</a> <a href="https://www.youtube.com/results?search_query=%23audiosystem">#audiosystem</a> <a href="https://www.youtube.com/results?search_query=%23venompandora">#venompandora</a></p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', '3nESQ46Md3M', 'draft'),
(17, 0, 'Venom GIIAS 2016', '<p style="text-align:justify">Venom GIIAS 2016 at Internasional Convention Exhibition ( ICE ) BSD CITY Tangerang, BSD HALL 1 - AG ZONA CAR AUDIO 11 - 21 Agustus 2016 <a href="https://www.youtube.com/results?search_query=%23venomindonesia">#venomindonesia</a> <a href="https://www.youtube.com/results?search_query=%23venomania">#venomania</a> <a href="https://www.youtube.com/results?search_query=%23venomaudio">#venomaudio</a> <a href="https://www.youtube.com/results?search_query=%23caraudio">#caraudio</a> <a href="https://www.youtube.com/results?search_query=%23venom">#venom</a> <a href="https://www.youtube.com/results?search_query=%23audiosystem">#audiosystem</a> <a href="https://www.youtube.com/results?search_query=%23venomenal">#venomenal</a> <a href="https://www.youtube.com/results?search_query=%23giias2016">#giias2016</a> <a href="https://www.youtube.com/results?search_query=%23bsdcitytangerang">#bsdcitytangerang</a> <a href="https://www.youtube.com/results?search_query=%23InternasionalConventionExhibition">#InternasionalConventionExhibition</a></p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'gS0t1wFenSw', 'draft'),
(18, 0, 'Launching Venom PANDORA VPR4.4 at GIIAS 2015', '', 'http://venom.levanpedia.com/assets/upload/video/', '6De7RnA1C_g', 'draft'),
(19, 0, 'Mike Lewis car makeover by Venom', '<p style="text-align:justify">Make over mobil Mike Lewis oleh Venom Indonesia Mobil ini didesign khusus sesuai dengan character Mike Lewis sebagai artist untuk info lebih lanjut hub Venom Indonesia telp. +62-216693936 www.venom-audio.com</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'EeFyAbgWlgQ', 'draft'),
(20, 0, 'Venomous Star : Anang Hermansyah', '<p style="text-align:justify">Venom celebrity, Anang hermansyah use Venom products for his car entertainment</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', '9e0IwtbicWc', 'draft'),
(21, 0, 'Venom IIMS 2012 ', '<p>Venom IIMS 2012&nbsp;</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'f3BSfb70G_M', 'draft'),
(22, 0, 'Venom 10 Year Journey', '<p>Venom 10 Year Journey</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', '1AXvoOvWBnc', 'draft'),
(23, 0, 'Indonesia International Motor Show', '<p>IIMS 2018 Jiexpo kemayoran</p>\r\n', 'http://venom.levanpedia.com/assets/upload/video/', 'Dut_uhKSBoA', 'draft'),
(26, 0, 'Atasi Macet Tanpa Bete Dengan Venom VPR 3.4', '<p>Kejebak macet di jalanan ibu kota? Atasi stress karena macet dengan mendengar musik favorit kamu. Didukung dengan perangkat audio yang berkualitas, penyanyi idola kamu se-akan bernyanyi tepat di depan kamu. Solusinya adalah Venom Pandora VPR 3.4. Feel the music become real.</p>\r\n', 'http://103.82.242.153/venom/assets/upload/video/', 'hP5tI6MBsao', 'draft'),
(27, 0, 'Modifikasi Xpander By Venom', '<p>Xpander pertama dengan Venom Pandora. Feel the 3D sound stage with extra bass. dengan pemasangan Plug and play</p>\r\n', 'http://103.82.242.153/venom/assets/upload/video/', 'x6Spr0HoGEE', 'draft'),
(28, 0, 'Cara Menghubungkan Venom Pandora ke Gadget Melalui Apps', '<p>ini adalah cara menyambungkan gadet ke venom apps bagi pengguna Venom Pandora VPR 3.4 Feel the 3D sound stage with extra bass</p>\r\n', 'http://103.82.242.153/venom/assets/upload/video/', 'tFcF93jRjAg', 'draft'),
(29, 0, 'Venom Inferno Series', '<p>Venom Inferno series 2 way system . Inferno series terdiri dari 2 tipe, yaitu Inferno full range yang diberi kode VIN 2 dan Inferno midbass yang diberi kode VIN 6, dimana keduanya merupakan perpaduan yang cocok untuk membentuk formasi sistem 2-way. Sebagai driver speaker yang dirancang untuk kalangan Audiophile, tentunya ditunjang pula oleh komponen terbaik, dimana Venom Inferno memiliki konus dengan bahan paper yang di coating bahan aluminium, hal tersebut dapat membuat warna suara menjadi berkelas.</p>\r\n', 'http://103.82.242.153/venom/assets/upload/video/', 'iRsrvulRJRE', 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `tb_voucer`
--

CREATE TABLE IF NOT EXISTS `tb_voucer` (
  `kode_voucer` varchar(15) NOT NULL,
  `point` int(15) NOT NULL,
  `harga` int(15) NOT NULL,
  `image` text,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_voucer`
--

INSERT INTO `tb_voucer` (`kode_voucer`, `point`, `harga`, `image`, `create_on`) VALUES
('V0001', 100, 110000, NULL, '2018-07-28 00:55:10'),
('V8055', 150, 155000, NULL, '2018-07-28 01:02:03'),
('V811', 55, 500000, NULL, '2018-07-28 01:00:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_album`
--
ALTER TABLE `tb_album`
  ADD PRIMARY KEY (`id_album`);

--
-- Indexes for table `tb_balance`
--
ALTER TABLE `tb_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tb_chat_rooms`
--
ALTER TABLE `tb_chat_rooms`
  ADD PRIMARY KEY (`chat_room_id`);

--
-- Indexes for table `tb_claim`
--
ALTER TABLE `tb_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_community`
--
ALTER TABLE `tb_community`
  ADD PRIMARY KEY (`id_community`);

--
-- Indexes for table `tb_diskon`
--
ALTER TABLE `tb_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dislike`
--
ALTER TABLE `tb_dislike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_event`
--
ALTER TABLE `tb_event`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `tb_foto`
--
ALTER TABLE `tb_foto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_halaman`
--
ALTER TABLE `tb_halaman`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`kode_invoice`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kat`);

--
-- Indexes for table `tb_kat_user`
--
ALTER TABLE `tb_kat_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_konfirmasi`
--
ALTER TABLE `tb_konfirmasi`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `tb_kota`
--
ALTER TABLE `tb_kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `tb_like`
--
ALTER TABLE `tb_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_merk`
--
ALTER TABLE `tb_merk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `tb_messages`
--
ALTER TABLE `tb_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `chat_room_id` (`chat_room_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `chat_room_id_2` (`chat_room_id`);

--
-- Indexes for table `tb_news`
--
ALTER TABLE `tb_news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `tb_otp`
--
ALTER TABLE `tb_otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_point`
--
ALTER TABLE `tb_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_present`
--
ALTER TABLE `tb_present`
  ADD PRIMARY KEY (`id_present`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_rating`
--
ALTER TABLE `tb_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reedem`
--
ALTER TABLE `tb_reedem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reward`
--
ALTER TABLE `tb_reward`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tb_simulator`
--
ALTER TABLE `tb_simulator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `tb_sosmed`
--
ALTER TABLE `tb_sosmed`
  ADD PRIMARY KEY (`id_sosmed`);

--
-- Indexes for table `tb_subkategori`
--
ALTER TABLE `tb_subkategori`
  ADD PRIMARY KEY (`id_kat`);

--
-- Indexes for table `tb_testimoni`
--
ALTER TABLE `tb_testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_video`
--
ALTER TABLE `tb_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_voucer`
--
ALTER TABLE `tb_voucer`
  ADD PRIMARY KEY (`kode_voucer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_album`
--
ALTER TABLE `tb_album`
  MODIFY `id_album` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_balance`
--
ALTER TABLE `tb_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_chat_rooms`
--
ALTER TABLE `tb_chat_rooms`
  MODIFY `chat_room_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `tb_claim`
--
ALTER TABLE `tb_claim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_community`
--
ALTER TABLE `tb_community`
  MODIFY `id_community` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=410;
--
-- AUTO_INCREMENT for table `tb_diskon`
--
ALTER TABLE `tb_diskon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_dislike`
--
ALTER TABLE `tb_dislike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tb_event`
--
ALTER TABLE `tb_event`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_foto`
--
ALTER TABLE `tb_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tb_halaman`
--
ALTER TABLE `tb_halaman`
  MODIFY `id_halaman` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kat` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_kat_user`
--
ALTER TABLE `tb_kat_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_konfirmasi`
--
ALTER TABLE `tb_konfirmasi`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tb_like`
--
ALTER TABLE `tb_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `tb_merk`
--
ALTER TABLE `tb_merk`
  MODIFY `id_merk` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_messages`
--
ALTER TABLE `tb_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT for table `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `tb_otp`
--
ALTER TABLE `tb_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tb_present`
--
ALTER TABLE `tb_present`
  MODIFY `id_present` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id_produk` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `tb_rating`
--
ALTER TABLE `tb_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_reedem`
--
ALTER TABLE `tb_reedem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_simulator`
--
ALTER TABLE `tb_simulator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  MODIFY `id_slide` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_sosmed`
--
ALTER TABLE `tb_sosmed`
  MODIFY `id_sosmed` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_subkategori`
--
ALTER TABLE `tb_subkategori`
  MODIFY `id_kat` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_testimoni`
--
ALTER TABLE `tb_testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `tb_video`
--
ALTER TABLE `tb_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
