<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Nerby_toko extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model');
    }

    

    function index_get(){
        $community=$this->Model->GetCommunity()->result_array();

            foreach ($community as $row) {

                $from =  $this->get('lat').",".$this->get('long');
                $to = $row['lat'].",".$row['lang'];

                $from = urlencode($from);
                $to = urlencode($to);

                $data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=id-ID&sensor=false");
                $data = json_decode($data);

                $time = 0;
                $distance = 0;

                foreach($data->rows[0]->elements as $road) {
                    $time += $road->duration->value;
                    $distance += $road->distance->value;
                }               

                $ray= array(
                    "id_community" => $row['id_community'],
                    "nama" => $row['nama'],
                    "pemilik" => $row['pemilik'],
                    "hp" => $row['hp'],
                    "email" => $row['email'],           
                    "alamat" => $row['alamat'], 
                    "kota_id" => $row['kota_id'],   
                    "lat" => $row['lat'],   
                    "lang" => $row['lang'], 
                    "images" => $row['images'], 
                    "jenis" => $row['jenis'],   
                    
                    "jarak" => $distance,
                    "waktu" => $time,
                    "alamat_toko_api" => $data->destination_addresses[0],         
                );
            }
            $konvjson=json_encode($ray); //konvert dulu ke JSON
            $konvobject=json_decode($konvjson); //baru konvert ke OBJECT

            function sort_objects_by_jarak($a, $b) {
                if($a->waktu_value == $b->waktu_value){ return 0 ; }
                return ($a->waktu_value > $b->waktu_value) ? 1 : -1;
            }

            usort($konvobject, 'sort_objects_by_jarak');
            
            $this->response([
                'kode' => 1,
                'result' => $konvobject,
                'pesan' =>'Data tidak kosong!'
            ], REST_Controller::HTTP_OK);
        
    }
}