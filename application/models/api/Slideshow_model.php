<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Slideshow_model extends CI_Model
{
	public $table = 'tb_slideshow';
    public $id = 'id_slide';
    public $order = 'ASC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all($category)
    {

        if($category == "all"){
            $this->db->where("status", $category);
        }else {
            $array = array('all', $category);
            $this->db->where_in("status", $array);
        }
        
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


}