<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Order Detail (<?php echo $kode;?>)</b>
        </h1>
        </section>
        
        <section class="content">
            
            <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO INVOICE</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped">
                <tbody><tr>
                  <td><strong>No Order</strong></td>
                  <td><?php echo $kode;?></td>
                </tr>
                <tr>
                  <td><strong>Tanggal Order</strong></td>
                  <td><?php echo $tanggal;?></td>
                </tr>
                <tr>
                  <td><strong>pemesan</strong></td>
                  <td><?php echo $nm;?></td>
                </tr>
                <tr>
                  <td><strong>Alamat Pengirim</strong></td>
                  <td><?php echo $alamat;?></td>
                </tr>
                <!--<tr>
                  <td>&nbsp;</td>
                  <td>Bekasi Jaya- Bekasi timur</td>
                </tr>-->
                <tr>
                  <td><strong>Tlp</strong></td>
                  <td><?php echo $hp;?></td>
                </tr>
                <tr>
                  <td><strong>Email</strong></td>
                  <td>admin@gmail.com</td>
                </tr>
              </tbody></table>
              </div>
              
              </div>
            </div>
            
            <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO PRODUCT</h3>
            </div>
            <div class="box-body">
                <table  class="table table-striped">
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th>Jumlah</th>
                      <th>Price</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                 <?php $tot = 0; $no=0; foreach($data_order as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $row['judul']; ?></td>
                      <td><?php echo $row['jumlah']; ?></td>
                      <td><?php echo number_format($row['harga'], 0, ',', '.'); ?></td>
                      <td><?php echo number_format($row['total'], 0, ',', '.'); ?></td>
                    </tr>
                    
                <?php $tot += $row['total']; } ?>
                  </tbody>
                    <tr>
                      <th colspan="3" ><div align="center">Total</div></th>
                      <th><?php echo number_format($tot, 0, ',', '.'); ?></th>
                    </tr>
                </table>
                
              </div>
              </div>
            </div>
            
            <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">History Order</h3>
            </div>
            <div class="box-body">
                <span class=" col-sm-12">04 Mart 2018</span>
               <span class=" col-sm-12 label label-info" style="padding: 10px">05.30 Response Order</span>
               <span class=" col-sm-12">10 Mart 2018</span>
               <span class=" col-sm-12 label label-info" style="padding: 10px">05.30 Order Berbentuk</span>

   
              </div>
              </div>
            </div>
            
            
            <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO PAYMENT</h3>
                </div>
            <div class="box-body">
                <table  class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Atas Nama</th>
                      <th>Bank</th>
                      <th>No. Rekening</th>
                      <th>Tanggal Transfer</th>
                      <th>Bukti Transfer</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_konfirmasi as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $row['atas_nama']; ?></td>
                      <td><?php echo $row['bank']; ?></td>
                      <td><?php echo $row['no_rek']; ?></td>
                      <td><?php echo $row['tanggal']; ?></td>
                      <td><?php if($row['images']!=null){ ?>
              
                    <img data-toggle="modal" data-target="#modal-default" src="<?php echo $row['images']; ?>" height="100" width="100" alt="user image" class="online"/>
                    <?php }else{ ?> 
                    <img src="http://levanpedia.com/vibe/assets/dist/img/user2-160x160.jpg" height="100" width="100" alt="user image" class="online"/>
                    <?php } ?></td>
                    </tr>
                    
                    <div class="modal fade" id="modal-default">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-body">
                            <img src="<?php echo $row['images']; ?>" height="100%" width="100%" alt="user image" class="online"/>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <?php } ?>
                  </tbody>
                </table>
                
                </div>
              </div>
            </div>
            
            
        </section>
        </div>
    </div>   
   
</body>
</html>