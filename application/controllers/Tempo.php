<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tempo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'level' => '',
			'tempo' => '',
			'id' => '',
			'data_level' => $this->Model->GetKatUser("where id not in ('1','10','8') order by id desc")->result_array()
		);

		$this->load->view('tempo/data_tempo', $data);
	}

	function edittempo($kode = 0){		
		$tampung = $this->Model->GetKatUser("where id = '$kode'")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'level' => $tampung[0]['nama'],
			'tempo' => $tampung[0]['tempo'],
			'id' => $tampung[0]['id'],
			'data_level' => $this->Model->GetKatUser("order by id desc")->result_array()
			);
		$this->load->view('tempo/data_tempo', $data);
	}

	function savedata(){
		if($_POST){
			$status = $_POST['status'];
			
			$tempo = $_POST['tempo'];
			$id = $_POST['id'];
			$level = $_POST['level'];
			if($status == "baru"){
				$data = array(
					'id' => $id,
					'nama' => $level,
					'tempo' => $tempo,
					);
				$result = $this->Model->Simpan('tb_kat_user', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'tempo');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'tempo');
				}
			}else{
				$data = array(
					'nama' => $level,
					'tempo' => $tempo,
					);
				$result = $this->Model->Update('tb_kat_user', $data, array('id' => $id));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'tempo');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'tempo');
				}
			}
		}else{
			echo('handak baapa nyawa tong!!!');
		}
	}

	function hapuskat($kode = 1){
		
		$result = $this->Model->Hapus('tb_kat_user', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'tempo');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'tempo');
		}
	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */