<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Bank extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Bank_model');
    }

    

    function index_get(){
    	$bank = $this->Bank_model->get_all();
                    $this->response([
                            'kode' => 1,
                            'result' => $bank,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }
}