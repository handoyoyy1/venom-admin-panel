<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Redeem extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Redeem_model');
        $this->load->model('api/Claim_model');
    }

    
    function index_get(){
        $data=$this->get('data');
        $user_id = $this->get('iduser');
        $status = $this->get('status');
        $kode = $this->get('kode');
        $otp_kode = rand(0,10000);
        
        if ($data=="mail") {
            $data = array(
                'user_id' => $user_id,
                'kode' => $otp_kode
            );
            $this->Claim_model->insert_otp($data);

            if ($this->User_model->exist_row_check('id_user', $user_id) > 0) {
                $user_detail = $this->User_model->get_by_id($user_id);

                    if($user_detail->email == null || $user_detail->email == ""){
                    }else {
                        $email = $user_detail->email;
                        $this->load->library('email');
                        $config = send_email();
                        $this->email->initialize($config);
                        $this->email->set_mailtype("html");
                        $this->email->set_newline("\r\n");
                
                        $list = array($email);
                        $this->email->to($list);
                        $this->email->from('bluecoreotp@gmail.com','Venom');
                        $this->email->subject('Kode OTP');
                        $this->email->message('New kode otp your account : '.$otp_kode);
                        //Send email
                        $this->email->send();
                        $message = array("kode"=>1, "pesan"=>"Sending mail  succes!");
                        $this->response($message, REST_Controller::HTTP_OK);
                    }                 
            }else{
                $message = array("kode"=>0, "pesan"=>"Sorry user empty!");
                $this->response($message, REST_Controller::HTTP_OK);
            }     
        }elseif ($data=="validasi"){
            if ($this->Claim_model->exist_row_check('tb_otp','kode', $kode) > 0) {
                $message = array("kode"=>1, "pesan"=>"Your code true!");
                $this->response($message, REST_Controller::HTTP_OK);

            }else{
                $message = array("kode"=>0, "pesan"=>"Your code wrong!");
                $this->response($message, REST_Controller::HTTP_OK);
            }

        }elseif ($data=="all"){
            
            $Redeem = $this->Redeem_model->get_all($user_id);

            $message = array("kode"=>1,"result"=>$Redeem, "pesan"=>"Data not empty!");
            $this->response($message, REST_Controller::HTTP_OK);

        }

    }
    
    function index_post(){
        $point = $this->post('point');
        $user_id = $this->post('iduser');
        
        if ($this->Redeem_model->exist_row_check('tb_user','id_user', $user_id) > 0){  
            $data_redeem = array(
                            'user_id' => $user_id,
                            'point' => $point
                        );
                        $this->Redeem_model->insert($data_redeem);
                    $this->response(['kode' => 0,'pesan' =>'thank your redeem have successfully!'], REST_Controller::HTTP_OK);
        }else{ 
                $this->response(['kode' => 0,'pesan' =>'Redeem is wrong! '], REST_Controller::HTTP_OK);
            }
        
    }
}