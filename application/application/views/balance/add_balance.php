<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/jquery-ui.css'?>">
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content-header">
        <h1>
          <b>TAMBAH DATA BALANCE</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH DATA BALANCE</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>balance/savedata" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">

                    <div class="form-group">
                      <label for="">Nama User</label>
                      <input type="hidden" class="form-control" value="" id="id_user" name="id_user" >
                      <input type="text" class="form-control" value="" id="nama" name="nama" placeholder="Nama nama" required> 
                    </div>
                    <div class="form-group">
                      <label for="">Jumlah Balance</label>
                      <input type="text" class="form-control" value="" id="" name="jumlah" placeholder="Jumlah nama" required> 
                    </div>
                    <div class="form-group">
                      <label for="">Keterangan</label>
                      <textarea  class="form-control" name="keterangan" placeholder="Isikan keterangan"></textarea>                        
                    </div>

                    <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>pembelian" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
                    
                  </div>
                  
                    
                  </div>
                  
                  
                </div><!-- /.item -->
                
               </form>
              </div><!-- /.chat -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved<a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
  <script type="text/javascript">
    $(document).ready(function(){

        $('#nama').autocomplete({
                source: "<?php echo site_url('balance/search_user');?>",  
                select: function (event, ui) {
                    $('[name="nama"]').val(ui.item.label); 
                    $('[name="id_user"]').val(ui.item.id); 
                }
            });

    });
  </script>
</body>
</html>