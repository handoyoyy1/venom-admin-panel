<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Voucer extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Reward_model');
    }

    

    function index_get(){
    	$voucer = $this->Reward_model->get_all_voucer();
                    $this->response([
                            'kode' => 1,
                            'result' => $voucer,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }
}