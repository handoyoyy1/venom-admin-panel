<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Foto extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_foto' => $this->Model->GetFoto("order by id desc")->result_array(),
		);
		$this->load->view('foto/foto', $data);
	}

	function addfoto()
	{
		$data = array(
			'data_album' => $this->Model->GetAlbum("order by id_album desc")->result_array(),
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('foto/add_foto', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/album/foto',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();

		$id = '';
		$album_id = $_POST['album_id'];
		$status_foto = $_POST['status_foto'];
		$user_id = $this->session->userdata('id_user');
		$file_name = $upload_data['file_name'];

		$data = array(	
			'id'=> $id,
			'album_id' => $album_id,
			'status_foto' => $status_foto,
			'user_id' => $user_id,
			'nama_foto' => base_url()."assets/upload/album/foto/".$file_name		
			);
		
		$result = $this->Model->Simpan('tb_foto', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'foto');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'foto');
		}		
	}

	function editfoto($kode = 0){
		$data_foto = $this->Model->GetFoto("where id = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id' => $data_foto[0]['id'],
			'status_foto' => $data_foto[0]['status_foto'],
			'album_id' => $data_foto[0]['album_id'],
			'data_album' => $this->Model->GetAlbum("order by id_album desc")->result_array(),
			'nama_foto' => $data_foto[0]['nama_foto']
			);
		$this->load->view('foto/edit_foto', $data);
	}

	function updatefoto(){
		$config = array(
				'upload_path' => './assets/upload/album/foto',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id' => $this->input->post('id'),
				'album_id' => $this->input->post('album_id'),
				'status_foto' => $this->input->post('status_foto'),
				'user_id' => $this->session->userdata('id_user'),
				'nama_foto' => base_url()."assets/upload/album/foto/".$upload_data['file_name']
				);
			$row=$this->Model->GetFoto("where id = '$data[id]'")->row();
			unlink(str_replace(base_url(), "", $row->nama_foto));
			$res = $this->Model->Updatefoto($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'foto');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'foto');
			}
		}else{
			
			$data = array(
				'id' => $this->input->post('id'),
				'album_id' => $this->input->post('album_id'),
				'status_foto' => $this->input->post('status_foto'),
				'user_id' => $this->session->userdata('id_user'),
				);

			$res = $this->Model->Updatefoto($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'foto');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'foto');
			}
		}
	}

	function hapus($kode = 1){
		$row=$this->Model->GetFoto("where id = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->nama_foto));
		$result = $this->Model->Hapus('tb_foto', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'foto');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'foto');
		}
	}
}