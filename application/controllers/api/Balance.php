<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Balance extends REST_Controller {
    public $table = 'tb_balance';

    function index_get(){
		$user_id = $this->get('iduser');
		
		if($user_id != null){
			$this->db->where('user_id',$user_id);
			$balance = $this->db->get($this->table)->result();
                    $this->response([
                            'kode' => 1,
                            'result' => $balance,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
		}else{
			$this->response(['kode' => 0,'pesan' =>'Data kosong!'], REST_Controller::HTTP_OK);
		}	

    }
}