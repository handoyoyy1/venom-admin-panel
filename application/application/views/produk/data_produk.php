<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>DATA PRODUK</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo base_url(); ?>produk/addproduk" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH PRODUK </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                  <div class="row">
                <form action="<?php echo base_url('produk/filter') ?>" method="POST" style="margin-top: 20px;">
                  <div class="col-lg-3">
                      <label for="">Kategori Home</label>
                        <select  name="id_subkat" class="form-control">
                          <option value="null">--Pilih SubKategori--</option>
                          <?php foreach($optsubkategori as $row) { ?>
                              <option value="<?php echo $row['id_kat'] ?>"><?php echo $row['kategori'] ?></option>
                          <?php } ?>
                        </select> 
                    </div>
                    <div class="col-lg-3">
                      <label for="">Kategori Simulator</label>
                        <select name="id_kat" class="form-control">
                          <option value="null">--Pilih Kategori--</option>
                          <?php foreach($optkategori as $row) { ?>
                              <option value="<?php echo $row['id_kat'] ?>"><?php echo $row['kategori'] ?></option>
                          <?php } ?>
                        </select> 
                    </div>
                    <div class="col-lg-1">
                      <label style="color: #fff">Kategori</label>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                    </div>
                    </form>
                  </div><br>
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="40">NO</th>
                      <th>Nama Barang</th>
                      <th width="120">HARGA</th>
                      <th width="40">QTY AWAL</th>
                      <th width="40">QTY TRX</th>
                      <th width="40">QTY SISA</th>
                      <!--<th>KONDISI</th>-->
                      <!--<th>MERK</th>-->
                      <!--<th width="100">KATEGORI HOME</th>-->
                      <th width="100">KATEGORI SIMULATOR</th>
                      <th width="70">TANGGAL</th>
                      <!--<th>STATUS</th>-->
                      <!--<th>COUNTER</th>-->
                      <!--<th width="300">KETERANGAN</th>-->
                      <th width="80">FOTO</th>

                      <th width="120">AKSI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_produk as $row) { $no++ ?>
                    <?php 
                      $foto_produk=explode(" ", $row['foto']);
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['judul']; ?></td>
                      <td><?php echo currency_format($row['harga']); ?></td>
                      <td><?php echo $row['jumlah']; ?></td>
                      <td><?php echo $row['trx']; ?></td>
                      <td><?php echo $row['qtysisa']; ?></td>
                      <!--<td><?php echo $row['kondisi']; ?></td>-->
                      <!--<td><?php echo $row['merk']; ?></td>-->
                      <!--<td><?php echo $row['kategori']; ?></td>-->
                      <td><?php echo $row['kategori']; ?></td>
                      <td><?php echo $row['tgl_input_pro']; ?></td>
                      <!--<td>
                      <?php if($row['status'] == "publish"){ echo '<span class="label label-success">Publish</span>';?>
                      <?php } else { ?> 
                      <span class="label label-danger">Draft</span>
                      <?php } ?>
                      </td>
                      <td><span style="font-size:14px" class="label label-warning"><?php echo $row['counter']; ?></span></td>-->
                      <!--<td><?php echo $row['ket']; ?></td>-->
                      <td>
                      <img class="img-thumbnail" src="<?php echo base_url()."assets/upload/".$foto_produk[0]; ?>" class="img-circl" alt="User Image" />
                      </td>
                    
                      <td>
                      <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>produk/editproduk/<?php echo $row['id_produk']; ?>"><i class="fa fa-pencil"></i></a>
                      <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>produk/hapuspro/<?php echo $row['id_produk']; ?>"><i class="fa fa-trash"></i></a>
                       <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>produk/detail/<?php echo $row['id_produk']; ?>"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>