<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  <?php
    function wordlimit($text, $limit){
      if (strlen($text)>$limit) {
        $word = mb_substr($text, 0,$limit-3)."...";
      }else{
        $word = $text;
      }
      return $word;
    }
  ?>
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <a style="margin-bottom:3px" href="<?php echo base_url(); ?>news/addnews" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> Tambah Berita </a>
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th width="200">Judul</th>
                      <th>Isi</th>
                      <th>Image</th>
                      <th>Tanggal</th>
                      <th>AKSI</th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php $no=0; foreach($data_news as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['judul']; ?></td>
                      <td><?php echo wordlimit($row['isi'], 40); ?></td>
                      <td><img style="height:80px" src="<?php echo /*base_url()."assets/upload/news/"*/$row['image']; ?>" class="img-circl" alt="User Image" /></td>
                      <td><?php echo $row['tanggal']; ?></td>    
                      <td>
                      <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>news/editnews/<?php echo $row['id_news']; ?>"><i class="fa fa-pencil"></i></a>
                      <!--<a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>news/detailnews/<?php echo $row['id_news']; ?>"><i class="fa fa-eye"></i></a>-->
                      <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>news/hapus/<?php echo $row['id_news']; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>