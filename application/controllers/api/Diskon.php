<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Diskon extends REST_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('api/Diskon_model');
    }
    
    function index_get(){
        $data=$this->get('data');
        $user_id = $this->get('iduser');
        $voucher = $this->get('voucher');
        $sekarang=date("Y-m-d");
        
        if ($data=="diskon") {
            
            if ($this->Diskon_model->exist_row_check('kode_voucher', $voucher) > 0) {
                $cekvoucher=$this->Diskon_model->cek_voucher($voucher, $user_id);
                $cekexpired=$this->Diskon_model->cek_expired($voucher);
                $tglexpired=strtotime($cekexpired->expired);
                $tglsekarang=strtotime($sekarang);
                if ($cekvoucher>0 OR $tglexpired>$tglsekarang) {
                   $message = array("kode"=>1,"diskon"=>$cekexpired->nominal,"category"=>$cekexpired->category,"pesan"=>"Voucher Valid");
                }else{
                   $message = array("kode"=>0,"pesan"=>"Voucher Expired"); 
                }
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                $message = array("kode"=>0,"pesan"=>"Voucher is Empaty"); 
                $this->response($message, REST_Controller::HTTP_OK);
                
            }
        
        
        }
        
    }
}