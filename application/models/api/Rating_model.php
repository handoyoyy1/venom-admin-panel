<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rating_model extends CI_Model
{
	public $table = 'tb_rating';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

}