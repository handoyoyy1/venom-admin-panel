<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Halaman_model extends CI_Model
{
	public $table = 'tb_halaman';
    public $id = 'id_halaman';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }
    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
}
?>