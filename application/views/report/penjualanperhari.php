<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Laporan Penjulan Perhari</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
        
              <div class="box">
                <div class="box-title" style="margin-top: 20px;margin-bottom: 20px;">
                  <form action="<?php echo base_url() ?>report/penjualanperhari" method="POST">
                    <div class="col-xs-4">
                      <label>Dari Tanggal</label>
                      <input type="date" name="dari" value="<?php echo $dari ?>" class="form-control" placeholder=".col-xs-3">
                    </div>
                    <div class="col-xs-4">
                      <label>Sampai Tanggal</label>
                      <input type="date" name="sampai" value="<?php echo $sampai ?>" class="form-control" placeholder=".col-xs-4">
                    </div>
                    <div class="col-xs-1">
                      <label style="color: #FFF">Dari</label>
                      <button type="submit" class="btn btn-primary">Proses</button>
                    </div>
                    <div class="col-xs-1">
                      <label style="color: #FFF">Dari</label>
                      <a target="_blank" href="<?php echo base_url("report/cetak/penjualanperhari?dari=".$dari."&sampai=".$sampai) ?>" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                    </div>
                  </form>
                </div><!-- /.box-title -->
                <div class="box-body" style="margin-top: 100px">
                 <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>Invoice</th>
                      <th>Qty</th>
                      <th>Total</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_invoice as $row) { $no++ ?>
                    <?php 
                      $total=0; 
                      $qty=0;
                      $data_order=$this->Model->GetDataOrder($row['kode_invoice'])->result_array(); 
                      foreach($data_order as $order){
                        $total=$total+$order['total'];
                        $qty=$qty+$order['jumlah'];
                      }
                    ?>
                    <tr>
                      <td><?php echo $row['tanggal']; ?></td>
                     <td><a href="<?php echo base_url(); ?>invoice/detail/<?php echo $row['kode_invoice']; ?>"><?php echo $row['kode_invoice']; ?></a></td>
                      <td><?php echo $qty; ?></td>
                      <td>Rp. <?php echo number_format($total); ?></td>
                      <td><?php echo $row['status']; ?> </td>
                         
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": false,
          "bInfo": false,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>