<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('inc/head'); ?>
    </head>
    <body onload=" window.print();" onfocus="window.close()">
        <div class="wrapper">
            <!-- Main content-->
            <section class="invoice">
                <!-- info row -->
                <div class="row invoice-info" style="padding:0px 15px;">
                    <div style="text-align:center; font-size:35px;">
                        Laporan Penjualan By Invoice
                    </div>
                    <div style="margin-bottom:20px; font-size:15px; text-align:center;">
                        Tanggal : <?php echo date("d F Y") ?>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Tanggal</th>
                          <th>Qty</th>
                          <th>Trans</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=0; foreach($data_invoice as $row) { $no++ ?>
                        <?php 
                          $total=0; 
                          $qty=0;
                          $data_order=$this->Model->GetDataOrder($row['kode_invoice'])->result_array(); 
                          foreach($data_order as $order){
                            $total=$total+$order['total'];
                            $qty=$qty+$order['jumlah'];
                          }
                        ?>
                        <tr>
                          <td><?php echo $no ?></td>
                          <td><?php echo $row['tanggal']; ?></td>
                          <td><?php echo $qty; ?></td>
                          <td></td>
                          <td>Rp. <?php echo number_format($total); ?></td>
                             
                        </tr>
                        <?php } ?>
                      </tbody>
                  </table>
                </div>
            </section>
        </div><!-- ./wrapper -->

        <!-- AdminLTE App -->
        <script src="../../dist/js/app.min.js"></script>
    </body>
</html>