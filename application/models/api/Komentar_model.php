<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Komentar_model extends CI_Model
{
	public $table = 'tb_komentar';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    function get_by_id($id)
    {
        $this->db->where('id_produk', $id);
        return $this->db->get($this->table)->result();
    }

}