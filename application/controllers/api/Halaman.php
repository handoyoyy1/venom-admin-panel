<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Halaman extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Halaman_model');
    }


    function index_get(){
        $data = $this->get('data');
        $id = $this->get('id');

        switch ($data) {
            case "detail":
                $halaman = $this->Halaman_model->get_by_id($id);
                $this->response([
                            'kode' => 1,
                            'id_halaman' => $halaman->id_halaman,
                            'judul' => $halaman->judul,
                            'isi' => $halaman->isi,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }  

    }
}