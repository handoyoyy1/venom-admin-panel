<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Testimoni extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/Testimoni_model');
    }

    function index_get(){
    	$testimoni = $this->Testimoni_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $testimoni,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }

    function index_post(){
        $id = $this->post('id');
        $komentar = $this->post('komentar');

           if ($komentar == null) {
               $message = array("kode"=>2,"pesan"=>"Komentar anda kosong!");
                $this->response($message, REST_Controller::HTTP_CREATED);
           }else{
                $data = array(
                    'id_user' => $id,
                    'komentar' => $komentar
                );
                $this->Testimoni_model->insert($data);
                $message = array("kode"=>1,"pesan"=>"Komentar berhasil disimpan!");
                $this->response($message, REST_Controller::HTTP_CREATED);
        }
    }
}