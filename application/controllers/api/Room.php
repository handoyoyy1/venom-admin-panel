<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Room extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Room_model');
    }
    
    function index_get(){
        $data = $this->get('data');
        $iduser = $this->get('iduser');
        $kode = $this->get('kode');

        switch ($data) {
            case "chatting":
                $chatting = $this->Room_model->get_messages($kode);
                $this->response([
                            'kode' => 1,
                            'result' => $chatting,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
                case "cek":
                    if ($this->Room_model->exist_row_check('user_id', $iduser) > 0) { 
                            $room = $this->Room_model->get_by_user($iduser);
                            $this->response(['kode' => 1,'pesan' =>$room->name ], REST_Controller::HTTP_OK);
                    }else{
                            $this->response(['kode' => 0,'pesan' =>'Data  kosong!' ], REST_Controller::HTTP_OK);
                    }
                break;
            case "masuk":
                $data_simpan = array(
                    'status' => 'active',
                    'baca' => 'no'
                );
                $room = $this->Room_model->update($iduser,$data_simpan);
                $this->response(['kode' => 1,'pesan' =>'Data room berhasil dirubah!' ], REST_Controller::HTTP_OK);
                break; 
            case "keluar":
                $data_simpan = array(
                    'status' => 'inactive'
                );
                $room = $this->Room_model->update($iduser,$data_simpan);
                $this->response(['kode' => 1,'pesan' =>'Data room berhasil dirubah!' ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         } 
    }

     function index_post(){
        $data = $this->post('data');
       $kode = $this->post('kode');
       $id_user = $this->post('iduser');
       $pesan = $this->post('pesan');
       
       switch ($data) {
            
            case "room":
                $data_simpan = array(
                    'name' => $kode,
                    'user_id' => $id_user
                );

        	     $this->Room_model->insert($data_simpan);
        	     $message = array("kode"=>1,"pesan"=>"Room berhasil disimpan!");
        	     $this->response($message, REST_Controller::HTTP_CREATED);
                break; 
            case "pesan":
                $data = $this->Room_model->get_by_id($kode);
                $data_simpan = array(
                    'chat_room_id' => $data->chat_room_id,
                    'message' => $pesan,
                    'user_id' => $id_user
                );

        	     $this->Room_model->insert_messages($data_simpan);
        	     $message = array("kode"=>1,"pesan"=>"Messages berhasil disimpan!");
        	     $this->response($message, REST_Controller::HTTP_CREATED);
                break;    
            default:
                    echo "Maaf data anda kosong..!"; 
                    break;
         }

       
 	}
}
