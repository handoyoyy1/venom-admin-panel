<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Video extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/Video_model');
    }

    

    function index_get(){
    	$Video = $this->Video_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $Video,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);	

    }
}