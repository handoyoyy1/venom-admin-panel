<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
		$this->load->model('Model');
	}

	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}
	
	public function barang()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_invoice' => $this->Model->GetInvoice('order by create_on DESC')->result_array(),
			
		);

		$this->load->view('invoice/barang', $data);
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_invoice' => $this->Model->GetInvoice('order by create_on DESC')->result_array(),
			
		);

		$this->load->view('invoice/data_invoice', $data);
	}
	
	function detail($kode = 0){
        $data_invoice = $this->Model->GetInvoice("where kode_invoice = $kode")->result_array();
        $userid = $data_invoice[0]['id_user'];
        $data_user = $this->Model->GetUserdata("where id_user = $userid")->result_array(); 
        
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_order' => $this->Model->GetDataOrder($kode)->result_array(),
			'kode' => $data_invoice[0]['kode_invoice'],
			'nm' => $data_invoice[0]['nama'],
			'hp' => $data_invoice[0]['no_hp'],
			'tanggal' => $data_invoice[0]['tanggal'],
			'alamat' => $data_invoice[0]['alamat'],
			'email' => $data_user[0]['email'],
			'group' => $data_user[0]['level'],
			'jenis' => $data_invoice[0]['jenis'],
			'kategori' => $data_invoice[0]['kategori'],
			'harga_total' => $data_invoice[0]['harga_total'],
			'diskon_total' => $data_invoice[0]['diskon_total'],
			'sub_total' => $data_invoice[0]['sub_total'],
			'diskon_voucer' => $data_invoice[0]['diskon_voucer'],
			'ppn' => $data_invoice[0]['ppn'],
			'grand_total' => $data_invoice[0]['grand_total'],
			'data_konfirmasi' => $this->Model->GetkonfirmasibyID("where kode_invoice = $kode")->result_array(),
			
			);
		$this->load->view('invoice/detail', $data);
	}

	function nota($kode = 0){
        $data_invoice = $this->Model->GetInvoice("where kode_invoice = $kode")->result_array();
        $userid = $data_invoice[0]['id_user'];
        $data_user = $this->Model->GetUserdata("where id_user = $userid")->result_array();  

        $diskon = $this->Model->GetKatUser("where id = ".$data_user[0]['level'])->result_array();
        
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_order' => $this->Model->GetDataOrder($kode)->result_array(),
			'kode' => $data_invoice[0]['kode_invoice'],
			'nm' => $data_invoice[0]['nama'],
			'hp' => $data_invoice[0]['no_hp'],
			'tanggal' => $data_invoice[0]['tanggal'],
			'alamat' => $data_invoice[0]['alamat'],
			'comunity_id' => $data_invoice[0]['comunity_id'],
			'email' => $data_user[0]['email'],
			'group' => $data_user[0]['level'],
			'jenis' => $data_invoice[0]['jenis'],
			'kategori' => $data_invoice[0]['kategori'],
			'harga_total' => $data_invoice[0]['harga_total'],
			'diskon_total' => $data_invoice[0]['diskon_total'],
			'sub_total' => $data_invoice[0]['sub_total'],
			'diskon_voucer' => $data_invoice[0]['diskon_voucer'],
			'ppn' => $data_invoice[0]['ppn'],
			'grand_total' => $data_invoice[0]['grand_total'],
			'admin_note' => $data_invoice[0]['admin_note'],
			'sales_note' => $data_invoice[0]['sales_note'],
			'total_harga' => $this->Model->SumDataOrder($kode),
			'credit_a' => $diskon[0]['credit_a'],
			'credit_b' => $diskon[0]['credit_b'],
			'diskon_a' => $diskon[0]['diskon_a'],
			'diskon_b' => $diskon[0]['diskon_b'],
			'diskon_c' => $diskon[0]['diskon_c'],
			'diskon_d' => $diskon[0]['diskon_d'],
			'diskon_e' => $diskon[0]['diskon_e'],
			
			);
		$this->load->view('invoice/nota', $data);
	}
	
	function update_status($kode,$status){
	    if($status == "paid"){
	        $status ="unpaid";
	    }else{
	        $status ="paid";
	    }
	    $data = array(
				'kode_invoice' => $kode,
				'status' => $status
				);

		$this->Model->UpdateInvoice($data);
	    header('location:'.base_url().'Invoice');
	}
	
	function update_barang_status($kode,$status){
	    switch ($status) {
            case "unaccept":
                $status ="delivery";
                $data = array(
				'kode_invoice' => $kode,
				'barang_status' => $status
				);
				$this->Model->UpdateInvoice($data);
	    header('location:'.base_url().'Invoice');
                break;
            case "delivery":
                $status ="accept";
                $data = array(
				'kode_invoice' => $kode,
				'barang_status' => $status
				);
				$this->Model->UpdateInvoice($data);
	    header('location:'.base_url().'Invoice');
                break;
                case "accept":
                $status ="unaccept";
                $data = array(
				'kode_invoice' => $kode,
				'barang_status' => $status
				);
				$this->Model->UpdateInvoice($data);
	    header('location:'.base_url().'Invoice');
                break;
            default:
                break;
        }
	    
	    

		
	}
	
	function pesan($kode){
	    $data = $this->Model->GetUserdata("where id_user = '$kode'")->result_array();
		$data = array(	
			'id' => $data[0]['id_user'],
			'nama' => $data[0]['first_name'].$data[0]['last_name']
			);
	    $this->load->view('invoice/pesan', $data);
	}
	
	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_invoice', array('kode_invoice' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'invoice');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'invoice');
		}
	}
	
	function add_pesan(){
	    $kode = $_POST['id'];
	    $judul = $_POST['judul'];
		$isi = $_POST['isi'];
	    $data = $this->Model->GetUserdata("where id_user = '$kode'")->result_array();
	    $token = $data[0]['token'];

		$data = array(	
			'id'=> '',
			'judul' => $judul,
			'pesan' => $isi			
			);
		
		$result = $this->Model->Simpan('tb_pesan', $data);
		
		$url = "https://fcm.googleapis.com/fcm/send";

        $values = array();
        $values['status'] = "pesan";
        $values['title'] = $judul;
        $values['message'] = $isi;
        $values['image'] = "img";
        $values['timestamp'] = date('Y-m-d G:i:s');
        $values['address'] = "Indonesia";
        
        $data = array();
        $data ['to'] = $token;
        $data ['data'] = $values;
        
        echo $content = json_encode($data);
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array(
                    "Content-type: application/json",
                    "Authorization: key=AIzaSyD0lW1j65HPlIJWxiVlNR3tc9qWSXWLabI"
                    ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        
        $json_response = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        if ( $status != 201 ) {
            die("goo: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }
        
        curl_close($curl);
        $response = json_decode($json_response, true);
        header('location:'.base_url().'Invoice');
	}
	
	
	public function word($kode = 0){
	    header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=Invoice"."-$kode".".doc");
        $data_invoice = $this->Model->GetInvoice("where kode_invoice = $kode")->result_array();
        
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_order' => $this->Model->GetDataOrder($kode)->result_array(),
			'kode' => $data_invoice[0]['kode_invoice'],
			'nm' => $data_invoice[0]['nama'],
			'hp' => $data_invoice[0]['no_hp'],
			'tanggal' => $data_invoice[0]['tanggal'],
			'alamat' => $data_invoice[0]['alamat'],
			'data_konfirmasi' => $this->Model->GetkonfirmasibyID("where kode_invoice = $kode")->result_array(),
			
			);
		$this->load->view('invoice/word', $data);
    }
	
	
	
	
	
}