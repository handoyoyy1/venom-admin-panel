<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		$this->load->helper('site_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	} 

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_event' => $this->Model->GetEvent("order by id_event desc")->result_array(),
		);
		$this->load->view('event/event', $data);
	}

	function addevent()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('event/add_event', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/event',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		
		$id_event = '';
		$nama = $_POST['nama'];
		$detail = $_POST['detail'];
		$lokasi = $_POST['lokasi'];
		$Latitude=$_POST['lat'];
		$Longitude=$_POST['lang'];
		$tanggal = $_POST['tgl'];
		if ($this->session->userdata('level')==1) {
			$status="publish";
		}else{
			$status="draft";
		}

		$image = base_url('assets/upload/event/'.$file_name);
		
		//$image = $file_name;

		$data = array(	
			'id_event'=> $id_event,
			'nama_event' => $nama,
			'detail_event' => $detail,
			'lokasi' => $lokasi,
			'lat' => $Latitude,
			'lang' => $Longitude,
			'tanggal' =>$tanggal,
			'status' =>$status,
			'image'	=> 	$image		
			);
		
		$result = $this->Model->Simpan('tb_event', $data);
        
        $to = "/topics/global";
        $values = array();
        $values['status'] = "Event";
        $values['title'] = $nama;
        $values['message'] = $detail;
        $values['image'] = $image;
        $values['timestamp'] = $tanggal.date(" h:i:s");
        $values['address'] = $lokasi;
        $values['lat'] = $Latitude;
        
        
        notification($to,$values);

		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan '$tanggal'</strong></div>");
			header('location:'.base_url().'event');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'event');
		}		
	}

	function editevent($kode = 0){
		$data_slide = $this->Model->GetEvent("where id_event = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_event' => $data_slide[0]['id_event'],
			'nama_event' => $data_slide[0]['nama_event'],
			'detail_event' => $data_slide[0]['detail_event'],
			'lokasi' => $data_slide[0]['lokasi'],
			'lat' => $data_slide[0]['lat'],
			'lang' => $data_slide[0]['lang'],
			'tanggal' => $data_slide[0]['tanggal'],
			);
		$this->load->view('event/edit_event', $data);
	}

	function updateevent(){
		$config = array(
			'upload_path' => './assets/upload/event',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		if ($file_name != null) {
			$data = array(
				'id_event' => $this->input->post('id'),
				'nama_event' => $this->input->post('nama'),
				'detail_event' => $this->input->post('detail'),
				'lokasi' => $this->input->post('lokasi'),
				'lat' => $this->input->post('Latitude'),
				'lang' => $this->input->post('Longitude'),
				'tanggal' => $this->input->post('tgl'),
				'image'	=> base_url('assets/upload/event/'.$file_name)
				//'image'	=> $file_name
				);

			$res = $this->Model->UpdateEvent($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'event');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'event');
			}
		}else{
			$data = array(
				'id_event' => $this->input->post('id'),
				'nama_event' => $this->input->post('nama'),
				'detail_event' => $this->input->post('detail'),
				'lokasi' => $this->input->post('lokasi'),
				'lat' => $this->input->post('Latitude'),
				'lang' => $this->input->post('Longitude'),
				'tanggal' => $this->input->post('tgl')
				);

			$res = $this->Model->UpdateEvent($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'event');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'event');
			}
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_event', array('id_event' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'event');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'event');
		}
	}
	
	function notification(){
	        // Server key from Firebase Console
        define( 'API_ACCESS_KEY', 'AIzaSyDe328lwM1xfdq9OcNzp-YgXkOOWf93Wfs' );
        
        $data = array("to" => "/topics/global",
                      "notification" => 
                      array( 
                          "title" => "Shareurcodes.com", 
                          "body" => "A Code Sharing Blog!",
                          "icon" => "icon.png", 
                          "data" => "http://shareurcodes.com"
                          ));                                                                    
        $data_string = json_encode($data); 
        
        echo "The Json Data : ".$data_string; 
        
        $headers = array
        (
             'Authorization: key=' . API_ACCESS_KEY, 
             'Content-Type: application/json'
        ); 
        $ch = curl_init();  
        
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );                                                                  
        curl_setopt( $ch,CURLOPT_POST, true );  
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string); 
        
        $result = curl_exec($ch);
        
        curl_close ($ch);
        
        echo "<p>&nbsp;</p>";
        echo "The Result : ".$result;
	}


}
