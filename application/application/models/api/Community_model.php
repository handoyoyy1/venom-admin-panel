<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Community_model extends CI_Model
{
	public $table = 'tb_community';
    public $id = 'id_community';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }
    
    function get_all_kota()
    {
        $this->db->order_by('namaKota', 'ASC');
        return $this->db->get('tb_kota')->result();
    }


    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    function get_by_kota($id)
    {
        $this->db->where('kota_id', $id);
        return $this->db->get($this->table)->result();
    }
    
    function get_by_cari($name)
    {
        $this->db->like('nama', $name);
        return $this->db->get($this->table)->result();
    }
    
    function get_by_qrcode($qrcode)
    {
        $this->db->where('qr_code', $qrcode);
        return $this->db->get($this->table)->row();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    public function exist_row_check($field,$data){
        $this->db->where($field,$data);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function get_by_userid($id)
    {
        $this->db->where("user_id", $id);
        return $this->db->get($this->table)->row();
    }


}