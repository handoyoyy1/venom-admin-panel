<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Halaman extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	} 

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data' => $this->Model->GetHalaman("order by id_halaman desc")->result_array(),
		);
		$this->load->view('halaman/halaman', $data);
	}

	function addhalaman()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('halaman/add_halaman', $data);
	}

	function savedata(){
		$id_halaman = '';
		$judul = $_POST['judul'];
		$isi = $_POST['isi'];

		$data = array(	
			'id_halaman'=> $id_news,
			'judul' => $judul,
			'isi' => $isi			
			);
		
		$result = $this->Model->Simpan('tb_halaman', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan '$tanggal'</strong></div>");
			header('location:'.base_url().'halaman');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'halaman');
		}		
	}

	function edithalaman($kode = 0){
		$data_slide = $this->Model->GetHalaman("where id_halaman = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_halaman' => $data_slide[0]['id_halaman'],
			'judul' => $data_slide[0]['judul'],
			'isi' => $data_slide[0]['isi']
			);
		$this->load->view('halaman/edit_halaman', $data);
	}

	function updatehalaman(){	
			$data = array(
				'id_halaman' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'isi' => $this->input->post('isi')
				);

			$res = $this->Model->UpdateHalaman($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'halaman');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'halaman');
			}
	
	}

	
	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_halaman', array('id_halaman' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'halaman');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'halaman');
		}
	}



}