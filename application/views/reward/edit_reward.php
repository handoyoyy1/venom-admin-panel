<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content-header">
        <h1>
          <b>EDIT DATA REWARD</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT REWARD</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>reward/updatereward" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Kode reward</label>
                        <input type="text" class="form-control" value="<?php echo $kode; ?>" id="" name="kode" placeholder="Isikan Kode reward" disabled="true">
                    </div>
                    <div class="form-group">
                      <label for="">Nama reward</label>
                        <input type="text" class="form-control" value="<?php echo $namareward; ?>" id="" name="nama" placeholder="Isikan Nama reward" required>
                    </div>

                    <div class="form-group">
                      <label for="">Point reward</label>
                        <input type="number" class="form-control" value="<?php echo $point; ?>" id="" name="point" placeholder="Isikan Point reward">
                    </div>

                    <div class="form-group">
                      <label for="">Stock reward</label>
                        <input type="number" class="form-control" value="<?php echo $stock; ?>" id="" name="stock" placeholder="Isikan Stock reward">
                    </div>

                    <div class="form-group">
                      <label for="">Alamat </label>
                         <textarea  class="form-control" name="alamat" placeholder="Isikan Alamat"><?php echo $alamat; ?></textarea></div>

                    <div class="form-group">
                      <label for="">expired</label>
                      <div class="input-group date">

                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                          <input type="date" value="<?php echo $expired; ?>" name="expired" class="form-control pull-right" id="datepicker">
                        </div>                       
                    </div>

                    <div class="form-group">
                      <label for="">Deskripsi reward</label>
                         <textarea id="ckeditor" class="ckeditor" name="deskripsi" placeholder="Isikan Deskripsi reward"  ><?php echo $deskripsi; ?></textarea>                   
                    </div>
                    
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Redeem reward</label>
                         <textarea id="ckeditor" class="ckeditor" name="redeem" placeholder="Isikan Redeem reward"  ><?php echo $redeem; ?></textarea>                   
                    </div>
                    
                    <div class="form-group">
                      <label for="">Term reward</label>
                         <textarea id="ckeditor" class="ckeditor" name="term" placeholder="Isikan Term reward" ><?php echo $term; ?></textarea>                   
                    </div>

                    <div class="form-group">
                      <label for="">Gambar reward</label>
                        <input type="file" class="form-control" value="" id="" name="file_upload" placeholder="">                        
                    </div>

                  </div>
                  
                    
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>reward" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved<a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>