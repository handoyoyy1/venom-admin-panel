<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Claim_model extends CI_Model
{
	public $table = 'tb_claim';
    public $id = 'id';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }

    function insert_otp($data)
    {
        $this->db->insert("tb_otp", $data);
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    public function exist_row_check($table,$field,$data){
        $this->db->where($field,$data);
        $this->db->from($table);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function check_claim($id,$kode){
        $this->db->where('user_id',$id);
        $this->db->where('reward_kode',$kode);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }


}