<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}
	
	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_user' => $this->Model->GetUserdata("where level != '0' and id_user !='61' and id_user !='17'  ")->result_array(),
		);
		$this->load->view('user/user', $data);
	}
	
	function adduser()
	{
		$data = array(
			'optlevel' => $this->Model->GetKatUser()->result_array(),
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('user/add_user', $data);
	}

	function savedata(){
		$id_user = '';
		$nama_user = $_POST['nama_user'];
		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->encode($_POST['password'], $key);
		$namadpn = $_POST['namadpn'];
		$namablk = $_POST['namablk'];
		$level = $_POST['level'];
		$nohp = $_POST['nohp'];
		$alamat = $_POST['alamat'];

		$data = array(	
			'id_user'=> $id_user,
			'nama_user' => $nama_user,
			'pass_user' => $password,
			'first_name' => $namadpn,
			'last_name' => $namablk,
			'level' => $level,
			'status' => 1,
			'alamat' => $alamat,
			'no_hp' => $nohp
			);
		
		$result = $this->Model->Simpan('tb_user', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan '$tanggal'</strong></div>");
			header('location:'.base_url().'user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user');
		}		
	}

	function detail($id){
		$data = $this->Model->GetUserdata("where id_user = '$id'")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'foto' => $data[0]['foto'],
			'id_user' => $data[0]['id_user'],
			'nama_user' => $data[0]['nama_user'],
			'alamat' => $data[0]['alamat'],
			'no_hp' => $data[0]['no_hp'],
			'level' => $data[0]['level'],
			);
		$this->load->view('user/detail', $data);
	}
	
	function edituser($kode = 0){
		$data = $this->Model->GetUserdata("where id_user = '$kode'")->result_array();
		$level_post_array = array();
		foreach($this->Model->GetUserdata("where id_user = '$kode'")->result_array() as $level){
			$level_post_array[] = $level['level'];
		}

		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->decode($data[0]['pass_user'], $key);

		$data = array(
			'level' => $this->Model->GetKatUser()->result_array(),
			'level_post' => $level_post_array,
			'nama' => $this->session->userdata('nama'),	
			'id_user' => $data[0]['id_user'],
			'nama_user' => $data[0]['nama_user'],
			'pass_user' => $password,
			'dpn' => $data[0]['first_name'],
			'blk' => $data[0]['last_name'],
			'alamat' => $data[0]['alamat'],
			'no_hp' => $data[0]['no_hp']
			);
		$this->load->view('user/edit_user', $data);
	}

	function updateuser(){

		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->encode($this->input->post('password'), $key);
		
			
			$data = array(
				'id_user' => $this->input->post('id'),
				'nama_user' => $this->input->post('nama_user'),
				'pass_user' => $password,
				'level' => $this->input->post('level'),
				'first_name' => $this->input->post('namadpn'),
				'last_name' => $this->input->post('namablk'),
				'alamat' => $this->input->post('alamat'),
				'no_hp' => $this->input->post('nohp')
				);

			$res = $this->Model->UpdateUser($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'user');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'user');
			}
	}
	
	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_user', array('id_user' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user');
		}
	}

	function profil(){
		$kode = $this->session->userdata('id_user');
		$data = $this->Model->GetUserdata("where id_user = '$kode' ")->result_array();

		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->decode($data[0]['pass_user'], $key);

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_user' => $data[0]['id_user'],
			'nama_user' => $data[0]['nama_user'],
			'pass_user' => $password,
			'level' => $data[0]['level'],
			'dpn' => $data[0]['first_name'],
			'blk' => $data[0]['last_name'],
			'alamat' => $data[0]['alamat'],
			'no_hp' => $data[0]['no_hp']
			);
		$this->load->view('user/profil', $data);
	}
	
	public function member()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_user' => $this->Model->GetUserdata("where level ='4'")->result_array(),

		);
		$this->load->view('user/member', $data);
	}
	
	public function dealer()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_user' => $this->Model->GetUserdata("where level not in ('1','2','3','4','8')")->result_array(),

		);
		$this->load->view('user/dealer', $data);
	}

	function editdealer($kode = 0){
		$data = $this->Model->GetUserdata("where id_user = '$kode'")->result_array();
		
		$level_post_array = array();
		foreach($this->Model->GetUserdata("where id_user = '$kode'")->result_array() as $level){
			$level_post_array[] = $level['level'];
		}
		
		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->decode($data[0]['pass_user'], $key);

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_user' => $data[0]['id_user'],
			'nama_user' => $data[0]['nama_user'],
			'pass_user' => $password,
			'level' => $this->Model->GetKatUser("where id not in ('1','2','3','4','8','10') ")->result_array(),
			'level_post' => $level_post_array,
			'dpn' => $data[0]['first_name'],
			'blk' => $data[0]['last_name'],
			'alamat' => $data[0]['alamat'],
			'no_hp' => $data[0]['no_hp']
			);
		$this->load->view('user/edit_dealer', $data);
	}

	function updatedealer(){
			
			$data = array(
				'id_user' => $this->input->post('id'),
				'level' => $this->input->post('level')
				);

			$res = $this->Model->UpdateUser($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'user/dealer');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'user/dealer');
			}
	}
	
	function hapus_member($kode = 1){
		
		$result = $this->Model->Hapus('tb_user', array('id_user' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'user/member');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user/member');
		}
	}
	
	public function lihat()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_user' => $this->Model->GetUserdata("where level = 0")->result_array(),
		);

		if($this->session->userdata('level') == 1 ){
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Tidak dapat mengakses halaman ini!!!</strong></div>");
			header('location:'.base_url().'dashboard');
		}else{
			$this->load->view('user/member', $data);
		}
		
	}

}

?>