<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_model extends CI_Model
{
	public $table = 'tb_invoice';
	public $id = 'id_user';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    function insertorder($data)
    {
        $this->db->insert('tb_order', $data);
    }

    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }
    
    function get_by_status($iduser,$status)
    {
        $this->db->where('id_user', $iduser);
        $this->db->where('status', $status);
        return $this->db->get($this->table)->result();
    }
    
     function get_by()
    {
        $this->db->order_by('create_on', 'DESC');
        $this->db->limit(10);
        return $this->db->get($this->table)->row();
    }
    
     function get_by_kode($id)
    {
        $this->db->where('kode_invoice', $id);
        return $this->db->get($this->table)->row();
    }
    
    function get_data_user($id)
    {
        $this->db->where('id_user', $id);
        return $this->db->get('tb_login')->row();
    }
    
    function data_invoice($kode){
        $this->db->select(' tb_produk.foto as foto, tb_order.jumlah, tb_produk.judul, tb_produk.harga, (tb_produk.harga * tb_order.jumlah) as total');
        $this->db->from('tb_order');
        $this->db->join('tb_produk','tb_produk.id_produk = tb_order.id_barang');
        $this->db->where('tb_order.kode_invoice',$kode);

        $this->db->order_by('id_order', 'ASC');
        return $this->db->get()->result();
    }
    
    function total($kode){
        $this->db->select('SUM( (tb_produk.harga * tb_order.jumlah)) as total');
        $this->db->from('tb_order');
        $this->db->join('tb_produk','tb_produk.id_produk = tb_order.id_barang');
        $this->db->where('tb_order.kode_invoice',$kode);
        return $this->db->get()->row();
    }

}