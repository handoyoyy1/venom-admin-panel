<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Diskon_model extends CI_Model
{
	public $table = 'tb_diskon';
    public $kode = 'kode_voucher';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }
    
    public function exist_row_check($field,$data){
        $this->db->where($field,$data);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }


    function cek_expired($kode)
    {
        $this->db->where($this->kode, $kode);
        return $this->db->get($this->table)->row();
    }

    function cek_voucher($kode,$id_user)
    {
        $this->db->where($this->kode, $kode);
        $this->db->where('id_user', $id_user);
        return $this->db->get('tb_invoice')->num_rows();
    }


}