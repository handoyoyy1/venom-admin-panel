<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SubKategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'subkategori' => '',
			'id_subkat' => '',
			'tgl_input_subkat' => '',
			'data_subkategori' => $this->Model->GetSubKat("order by id_kat desc")->result_array(),
		);

		$this->load->view('subkategori/data_subkategori', $data);
	}

	function editsubkategori($kode = 0){		
		$tampung = $this->Model->GetSubKat("where id_kat = '$kode'")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'tgl_input_subkat' => $tampung[0]['tgl_input_kat'],
			'subkategori' => $tampung[0]['kategori'],
			'id_subkat' => $tampung[0]['id_kat'],
			'data_subkategori' => $this->Model->GetSubKat("where id_kat != '$kode' order by id_kat desc")->result_array()
			);
		$this->load->view('subkategori/data_subkategori', $data);
	}

	function savedata(){
		if($_POST){
			$status = $_POST['status'];
			$id_subkat = $_POST['id_subkat'];
			$subkategori = $_POST['subkategori'];
			$tgl_input_subkat = $_POST['tgl_input_subkat'];
			if($status == "baru"){
				$data = array(
					'id_kat' => $id_subkat,
					'kategori' => $subkategori,
					'tgl_input_kat' => date("Y-m-d H:i:s"),
					);
				$result = $this->Model->Simpan('tb_subkategori', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'subkategori');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'subkategori');
				}
			}else{
				$data = array(
					'kategori' => $subkategori
					);
				$result = $this->Model->Update('tb_subkategori', $data, array('id_kat' => $id_subkat));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'subkategori');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'subkategori');
				}
			}
		}else{
			echo('handak baapa nyawa tong!!!');
		}
	}

	function hapussubkat($kode = 1){
		
		$result = $this->Model->Hapus('tb_subkategori', array('id_kat' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'subkategori');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'subkategori');
		}
	}
}

// Developed by Muhammad Ridho
// Email: kenduanything23@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */