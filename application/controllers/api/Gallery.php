<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Gallery extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('api/Gallery_model','api/Video_model'));
    }

    

    function index_get(){
        $data = $this->get('data');
        $idalbum = $this->get('idalbum');
        $id_user = $this->get('iduser');
        $community_id = $this->get('idcommunity');
        
        switch ($data) {
            case "album":
                $community = $this->Gallery_model->get_all_album();
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;                
            case "foto":
                $community = $this->Gallery_model->get_all_foto($idalbum);
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "fotolimit":
                $community = $this->Gallery_model->get_all_foto_limit($idalbum);
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "video":
                $community = $this->Video_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $community,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "getalbum":
                if($this->Gallery_model->exist_row_check('tb_album','community_id', $community_id) > 0)
                {
                    $community = $this->Gallery_model->get_by_communityid($community_id);
                    $this->response([
                                'kode' => 1,
                                'albumid' => $community->id_album,
                                'nama' => $community->nama,
                                'pesan' =>'Data tidak kosong!'
                            ], REST_Controller::HTTP_OK);
                }else{
                    $this->response([
                                'kode' => 1,
                                'albumid' => 'NULL',
                                'nama' => 'NULL',
                                'pesan' =>'Data tidak kosong!'
                            ], REST_Controller::HTTP_OK);

                }

                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }
    }
    
    function index_post(){
        $id_foto = $this->post('idfoto');
        $id_user = $this->post('iduser');
        $data = $this->post('data');
        
        $nama_foto = $this->post('namafoto');
        $album_id = $this->post('albumid');
        $judul = $this->post('judul');
        $deskripsi = $this->post('deskripsi');
        $url = $this->post('videoid');
       
	     
	    switch ($data) {
            case "like":
                if ($this->Gallery_model->exist_row_check_like($id_foto, $id_user) <= 0) {
                        $data_simpan = array(
                        'foto_id' => $id_foto,
                        'user_id' => $id_user,
                        'name' => 'like'
                    );
            	     $this->Gallery_model->insert_like($data_simpan);
            	     $message = array("kode"=>1,"pesan"=>"Data like berhasil disimpan!");
            	     $this->response($message, REST_Controller::HTTP_CREATED); 
                }else{
                    $message = array("kode"=>0,"pesan"=>"Data dislike pernah disimpan!");
                    $this->response($message, REST_Controller::HTTP_CREATED);
                }
                
        	     
                break;
            case "dislike":
                if ($this->Gallery_model->exist_row_check_dislike($id_foto, $id_user) <= 0) {
                    $data_simpan = array(
                        'foto_id' => $id_foto,
                        'user_id' => $id_user,
                        'name' => 'dislike'
                    );
                     $this->Gallery_model->insert_dislike($data_simpan);
                     $message = array("kode"=>0,"pesan"=>"Data dislike pernah disimpan!");
                     $this->response($message, REST_Controller::HTTP_CREATED);  
                }else{
                    
                }
                
                 
                break;     
            case "foto":
                $decode = base64_decode($nama_foto);
                $nama = uniqid() . '.png';
                $path = "./assets/upload/album/foto/".$nama;
                $file = fopen($path, 'wb');
                $is_writen = fwrite($file, $decode);
                fclose($file);
                $nama_foto = base_url()."assets/upload/album/foto/".$nama;
                $data_simpan = array(
                    'nama_foto' => $nama_foto,
                    'album_id' => $album_id,
                    'status_foto' => 'draft',
                    'user_id' => $id_user
                );
        	     $this->Gallery_model->insert_foto($data_simpan);
        	     $message = array("kode"=>1,"pesan"=>"Data berhasil disimpan!");
        	     $this->response($message, REST_Controller::HTTP_CREATED);
                break; 
            case "video":
                $data_simpan = array(
                    'judul' => $judul,
                    'deskripsi' => $deskripsi,
                    'url' => $url,
                    'status' => 'draft',
                    'user_id' => $id_user
                );
        	     $this->Gallery_model->insert_video($data_simpan);
        	     $message = array("kode"=>1,"pesan"=>"Data berhasil disimpan!");
        	     $this->response($message, REST_Controller::HTTP_CREATED);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }
 	}
}