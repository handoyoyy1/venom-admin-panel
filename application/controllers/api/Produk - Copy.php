<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Produk extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Produk_model');
    }

    

    function index_get(){
    	$data = $this->get('data');
    	$name = $this->get('name');
        $produk_id = $this->get('idproduk');
        $kategori_id = $this->get('idkategori');
        $subkategori_id = $this->get('idsubkategori');
        $user_id = $this->get('iduser');
        switch ($data) {
            case "all":
                $produk = $this->Produk_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "hot":
                $produk = $this->Produk_model->get_by_hot($user_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "express":
                $produk = $this->Produk_model->get_by_express($user_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "simulator":
                $produk = $this->Produk_model->get_by_simulator($kategori_id,$user_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "kategori":
                $produk = $this->Produk_model->get_by_kategori($kategori_id,$user_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
                case "subkategori":
                $produk = $this->Produk_model->get_by_subkategori($subkategori_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "cari":
                $produk = $this->Produk_model->get_all_cari($name,$id_user);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "detail":
                $produk_detail = $this->Produk_model->get_by_id($produk_id,$id_user);
                $this->response([
                            'kode' => 1,
                            'id_produk' => $produk_detail->id_produk,
                            'judul' => $produk_detail->judul,
                            'harga' => $produk_detail->harga,
                            'jumlah' => $produk_detail->jumlah,
                            //'kondisi' => $produk_detail->kondisi,
                            'id_merk' => $produk_detail->id_merk,
                            'id_kat' => $produk_detail->id_kat,
                            'id_subkat' => $produk_detail->id_subkat,
                            'ket' => $produk_detail->ket,
                            'status' => $produk_detail->status,
                            'counter' => $produk_detail->counter,
                            'tgl_input_pro' => $produk_detail->tgl_input_pro,
                            'foto' =>explode(" ", substr($produk_detail->foto,0,-1)) ,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }
}