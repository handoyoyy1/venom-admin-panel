<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Invoice extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Invoice_model');
    }

    function index_get(){
        $data = $this->get('data');
        $user_id = $this->get('iduser');
        $invoice = $this->get('kode');
        $status = $this->get('status');

        switch ($data) {
            
            case "all":
                $invoice = $this->Invoice_model->get_by_status($user_id,$status);
                $this->response([
                            'kode' => 1,
                            'result' => $invoice,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "detail":
                //$diskon = $this->Invoice_model->get_diskon($invoice);
                $detail = $this->Invoice_model->data_invoice($invoice);
                $inv = $this->Invoice_model->get_by_kode($invoice);
                //$total = $this->Invoice_model->total($invoice,$diskon);
                $this->response([
                            'kode' => 1,
                            'nama' =>$inv->nama,
                            'nohp' =>$inv->no_hp,
                            'alamat' => $inv->alamat,
                            'kode_invoice'=> $inv->kode_invoice,
                            'tanggal'=> $inv->tanggal,
                            'order' => $detail,
                            //'total_all' => $total->total,
                            'status' => $inv->status,
							'jenis' => $inv->jenis,
							'kategori' => $inv->kategori,
                            //'diskon' => $diskon ,
							'harga_total' =>$inv->harga_total,
							'diskon_total' =>$inv->diskon_total,
							'sub_total' =>$inv->sub_total,
							'diskon_voucer' =>$inv->diskon_voucer,
							'ppn' =>$inv->ppn,
							'grand_total' =>$inv->grand_total,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }  

    }

     function index_post(){
       
      // $kode_invoice = rand(0,1000000);
       $id_user = $this->post('iduser');
       $alamat = $this->post('alamat');
	   $keterangan = $this->post('nota');
       $nama = $this->post('nama');
       $no_hp = $this->post('nohp');
       $cart = $this->post('cart');
       $jenis = $this->post('jenis');
       $category = $this->post('category');
       $idcomunity = $this->post('idcommunity');
	   $harga_total = $this->post('hargatotal');
	   $diskon_total = $this->post('diskontotal');
	   $sub_total = $this->post('subtotal');
	   $diskon_voucer = $this->post('diskonvoucer');
	   $ppn = $this->post('ppn');
	   $grand_total = $this->post('grandtotal');
       
       $count = $this->db->count_all('tb_invoice');
	   $row = $this->Invoice_model->get_by();
	   if($count!= 0){
          $kode_invoice = $row->kode_invoice + 1;
    	}else{
    	    $kode_invoice = '100001';
    	}
       
       $config = array();
		$config['useragent'] = "CodeIgniter";
		$config['mailpath'] = "/usr/bin/sendmail";
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "localhost";
		$config['smtp_port'] = "25";
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['wordwrap'] = TRUE;
		
		$this->load->library('email');

       $data_simpan = array(
                    'kode_invoice' => $kode_invoice,
                    'id_user' => $id_user,
                    'status' => "unpaid",
                    'alamat' => $alamat,
					'keterangan' => $keterangan,
                    'nama' => $nama,
                    'jenis' => $jenis,
                    'comunity_id' => $idcomunity,
                    'no_hp' => $no_hp, 
                    'cart' => $cart,
                    'kategori' => $category,
					'harga_total' => $harga_total,
					'diskon_total' => $diskon_total,
					'sub_total' => $sub_total,
					'diskon_voucer' => $diskon_voucer,
					'ppn' => $ppn,
					'grand_total' => $grand_total
                );
                
                
        $json =json_decode($cart);

	   $masuk = $this->Invoice_model->insert($data_simpan);
	     
	     foreach($json as $row){
          $id  = $row->id_product;
          $jml = $row->jml_barang;
               $order = array(
                    'id_barang' => $id,
                    'jumlah' => $jml,
                    'kode_invoice' => $kode_invoice
               );
           

          $data_order = $this->Invoice_model->insertorder($order);
	     }
	     
     if($masuk == 1 && $data_order){
           
           $data_user = $this->Invoice_model->get_data_user($order);
           $list_order = $this->Invoice_model->list($kode_invoice);
           $email = $data_user->email;
           
           if($email != null){
               $bodymember = "
                		
                    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                        <html xmlns='http://www.w3.org/1999/xhtml'>
                        <head>
                        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
                        <title>Untitled Document</title>
                        </head>
                        
                        <body>
                          <br><br>
                        <table width='100%'>
                           <tr>
                             <td width='100px'>Nama</td>
                             <td width='400px'>$nama</td>
                              <td width='100px'>No. Invoice</td>
                              <td>$kode_invoice</td>
                           </tr>
                           <tr>
                             <td >No. Telepon</td>
                              <td>$no_hp</td>
                              <td>Tanggal</td>
                              <td>$tgl</td>
                            </tr>
                             <tr>
                               <td>$alamat</td>
                               <td colspan='3'>ini alamat</td>
                             </tr>
                           </table >
                         <br><br>
                            <table  width='100%' class='table table-bordered table-striped' border='1'>
                              <thead>
                                <tr>
                                 <th>NO</th>
                                  <th>Nama Barang</th>
                                   <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th>Total</th>
                                  </tr>
                             </thead>
                              <tbody>
                              foreach($list_order as $row){
                                <tr>
                                   <td>1</td>
                                    <td>$row-></td>
                                     <td>$row-></td>
                                     <td>$row-></td>
                                     <td>$row-></td>
                                 </tr>
                              </tbody>
                            <tr>
                              }
                           <th colspan='4' ><div align='center'>Total</div></th>
                              <th>ini total all</th>
                           </tr>
                          </table>
                          <br><br>
                          <table >
                           <tr>
                             <td rowspan='3'>image</td>
                             <td width='120px'>Nama Bank</td>
                              <td >ini data atas nama</td>
                           </tr>
                           <tr>
                             <td>Atas Nama</td>
                              <td>ini atas nama</td>
                            </tr>
                            <tr>
                             <td>No. Rekening</td>
                              <td>ini nomor</td>
                            </tr>
                           </table >
                        </body>
                        </html>

                
                		 ";
                
                        $this->email->initialize($config);
                
                        $this->email->from("hollowdark238@gmail.com", "admin");
                        $this->email->to($email);
                        
                        $this->email->subject('Admin Olshop');
                        $this->email->message($bodymember);
                        
                
                       // $this->email->send();
                       $this->email->send();
           }
       }
       
	     $message = array("kode"=>1,"pesan"=>"Invoice berhasil disimpan!", "invoice"=>$kode_invoice);
	     $this->response($message, REST_Controller::HTTP_CREATED);
 	}
}
