<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}

	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_konfirmasi' => $this->Model->GetKonfirmasi("order by id_konfirmasi desc")->result_array(),
		);

		$this->load->view('konfirmasi/konfirmasi', $data);
	}
	
	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_konfirmasi', array('id_konfirmasi' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'Konfirmasi');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'Konfirmasi');
		}
	}
}