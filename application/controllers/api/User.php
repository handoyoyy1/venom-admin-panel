<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('site_helper');
        $this->load->model('api/User_model');
        $this->load->model('api/Room_model');
    }

    function index_get(){
        $data = $this->get('data');
        $code = $this->get('code');
        $user = $this->get('user');
        $pass = $this->get('pass');
        $id = $this->get('iduser');
        $email = $this->get('email');
        $token = $this->get('token');

        if ($data == "login") {
            if ($this->User_model->exist_row_check('nama_user', $user) > 0) {
                $user_detail = $this->User_model->get_user_detail($user);
                    $this->load->library('encrypt'); 
                    $key = 'vyanarypratamabanyuwangi12345678';
                    $password =  $this->encrypt->decode($user_detail->pass_user, $key);
                    if ($password == $pass) {
                        $nama = $user_detail->first_name.' '.$user_detail->last_name;
                        $data = array(
                        'token' => $token,
                        );
                        $this->User_model->update($user_detail->id_user, $data);
                        $message = array("kode"=>1,"id"=>$user_detail->id_user, "nama"=>$user_detail->first_name.' '.$user_detail->last_name, "alamat"=>$user_detail->alamat,  "email"=>$user_detail->email, "level"=>$user_detail->level,"point"=>$user_detail->point,"balance"=>$user_detail->balance, "pesan"=>"Data tidak kosong!");
                        $this->response($message, REST_Controller::HTTP_OK);
                        return TRUE;
                    }else{
                        $message = array("kode"=>3,"pesan"=>"Passowrd anda salah!");
                        $this->response($message, REST_Controller::HTTP_OK);
                    }
            }else{
                $message = array("kode"=>2, "pesan"=>"Maaf username anda salah!");
                $this->response($message, REST_Controller::HTTP_OK);
            }
        }elseif ($data == "pass") {
            if ($this->User_model->exist_row_check('id_user', $id) > 0) {
                $user_detail = $this->User_model->get_by_id($id);
                    $this->load->library('encrypt'); 
                    $key = 'vyanarypratamabanyuwangi12345678';
                    $password =  $this->encrypt->decode($user_detail->pass_user, $key);
                    if ($password == $pass) {
                        $message = array("kode"=>1,"pesan"=>"Passowrd anda benar!");
                        $this->response($message, REST_Controller::HTTP_OK);
                    }else{
                        $message = array("kode"=>0,"pesan"=>"Passowrd anda salah!");
                        $this->response($message, REST_Controller::HTTP_OK);
                    }
            }else{
                $message = array("kode"=>0, "pesan"=>"Maaf username anda salah!");
                $this->response($message, REST_Controller::HTTP_OK);
            }

        }elseif ($data == "detail") {
            $user_detail = $this->User_model->get_by_id($id);
                $this->response([
                            'kode' => 1,
                            'id_user' => $user_detail->id_user,
                            'user' => $user_detail->nama_user,
                            'first_name' => $user_detail->first_name,
                            'last_name' => $user_detail->last_name,
                            'foto' => $user_detail->foto,
                            'email' => $user_detail->email,
                            'alamat' => $user_detail->alamat,
                            'phone' => $user_detail->no_hp,
                            'point' => $user_detail->point,
                            'balance' => $user_detail->balance,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
        }elseif ($data == "verificate") {
            if ($this->User_model->exist_row_check('code_verifikasi', $code) > 0) {
                $data = array('status' => 1);
                $this->User_model->update($id, $data);
                $this->response([
                            'kode' => 1,
                            'pesan' =>'Your akun activated!'
                        ], REST_Controller::HTTP_OK);
                
            }else{
                $this->response([
                            'kode' => 0,
                            'pesan' =>'Your code Invalid!'
                        ], REST_Controller::HTTP_OK);
            }
        }elseif ($data == "forgot") {
            if ($this->User_model->exist_row_check('email', $email) > 0) {
                $user_detail = $this->User_model->get_user_detail_email($email);
                $nama_user = $user_detail->nama_user;
                $this->load->library('encrypt'); 
                $pass = rand(0,1000000);
                $key = 'vyanarypratamabanyuwangi12345678';
                $password =  $this->encrypt->encode($pass, $key);
                
                
                $this->load->library('email');
                $config = send_email();
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");
        
                $list = array($email);
                $this->email->to($list);
                $this->email->from('bluecoreotp@gmail.com','Venom');
                $this->email->subject('Reset akun');
                $this->email->message('New password your account :'.$pass.'<br> username your account :'.$nama_user);
                
                //Send email
                $this->email->send();
                
                
                
                $data = array('pass_user' => $password);
                $this->User_model->update($user_detail->id_user, $data);
                $this->response([
                            'kode' => 1,
                            'pesan' =>'Succes reset password, cek your email!'
                        ], REST_Controller::HTTP_OK);
                
            }else{
                $this->response([
                            'kode' => 0,
                            'pesan' =>'Your email not register!'
                        ], REST_Controller::HTTP_OK);
            }
            
        }elseif ($data == "gmail") {
            if ($this->User_model->exist_row_check('email', $email) > 0) {
                $user_detail = $this->User_model->get_user_detail_email($email);
                $data = array(
                        'token' => $token,
                        );
                        $this->User_model->update($user_detail->id_user, $data);
                
                $message = array("kode"=>1,"id"=>$user_detail->id_user, "nama"=>$user_detail->first_name.' '.$user_detail->last_name, "level"=>$user_detail->level,"point"=>$user_detail->point,"balance"=>$user_detail->balance, "pesan"=>"Anda berhasil Login!");
                $this->response($message , REST_Controller::HTTP_OK);
            }else{
                $this->response([
                            'kode' => 0,
                            'pesan' =>'Your email not register!'
                        ], REST_Controller::HTTP_OK);
            }
        }
        
        
    }

    function index_post(){
        $data = $this->post('data');
        $first = $this->post('first');
        $last = $this->post('last');
        $this->load->library('encrypt'); 
        $key = 'vyanarypratamabanyuwangi12345678';
        $password =  $this->encrypt->encode($this->input->post('pass'), $key);
        $nama_user = $this->post('user');
        $foto = $this->post('foto');
        $alamat = $this->post('address');
        $email = $this->post('email');
        $no_hp = $this->post('phone');
        $token = $this->post('token');
        $level = $this->post('level');
        $code_verifikasi = rand(0,1000000);
        
        if ($this->User_model->exist_row_check('email', $email) > 0) {
            $message = array("kode"=>3,"pesan"=>"Email sudah digunakan, mohon diganti yang lain!");
                $this->response($message, REST_Controller::HTTP_OK);
        }else{
            if ($this->User_model->exist_row_check('nama_user', $nama_user) > 0) {
                $message = array("kode"=>2,"pesan"=>"Username sudah digunakan, mohon diganti yang lain!");
                $this->response($message, REST_Controller::HTTP_OK);
             } else {
                if ($data =="daftar") {
                    $data = array(
                        'nama_user' => $nama_user,
                        'pass_user' => $password,
                        'first_name' => $first,
                        'last_name' => $last,
                        'level' => $level,
                        'status' => "1",
                        'alamat' => $alamat,	
                        'token' => $token,
                        'email' => $email,
                        'no_hp' => $no_hp,
                        'code_verifikasi' => $code_verifikasi
    
                    );
                    $this->User_model->insert($data);
                    
                    $user_detail = $this->User_model->get_user_detail($nama_user);
                    
                        $data_simpan = array(
                        'name' => $code_verifikasi,
                        'user_id' => $user_detail->id_user
                    );
    
            	     $this->Room_model->insert($data_simpan);
                    
                   /* $this->load->library('email');
                    $config = send_email();
                    $this->email->initialize($config);
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");
            
                    $list = array($email);
                    $this->email->to($list);
                    $this->email->from('admin@levanpedia.com','Vibe24jam');
                    $this->email->subject('verifikasi akun');
                    $this->email->message('Kode verifikasi akun anda :'.$code_verifikasi);
                    
                    //Send email
                    $this->email->send();*/
                    
                    $message = array("kode"=>1,"id"=>$user_detail->id_user, "nama"=>$user_detail->nama_user, "level"=>$user_detail->level, "pesan"=>"Anda berhasil terdaftar!");
                    $this->response($message, REST_Controller::HTTP_OK);
                    
                    
                }else if($data =="gmail") {
    
                    $data = array(
                        'nama_user' => $nama_user,
                        'first_name' => $first,
                        'level' => $level,
                        'status' => "1",
                        'foto' => $foto,
                        'token' => $token,
                        'no_hp' => $no_hp,
                        'email' => $email
    
                    );
                    $this->User_model->insert($data);
                    
                    $user_detail = $this->User_model->get_user_detail_email($email);
                    
                    $code_verifikasi = rand(0,1000000);
                        $data_simpan = array(
                        'name' => $code_verifikasi,
                        'user_id' => $user_detail->id_user
                    );
    
            	     $this->Room_model->insert($data_simpan);
            	     
                    
                    $message = array("kode"=>1,"id"=>$user_detail->id_user, "nama"=>$user_detail->nama_user, "level"=>$user_detail->level, "pesan"=>"Anda berhasil terdaftar!");
                    $this->response($message, REST_Controller::HTTP_OK);
                }
             }
        }
    }

    

     function index_put(){
        $id = $this->put('id');
        $data = $this->put('data');
        $passlama = $this->put('passlama');
        $passbaru = $this->put('passbaru');
        //$photo = $this->put('img');
        $token = $this->put('token');
        $first = $this->put('first');
        $last = $this->put('last');
        $nama_user = $this->put('user');
        $status = 1;
        $foto = $this->put('foto');
        $alamat = $this->put('address');
        $no_hp = $this->put('phone');
        $email = $this->put('email');
        
        if($data=="password"){
            $user_detail = $this->User_model->get_by_id($id);
            $this->load->library('encrypt'); 
                    $key = 'vyanarypratamabanyuwangi12345678';
                    $password =  $this->encrypt->decode($user_detail->pass_user, $key);

            if($passlama != $password){
                $message = array("kode"=>2,"pesan"=>"Maaf password lama anda salah!!");
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                $key = 'vyanarypratamabanyuwangi12345678';
                $password =  $this->encrypt->encode($passbaru, $key);
                    $data = array(
                        'pass_user' => $password
                        );
                        $this->User_model->update($id, $data);
                    $message = array("kode"=>1,"pesan"=>"Password berhasil dirubah!");
                    $this->response($message, REST_Controller::HTTP_OK);
            }
            
        }else if ($data == "img") {
                $decode = base64_decode($foto);
                $nama = uniqid() . '.png';
                $path = "./assets/upload/user/".$nama;
                $file = fopen($path, 'wb');
                $is_writen = fwrite($file, $decode);
                fclose($file);

                $data_photo = array(
                        'foto' => base_url()."assets/upload/user/".$nama
                        );
                        $this->User_model->update($id, $data_photo);
                    $message = array("kode"=>1,"pesan"=>"Gambar berhasil dirubah!");
                    $this->response($message, REST_Controller::HTTP_OK);
        }else if ($data == "token") {
            $data = array(
                        'token' => $token,
                        );
                        $this->User_model->update($id, $data);
                    $message = array("kode"=>1,"pesan"=>"Token berhasil dirubah!");
                    $this->response($message, REST_Controller::HTTP_OK);
            
        }else if ($data == "profil") {
                    $data_photo = array(
                    'nama_user' => $nama_user,
                    'first_name' => $first,
                    'last_name' => $last,
                    'alamat' => $alamat,
                    'no_hp' => $no_hp,
                    'email' => $email
                            );
                    $this->User_model->update($id, $data_photo);
                    $message = array("kode"=>1,"pesan"=>"profil berhasil dirubah!");
                    $this->response($message, REST_Controller::HTTP_OK);
        }
    
    }
    
}