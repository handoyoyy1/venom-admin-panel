<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testimoni_model extends CI_Model
{
	public $table = 'tb_testimoni';
    public $id = 'tb_testimoni.id_user';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all()
    {
     
     $this->db->select('tb_testimoni.komentar,tb_login.nama_user, tb_login.foto');
        $this->db->from('tb_testimoni');
        $this->db->join('tb_login','tb_login.id_user = tb_testimoni.id_user');
        $this->db->order_by($this->id, $this->order);
        $this->db->limit(6); 
        return $this->db->get()->result();
    }

    public function insert($data){
        
        $this->db->insert('tb_testimoni', $data);
    }


}