<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model
{
	public $table = 'tb_produk';
    public $id = 'id_produk';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }



    function get_all()  
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where("harga !=", 0);
        $this->db->where("id_subkat", 6);
        $this->db->where("id_subkat", 7);     
        return $this->db->get(" tb_subkategori")->result();
    }
    
    function get_all_cari($data,$id_user)  
    {

        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table);        
        $this->db->where("id_kat !=", 6);
        $this->db->where("id_kat !=", 7);
        $this->db->where("id_kat !=", 8);
        $this->db->where("id_kat !=", 9);
        $this->db->where("id_kat !=", 10);
        $this->db->where("id_kat !=", 11);
        $this->db->where("id_kat !=", 12);
        $this->db->where("id_kat !=", 13);
        $this->db->where("id_kat !=", 14);
        $this->db->where("id_kat !=", 15);
        $this->db->where("id_kat !=", 16);
        $this->db->where("id_kat !=", 17);
        $this->db->where("id_kat !=", 18);
        $this->db->where("id_kat !=", 19);
        $this->db->where("id_kat !=", 20);
        $this->db->where("harga !=", "null");
        $this->db->order_by($this->id, $this->order);
        $this->db->like("judul", $data);
        return $this->db->get()->result();
    }
    
    function get_by_hot($id_user)  
    {
        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table);      
        $this->db->where("id_subkat", 6);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get()->result();
    }
    
    function get_by_express($id_user)
    {
        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table);
        $this->db->where("harga !=", 0);
        $this->db->where("id_kat =", 7);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get()->result();
    }

    function get_by_id($id,$id_user)
    {
        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table); 
        $this->db->where("id_subkat", $id);
        return $this->db->get()->row();
    }

    function get_by_kategori($id,$id_user)
    {
        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table);
        $this->db->where("id_subkat", $id);
        return $this->db->get()->result();
                
    }
    
    
    function get_by_simulator($id,$id_user)
    {
        if($id_user != "0"){
        $this->db->select('tb_kat_user.diskon as diskon');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        $data= $this->db->get()->row();
        $diskon = $data->diskon;
        }else {
            $diskon = "0";
        }
        
        $this->db->select('id_produk,judul,FLOOR((harga-harga*'.$diskon.'/100)) as harga ,point,jumlah,id_merk,id_kat,ket,status,counter,
            tgl_input_pro,foto,id_subkat');
        $this->db->from($this->table);
        $this->db->where("id_kat", $id);
        return $this->db->get()->result();
        
        
    }

     function get_all_kategori()  
    {
        $this->db->where("id_kat !=", 6);
        $this->db->where("id_kat !=", 7);
        return $this->db->get("tb_subkategori")->result();
    }
    
     function get_all_subkategori()  
    {
        $this->db->where("id_subkat !=", 1);
        $this->db->where("id_subkat !=", 2);
        $this->db->where("id_subkat !=", 3);
        $this->db->where("id_subkat !=", 4);
        $this->db->where("id_subkat !=", 5);
        return $this->db->get("tb_subkategori")->result();
    }


}