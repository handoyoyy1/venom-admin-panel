<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
<?php
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
 
	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}
$bulan1 = array(
                '1' => 'JANUARI',
                '2' => 'FEBRUARI',
                '3' => 'MARET',
                '4' => 'APRIL',
                '5' => 'MEI',
                '6' => 'JUNI',
                '7' => 'JULI',
                '8' => 'AGUSTUS',
                '9' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );
?>
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->

<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Bulan</th>
                      <!--<th>Nota</th>-->
                      <th>Sales</th>
                      <!--<th>AKSI</th>-->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_mount as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php if ($row['bulan'] == "1"){echo "Januari";} else if ($row['bulan'] == "2") {echo "Februari";} else if ($row['bulan'] == "3") {echo "Maret";}
					   else if ($row['bulan'] == "4") {echo "April";} else if ($row['bulan'] == "5") {echo "Mei";} else if ($row['bulan'] == "6") {echo "Juni";} else if ($row['bulan'] == "7") {echo "Juli";}
					   else if ($row['bulan'] == "8") {echo "Agustus";} else if ($row['bulan'] == "9") {echo "September";} else if ($row['bulan'] == "10") {echo "Oktober";} else if ($row['bulan'] == "11") {echo "November";}
					   else {echo "Desember";}?></td> 
                      <!--<td><?php echo $row['nota']; ?></td>-->
                      <td>Rp.&nbsp;<?php echo number_format($row['grand_total']); ?></td>
                      <!--<td>
                      <a class="btn btn-warning btn-sm" href="<?php //echo base_url(); ?>pesan/editpesan/<?php// echo $row['id']; ?>"><i class="fa fa-pencil"></i></a>
                      <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php //echo base_url(); ?>pesan/sales_hapus/<?php //echo $row['kode_invoice']; ?>"><i class="fa fa-trash"></i></a>
                      </td>-->
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>