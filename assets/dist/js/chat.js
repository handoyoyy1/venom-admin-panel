//create firebase reference

var dbRef = new Firebase("https://venom-id.firebaseio.com/");
var userRef = dbRef.child('messages');
var chatsRef = userRef.child(document.querySelector('#admin').value + '_' + document.querySelector('#token').value)
var chatsRef2 = userRef.child(document.querySelector('#token').value + '_' + document.querySelector('#admin').value)


//save chat
document.querySelector('#save').addEventListener("click", function(event) {
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
         "Aug", "Sept", "Oct", "Nov", "Dec"];
  var a = new Date(),
  b = a.getDate(),
  c = months[a.getMonth()];
  d = a.getFullYear(),  
  e = a.getHours(),
  f = a.getMinutes(), 
  g = a.getSeconds(),

  date = b + ' ' + c + ' ' + d + " " + e + ":" + f + ":" + g , 
     chatForm = document.querySelector('#msg_form');
  event.preventDefault();
  if (document.querySelector('#admin').value != '' && document.querySelector('#message').value != '') {
  chatsRef
  .push({
  user: document.querySelector('#admin').value,
  message: document.querySelector('#message').value,
  date: date
  })

  chatsRef2
  .push({
  user: document.querySelector('#admin').value,
  message: document.querySelector('#message').value,
  date: date
  })

  chatForm.reset();
  } else {
  alert('Please fill atlease admin or message!');
  }
}, false);


//load older conatcts as well as any newly added one...
chatsRef.on("child_added", function(snap) {
  console.log("added", snap.key(), snap.val());
  document.querySelector('#message_box').innerHTML += (chatHtmlFromObject(snap.val()));
});

//prepare conatct object's HTML
function chatHtmlFromObject(chat) {
  console.log(chat);
   var bubble = (chat.user == document.querySelector('#admin').value ? "bubble-right" : "bubble-left");
  var html = '<div class="' + bubble + '"><p><span class="admin">' + chat.user + '</span><span class="msgc">' + chat.message + '</span><span class="date">' + chat.date + '</span></p></div>';
  return html;
}