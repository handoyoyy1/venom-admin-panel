
<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- isi conten disini -->


<section class="content-header">
        <h1>
          <b>EDIT DATA HALAMAN</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT HALAMAN</h3>
              </div>
               <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>diskon/updatediskon" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-10">
                    <div class="form-group">
                      <label for="">Kode Voucher</label>
                        <input type="text" class="form-control" value="<?php echo $kode_voucher ?>" id="kode" name="kode_voucher" placeholder="Isikan Kode Voucher" required>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label style="color:#fff">vdvdv</label>
                      <a class="btn btn-primary" onclick="acakkode()">Acak Kode Voucher</a>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="">Nominal</label>
                      <input type="number" class="form-control" value="<?php echo $nominal ?>" name="nominal" required placeholder="Masukkan Nominal Diskon">
                    </div>
                    <div class="form-group">
                      <label>Kategori</label>
                      <select name="category" class="form-control" required>
                        <option>--Pilih Kategori--</option>
                          <option <?php if($category=="voucher"){echo "SELECTED";} ?> value="voucher">Voucher</option>
                          <option <?php if($category=="diskon"){echo "SELECTED";} ?> value="diskon">Diskon</option>
                      </select> 
                    </div>
                    <div class="form-group">
                      <label for="">Expired</label>
                      <input type="date" class="form-control" value="<?php echo $expired ?>" name="expired" required placeholder="Masukkan Expired Diskon">
                    </div>
                  </div>
                  
                    
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>diskon" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
        $('#pesan-flash').delay(4000).fadeOut();
        $('#pesan-error-flash').delay(5000).fadeOut();
      });
      function acakkode() {
        var acak="<?php echo substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8); ?>";
        console.log(acak);
        document.getElementById('kode').value = acak;
      }
    </script>
</body>
</html>