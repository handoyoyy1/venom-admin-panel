<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Present extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('site_helper');
        $this->load->model('api/Present_model');
    }
    
    function index_get(){	

    }

    function index_post(){
        //$data = $this->post('data');
       
       $id_ivent = $this->post('idevent');
       $id_user = $this->post('iduser');
       $name = $this->post('name');

       $data_simpan = array(
                    'event_id' => $id_ivent,
                    'user_id' => $id_user,
                    'name' => $name,
                );

	     $this->Present_model->insert($data_simpan);
	     $message = array("kode"=>1,"pesan"=>"Present berhasil disimpan!");
	     $this->response($message, REST_Controller::HTTP_CREATED);
 	}
}
