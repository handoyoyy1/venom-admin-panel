<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	public $table = 'tb_user';
    public $id = 'id_user';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }

    public function exist_row_check($field,$data){
        $this->db->where($field,$data);
        $this->db->from('tb_user');
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_user_detail($user)
    {
        $this->db->where('nama_user', $user);
        return $this->db->get($this->table)->row();
    }
    
    function get_user_detail_email($email)
    {
        $this->db->where('email', $email);
        return $this->db->get($this->table)->row();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

}