<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        
		<!-- Content Header (Page header) -->
		<section class="content-header">
          <!-- Small boxes (Stat box) -->
        <h1>
          <b>Data Invoice</b>
        </h1>
            <!-- isi conten disini -->
			<div class="row">
            <div class="col-md-12">
			<div class="box">
               <div class="box-title">
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="40">NO</th>
                      <th width="100">Tanggal</th>
                      <th width="100">Kode Invoice</th>
                      <th width="150">Nama</th>
                      <th width="100">Pembayaran</th>
                      <th width="100">Kategori</th>
                      <th width="100">Tempo</th>
					  <th width="100">Status</th>
					  <th width="100">Status Barang</th>
                      <th width="90">Pesan</th>
                      <!--<th>AKSI</th>-->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_tempo as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['create_on']; ?></td>
                      <td> <a href="<?php echo base_url(); ?>invoice/detail/<?php echo $row['kode_invoice']; ?>"><?php echo $row['kode_invoice']; ?></a>                      
                      </td>
                        <td><?php echo $row['nama']; ?></td>
                         <td><?php if ($row['jenis']== "credit") { 
							 echo "Tempo";
							 } 
							 else { 
							 echo $row['jenis'];
							 } ?></td>
                         <td><?php echo $row['kategori']; ?></td>
                         <td><?php echo $row['jatuh_tempo']; ?></td>
                      <td>
                      <?php if($row['status']=="paid"){ ?> 
                      <a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>invoice/update_status/<?php echo $row['kode_invoice']."/".$row['status']; ?>"><?php echo $row['status']; ?>  </a></td>
                   
                      <?php }else{?> 
                      <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>invoice/update_status/<?php echo $row['kode_invoice']."/".$row['status']; ?>"><?php echo $row['status']; ?>  </a></td>
                      <?php } ?>
                      <td><a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>invoice/update_barang_status/<?php echo $row['kode_invoice']."/".$row['barang_status']; ?>"><?php echo $row['barang_status']; ?>  </a></td>
                      <td><a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>invoice/pesan/<?php echo $row['id_user']; ?>">kirim pesan  </a></td>
                      </td>
					  <!--
                      <td>
                      <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>invoice/hapus/<?php echo $row['kode_invoice']; ?>"><i class="fa fa-trash"></i></a>
                      </td>
					  -->
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              
            </div><!-- /.box -->
         
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->




    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
	  </div>
	  </div>
	  </div>
	  </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>