<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Simulator_model extends CI_Model
{
	public $table = 'tb_simulator';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

}