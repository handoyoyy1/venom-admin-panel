<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi_model extends CI_Model
{
	public $table = 'tb_konfirmasi';
	public $id = 'id_user';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }

}