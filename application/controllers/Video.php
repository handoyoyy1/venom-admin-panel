<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_video' => $this->Model->GetVideo("order by id desc")->result_array(),
		);
		$this->load->view('video/video', $data);
	}

	function addvideo()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('video/add_video', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/video',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();

		$id = '';
		$judul = $_POST['judul'];
		$deskripsi = $_POST['deskripsi'];
		$url = $_POST['url'];
		$file_name = $upload_data['file_name'];

		$data = array(	
			'id'=> $id,
			'judul' => $judul,
			'deskripsi' => $deskripsi,
			'url' => $url,
			'foto' => base_url()."assets/upload/video/".$file_name		
			);
		
		$result = $this->Model->Simpan('tb_video', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'video');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'video');
		}		
	}

	function editvideo($kode = 0){
		$data_video = $this->Model->GetVideo("where id = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id' => $data_video[0]['id'],
			'judul' => $data_video[0]['judul'],
			'deskripsi' => $data_video[0]['deskripsi'],
			'url' => $data_video[0]['url'],
			'foto' => $data_video[0]['foto']
			);
		$this->load->view('video/edit_video', $data);
	}

	function updatevideo(){
		$config = array(
				'upload_path' => './assets/upload/video',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
				'url' => $this->input->post('url'),
				'foto' => base_url()."assets/upload/video/".$upload_data['file_name']
				);
			$row=$this->Model->GetVideo("where id = '$data[id]'")->row();
			unlink(str_replace(base_url(), "", $row->foto));
			$res = $this->Model->Updatevideo($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'video');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'video');
			}
		}else{
			
			$data = array(
				'id' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
				'url' => $this->input->post('url'),
				);

			$res = $this->Model->Updatevideo($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'video');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'video');
			}
		}
	}

	function hapus($kode = 1){
		$row=$this->Model->GetVideo("where id = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->foto));
		$result = $this->Model->Hapus('tb_video', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'video');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'video');
		}
	}
}