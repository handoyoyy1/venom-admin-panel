<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reward extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		$this->load->helper('site_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	} 

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_reward' => $this->Model->Getreward("order by kode desc")->result_array(),
		);
		$this->load->view('reward/reward', $data);
	}

	function addreward()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('reward/add_reward', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/reward',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		
		$kode = $_POST['kode'];
		$nama = $_POST['nama'];
		$stock = $_POST['stock'];
		$point = $_POST['point'];
		$alamat = $_POST['alamat'];
		$deskripsi = $_POST['deskripsi'];
		$redeem=$_POST['redeem'];
		$term=$_POST['term'];
		$expired = $_POST['expired'];
		$image = base_url('assets/upload/reward/'.$file_name);

		$qr_code = $_POST['kode'];
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/upload/reward/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name=$qr_code.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = $qr_code; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $qr_img = base_url('assets/upload/reward/qrcode/'.$image_name);
 

		$data = array(	
			'kode'=> $kode,
			'nama' => $nama,
			'stock' => $stock,
			'point' => $point,
			'alamat' => $alamat,
			'deskripsi' => $deskripsi,
			'redeem' => $redeem,
			'term' => $term,
			'expired' =>$expired,
			'images'	=> 	$image,
			'img_qrcode'	=> 	$qr_img	
			);
		
		$result = $this->Model->Simpan('tb_reward', $data);

		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan '$tanggal'</strong></div>");
			header('location:'.base_url().'reward');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'reward');
		}		
	}

	function editreward($kode = 0){
		$data_slide = $this->Model->Getreward("where kode = '$kode'")->result_array();

		
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'kode' => $data_slide[0]['kode'],
			'namareward' => $data_slide[0]['nama'],
			'stock' => $data_slide[0]['stock'],
			'point' => $data_slide[0]['point'],
			'alamat' => $data_slide[0]['alamat'],
			'deskripsi' => $data_slide[0]['deskripsi'],
			'redeem' => $data_slide[0]['redeem'],
			'term' => $data_slide[0]['term'],
			'expired' => $data_slide[0]['expired'],
			'image'	=> $data_slide[0]['images']	
			);
		$this->load->view('reward/edit_reward', $data);
	}

	function updatereward(){
		$config = array(
			'upload_path' => './assets/upload/reward',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		if ($file_name != null) {
			$data = array(
				'kode' => $this->input->post('kode'),
				'nama' => $this->input->post('nama'),
				'stock' => $this->input->post('stock'),
				'point' => $this->input->post('point'),
				'alamat' => $this->input->post('alamat'),
				'deskripsi' => $this->input->post('deskripsi'),
				'redeem' => $this->input->post('redeem'),
				'term' => $this->input->post('term'),
				'expired' => $this->input->post('expired'),
				'images'	=> base_url('assets/upload/reward/'.$file_name)
				);

			$res = $this->Model->Updatereward($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'reward');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'reward');
			}
		}else{
			$data = array(
				'kode' => $this->input->post('kode'),
				'nama' => $this->input->post('nama'),
				'stock' => $this->input->post('stock'),
				'point' => $this->input->post('point'),
				'alamat' => $this->input->post('alamat'),
				'deskripsi' => $this->input->post('deskripsi'),
				'redeem' => $this->input->post('redeem'),
				'term' => $this->input->post('term'),
				'expired' => $this->input->post('expired')
				);

			$res = $this->Model->Updatereward($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'reward');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'reward');
			}
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_reward', array('kode' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'reward');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'reward');
		}
	}
	


}
