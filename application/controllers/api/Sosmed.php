<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Sosmed extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Sosmed_model');
    }


    function index_get(){
        $data = $this->get('data');
        $sosmed_id = $this->get('idsosmed');
        $kategori = $this->get('kategori');

        switch ($data) {
            case "all":
                $sosmed = $this->Sosmed_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $sosmed,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "detail":
                $sosmed_detail = $this->Sosmed_model->get_by_id($sosmed_id);
                $this->response([
                            'kode' => 1,
                            'id_sosmed' => $sosmed_detail->id_sosmed,
                            'nama_sosmed' => $sosmed_detail->nama_sosmed,
                            'url' => $sosmed_detail->url,
                            'kategori' => $sosmed_detail->kategori,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "detailkategori":
                $sosmed_kategori = $this->Sosmed_model->get_by_kategori($kategori);
                $this->response([
                            'kode' => 1,
                            'id_sosmed' => $sosmed_kategori->id_sosmed,
                            'nama_sosmed' => $sosmed_kategori->nama_sosmed,
                            'url' => $sosmed_kategori->url,
                            'kategori' => $sosmed_kategori->kategori,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }  

    }
}