<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		$this->load->view('report/form', $data);
	}

	public function penjualanperhari()
	{
		$dari=$this->input->POST('dari');
		$sampai=$this->input->POST('sampai');
		$sekarang=date("Y-m-d");
		if (!$dari and !$sampai) {
			$dari=$sekarang;
			$sampai=$sekarang;
			$data_invoice=$this->Model->GetInvoice("WHERE tanggal LIKE '%$sekarang%'")->result_array();
		}else{
			$data_invoice=$this->Model->GetInvoice("WHERE tanggal BETWEEN '$dari' AND '$sampai' order by tanggal desc")->result_array();
		}
		
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'dari' => $dari,
			'sampai' => $sampai,
			'data_invoice' => $data_invoice,
		);
		$this->load->view('report/penjualanperhari', $data);
	}

	public function penjualanbyinvoice()
	{
		$kategori=$this->input->POST('kategori');
		if (!$kategori or $kategori=="semua") {
			$data_invoice=$this->Model->GetInvoice("order by tanggal desc")->result_array();
		}else{
			$data_invoice=$this->Model->GetInvoice("WHERE kategori = '$kategori' order by tanggal desc")->result_array();
		}
		
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_invoice' => $data_invoice,
			'kategori' => $kategori
		);
		$this->load->view('report/penjualanbyinvoice', $data);
	}


	public function cetak($page)
	{
		if ($page=="penjualanperhari") {
			$dari=$this->input->GET('dari');
			$sampai=$this->input->GET('sampai');
			$sekarang=date("Y-m-d");
			if (!$dari and !$sampai) {
				$dari=$sekarang;
				$sampai=$sekarang;
				$data_invoice=$this->Model->GetInvoice("WHERE tanggal LIKE '%$sekarang%'")->result_array();
			}else{
				$data_invoice=$this->Model->GetInvoice("WHERE tanggal BETWEEN '$dari' AND '$sampai' order by tanggal desc")->result_array();
			}
			$data = array(
				'nama' => $this->session->userdata('nama'),
				'data_invoice' => $data_invoice,
				'dari' => $dari,
				'sampai' => $sampai,
			);
			$this->load->view('report/cetak/penjualanperhari', $data);
		}elseif ($page=="penjualanbyinvoice") {
			$kategori=$this->input->GET('kategori');
			if (!$kategori or $kategori=="semua" or $kategori=="") {
				$data_invoice=$this->Model->GetInvoice("order by tanggal desc")->result_array();
			}else{
				$data_invoice=$this->Model->GetInvoice("WHERE kategori = '$kategori' order by tanggal desc")->result_array();
			}
			
			$data = array(
				'nama' => $this->session->userdata('nama'),
				'data_invoice' => $data_invoice,
				'kategori' => $kategori,
			);
			$this->load->view('report/cetak/penjualanbyinvoice', $data);
		}
	}
}