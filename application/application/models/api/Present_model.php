<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Present_model extends CI_Model
{
	public $table = 'tb_present';
	public $id = 'id_present';

	function __construct()
    {
        parent::__construct();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }

}