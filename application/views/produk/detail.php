<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>DATA PRODUK</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
        <div class="row">
         <div class="col-md-4">
              <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Foto</h3>
                  </div>
                  <div class="box-body" align="center">
                    <?php 
                      $foto = explode(" ", $foto_produk);
                    ?>
                    <div class="row">
                    <?php for ($i=0; $i < count($foto)-1; $i++) { ?>
                      <div class="col-lg-6" style="margin: 0;padding: 10px">
                        <img src="<?php echo base_url('assets/upload/'.$foto[$i]); ?>" class="img-responsive img-thumbnail">
                      </div>
                    <?php } ?>
                    </div>
                  </div>
              </div>
            </div>  
             <div class="col-md-8">
              <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">INFO PRODUK</h3>
              </div>
              <div class="box-body">
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <td><strong>Nama Produk</strong></td>
                        <td><?php echo $judul;?></td>
                      </tr>
                      <tr>
                        <td><strong>Merk</strong></td>
                        <td><?php echo $merk->merk;?></td>
                      </tr>
                      <tr>
                        <td><strong>Kategori.</strong></td>
                        <td><?php echo $kategori->kategori;?></td>
                      </tr>
                      <tr>
                        <td><strong>Jumlah.</strong></td>
                        <td><?php echo $jumlah;?></td>
                      </tr>
                      <tr>
                        <td><strong>Harga.</strong></td>
                        <td>Rp. <?php echo number_format($harga);?></td>
                      </tr>
                      <tr>
                        <td><strong>Keterangan.</strong></td>
                        <td><?php echo $ket;?></td>
                      </tr>
                      <tr>
                        <td><strong>Status.</strong></td>
                        <td><?php echo $status;?></td>
                      </tr>
                      <tr>
                        <td><strong>Tanggal Input.</strong></td>
                        <td><?php echo $tgl_input_pro;?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                
                </div>
            </div>                  
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Vibe, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
</body>
</html>