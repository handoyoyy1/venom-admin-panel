<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Order Detail (<?php echo $kode;?>)</b>
        </h1>
        </section>
        
        <section class="content">
            
            <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO INVOICE</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped">
                <tbody><tr>
                  <td><strong>No Order</strong></td>
                  <td><?php echo $kode;?></td>
                </tr>
                <tr>
                  <td><strong>Tanggal Order</strong></td>
                  <td><?php echo $tanggal;?></td>
                </tr>
                <tr>
                  <td><strong>pemesan</strong></td>
                  <td><?php echo $nm;?></td>
                </tr>
                <tr>
                  <td><strong>Email</strong></td>
                  <td><?php echo $email;?></td>
                </tr>
                <tr>
                  <td><strong>penerima</strong></td>
                  <td><?php echo $nama;?></td>
                </tr>
                <!--<tr>
                  <td>&nbsp;</td>
                  <td>Bekasi Jaya- Bekasi timur</td>
                </tr>-->
                <tr>
                  <td><strong>Telpon/handphone</strong></td>
                  <td><?php echo $hp;?></td>
                </tr>
                <tr>
                  <td><strong>Alamat Penerima</strong></td>
                  <td><?php echo $alamat;?></td>
                </tr>
                
              </tbody></table>
              </div>
              
              </div>
            </div>
            
            <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO PRODUCT</h3>
            </div>
            <div class="box-body">
                <table  class="table table-striped">
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th>Jumlah</th>
                      <th>Price</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                 <?php $tot = 0; $no=0; foreach($data_order as $row) { $no++ ?>
                 <?php $foto_produk=explode(" ", $row['foto']);?>
                    <tr>
                      <td>
                        <a href=""></a>
                        <a title="<?php echo $row['judul']; ?>" data-toggle="modal" data-target="#modalgambarn<?php echo $no ?>" ><?php echo $row['judul']; ?></a>
                            <div class="modal fade" id="modalgambarn<?php echo $no ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"><?php echo $row['judul']; ?></h4>
                                  </div>
                                  <div class="modal-body" align="center">
                                    <img height="100%" width="100%" src="<?php echo base_url()."assets/upload/".$foto_produk[0]; ?>" class="img-fluid">
                                  </div>
                              </div>
                            </div>
                        </div>
                      </td>
                      <td><?php echo $row['jumlah']; ?></td>
                      <td><?php echo number_format($row['harga'], 0, ',', '.'); ?></td>
                      <td><?php echo number_format($row['total'], 0, ',', '.'); ?></td>
                    </tr>
                    
                <?php $tot += $row['total']; } ?>
                  </tbody>
                    <tr>
                      <th colspan="3" ><div align="center">Total</div></th>
                      <th><?php echo number_format($tot, 0, ',', '.'); ?></th>
                    </tr>
                </table>
                
              </div>
              </div>
            </div>
            
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">History Order</h3>
                    </div>
                
                <div class="box-body">
                    <div class="form-group">
                        <span class=" col-sm-12">04 Mart 2018</span>
                       <span class=" col-sm-12 label label-info" style="padding: 10px">05.30 Response Order</span>
                       <span class=" col-sm-12">10 Mart 2018</span>
                       <span class=" col-sm-12 label label-info" style="padding: 10px">05.30 Order Berbentuk</span>
                       <a  href="<?php echo base_url(); ?>invoice/word/<?php echo $kode;?>" class="btn btn-primary"><i class="fa fa-save"></i>Save </a>
                         <a onclick="myPrint()" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    </div>
                 </div>
              </div>
            </div>
            
            
            <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">INFO PAYMENT</h3>
                </div>
            <div class="box-body">
                <table  class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Atas Nama</th>
                      <th>Bank</th>
                      <th>No. Rekening</th>
                      <th>Tanggal Transfer</th>
                      <th>Bukti Transfer</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_konfirmasi as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $row['atas_nama']; ?></td>
                      <td><?php echo $row['bank']; ?></td>
                      <td><?php echo $row['no_rek']; ?></td>
                      <td><?php echo $row['tanggal']; ?></td>
                      <td><?php if($row['images']!=null){ ?>
              
                    <img data-toggle="modal" data-target="#modal-img" src="<?php echo $row['images']; ?>" height="100" width="100" alt="user image" class="online"/>
                    <?php }else{ ?> 
                    <img src="http://venom.levanpedia.com/assets/dist/img/user2-160x160.jpg" height="100" width="100" alt="user image" class="online"/>
                    <?php } ?>
                    
                    <div class="modal fade" id="modal-img">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-body">
                            <img src="<?php echo $row['images']; ?>" height="100%" width="100%" alt="user image" />
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    
                    </td>
                    </tr>
                    
                    
                    <?php } ?>
                  </tbody>
                </table>
                
                </div>
              </div>
            </div>
            
            
        </section>
        </div>
    </div>   

    <script>
    function myPrint() {
        window.print();
    }
    </script>
        
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
   
</body>
</html>