<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>DATA Level User</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">

              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
					<tr>
						<td rowspan="2" >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">NO</span></span></strong></span></span></p>
						</td>
						<td rowspan="2" >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nama Level</span></span></strong></span></span></p>
						</td>
						<td colspan="2" style="background-color: #06f8f5;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong>Karton Kredit</strong></span></span></p>
						</td>
						<td colspan="5" style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong>Karton Cash</strong></span></span></p>
						</td>
						<td colspan="2" >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong>Satuan Kredit</strong></span></span></p>
						</td>
						<td colspan="5" style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong>Satuan Cash</strong></span></span></p>
						</td>
						<td rowspan="2" >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">AKSI</span></span></strong></span></span></p>
						</td>
					</tr>
					<tr>
						<td style="background-color: #06f8f5;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 1</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #06f8f5;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 2</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 1</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 2</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 3</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc4</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f74bf9;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 5</span></span></strong></span></span></p>
						</td>
						<td >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 1</span></span></strong></span></span></p>
						</td>
						<td >
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 2</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 1</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 2</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 3</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 4</span></span></strong></span></span></p>
						</td>
						<td style="background-color: #f0f809;">
						<p style=" text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Disc 5</span></span></strong></span></span></p>
						</td>
					</tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_level as $row) { $no++ ?>
                    <tr>
                      <td width="40px"><?php echo $no; ?></td>
                      <td><?php echo $row['nama']; ?></td>
					  <td width="60px" style="background-color: #06f8f5;"><?php echo $row['credit_a']; ?>%</td>
                      <td width="60px" style="background-color: #06f8f5;"><?php echo $row['credit_b']; ?>%</td>
                      <td width="60px" style="background-color: #f74bf9;"><?php echo $row['diskon_a']; ?>%</td>
                      <td width="60px" style="background-color: #f74bf9;"><?php echo $row['diskon_b']; ?>%</td>
                      <td width="60px" style="background-color: #f74bf9;"><?php echo $row['diskon_c']; ?>%</td>
                      <td width="60px" style="background-color: #f74bf9;"><?php echo $row['diskon_d']; ?>%</td>
                      <td width="60px" style="background-color: #f74bf9;"><?php echo $row['diskon_e']; ?>%</td>
                      <td width="60px"><?php echo $row['satuan_credit_a']; ?>%</td>
                      <td width="60px"><?php echo $row['satuan_credit_b']; ?>%</td>
                      <td width="60px" style="background-color: #f0f809;"><?php echo $row['satuan_diskon_a']; ?>%</td>
                      <td width="60px" style="background-color: #f0f809;"><?php echo $row['satuan_diskon_b']; ?>%</td>
                      <td width="60px" style="background-color: #f0f809;"><?php echo $row['satuan_diskon_c']; ?>%</td>
                      <td width="60px" style="background-color: #f0f809;"><?php echo $row['satuan_diskon_d']; ?>%</td>
                      <td width="60px" style="background-color: #f0f809;"><?php echo $row['satuan_diskon_e']; ?>%</td>
                      <td width="50px">
                      

                      <?php if($row['nama'] != 'Wartawan' && $row['nama'] != 'Admin' && $row['nama'] != 'Super admin' && $row['nama'] != 'Owner'){ ?> 
                      <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>level/editlevel/<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i></a>
                    <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM TAMBAH/EDIT Level User</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item" style=" text-align:center;font-size:10pt">
                  <form role="form" action="<?php echo base_url(); ?>level/savedata" method="post">
				    <div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Nama Level</label>
                        <input type="text" class="form-control" value="<?php echo $level; ?>" id="" name="level" placeholder="Isikan level" required>
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Credit Diskon1</label>
                        <input style="background-color: #06f8f5;"type="text" class="form-control" value="<?php echo $credit_a; ?>" id="" name="credit_a"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Credit Diskon2</label>
                        <input style="background-color: #06f8f5;"type="text" class="form-control" value="<?php echo $credit_b; ?>" id="" name="credit_b"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Cash Diskon1</label>
                        <input style="background-color: #f74bf9;"type="text" class="form-control" value="<?php echo $diskon_a; ?>" id="" name="diskon_a"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Cash Diskon2</label>
                        <input style="background-color: #f74bf9;"type="text" class="form-control" value="<?php echo $diskon_b; ?>" id="" name="diskon_b"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Cash Diskon3</label>
                        <input style="background-color: #f74bf9;"type="text" class="form-control" value="<?php echo $diskon_c; ?>" id="" name="diskon_c"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Cash Diskon4</label>
                        <input style="background-color: #f74bf9;"type="text" class="form-control" value="<?php echo $diskon_d; ?>" id="" name="diskon_d"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Karton Cash Diskon5</label>
                        <input style="background-color: #f74bf9;"type="text" class="form-control" value="<?php echo $diskon_e; ?>" id="" name="diskon_e"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Credit Diskon1</label>
                        <input type="text" class="form-control" value="<?php echo $satuan_credit_a; ?>" id="" name="satuan_credit_a"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Credit Diskon2</label>
                        <input type="text" class="form-control" value="<?php echo $satuan_credit_b; ?>" id="" name="satuan_credit_b"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Cash Diskon1</label>
                        <input style="background-color: #f0f809;"type="text" class="form-control" value="<?php echo $satuan_diskon_a; ?>" id="" name="satuan_diskon_a"  >
                    </div>
                    </div> 
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Cash Diskon2</label>
                        <input style="background-color: #f0f809;"type="text" class="form-control" value="<?php echo $satuan_diskon_b; ?>" id="" name="satuan_diskon_b"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Cash Diskon3</label>
                        <input style="background-color: #f0f809;" type="text" class="form-control" value="<?php echo $satuan_diskon_c; ?>" id="" name="satuan_diskon_c"  >
                    </div>
                    </div>
					<div class="col-lg-2">
                    <div class="form-group">
                      <label for="namalengkap">Satuan Cash Diskon4</label>
                        <input style="background-color: #f0f809;" type="text" class="form-control" value="<?php echo $satuan_diskon_d; ?>" id="" name="satuan_diskon_d"  >
                    </div>
                    </div>
					<div class="col-lg-2" >
                    <div class="form-group">
                      <label for="namalengkap">Satuan Cash Diskon5</label>
                        <input style="background-color: #f0f809;" type="text" class="form-control" value="<?php echo $satuan_diskon_e; ?>" id="" name="satuan_diskon_e"  >
                    </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <input type="hidden" name="status" value="<?php echo $status; ?>" />
                    <div class>
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                      <?php if($status == "baru"){ echo '<button type="reset" class="btn btn-warning btn-block btn-flat">Batal</button>';?>
                      <?php } else { ?> 
                      <a href="<?php echo base_url(); ?>level" class="btn btn-warning btn-block btn-flat">Kembali</a>
                      <?php } ?>
                    </div><!-- /.col -->
                  </form>
                </div><!-- /.item -->
               
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Vibe, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>