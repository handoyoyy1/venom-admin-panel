<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Disclaimer extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		echo "
<h1>Kebijakan Privacy</h1>     
<hr />
<p>
  Dengan menginstal aplikasi ini, Anda dianggap telah mengerti dan menyetujui seluruh syarat dan kondisi (disclaimer) yang berlaku dalam penggunaan aplikasi ini, sebagaimana tercantum dibawah ini :
</p>
<ul>
  <li>Aplikasi ini dapat mengakses kamera</li>
  <li>Aplikasi ini dapat mengakses internet</li>
  <li>Aplikasi ini dapat mengakses Kontak</li>
  <li>Aplikasi ini berisi iklan google</li>
</ul>
";
	}
	
	
}