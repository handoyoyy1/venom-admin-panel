<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sosmed_model extends CI_Model
{
	public $table = 'tb_sosmed';
    public $id = 'id_sosmed';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    function get_by_kategori($data)
    {
        $this->db->where('kategori', $data);
        return $this->db->get($this->table)->row();
    }


}