<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		date_default_timezone_set("Asia/Jakarta");

	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function search_produk(){
		if (isset($_GET['term'])) {
		  	$result = $this->Model->search_produk($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
		     		'label'	=> $row->judul,
		     		'id'	=> $row->id_produk,
					
				);
		     	echo json_encode($arr_result);
		   	}
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_pembelian' => $this->Model->Getpembelian("order by id_pembelian desc")->result_array(),
		);
		$this->load->view('pembelian/pembelian', $data);
	}

	function addpembelian()
	{
		$data = array(
			'data_album' => $this->Model->GetAlbum("order by id_album desc")->result_array(),
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('pembelian/add_pembelian', $data);
	}

	function savedata(){
		$produk_id = $_POST['produk_id'];
		$jumlah = $_POST['jumlah'];
		$user_id = $this->session->userdata('id_user');
		$keterangan = $_POST['keterangan'];

		$data = array(	
			'produk_id'=> $produk_id,
			'jml' => $jumlah,
			'user_id' => $user_id,
			'keterangan' =>$keterangan		
			);
		
		$result = $this->Model->Simpan('tb_pembelian', $data);
		if($result == 1){
			$data_produk = $this->Model->GetProduk("where id_produk = '$produk_id'")->result_array();
			$total = $jumlah+$data_produk[0]['jumlah'];
			$data = array(
    			'id_produk' => $produk_id,
    			'jumlah' => $total,   
			); 
			$res = $this->Model->UpdateProduk($data);
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pembelian');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pembelian');
		}		
	}

	
	function hapus($kode = 1){
		$row=$this->Model->Getpembelian("where id = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->nama_pembelian));
		$result = $this->Model->Hapus('tb_pembelian', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pembelian');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pembelian');
		}
	}
}