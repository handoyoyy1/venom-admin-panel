<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
		$this->load->helper('site_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	} 
	
	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_pesan' => $this->Model->GetPesan("order by id desc")->result_array(),
		);
		$this->load->view('pesan/pesan', $data);
	}
	
	function addpesan($id=null)
	{
	    if($id==null){
	        $data = array(
	            'id' => '',
			'nama' => $this->session->userdata('nama')
		);
	    }else{
	        $data = array(
	            'id' => $id,
			'nama' => $this->session->userdata('nama')
		);
	    }
		
		
		$this->load->view('pesan/add_pesan', $data);
	}

	function savedata(){
		$action = $this->input->post('action');
		$judul = $_POST['judul'];
		$isi = $_POST['isi'];
		
		$id = $_POST['id'];
		
		if($id == null){
		    $id = '';
		}else{
		   $id = $id;
		}

		$data = array(	
			'judul' => $judul,
			'pesan' => $isi			
			);
		
		$result = $this->Model->Simpan('tb_pesan', $data);

		
		if($id != null){
		    $user = $this->Model->get_user_id($id);
		    $to = $user->token; 
		}else{
		   $to = "/topics/global"; 
		}
		
        $values = array();
        $values['status'] = "Pesan";
        $values['title'] = $judul;
        $values['message'] = $isi;
        $values['image'] = "";
        $values['timestamp'] = date("Y-m-d h:i:sa");
        $values['address'] = "Indonesia";
        
        notification($to,$values);

		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan'</strong></div>");
			header('location:'.base_url().'pesan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pesan');
		}		
	}
	
	function editpesan($kode = 0){
		$data = $this->Model->GetPesan("where id = '$kode'")->result_array();

		
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id' => $data[0]['id'],
			'judul' => $data[0]['judul'],
			'pesan' => $data[0]['pesan']
			);
		$this->load->view('pesan/edit_pesan', $data);
	}
	
	function updatepesan(){
		
		$data = array(
			'id' => $this->input->post('id'),
			'judul' => $this->input->post('judul'),
			'pesan' => $this->input->post('isi')
			        
			);
		$res = $this->Model->UpdatePesan($data);

		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'pesan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pesan');
		}
	}

	
	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_pesan', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pesan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pesan');
		}
	}
	
    
    
    public function komentar(){
        $data = array(
			'nama' => $this->session->userdata('nama'),
			'data_komentar' => $this->Model->GetKomentarall("order by id desc")->result_array(),
		);
		$this->load->view('pesan/komentar', $data);
        
    }
    
	public function sales(){
        $data = array(
			'nama' => $this->session->userdata('nama'),
			'data_sales' => $this->Model->Getsalestoday("GROUP by create_on order by create_on desc ")->result_array(),
		);
		$this->load->view('pesan/sales', $data);
        
    }
	
	public function mount(){
        $data = array(
			'nama' => $this->session->userdata('nama'),
			'data_mount' => $this->Model->Getsalesmount()->result_array(),
		);
		$this->load->view('pesan/mount', $data);
        
    }
	
	public function year(){
        $data = array(
			'nama' => $this->session->userdata('nama'),
			'data_year' => $this->Model->Getsalesyear()->result_array(),
		);
		$this->load->view('pesan/year', $data);
        
    }
	
    function komentar_hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_komentar', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pesan/komentar');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pesan/komentar');
		}
	}
	
	function sales_hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_invoice', array('kode_invoice' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pesan/sales');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pesan/sales');
		}
	}
}

?>