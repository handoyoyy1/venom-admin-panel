<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Album extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_album' => $this->Model->GetAlbum("order by id_album desc")->result_array(),
		);
		$this->load->view('album/album', $data);
	}

	function addalbum()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('album/add_album', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/album',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();

		$id_album = '';
		$nama = $_POST['nama'];
		$file_name = $upload_data['file_name'];

		$data = array(	
			'id_album'=> $id_album,
			'nama' => $nama,
			'foto' => base_url()."assets/upload/album/".$file_name		
			);
		
		$result = $this->Model->Simpan('tb_album', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'album');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'album');
		}		
	}

	function editalbum($kode = 0){
		$data_album = $this->Model->GetAlbum("where id_album = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_album' => $data_album[0]['id_album'],
			'nama' => $data_album[0]['nama'],
			'foto' => $data_album[0]['foto']
			);
		$this->load->view('album/edit_album', $data);
	}

	function updatealbum(){
		$config = array(
				'upload_path' => './assets/upload/album',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_album' => $this->input->post('id'),
				'nama' => $this->input->post('nama'),
				'foto' => base_url()."assets/upload/album/".$upload_data['file_name']
				);
			$row=$this->Model->GetAlbum("where id_album = '$data[id_album]'")->row();
			unlink(str_replace(base_url(), "", $row->foto));
			$res = $this->Model->Updatealbum($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'album');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'album');
			}
		}else{
			
			$data = array(
				'id_album' => $this->input->post('id'),
				'nama' => $this->input->post('nama'),
				);

			$res = $this->Model->Updatealbum($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'album');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'album');
			}
		}
	}

	function hapus($kode = 1){
		$row=$this->Model->GetAlbum("where id_album = '$kode'")->row();
			unlink(str_replace(base_url(), "", $row->foto));
		$result = $this->Model->Hapus('tb_album', array('id_album' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'album');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'album');
		}
	}
}