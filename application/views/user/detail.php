<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Detail User</b>
        </h1>
        </section>
        
        <section class="content">

            <div class="col-md-4">
              <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Foto</h3>
                  </div>
                  <div class="box-body" align="center">
                    <img src="<?php echo $foto;?>" class="img-fluid" alt="user image" class="online"/>
                  </div>
              </div>
            </div>  
             <div class="col-md-8">
              <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">INFO USER</h3>
              </div>
              <div class="box-body">
                <table class="table table-striped">
                  <tbody><tr>
                    <td><strong>Nama</strong></td>
                    <td><?php echo $nama_user;?></td>
                  </tr>
                  <tr>
                    <td><strong>Alamat</strong></td>
                    <td><?php echo $alamat;?></td>
                  </tr>
                  <tr>
                    <td><strong>No Hp.</strong></td>
                    <td><?php echo $no_hp;?></td>
                  </tr>
                  <tr>
                    <td><strong>Email</strong></td>
                    <!--<td><?php echo $email;?></td>-->
                    <td>
                      <?php if ($level==3) {
                        echo "Admin";
                      }elseif ($level==4) {
                        echo "Dealer";
                      }elseif ($level==2) {
                        echo "Wartawan";
                      }elseif ($level==1) {
                        echo "Super Admin";
                      } ?>
                    </td>
                  </tr>
                  
                </tbody></table>
                </div>
                
                </div>
            </div>                     
        </section>
        </div>
    </div>   

    <script>
    function myPrint() {
        window.print();
    }
    </script>
        
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
   
</body>
</html>