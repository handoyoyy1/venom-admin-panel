<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model
{
	public $table = 'tb_produk';
    public $id = 'id_produk';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }
	
	function get_by_diskon($id_user){
		if($id_user != "0"){
		$this->db->select('*');
        $this->db->from('tb_user');
        $this->db->join('tb_kat_user', 'tb_kat_user.id = tb_user.level');
        $this->db->where("id_user", $id_user);
        return $this->db->get()->row();
		}else {
		 $this->db->where("id", "10");
         return  $this->db->get('tb_kat_user')->row();
        }
			

    }



    function get_all()  
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where("harga !=", 0);
        $this->db->where("id_subkat", 6);
        $this->db->where("id_subkat", 7);     
        return $this->db->get(" tb_subkategori")->result();
    }

    
    function get_all_cari($data,$D1,$D2,$D3,$D4,$D5)  
    {
		$this->db->or_like('judul', $data); 
        $this->db->order_by($this->id, $this->order);
		$this->db->select('id_produk,harga,judul,foto,ket');
        $query = $this->db->get($this->table);
        
        $produk = array();

        if ($query->num_rows() > 0)
        {
            // Loop through all rows
            foreach ($query->result() as $row)
            {
                $HARGA  = $row->harga;
                $HARGA2 = $HARGA  - ($HARGA  * ($D1 / 100) ); 
                $HARGA3 = $HARGA2 - ($HARGA2 * ($D2 / 100) ); 
                $HARGA4 = $HARGA3 - ($HARGA3 * ($D3 / 100) ); 
                $HARGA5 = $HARGA4 - ($HARGA4 * ($D4 / 100) ); 
                $HARGA6 = $HARGA5 - ($HARGA5 * ($D5 / 100) );
                $produk[] = array(
                    
                    'id_produk' => $row->id_produk,
                    'judul' => $row->judul,
                    'harga' => $row->harga,
                    'diskon' => round($HARGA6,0),
                    'foto' => $row->foto,
                    'ket' => $row->ket,
                );
            }
        }

        return $produk;

    }
    
    function get_by_hot($id_user,$D1,$D2,$D3,$D4,$D5)  
    {
       
        if($id_user == "4" || $id_user == "0"){
                $kategori = "6";
            }else {
                $kategori = "9";
            }  
		
		$this->db->where("id_subkat", $kategori);
        $this->db->order_by($this->id, $this->order);
		$this->db->select('id_produk,harga,judul,foto,ket,url,id_kat,id_subkat');
        $query = $this->db->get($this->table);
        
        $produk = array();

        if ($query->num_rows() > 0)
        {
            // Loop through all rows
            foreach ($query->result() as $row)
            {
                $HARGA  = $row->harga;
                $HARGA2 = $HARGA  - ($HARGA  * ($D1 / 100) ); 
                $HARGA3 = $HARGA2 - ($HARGA2 * ($D2 / 100) ); 
                $HARGA4 = $HARGA3 - ($HARGA3 * ($D3 / 100) ); 
                $HARGA5 = $HARGA4 - ($HARGA4 * ($D4 / 100) ); 
                $HARGA6 = $HARGA5 - ($HARGA5 * ($D5 / 100) );
                $produk[] = array(
                    
                    'id_produk' => $row->id_produk,
                    'judul' => $row->judul,
                    'harga' => $row->harga,
                    'diskon' => round($HARGA6,0),
                    'foto' => $row->foto,
                    'ket' => $row->ket,
                    'url' => $row->url,
					'id_kat' => $row->id_kat,
					'id_subkat' => $row->id_subkat,
                );
            }
        }

        return $produk;
    }
    
    function get_by_express($D1,$D2,$D3,$D4,$D5)
    {	
		$this->db->where("harga !=", 0);
        $this->db->where("id_kat =", 7);
        $this->db->order_by($this->id, $this->order);
		$this->db->select('id_produk,harga,judul,foto,ket');
        $query = $this->db->get($this->table);
        
        $produk = array();

        if ($query->num_rows() > 0)
        {
            // Loop through all rows
            foreach ($query->result() as $row)
            {
                $HARGA  = $row->harga;
                $HARGA2 = $HARGA  - ($HARGA  * ($D1 / 100) ); 
                $HARGA3 = $HARGA2 - ($HARGA2 * ($D2 / 100) ); 
                $HARGA4 = $HARGA3 - ($HARGA3 * ($D3 / 100) ); 
                $HARGA5 = $HARGA4 - ($HARGA4 * ($D4 / 100) ); 
                $HARGA6 = $HARGA5 - ($HARGA5 * ($D5 / 100) );
                $produk[] = array(
                    
                    'id_produk' => $row->id_produk,
                    'judul' => $row->judul,
                    'harga' => $row->harga,
                    'diskon' => round($HARGA6,0),
                    'foto' => $row->foto,
                    'ket' => $row->ket,
                );
            }
        }

        return $produk;
    }

 

    function get_by_kategori($id,$D1,$D2,$D3,$D4,$D5,$idkat)
    {
		
		if($idkat != "0"){
        $this->db->where("id_subkat", $id);
        $this->db->where("id_kat", $idkat);
        }else{
            $this->db->where("id_subkat", $id);
        $this->db->where("id_kat !=", 0);
        }
		$this->db->select('id_produk,harga,judul,foto,ket');
        $query = $this->db->get($this->table);
        
        $produk = array();

        if ($query->num_rows() > 0)
        {
            // Loop through all rows
            foreach ($query->result() as $row)
            {
                $HARGA  = $row->harga;
                $HARGA2 = $HARGA  - ($HARGA  * ($D1 / 100) ); 
                $HARGA3 = $HARGA2 - ($HARGA2 * ($D2 / 100) ); 
                $HARGA4 = $HARGA3 - ($HARGA3 * ($D3 / 100) ); 
                $HARGA5 = $HARGA4 - ($HARGA4 * ($D4 / 100) ); 
                $HARGA6 = $HARGA5 - ($HARGA5 * ($D5 / 100) );
                $produk[] = array(
                    
                    'id_produk' => $row->id_produk,
                    'judul' => $row->judul,
                    'harga' => $row->harga,
                    'diskon' => round($HARGA6,0),
                    'foto' => $row->foto,
                    'ket' => $row->ket,
                );
            }
        }

        return $produk;
                
    }
    
    
    function get_by_simulator($id,$D1,$D2,$D3,$D4,$D5)
    {
		
		$this->db->where("id_kat", $id);
		$this->db->select('id_produk,harga,judul,foto,ket,id_kat');
        $query = $this->db->get($this->table);
        
        $produk = array();

        if ($query->num_rows() > 0)
        {
            // Loop through all rows
            foreach ($query->result() as $row)
            {
                $HARGA  = $row->harga;
                $HARGA2 = $HARGA  - ($HARGA  * ($D1 / 100) ); 
                $HARGA3 = $HARGA2 - ($HARGA2 * ($D2 / 100) ); 
                $HARGA4 = $HARGA3 - ($HARGA3 * ($D3 / 100) ); 
                $HARGA5 = $HARGA4 - ($HARGA4 * ($D4 / 100) ); 
                $HARGA6 = $HARGA5 - ($HARGA5 * ($D5 / 100) );
                $produk[] = array(
                    
                    'id_produk' => $row->id_produk,
                    'judul' => $row->judul,
                    'harga' => $row->harga,
                    'diskon' => round($HARGA6,0),
                    'foto' => $row->foto,
                    'ket' => $row->ket,
					'id_kat' => $row->id_kat,
                );
            }
        }

        return $produk;
        
        
    }

     function get_all_kategori()  
    {
        $this->db->where("id_kat !=", 6);
        $this->db->where("id_kat !=", 7);
        $this->db->where("id_kat !=", 9);
        return $this->db->get("tb_subkategori")->result();
    }

    function get_all_subkategori($id)
    {
        $this->db->select("a.id_kat as id_kat,a.subkat_id as subkat_id, a.kategori as kategori, a.images as images");
        $this->db->from("tb_kategori a");
        $this->db->join('tb_subkategori b', 'b.id_kat = a.subkat_id');
        $this->db->where("a.subkat_id", $id);
        return $this->db->get()->result();
    }


}