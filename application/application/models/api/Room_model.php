<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Room_model extends CI_Model
{
	public $table = 'tb_chat_rooms';
	public $id = 'chat_room_id';

	function __construct()
    {
        parent::__construct();
    }
    
    function update($id, $data)
    {
        $this->db->where("user_id", $id);
        $this->db->update($this->table, $data);
    }
    
    function get_by_user($id)
    {
        $this->db->where("user_id", $id);
        return $this->db->get($this->table)->row();
    }

    
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    function get_messages($kode)
    {
        $this->db->where('name', $kode);
        $room = $this->db->get('tb_chat_rooms')->row();
        
        $data = $this->db->query('SELECT m.user_id AS user_id, m.message AS pesan,m.created_at AS waktu, u.first_name AS first_name, u.`last_name` AS last_name, u.foto AS img,u.level as level FROM tb_messages m JOIN tb_user u ON m.user_id = u.id_user WHERE m.chat_room_id = '.$room->chat_room_id);
        return $data->result(); 
    }
    
    function insert_messages($data)
    {
        $this->db->insert("tb_messages", $data);
    }
    
    function get_by_id($id)
    {
        $this->db->where("name", $id);
        return $this->db->get($this->table)->row();
    }
    
    public function exist_row_check($field,$data){
        $this->db->where($field,$data);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    

}