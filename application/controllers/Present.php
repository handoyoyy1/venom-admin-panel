<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Present extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_present' => $this->Model->GetPresent("RIGHT JOIN tb_event ON tb_present.event_id=tb_event.id_event GROUP BY tb_event.id_event")->result_array(),
		);
		$this->load->view('present/present', $data);
	}

	public function detail($id)
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'event' => $this->Model->GetEvent("WHERE id_event='$id'")->row(),
			'data_user' => $this->Model->GetPresentByEvent("LEFT JOIN tb_user ON tb_user.id_user=tb_present.user_id WHERE tb_present.event_id='$id' GROUP BY tb_present.user_id")->result_array(),
		);
		$this->load->view('present/detail_present', $data);
	}
}