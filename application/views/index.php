<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('inc/head'); ?>

  </head>
  <body class="skin-blue">
  <!-- wrapper di bawah footer -->
    <div class="wrapper">
      
      <?php $this->load->view('inc/head2'); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php $this->load->view('inc/sidebar'); ?>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <b>DASHBOARD</b>
          </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
		             
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $komentar[0]['jumlah']; ?></h3>
                  <p><b>TOTAL KOMENTAR</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-comments-o"></i>
                </div>
                <a href="<?php echo base_url(); ?>pesan/komentar" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			<?php if($this->session->userdata('level') != '2'){ ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_product; ?></h3>
                  <p><b>TOTAL PRODUCT</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-photo"></i>
                </div>
				
                <a href="<?php echo base_url(); ?>produk" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			<?php } ?>
			<?php if($this->session->userdata('level') != '2'){ ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $jmlmember[0]['jumlah']; ?></h3>
                  <p><b>TOTAL MEMBER</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="<?php echo base_url(); ?>user/member" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
			  <?php } ?>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $jmltesti[0]['jumlah']; ?></h3>
                  <p><b>TOTAL TESTIMONI</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-folder"></i>
                </div>
                <a href="#" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			<div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                <div class="inner">
                  <h3>Rp. <?php echo number_format($sales[0]['grand_total'],0,'.','.'); ?></h3>
                  <p><b>TOTAL PENJUALAN HARI INI</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-money"></i>
                </div>
                <a href="<?php echo base_url(); ?>pesan/sales" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
			  <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-grey">
                <div class="inner">
                  <h3>Rp. <?php echo number_format($sales_mount[0]['grand_total'],0,'.','.'); ?></h3>
                  <p><b>TOTAL PENJUALAN BULAN INI</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-money"></i>
                </div>
                <a href="<?php echo base_url(); ?>pesan/mount" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Chat box -->
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-comments-o"></i>
                  <h3 class="box-title">Testimoni</h3>
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <?php foreach($testimoni as $row) { ?>
                  <div class="item">
                    <?php if($row['foto']!=null){ ?>  
                    <img src="<?php echo $row['foto']; ?>" alt="user image" class="online"/>
                    <?php }else{ ?> 
                    <img src="http://levanpedia.com/vibe/assets/dist/img/user2-160x160.jpg" alt="user image" class="online"/>
                    <?php } ?>
                    <p class="message">
                      <a href="#" class="name">
                        <?php echo $row['nama_user']; ?>
                      </a>
                      <?php echo $row['komentar']; ?>
                    </p>
                  </div><!-- /.item -->
                  <?php } ?>
                  <!-- chat item -->
                </div><!-- /.chat -->
              </div><!-- /.box (chat box) -->
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <!-- <b>Version</b> 2.0 -->
        </div>
        <strong>©2018 Venom, All Rights Reserved <a href="#"></a></strong>
      </footer>
    </div><!-- ./wrapper -->
    <?php $this->load->view('inc/footer', TRUE); ?>
  </body>
</html>