<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bank_model extends CI_Model
{
	public $table = 'tb_bank';
    public $id = 'id_bank';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


}