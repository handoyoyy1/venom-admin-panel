<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'optsubkategori' => $this->Model->GetSubKat()->result_array(),
			'optkategori' => $this->Model->GetKat()->result_array(),
			'data_produk' => $this->Model->GetProdukKatMerko('GROUP by p.id_produk ORDER by p.id_produk')->result_array(),
			'id_kat'=>null,
			'id_subkat'=>null

		);

		$this->load->view('produk/data_produk', $data);
	}

	public function filter()
	{
		//echo $kategori;
		$kategori=$this->input->POST('id_kat');
		$subkategori=$this->input->POST('id_subkat');
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'optsubkategori' => $this->Model->GetSubKat()->result_array(),
			'optkategori' => $this->Model->GetKat()->result_array(),
			'data_produk' => $this->Model->GetProdukKatMerko('where p.id_kat='.$kategori.' OR p.id_subkat='.$subkategori)->result_array(),
		);

		$this->load->view('produk/data_produk', $data);
	}


	function addproduk()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'optsubkategori' => $this->Model->GetSubKat()->result_array(),
			'optkategori' => $this->Model->GetKat()->result_array(),
			'optmerk' => $this->Model->GetMerk()->result_array(),
		);
		
		$this->load->view('produk/add_produk', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$jml_foto=sizeof($_FILES['file_upload']['tmp_name']);
		$filefoto=$_FILES['file_upload'];

		$nama_foto="";
		
		for($i=0;$i<$jml_foto;$i++){	
			$_FILES['file_upload']['name']=$filefoto['name'][$i];
			$_FILES['file_upload']['type']=$filefoto['type'][$i];
			$_FILES['file_upload']['tmp_name']=$filefoto['tmp_name'][$i];
			$_FILES['file_upload']['error']=$filefoto['error'][$i];
			$_FILES['file_upload']['size']=$filefoto['size'][$i];
			if ($this->upload->do_upload('file_upload')) {
				$upload_data = $this->upload->data();
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'produk');
			}

			$nama_foto=$nama_foto."".$upload_data['file_name']." ";
		}

		$id_produk = '';
		$kode = $_POST['kode'];
		$judul = $_POST['judul'];
		$harga = $_POST['harga'];		
		$jumlah = $_POST['jumlah'];
		$point = $_POST['point'];
		$id_merk = $_POST['id_merk'];
		$id_kat = $_POST['id_kat'];
		$id_subkat = $_POST['id_subkat'];
		$url = $_POST['url'];
		$carton = $_POST['carton'];
		$ket = $_POST['ket'];
		$file_name = $nama_foto;

		$data = array(	
			'id_produk'=> $id_produk,
			'kode' => $kode,
			'judul' => $judul,
			'harga' => $harga,
			'jumlah' => $jumlah,
			'point' => $point,
			'id_merk' => $id_merk,
			'id_kat' => $id_kat,
			'id_subkat' => $id_subkat,
			'url' => $url,
			'carton' => $carton,
			'ket' => $ket,
			'tgl_input_pro' => date("Y-m-d H:i:s"),
			'foto' => $file_name,
			);
		
		$result = $this->Model->Simpan('tb_produk', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'produk');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}		
	}

	function editproduk($kode = 0){
		$data_produk = $this->Model->GetProduk("where id_produk = '$kode'")->result_array();

		/*menjadikan kategori ke array*/
		$kategori_post_array = array();
		foreach($this->Model->GetProduk("where id_produk = '$kode'")->result_array() as $kat){
			$kategori_post_array[] = $kat['id_kat'];
		}
        
        $subkategori_post_array = array();
		foreach($this->Model->GetProduk("where id_produk = '$kode'")->result_array() as $subkat){
			$subkategori_post_array[] = $subkat['id_subkat'];
		}
        
		$merk_post_array = array();
		foreach($this->Model->GetProduk("where id_produk = '$kode'")->result_array() as $merk){
			$merk_post_array[] = $merk['id_merk'];
		}

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_produk' => $data_produk[0]['id_produk'],
			'kode' => $data_produk[0]['kode'],
			'judul' => $data_produk[0]['judul'],
			'harga' => $data_produk[0]['harga'],
			'jumlah' => $data_produk[0]['jumlah'],
			'point' => $data_produk[0]['point'],
			'ket' => $data_produk[0]['ket'],
			'url' => $data_produk[0]['url'],
			'foto_produk' => $data_produk[0]['foto'],
			'tgl_input_pro' => $data_produk[0]['tgl_input_pro'],
			'carton' => $data_produk[0]['carton'],
			'kategori' => $this->Model->GetKat()->result_array(),
			'subkategori' => $this->Model->GetSubKat()->result_array(),
			'merk' => $this->Model->GetMerk()->result_array(),
			'label_post' => $kategori_post_array,
			'merk_post' => $merk_post_array,
            'subkategori_post' => $subkategori_post_array,
			);
		$this->load->view('produk/edit_produk', $data);
	}

	function updateproduki(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
            
            
            $data = array(
    			'id_produk' => $id_produk,
    			'kode' => $kode,
    			'judul' => $judul,
    			'harga' => $harga,
    			'jumlah' => $jumlah,
    		//	'kondisi' => $this->input->post('kondisi'),
    			'id_merk' => $id_merk,
    			'id_kat' => $id_kat,
    			'id_subkat' => $id_subkat,
    			'country' => $country,
    			'alcohol' => $alcohol,
    			'url' => $url,
    			'volume' => $volume,
    			'note' => $note,
    			'service' => $service,
    			'tgl_input_pro' => $tgl,
    			'carton' => $carton,
    			'ket' => $ket,
    
    			'foto' => base_url()."assets/upload/".$file_name     
			); 
			$res = $this->Model->UpdateProduk($data);
        

		
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}
	}

	function hapuspro($kode = 1){
		
		$result = $this->Model->Hapus('tb_produk', array('id_produk' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'produk');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}
	}

	function updateproduk(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$jml_foto=sizeof($_FILES['file_upload']['tmp_name']);
		$filefoto=$_FILES['file_upload'];
		
		$id_produk = $this->input->post('id_produk');
            $kode = $this->input->post('kode');
            $judul = $this->input->post('judul');
            $harga = $this->input->post('harga');
            $jumlah = $this->input->post('jumlah');
            $carton = $this->input->post('carton');
            $url = $this->input->post('url');
            $id_merk = $this->input->post('id_merk');
            $id_kat = $this->input->post('id_kat');
            $id_subkat = $this->input->post('id_subkat');
            $tgl = $this->input->post('tgl_input_pro');
            $ket = $this->input->post('ket');
            $nama_foto=$this->input->post('foto');
		
		for($i=0;$i<$jml_foto;$i++){	
			$_FILES['file_upload']['name']=$filefoto['name'][$i];
			$_FILES['file_upload']['type']=$filefoto['type'][$i];
			$_FILES['file_upload']['tmp_name']=$filefoto['tmp_name'][$i];
			$_FILES['file_upload']['error']=$filefoto['error'][$i];
			$_FILES['file_upload']['size']=$filefoto['size'][$i];
			if ($this->upload->do_upload('file_upload')) {
				$upload_data = $this->upload->data();
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'produk');
			}

			$nama_foto=$nama_foto."".$upload_data['file_name']." ";
		}
		$file_name = $nama_foto;
        if($harga != null){
            
            if ($upload_data['file_name'] != null) {
    		
        		$data = array(
        			'id_produk' => $id_produk,
        			'kode' => $kode,
        			'judul' => $judul,
        			'harga' => $harga,
        			'jumlah' => $jumlah,
        			'url' => $url,
        			'carton' => $carton,
        			'id_merk' => $id_merk,
        			'id_kat' =>  $id_kat,
        			'id_subkat' =>  $id_subkat,
        			'ket' => $ket,
        			'tgl_input_pro' => $tgl,
        			
        			'foto' => $file_name
        			);
            }else{
                $data = array(
        			'id_produk' => $id_produk,
        			'kode' => $kode,
        			'judul' => $judul,
        			'harga' => $harga,
        			'jumlah' => $jumlah,
        			'carton' => $carton,
        			'id_merk' => $id_merk,
        			'id_kat' =>  $id_kat,
        			'id_subkat' =>  $id_subkat,
        			'ket' => $ket,
        			'url' => $url,
        			'tgl_input_pro' => $tgl,
        			);
            }
        }else{
            if ($upload_data['file_name'] != null) {
                $data = array(
        			'id_produk' => $id_produk,
        			'kode' => $kode,
        			'judul' => $judul,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' => $id_kat,
        			'id_subkat' => $id_subkat,
        			'url' => $url,
        			'tgl_input_pro' => $tgl,
        			'carton' => $carton,
        			'ket' => $ket,
        
        			'foto' => $file_name     
    			);
            }else{
                $data = array(
        			'id_produk' => $id_produk,
        			'kode' => $kode,
        			'judul' => $judul,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' => $id_kat,
        			'url' => $url,
        			'id_subkat' => $id_subkat,
        			'tgl_input_pro' => $tgl,
        			'carton' => $carton,
        			'ket' => $ket, 
    			);
            }
        }
		
		$res = $this->Model->UpdateProduk($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produk');
		}
	}

	public function hapusgambar($key){
		$id_produk=$this->input->get('id_produk');
		$row = $this->Model->GetProduk("where id_produk = '$id_produk'")->row();
		$foto_produk=explode(" ", $row->foto);
		unlink("assets/upload/".$foto_produk[$key]);
		unset($foto_produk[$key]);
		$foto_produk_array=implode(" ", $foto_produk);
		$data = array(
			'id_produk' => $id_produk,
			'foto' => $foto_produk_array, 
		);
		$res = $this->Model->UpdateProduk($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produk/editproduk/'.$id_produk);
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus gambar GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produk/editproduk/'.$id_produk);
		}
	}

	public function simpan(){
		$id['kode']=$this->input->post('id',TRUE);
		$data_array = $this->add_data(array('kode','nip','jenisarsip','tanggal','titel','deskripsi',
			'kodearsip','lokasifisik','catatan'),TRUE);

                // kita cek dulu dengan kode error 4
		if ( ! in_array(4, $_FILES['lampiran']['error'])) {
            // jika file tidak kosong tambahkan file gambar
            //$file1 = array();
			if($this->input->post('dok') == !NULL){
				$file1 = $this->upload($_FILES['lampiran']['name']);
				$file2 = $this->input->post('dok');
				$lampiran = $file1.'*'.$file2;
				$file['lampiran'] = $lampiran;
			}else{
				$file1 = $this->upload($_FILES['lampiran']['name']);
				$file['lampiran'] = $file1;
			}
		}else{
			$file['lampiran'] = $this->input->post('dok');
		}

		if($this->input->post(NULL,TRUE)==TRUE){
			$data = array_merge($data_array,$file);
			if($id['kode'] == false or $id['kode']==null){
                                //print_r($_POST); die();
				$this->Model_home->tambah_data('tbl_arsip_kegiatan',$data);
				$this->pesan('pesan','Berhasil Menyimpan Data');
				redirect(base_url().'arsip_kegiatan/tambah');
			}else{

				$this->model_home->update_data($id,'tbl_arsip_kegiatan',$data);
				$this->pesan('pesan','Berhasil Mengubah Data');
				redirect(base_url().'arsip_kegiatan/tambah');

			}
		}
	}

}

// Developed by Bluecore
// Email: bluecoresuite@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */