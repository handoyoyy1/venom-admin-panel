<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_bank' => $this->Model->GetBank("order by id_bank desc")->result_array(),
		);
		$this->load->view('bank/bank', $data);
	}

	function addbank()
	{
		$data = array(
			'nama' => $this->session->userdata('nama')
		);
		
		$this->load->view('bank/add_bank', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload/bank',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();

		$id_bank = '';
		$nama = $_POST['nama'];
		$atas_nama = $_POST['atas_nama'];
		$norek = $_POST['norek'];
		$file_name = $upload_data['file_name'];

		$data = array(	
			'id_bank'=> $id_bank,
			'nama_bank' => $nama,
			'atas_nama' => $atas_nama,
			'no_rek' => $norek,
			'image' =>/* base_url()."assets/upload/bank/".*/$file_name		
			);
		
		$result = $this->Model->Simpan('tb_bank', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'bank');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'bank');
		}		
	}

	function editbank($kode = 0){
		$data_slide = $this->Model->GetBank("where id_bank = '$kode'")->result_array();

		

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_bank' => $data_slide[0]['id_bank'],
			'nama' => $data_slide[0]['nama_bank'],
			'atas_nama' => $data_slide[0]['atas_nama'],
			'norek' => $data_slide[0]['no_rek'],
			'image' => $data_slide[0]['image']
			);
		$this->load->view('bank/edit_bank', $data);
	}

	function updatebank(){
		$config = array(
				'upload_path' => './assets/upload/bank',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',

			);
			$this->load->library('upload', $config);	
			$this->upload->do_upload('file_upload');
			$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_bank' => $this->input->post('id'),
				'nama_bank' => $this->input->post('nama'),
				'atas_nama' => $this->input->post('atas_nama'),
				'no_rek' => $this->input->post('norek'),
				'image' => /*base_url()."assets/upload/bank/".*/$upload_data['file_name']
				);

			$res = $this->Model->UpdateBank($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'bank');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'bank');
			}
		}else{
			
			$data = array(
				'id_bank' => $this->input->post('id'),
				'nama_bank' => $this->input->post('nama'),
				'atas_nama' => $this->input->post('atas_nama'),
				'no_rek' => $this->input->post('norek')
				);

			$res = $this->Model->UpdateBank($data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
				header('location:'.base_url().'bank');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'bank');
			}
		}
	}

	function hapus($kode = 1){
		
		$result = $this->Model->Hapus('tb_bank', array('id_bank' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'bank');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'bank');
		}
	}
}