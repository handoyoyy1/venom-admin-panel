<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Level extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'level' => '',
			'credit_a' => '',
			'credit_b' => '',
			'diskon_a' => '',
			'diskon_b' => '',
			'diskon_c' => '',
			'diskon_d' => '',
			'diskon_e' => '',
			'satuan_credit_a' => '',
			'satuan_credit_b' => '',
			'satuan_diskon_a' => '',
			'satuan_diskon_b' => '',
			'satuan_diskon_c' => '',
			'satuan_diskon_d' => '',
			'satuan_diskon_e' => '',
			'id' => '',
			'data_level' => $this->Model->GetKatUser("where id not in ('1','10','8') order by id desc")->result_array()
		);

		$this->load->view('level/data_level', $data);
	}

	function editlevel($kode = 0){		
		$tampung = $this->Model->GetKatUser("where id = '$kode'")->result_array();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'level' => $tampung[0]['nama'],
			'credit_a' => $tampung[0]['credit_a'],
			'credit_b' => $tampung[0]['credit_b'],
			'diskon_a' => $tampung[0]['diskon_a'],
			'diskon_b' => $tampung[0]['diskon_b'],
			'diskon_c' => $tampung[0]['diskon_c'],
			'diskon_d' => $tampung[0]['diskon_d'],
			'diskon_e' => $tampung[0]['diskon_e'],
			'satuan_credit_a' => $tampung[0]['satuan_credit_a'],
			'satuan_credit_b' => $tampung[0]['satuan_credit_b'],
			'satuan_diskon_a' => $tampung[0]['satuan_diskon_a'],
			'satuan_diskon_b' => $tampung[0]['satuan_diskon_b'],
			'satuan_diskon_c' => $tampung[0]['satuan_diskon_c'],
			'satuan_diskon_d' => $tampung[0]['satuan_diskon_d'],
			'satuan_diskon_e' => $tampung[0]['satuan_diskon_e'],
			'id' => $tampung[0]['id'],
			'data_level' => $this->Model->GetKatUser("order by id desc")->result_array()
			);
		$this->load->view('level/data_level', $data);
	}

	function savedata(){
		if($_POST){
			$status = $_POST['status'];
			$credit_a = $_POST['credit_a'];
			$credit_b = $_POST['credit_b'];
			$diskon_a = $_POST['diskon_a'];
			$diskon_b = $_POST['diskon_b'];
			$diskon_c = $_POST['diskon_c'];
			$diskon_d = $_POST['diskon_d'];
			$diskon_e = $_POST['diskon_e'];
			$satuan_credit_a = $_POST['satuan_credit_a'];
			$satuan_credit_b = $_POST['satuan_credit_b'];
			$satuan_diskon_a = $_POST['satuan_diskon_a'];
			$satuan_diskon_b = $_POST['satuan_diskon_b'];
			$satuan_diskon_c = $_POST['satuan_diskon_c'];
			$satuan_diskon_d = $_POST['satuan_diskon_d'];
			$satuan_diskon_e = $_POST['satuan_diskon_e'];
			$id = $_POST['id'];
			$level = $_POST['level'];
			if($status == "baru"){
				$data = array(
					'id' => $id,
					'nama' => $level,
					'satuan_credit_a' => $satuan_credit_a,
					'satuan_credit_b' => $satuan_credit_b,
					'satuan_diskon_a' => $satuan_diskon_a,
					'satuan_diskon_b' => $satuan_diskon_b,
					'satuan_diskon_c' => $satuan_diskon_c,
					'satuan_diskon_d' => $satuan_diskon_d,
					'satuan_diskon_e' => $satuan_diskon_e,
					);
				$result = $this->Model->Simpan('tb_kat_user', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'level');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'level');
				}
			}else{
				$data = array(
					'nama' => $level,
					'credit_a' => $credit_a,
					'credit_b' => $credit_b,
					'diskon_a' => $diskon_a,
					'diskon_b' => $diskon_b,
					'diskon_c' => $diskon_c,
					'diskon_d' => $diskon_d,
					'diskon_e' => $diskon_e,
					'satuan_credit_a' => $satuan_credit_a,
					'satuan_credit_b' => $satuan_credit_b,
					'satuan_diskon_a' => $satuan_diskon_a,
					'satuan_diskon_b' => $satuan_diskon_b,
					'satuan_diskon_c' => $satuan_diskon_c,
					'satuan_diskon_d' => $satuan_diskon_d,
					'satuan_diskon_e' => $satuan_diskon_e,
					);
				$result = $this->Model->Update('tb_kat_user', $data, array('id' => $id));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'level');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'level');
				}
			}
		}else{
			echo('handak baapa nyawa tong!!!');
		}
	}

	function hapuskat($kode = 1){
		
		$result = $this->Model->Hapus('tb_kat_user', array('id' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'level');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'level');
		}
	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */