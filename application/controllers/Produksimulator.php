<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produksimulator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_produksimulator' => $this->Model->GetProdukKatMerko("WHERE p.id_kat!=1 AND p.id_kat!=2 AND p.id_kat!=3 AND p.id_kat!=6 AND p.id_kat!=7  order by id_produk desc")->result_array(),
		);

		$this->load->view('produksimulator/data_produksimulator', $data);
	}

	function addproduksimulator()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'optkategori' => $this->Model->GetKat("WHERE id_kat!=1 AND id_kat!=2 AND id_kat!=3 AND id_kat!=6 AND id_kat!=7")->result_array(),
			'optmerk' => $this->Model->GetMerk()->result_array(),
		);
		
		$this->load->view('produksimulator/add_produksimulator', $data);
	}

	function savedata(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$jml_foto=sizeof($_FILES['file_upload']['tmp_name']);
		$filefoto=$_FILES['file_upload'];

		$nama_foto="";
		
		for($i=0;$i<$jml_foto;$i++){	
			$_FILES['file_upload']['name']=$filefoto['name'][$i];
			$_FILES['file_upload']['type']=$filefoto['type'][$i];
			$_FILES['file_upload']['tmp_name']=$filefoto['tmp_name'][$i];
			$_FILES['file_upload']['error']=$filefoto['error'][$i];
			$_FILES['file_upload']['size']=$filefoto['size'][$i];
			if ($this->upload->do_upload('file_upload')) {
				$upload_data = $this->upload->data();
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'produksimulator');
			}

			$nama_foto=$nama_foto."".$upload_data['file_name']." ";
		}

		$id_produk = '';
		$judul = $_POST['judul'];
		$harga = $_POST['harga'];		
		$jumlah = $_POST['jumlah'];
	//	$kondisi = $_POST['kondisi'];
		$id_merk = $_POST['id_merk'];
		$id_kat = $_POST['id_kat'];
		//$status = $_POST['status'];
		$ket = $_POST['ket'];
		$tgl_input_pro = $_POST['tgl_input_pro'];
		$file_name = $nama_foto;

		$data = array(	
			'id_produk'=> $id_produk,
			'judul' => $judul,
			'harga' => $harga,
			'jumlah' => $jumlah,
	//		'kondisi' => $kondisi,
			'id_merk' => $id_merk,
			'id_kat' => $id_kat,
		//	'status' => $status,
			'ket' => $ket,
			'tgl_input_pro' => date("Y-m-d H:i:s"),
			'foto' => $file_name,
			);
		
		$result = $this->Model->Simpan('tb_produksimulator', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}		
	}

	function editproduksimulator($kode = 0){
		$data_produksimulator = $this->Model->GetProduk("where id_produk = '$kode'")->result_array();

		/*menjadikan kategori ke array*/
		$kategori_post_array = array();
		foreach($this->Model->GetProduk("where id_produk = '$kode'")->result_array() as $kat){
			$kategori_post_array[] = $kat['id_kat'];
		}

		$merk_post_array = array();
		foreach($this->Model->GetProduk("where id_produk = '$kode'")->result_array() as $merk){
			$merk_post_array[] = $merk['id_merk'];
		}

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_produk' => $data_produksimulator[0]['id_produk'],
			'judul' => $data_produksimulator[0]['judul'],
			'harga' => $data_produksimulator[0]['harga'],
			'jumlah' => $data_produksimulator[0]['jumlah'],
		//	'status' => $data_produksimulator[0]['status'],
			'ket' => $data_produksimulator[0]['ket'],
			'foto_produksimulator' => $data_produksimulator[0]['foto'],
			'tgl_input_pro' => $data_produksimulator[0]['tgl_input_pro'],
		//	'status' => $data_produksimulator[0]['status'],
			'kategori' => $this->Model->GetKat()->result_array(),
			'merk' => $this->Model->GetMerk()->result_array(),
			'label_post' => $kategori_post_array,
			'merk_post' => $merk_post_array,
			);
		$this->load->view('produksimulator/edit_produksimulator', $data);
	}

	function updateproduksimulatori(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',

		);
		$this->load->library('upload', $config);	
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
            
            
            $data = array(
    			'id_produk' => $id_produk,
    			'judul' => $judul,
    			'harga' => $harga,
    			'jumlah' => $jumlah,
    		//	'kondisi' => $this->input->post('kondisi'),
    			'id_merk' => $id_merk,
    			'id_kat' => $id_kat,
    			'country' => $country,
    			'alcohol' => $alcohol,
    			'volume' => $volume,
    			'note' => $note,
    			'service' => $service,
    			'tgl_input_pro' => $tgl,
    		//	'status' => $this->input->post('status'),
    			'ket' => $ket,
    
    			'foto' => base_url()."assets/upload/".$file_name     
			); 
			$res = $this->Model->UpdateProduk($data);
        

		
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}
	}

	function hapuspro($kode = 1){
		
		$result = $this->Model->Hapus('tb_produksimulator', array('id_produk' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}
	}

	function updateproduksimulator(){
		$config = array(
			'upload_path' => './assets/upload',
			'allowed_types' => 'gif|jpg|JPG|png',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$jml_foto=sizeof($_FILES['file_upload']['tmp_name']);
		$filefoto=$_FILES['file_upload'];
		
		$id_produk = $this->input->post('id_produk');
            $judul = $this->input->post('judul');
            $harga = $this->input->post('harga');
            $jumlah = $this->input->post('jumlah');
            $id_merk = $this->input->post('id_merk');
            $id_kat = $this->input->post('id_kat');
            $tgl = $this->input->post('tgl_input_pro');
            $ket = $this->input->post('ket');
            $nama_foto=$this->input->post('foto');
		
		for($i=0;$i<$jml_foto;$i++){	
			$_FILES['file_upload']['name']=$filefoto['name'][$i];
			$_FILES['file_upload']['type']=$filefoto['type'][$i];
			$_FILES['file_upload']['tmp_name']=$filefoto['tmp_name'][$i];
			$_FILES['file_upload']['error']=$filefoto['error'][$i];
			$_FILES['file_upload']['size']=$filefoto['size'][$i];
			if ($this->upload->do_upload('file_upload')) {
				$upload_data = $this->upload->data();
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'produksimulator');
			}

			$nama_foto=$nama_foto."".$upload_data['file_name']." ";
		}
		$file_name = $nama_foto;
        if($harga != null){
            
            if ($upload_data['file_name'] != null) {
    		
        		$data = array(
        			'id_produk' => $id_produk,
        			'judul' => $judul,
        			'harga' => $harga,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' =>  $id_kat,
        			'ket' => $ket,
        			'tgl_input_pro' => $tgl,
        			
        			'foto' => $file_name
        			);
            }else{
                $data = array(
        			'id_produk' => $id_produk,
        			'judul' => $judul,
        			'harga' => $harga,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' =>  $id_kat,
        			'ket' => $ket,
        			'tgl_input_pro' => $tgl,
        			);
            }
        }else{
            if ($upload_data['file_name'] != null) {
                $data = array(
        			'id_produk' => $id_produk,
        			'judul' => $judul,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' => $id_kat,
        			'tgl_input_pro' => $tgl,
        		//	'status' => $this->input->post('status'),
        			'ket' => $ket,
        
        			'foto' => $file_name     
    			);
            }else{
                $data = array(
        			'id_produk' => $id_produk,
        			'judul' => $judul,
        			'jumlah' => $jumlah,
        		//	'kondisi' => $this->input->post('kondisi'),
        			'id_merk' => $id_merk,
        			'id_kat' => $id_kat,
        			'tgl_input_pro' => $tgl,
        		//	'status' => $this->input->post('status'),
        			'ket' => $ket, 
    			);
            }
        }
		
		$res = $this->Model->UpdateProduk($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator');
		}
	}

	public function hapusgambar($key){
		$id_produk=$this->input->get('id_produk');
		$row = $this->Model->GetProduk("where id_produk = '$id_produk'")->row();
		$foto_produksimulator=explode(" ", $row->foto);
		unlink("assets/upload/".$foto_produksimulator[$key]);
		unset($foto_produksimulator[$key]);
		$foto_produksimulator_array=implode(" ", $foto_produksimulator);
		$data = array(
			'id_produk' => $id_produk,
			'foto' => $foto_produksimulator_array, 
		);
		$res = $this->Model->UpdateProduk($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator/editproduksimulator/'.$id_produk);
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus gambar GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'produksimulator/editproduksimulator/'.$id_produk);
		}
	}

	function detail($kode = 0){
		$data_produksimulator = $this->Model->GetProduk("where id_produk = '$kode'")->result_array();

		$id_ket=$data_produksimulator[0]['id_kat'];
		$id_merk=$data_produksimulator[0]['id_merk'];

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_produk' => $data_produksimulator[0]['id_produk'],
			'judul' => $data_produksimulator[0]['judul'],
			'harga' => $data_produksimulator[0]['harga'],
			'jumlah' => $data_produksimulator[0]['jumlah'],
		//	'status' => $data_produksimulator[0]['status'],
			'ket' => $data_produksimulator[0]['ket'],
			'foto_produksimulator' => $data_produksimulator[0]['foto'],
			'tgl_input_pro' => $data_produksimulator[0]['tgl_input_pro'],
			'status' => $data_produksimulator[0]['status'],
			'kategori' => $this->Model->GetKat("where id_kat = '$id_ket'")->row(),
			'merk' => $this->Model->GetMerk("where id_merk = '$id_merk'")->row(),
			);
		$this->load->view('produksimulator/detail', $data);
	}

	public function simpan(){
		$id['kode']=$this->input->post('id',TRUE);
		$data_array = $this->add_data(array('kode','nip','jenisarsip','tanggal','titel','deskripsi',
			'kodearsip','lokasifisik','catatan'),TRUE);

                // kita cek dulu dengan kode error 4
		if ( ! in_array(4, $_FILES['lampiran']['error'])) {
            // jika file tidak kosong tambahkan file gambar
            //$file1 = array();
			if($this->input->post('dok') == !NULL){
				$file1 = $this->upload($_FILES['lampiran']['name']);
				$file2 = $this->input->post('dok');
				$lampiran = $file1.'*'.$file2;
				$file['lampiran'] = $lampiran;
			}else{
				$file1 = $this->upload($_FILES['lampiran']['name']);
				$file['lampiran'] = $file1;
			}
		}else{
			$file['lampiran'] = $this->input->post('dok');
		}

		if($this->input->post(NULL,TRUE)==TRUE){
			$data = array_merge($data_array,$file);
			if($id['kode'] == false or $id['kode']==null){
                                //print_r($_POST); die();
				$this->Model_home->tambah_data('tbl_arsip_kegiatan',$data);
				$this->pesan('pesan','Berhasil Menyimpan Data');
				redirect(base_url().'arsip_kegiatan/tambah');
			}else{

				$this->model_home->update_data($id,'tbl_arsip_kegiatan',$data);
				$this->pesan('pesan','Berhasil Mengubah Data');
				redirect(base_url().'arsip_kegiatan/tambah');

			}
		}
	}

}

// Developed by Muhammad Ridho
// Email: kenduanything23@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */