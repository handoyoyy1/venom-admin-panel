<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	// voucer
	function GetVoucer($where= ""){
		$data = $this->db->query('select * from tb_voucer '.$where);
		return $data;
	}

	function UpdateVoucer($data){
        $this->db->where('kode',$data['kode']);
        $this->db->update('tb_voucer',$data);
    }
	
	
	//balance
	function Getbalance($where= ""){
		$data = $this->db->query('select a.create_on AS tanggal,a.balance AS nilai,  b.first_name AS nama, c.first_name AS name, a.keterangan AS kt from tb_balance  a join tb_user b on b.id_user=a.user_id JOIN tb_user c ON c.id_user=a.member_input '.$where);
		return $data;
	}

	//ambil data tabel produk
	function search_user($title){
		$this->db->like('first_name', $title , 'both');
		$this->db->like('nama_user', $title , 'both');
		$this->db->order_by('first_name', 'ASC');
		$this->db->limit(15);
		return $this->db->get('tb_user')->result();
	}

	 //claim
	function Getclaim($where= ""){
		$data = $this->db->query('select tb_claim.reward_kode AS kode, tb_reward.nama AS reward, tb_user.first_name AS nama, tb_claim.status AS status, tb_claim.create_on AS tanggal from tb_claim right join tb_user on tb_user.id_user=tb_claim.user_id join tb_reward on tb_reward.kode=tb_claim.reward_kode '.$where);
		return $data;
	}

	 //rating
	function GetReward($where= ""){
		$data = $this->db->query('select * from tb_reward '.$where);
		return $data;
	}
	
	function UpdateReward($data){
        $this->db->where('kode',$data['kode']);
        $this->db->update('tb_reward',$data);
    }
	
	function get_user_id($id)
    {
        $this->db->where('id_user', $id);
        return $this->db->get('tb_user')->row();
    }
	
	public function GetRooms()
	{
		$data = $this->db->query('SELECT rm.`chat_room_id` AS id, rm.`name` AS room, u.first_name AS first_name, u.`last_name` AS last_name
FROM tb_chat_rooms rm JOIN tb_user u ON rm.user_id = u.id_user where rm.status = "active" ');
		return $data;
	}
	
	function GetRoom($where= ""){
		$data = $this->db->query('SELECT rm.`chat_room_id` AS id, rm.`name` AS room, u.first_name AS first_name, u.`last_name` AS last_name
FROM tb_chat_rooms rm JOIN tb_user u ON rm.user_id = u.id_user '.$where);
		return $data;
	}
	
	function UpdateRooms($data){
        $this->db->where('chat_room_id',$data['chat_room_id']);
        $this->db->update('tb_chat_rooms',$data);
    }
	
	function UpdateChatByRooms($data){
        $this->db->where('baca','no');
        $this->db->update('tb_chat_rooms',$data);
    }

	public function GetMessages($id="")
	{
		$data = $this->db->query('SELECT  m.message AS pesan,m.created_at AS waktu, u.first_name AS first_name, u.`last_name` AS last_name, u.foto AS img FROM tb_messages m JOIN tb_user u ON m.user_id = u.id_user WHERE m.chat_room_id = '.$id);
		return $data;
	}
	
	
	
	//pembelian
	function Getpembelian($where= ""){
		$data = $this->db->query('select * from tb_pembelian join tb_produk on tb_produk.id_produk=tb_pembelian.produk_id JOIN tb_user ON tb_user.id_user=tb_pembelian.user_id '.$where);
		return $data;
	}

	 //rating
	function GetRating($where= ""){
		$data = $this->db->query('select * from tb_rating '.$where);
		return $data;
	}
	
	function UpdateRating($data){
        $this->db->where('id',$data['id']);
        $this->db->update('tb_rating',$data);
    }
	
	function GetDiskon($where= ""){
		$data = $this->db->query('select * from tb_diskon '.$where);
		return $data;
	}

	function UpdateDiskon($data){
        $this->db->where('id',$data['id']);
        $this->db->update('tb_diskon',$data);
    }
	
	public function GetKota($where= "")
	{
		$data = $this->db->query('select * from tb_kota '.$where);
		return $data;
	}

	//ambil data tabel kategori User
	public function GetKatUser($where= "")
	{
		$data = $this->db->query('select * from tb_kat_user '.$where);
		return $data;
	}

	//ambil data user
	function GetUser($where) {
        $data = $this->db->query('select * from tb_user '.$where);
		return $data;
    }

	//ambil data tabel kategori
	public function GetKat($where= "")
	{
		$data = $this->db->query('select * from tb_kategori '.$where);
		return $data;
	}
	
	//ambil data tabel subkategori
	public function GetSubKat($where= "")
	{
		$data = $this->db->query('select * from tb_subkategori '.$where);
		return $data;
	}

	//ambil data tabel merk
	public function GetMerk($where= "")
	{
		$data = $this->db->query('select * from tb_merk '.$where);
		return $data;
	}

	//ambil data tabel produk
	function search_produk($title){
		$this->db->like('judul', $title , 'both');
		$this->db->order_by('judul', 'ASC');
		$this->db->limit(15);
		return $this->db->get('tb_produk')->result();
	}

	//ambil data tabel produk
	public function GetProduk($where= "")
	{
		$data = $this->db->query('select * from tb_produk '.$where);
		return $data;
	}

	public function GetTotProduk()
	{
		$data = $this->db->query('select * from tb_produk group by id_kat ');
		return $data;
	}

	public function GetDetailProduk($where = ""){
		return $this->db->query("select tb_merk.merk, tb_produk.*  from tb_produk inner join tb_merk on tb_merk.id_merk=tb_produk.id_merk $where;");
	}

	public function count_all() {
		return $this->db->count_all('tb_produk');
	}

	//ambil data dari 3 tabel
	public function GetProdukKatMerko($where= "") {
    $data = $this->db->query('SELECT p.*, q.kategori, c.merk,s.kategori as subkategori,((p.harga)+(p.harga*10/100)) as harga_ppn 
	FROM tb_produk p 
	LEFT JOIN tb_kategori q ON(p.id_kat = q.id_kat) 
	LEFT JOIN tb_merk c ON(p.id_merk = c.id_merk) 
	LEFT JOIN tb_subkategori s ON(p.id_subkat = s.id_kat) '.$where);
    return $data;
    }

	//batas crud data
	public function Simpan($table, $data){
		return $this->db->insert($table, $data);
	}

	public function Update($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}

	public function Hapus($table,$where){
		return $this->db->delete($table,$where);
	}

	function UpdateProduk($data){
        $this->db->where('id_produk',$data['id_produk']);
        $this->db->update('tb_produk',$data);
    }

    //slide

    function GetSlide($where= ""){
		$data = $this->db->query('select * from tb_slideshow '.$where);
		return $data;
	}

	function UpdateSlide($data){
        $this->db->where('id_slide',$data['id_slide']);
        $this->db->update('tb_slideshow',$data);
    }

    //sosmed
	function GetSosmed($where= ""){
		$data = $this->db->query('select * from tb_sosmed '.$where);
		return $data;
	}

	function UpdateSosmed($data){
        $this->db->where('id_sosmed',$data['id_sosmed']);
        $this->db->update('tb_sosmed',$data);
    }

    //event
    function GetEvent($where= ""){
		$data = $this->db->query('select * from tb_event '.$where);
		return $data;
	}

	function UpdateEvent($data){
        $this->db->where('id_event',$data['id_event']);
        $this->db->update('tb_event',$data);
    }

    function UpdateEventByRead($data){
        $this->db->where('baca','no');
        $this->db->update('tb_event',$data);
    }

    //testimoni
    function GetTestimoni(){
		$data = $this->db->query('select tb_user.nama_user, tb_user.foto, tb_testimoni.komentar from tb_user, tb_testimoni where tb_user.id_user = tb_testimoni.id_user');
		return $data;
	}

	//berita

	function GetNews($where= ""){
		$data = $this->db->query('select * from tb_news '.$where);
		return $data;
	}

	function UpdateNews($data){
        $this->db->where('id_news',$data['id_news']);
        $this->db->update('tb_news',$data);
    }

    function UpdateNewsByRead($data){
        $this->db->where('baca','no');
        $this->db->update('tb_news',$data);
    }

    function GetInvoice($where= ""){
		$data = $this->db->query('select * from tb_invoice '.$where);
		return $data;
	}
	
	function UpdateInvoice($data){
        $this->db->where('kode_invoice',$data['kode_invoice']);
        $this->db->update('tb_invoice',$data);
    }

    function UpdateInvoiceByRead($data){
        $this->db->where('baca','no');
        $this->db->update('tb_invoice',$data);
    }
    
    function GetkonfirmasibyID($where){
		$data = $this->db->query('select * from tb_konfirmasi '.$where);
		return $data;
	}

	function Getkonfirmasi(){
		$data = $this->db->query('select * from tb_konfirmasi');
		return $data;
	}

	function GetBank($where= ""){
		$data = $this->db->query('select * from tb_bank '.$where);
		return $data;
	}

	function UpdateBank($data){
        $this->db->where('id_bank',$data['id_bank']);
        $this->db->update('tb_bank',$data);
    }
    
	
	function GetUserdata($where= ""){
		$data = $this->db->query('select * from tb_user '.$where);
		return $data;
	}
	
	function UpdateUser($data){
        $this->db->where('id_user',$data['id_user']);
        $this->db->update('tb_user',$data);
    }
    function GetKomentarall($where= ""){
		$data = $this->db->query('select * from tb_komentar '.$where);
		return $data;
	}
	
	
    
    function GetPesan($where= ""){
		$data = $this->db->query('select * from tb_pesan '.$where);
		return $data;
	}
	
	function UpdatePesan($data){
        $this->db->where('id',$data['id']);
        $this->db->update('tb_pesan',$data);
    }
    
    function GetHalaman($where= ""){
		$data = $this->db->query('select * from tb_halaman '.$where);
		return $data;
	}
	
	function UpdateHalaman($data){
        $this->db->where('id_halaman',$data['id_halaman']);
        $this->db->update('tb_halaman',$data);
    }

	//batas crud data

	 function GetKomentar(){
		$data = $this->db->query('select ifnull(count(komentar),0) as jumlah from tb_komentar ');
		return $data;
	}

	function GetJmlTestimoni(){
		$data = $this->db->query('select ifnull(count(komentar),0) as jumlah from tb_testimoni ');
		return $data;
	}
	
	function GetJmlMember(){
		$data = $this->db->query('select ifnull(count(id_user),0) as jumlah from tb_user where level != 1 and level !=2 and level != 3');
		return $data;
	}

	 function GetDataOrder($kode){
	     $data = $this->db->query('select tb_produk.kode, tb_order.jumlah, tb_produk.judul, tb_produk.foto, tb_produk.harga, tb_produk.harga * tb_order.jumlah as total from tb_order, tb_produk where tb_order.id_barang = tb_produk.id_produk and tb_order.kode_invoice = "'.$kode.'"');
	     return $data;
	 }

	 function SumDataOrder($kode){
	     $this->db->select('sum(tb_produk.harga*tb_order.jumlah) as total');
	     $this->db->from('tb_order');
	     $this->db->join('tb_produk', 'tb_produk.id_produk = tb_order.id_barang');
	     $this->db->where('kode_invoice', $kode);
		$data = $this->db->get()->row();
	     return $data->total;
	 }

    //model untuk visitor/pengunjung
    function GetVisitor($where = ""){
		return $this->db->query("select * from tb_visitor $where");		
	}

	function GetProductView(){
		return $this->db->query("select sum(counter) as totalview from tb_produk where status = 'publish'");
	}
	//batas query pengunjung

	public function GetKate($where= "")
	{
		$data = $this->db->query('select count(*) as totalkategori from tb_kategori '.$where);
		return $data;
	}

	function TotalKat(){
		return $this->db->query("select count(*) as totalkategori from tb_produk group by id_kat; ");
	}

		//ambil data tabel album
	public function GetAlbum($where= "")
	{
		$data = $this->db->query('select * from tb_album '.$where);
		return $data;
	}

	function UpdateAlbum($data){
        $this->db->where('id_album',$data['id_album']);
        $this->db->update('tb_album',$data);
    }

	//ambil data tabel foto
	public function GetFoto($where= "")
	{

		$data = $this->db->query('select * from tb_foto left join tb_album on tb_foto.album_id=tb_album.id_album left join tb_user on tb_foto.user_id=tb_user.id_user '.$where);
		return $data;
	}

	function UpdateFoto($data){
        $this->db->where('id',$data['id']);
        $this->db->update('tb_foto',$data);
    }

	//ambil data tabel video
	public function GetVideo($where= "")
	{
		$data = $this->db->query('select * from tb_video '.$where);
		return $data;
	}

	function UpdateVideo($data){
        $this->db->where('id',$data['id']);
        $this->db->update('tb_video',$data);
    }

    public function GetCommunity($where= "")
	{
		$data = $this->db->query('select * from tb_community '.$where);
		return $data;
	}

	function UpdateCommunity($data){
        $this->db->where('id_community',$data['id_community']);
        $this->db->update('tb_community',$data);
    }
    
    public function exist_row_check($table,$field,$data){
        $this->db->where($field,$data);
        $this->db->from($table);
        $query = $this->db->get();
        return $query->num_rows();
    }

	


}

/* End of file model.php */
/* Location: ./application/models/model.php */