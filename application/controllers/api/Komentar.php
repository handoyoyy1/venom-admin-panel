<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Komentar extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Komentar_model');
    }
    
    function index_get(){
        $data = $this->get('data');
        $id_pro = $this->get('idproduk');

        switch ($data) {
            
            case "komentar":
                $komentar = $this->Komentar_model->get_by_id($id_pro);
                $this->response([
                            'kode' => 1,
                            'result' => $komentar,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         } 
    }

     function index_post(){
        $data = $this->post('data');
       
       $id_produk = $this->post('idpro');
       $id_user = $this->post('iduser');
       $komentar = $this->post('komentar');

       $data_simpan = array(
                    'id_produk' => $id_produk,
                    'id_user' => $id_user,
                    'komentar' => $komentar,
                    'tanggal' => date("Y-m-d")
                );

	     $this->Komentar_model->insert($data_simpan);
	     $message = array("kode"=>1,"pesan"=>"Komentar berhasil disimpan!");
	     $this->response($message, REST_Controller::HTTP_CREATED);
 	}
}
