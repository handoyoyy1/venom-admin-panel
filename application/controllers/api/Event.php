<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Event extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/Event_model');
    }

    

    function index_get(){
    	$data = $this->get('data');
        $event_id = $this->get('idevent');

        switch ($data) {
            case "all":
                $event = $this->Event_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $event,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "detail":
                $event_detail = $this->Event_model->get_by_id($event_id);
                $this->response([
                            'kode' => 1,
                            'id_event' => $event_detail->id_event,
                            'nama_event' => $event_detail->nama_event,
                            'detail_event' => $event_detail->detail_event,
                            'lokasi' => $event_detail->lokasi,
                            'lat' => $event_detail->Latitude,
                            
                            'tanggal' => $event_detail->tanggal,
                            
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }
}