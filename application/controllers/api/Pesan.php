<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Pesan extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('api/Pesan_model');
    }

    

    function index_get(){
    	$data = $this->get('data');
        $id = $this->get('id');

        switch ($data) {
            case "all":
                $pesan = $this->Pesan_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $pesan,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "detail":
                $pesan_detail = $this->Pesan_model->get_by_id($id);
                $this->response([
                            'kode' => 1,
                            'id' => $pesan_detail->id,
                            'judul' => $pesan_detail->judul,
                            'pesan' => $pesan_detail->pesan,
                            'create_on' => $pesan_detail->create_on,
                            
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }
}