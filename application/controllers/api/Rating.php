<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Rating extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Rating_model');
    }
    
    function index_get(){
      
    }

    function index_post(){
    $user_id = $this->post('iduser');
    $jumlah = $this->post('jumlah');
    $community_id = $this->post('idcommunity');
    $img = $this->post('img');
    $comment = $this->post('comment');

    $data_simpan = array(
      'id' => " ",
      'community_id' => $community_id,
      'user_id' => $user_id,
      'jumlah' => $jumlah,
      'img' => $img,
      'comment' => $comment,
    );

    $this->Rating_model->insert($data_simpan);
    $message = array("kode"=>1,"pesan"=>"Rating berhasil disimpan!");
    $this->response($message, REST_Controller::HTTP_CREATED);
  }
}
