<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Claim extends REST_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('api/User_model');
        $this->load->model('api/Claim_model');
        $this->load->model('api/Reward_model');
        $this->load->model('api/User_model');
        $this->load->helper('site_helper');
    }
    
    function index_get(){
        $data=$this->get('data');
        $user_id = $this->get('iduser');
        $kode = $this->get('kode');
        $otp_kode = rand(0,10000);
        
        if ($data=="mail") {
            $data = array(
                'user_id' => $user_id,
                'kode' => $otp_kode
            );
            $this->Claim_model->insert_otp($data);

            if ($this->User_model->exist_row_check('id_user', $user_id) > 0) {
                $user_detail = $this->User_model->get_by_id($user_id);

                    if($user_detail->email == null || $user_detail->email == ""){
                    }else {
                        $email = $user_detail->email;
                        $this->load->library('email');
                        $config = send_email();
                        $this->email->initialize($config);
                        $this->email->set_mailtype("html");
                        $this->email->set_newline("\r\n");
                
                        $list = array($email);
                        $this->email->to($list);
                        $this->email->from('bluecoreotp@gmail.com','Venom');
                        $this->email->subject('Kode OTP');
                        $this->email->message('New kode otp your account : '.$otp_kode);
                        //Send email
                        $this->email->send();
                        $message = array("kode"=>1, "pesan"=>"Sending mail  succes!");
                        $this->response($message, REST_Controller::HTTP_OK);
                    }                 
            }else{
                $message = array("kode"=>0, "pesan"=>"Sorry user empty!");
                $this->response($message, REST_Controller::HTTP_OK);
            }     
        }elseif ($data=="validasi"){
            if ($this->Claim_model->exist_row_check('tb_otp','kode', $kode) > 0) {
                $message = array("kode"=>1, "pesan"=>"Your code true!");
                $this->response($message, REST_Controller::HTTP_OK);

            }else{
                $message = array("kode"=>0, "pesan"=>"Your code wrong!");
                $this->response($message, REST_Controller::HTTP_OK);
            }

        }elseif ($data=="hadiah"){
            if ($this->Claim_model->exist_row_check('tb_reward','kode', $kode) > 0) {

                $detail = $this->Reward_model->get_by_id($kode);
                $this->response([
                            'kode' => 1,
                            'kode_reward' => $detail->kode,
                            'nama' => $detail->nama,
                            'point' => $detail->point,
                            'deskripsi' => $detail->deskripsi,
                            'images' => $detail->images,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);


            }else{
                $message = array("kode"=>0, "pesan"=>"Your code wrong!");
                $this->response($message, REST_Controller::HTTP_OK);
            }
        }  
    }

    function index_post(){
        $data = $this->post('data');
        $user_id = $this->post('iduser');
        $kode = $this->post('kode');
        $categori = $this->post('categori');

        if ($data=="barang") {

            if($this->Claim_model->check_claim($user_id,$kode) > 0){
                $message = array("kode"=>0, "pesan"=>"Sorry Claim Reward Failed!!");
                $this->response($message, REST_Controller::HTTP_OK);
            }else {
                $data = array(
                    'user_id' => $user_id,
                    'categori' => $categori,
                    'reward_kode' => $kode
                );
                $this->Claim_model->insert($data);
                $message = array("kode"=>1, "pesan"=>"Claim Reward Succes!");
                $this->response($message, REST_Controller::HTTP_OK);
            }


        }else if ($data=="voucer"){
            $Reward = $this->Reward_model->get_by_id($kode);
            $User = $this->User_model->get_by_id($user_id);

            if($Reward->point < $User->point){
                $data = array(
                    'user_id' => $user_id,
                    'categori' => $categori,
                    'reward_kode' => $kode
                );
                $this->Claim_model->insert($data);
                
                $message = array("kode"=>1, "pesan"=>"Claim Reward Succes!");
                $this->response($message, REST_Controller::HTTP_OK);

            }else {
                $message = array("kode"=>0, "pesan"=>"Claim Reward Wrong!");
                $this->response($message, REST_Controller::HTTP_OK);

            }

            



        }


    }
}