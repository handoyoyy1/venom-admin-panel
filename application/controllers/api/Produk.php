<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Produk extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Produk_model');
    }

    

    function index_get(){
    	$data = $this->get('data');
    	$name = $this->get('name');
        $produk_id = $this->get('idproduk');
        $kategori_id = $this->get('idkategori');
        $subkategori_id = $this->get('idsubkategori');
        $user_id = $this->get('iduser');
		$harga = $this->get('harga');
        switch ($data) {
			case "kalkulasi":
                $kalkulasi = $this->Produk_model->get_by_kalkulasi($user_id,$harga);
                $this->response($kalkulasi, REST_Controller::HTTP_OK);
                break;
			case "diskon":
                $diskon = $this->Produk_model->get_by_diskon($user_id);
				$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;
        
                $this->response([
                            'kode' => 1,
                            'credit_a' => $diskon->credit_a,
							'credit_b' => $diskon->credit_b,
							'diskon_a' => $D1,
							'diskon_b' => $D2,
							'diskon_c' => $D3,
							'diskon_d' => $D4,
							'diskon_e' => $D5,
							'satuan_credit_a' => $diskon->satuan_credit_a,
							'satuan_credit_b' => $diskon->satuan_credit_b,
							'satuan_diskon_a' => $diskon->satuan_diskon_a,
							'satuan_diskon_b' => $diskon->satuan_diskon_b,
							'satuan_diskon_c' => $diskon->satuan_diskon_c,
							'satuan_diskon_d' => $diskon->satuan_diskon_d,
							'satuan_diskon_e' => $diskon->satuan_diskon_e,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "all":
                $produk = $this->Produk_model->get_all();
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            case "hot":
				/*$diskon = $this->Produk_model->get_by_diskon($user_id);
				$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;*/
                $produk = $this->Produk_model->get_by_hot($user_id/*,$D1,$D2,$D3,$D4,$D5*/);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "express":
				/*$diskon = $this->Produk_model->get_by_diskon($user_id);
				$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;*/
                $produk = $this->Produk_model->get_by_express($subkategori_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
			case "promo":
                $produk = $this->Produk_model->get_by_promo();
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "simulator":
				$diskon = $this->Produk_model->get_by_diskon($user_id);
				//$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;
                $produk = $this->Produk_model->get_by_simulator($kategori_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "produk":
				/*$diskon = $this->Produk_model->get_by_diskon($user_id);
				$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;*/
                $produk = $this->Produk_model->get_by_kategori($kategori_id,/*,$D1,$D2,$D3,$D4,$D5,*/$subkategori_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
                case "subkategori":
                $produk = $this->Produk_model->get_by_subkategori($subkategori_id);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "cari":
                //$diskon = $this->Produk_model->get_by_diskon($user_id);
				//$D1 = $diskon->diskon_a; $D2= $diskon->diskon_b;$D3= $diskon->diskon_c;$D4= $diskon->diskon_d;$D5 = $diskon->diskon_e;
                $produk = $this->Produk_model->get_all_cari($name/*,$D1,$D2,$D3,$D4,$D5*/);
                $this->response([
                            'kode' => 1,
                            'result' => $produk,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            case "detail":
                $produk_detail = $this->Produk_model->get_by_id($produk_id,$id_user);
                $this->response([
                            'kode' => 1,
                            'id_produk' => $produk_detail->id_produk,
                            'judul' => $produk_detail->judul,
                            'harga' => $produk_detail->harga,
                            'jumlah' => $produk_detail->jumlah,
                            //'kondisi' => $produk_detail->kondisi,
                            'id_merk' => $produk_detail->id_merk,
                            'id_kat' => $produk_detail->id_kat,
                            'id_subkat' => $produk_detail->id_subkat,
                            'ket' => $produk_detail->ket,
                            'status' => $produk_detail->status,
                            'counter' => $produk_detail->counter,
                            'tgl_input_pro' => $produk_detail->tgl_input_pro,
                            'foto' =>explode(" ", substr($produk_detail->foto,0,-1)) ,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break;
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }
}