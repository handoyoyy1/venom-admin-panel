<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo $nama; ?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li>
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-home"></i> <span>Dashboard</span> 
              </a>
            </li>
            <?php if($this->session->userdata('level') == '2'){ ?>
                    <li>
                      <a href="<?php echo base_url(); ?>news">
                        <i class="fa fa-newspaper-o"></i> <span>News</span> 
                      </a>
                    </li>
                    <?php } ?>
            <?php if($this->session->userdata('level') == '2'){ ?>
                    <li>
                      <a href="<?php echo base_url(); ?>event">
                        <i class="glyphicon glyphicon-calendar"></i> <span>Event</span> 
                      </a>
                    </li>
            <?php } ?>
            <?php if( $this->session->userdata('level') == '1'){ ?>
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-file-o"></i>
                    <span>Page</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                    <!--<li>
                      <a href="<?php //echo base_url(); ?>halaman">
                        <i class="fa fa-list"></i> <span>Halaman</span> 
                      </a>
                    </li>-->
                    <?php if( $this->session->userdata('level') == '1' || $this->session->userdata('level') == '3' ){ ?>
                    <li>
                      <a href="<?php echo base_url(); ?>news">
                        <i class="fa fa-newspaper-o"></i> <span>Berita</span> 
                      </a>
                    </li>
                    <?php } ?>
                    <li>
                      <a href="<?php echo base_url(); ?>event">
                        <i class="glyphicon glyphicon-calendar"></i> <span>Event</span> 
                      </a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('level') == '1' || $this->session->userdata('level') == '3'){ ?>
            <li class="treeview">
                  <a href="#">
                    <i class="fa  fa-archive"></i>
                    <span>Master</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                    <li>
                      <a href="<?php echo base_url(); ?>bank">
                        <i class="fa fa-bank"></i> <span>Bank</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>subkategori">
                        <i class="fa fa-tag"></i><span>Kategori Home</span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>kategori">
                        <i class="fa fa-tag"></i> <span>Kategori Simulator</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>merk">
                        <i class="fa fa-diamond"></i> <span>Merk</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>level">
                        <i class="fa fa-user"></i> <span>Level</span> 
                      </a>
                    </li>
                    
                  </ul>
            </li>
            <li class="treeview">
                  <a href="#">
                    <i class="fa  fa-shopping-cart"></i>
                    <span>Produk</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                    <li>
                      <a href="<?php echo base_url(); ?>produk">
                        <i class="fa fa-photo"></i> <span>Produk</span> 
                      </a>
                    </li>
                    <!--<li>
                      <a href="<?php echo base_url(); ?>produkexpress">
                        <i class="fa fa-photo"></i> <span>Produk Express</span> 
                      </a>
                    </li>-->
                    <li>
                      <a href="<?php echo base_url(); ?>produksimulator">
                        <i class="fa fa-photo"></i> <span>Produk Simulator</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>diskon">
                        <i class="fa fa-cc-discover"></i> <span>Voucher Diskon</span> 
                      </a>
                    </li>
                </ul>
            </li>
             <li class="treeview">
                  <a href="#">
                    <i class="fa fa-camera-retro"></i>
                    <span>Galeri</span>
                    <span class="pull-right-container"></span>
                  </a>
                <ul class="treeview-menu">
                  <li>
                    <a href="<?php echo base_url(); ?>album">
                      <i class="fa fa-list-alt"></i> <span>Album</span> 
                    </a>
                  </li>
    
                  <li>
                    <a href="<?php echo base_url(); ?>foto">
                      <i class="fa fa-photo"></i> <span>Foto </span> 
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>video">
                      <i class="fa fa-video-camera"></i> <span>Video </span> 
                    </a>
                  </li>
                </ul>
            </li>
            <?php if( $this->session->userdata('level') == '1'){ ?>
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-file-o"></i>
                    <span>Transaksi</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                    
                    <?php if( $this->session->userdata('level') == '1' || $this->session->userdata('level') == '3' ){ ?>
                    <li>
                      <a href="<?php echo base_url(); ?>pembelian">
                        <i class="fa  fa-shopping-cart"></i> <span>Pembelian</span> 
                      </a>
                    </li>
                    <?php } ?>
                    
                </ul>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>reward">
                <i class="fa fa-star"></i> <span>Reward</span> 
              </a>
            </li>
            <?php } ?>
            <li>
              <a href="<?php echo base_url(); ?>community">
                <i class="fa fa-users"></i> <span>Community</span> 
              </a>
            </li>
            
            <li>
              <a href="<?php echo base_url(); ?>user/member">
                <i class="fa fa-users"></i> <span>Member</span> 
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>rating">
                <i class="fa fa-star"></i> <span>Rating</span> 
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>chatting">
                <i class="fa fa-comments"></i> <span>Chatting</span> 
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>pesan">
                <i class="fa  fa-envelope-o"></i> <span>Pesan</span> 
              </a>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('level') == '4'){ ?>
            
            <li>
              <a href="<?php echo base_url(); ?>community/editcommunity">
                <i class="fa fa-users"></i> <span>Community</span> 
              </a>
            </li>
            
            <?php } ?>
           
             <!--<li>
              <a href="<?php //echo base_url(); ?>pesan">
                <i class="fa  fa-envelope-o"></i> <span>Pesan</span> 
              </a>
            </li>
            <li>
              <a href="<?php //echo base_url(); ?>pesan/komentar">
                <i class="fa  fa-comments-o"></i> <span>Komentar</span> 
              </a>
            </li>-->
            <?php if($this->session->userdata('level') == '1' || $this->session->userdata('level') == '3'){ ?>           
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>Status</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>invoice">
                    <i class="fa fa-credit-card"></i> <span>Invoice</span> 
                  </a>
                </li>
  
                <li>
                  <a href="<?php echo base_url(); ?>invoice/barang">
                    <i class="fa fa-money"></i> <span>Barang </span> 
                  </a>
                </li>
                </ul>
            </li>
            <?php } ?>

            <?php if($this->session->userdata('level') == '1' || $this->session->userdata('level') == '3'){ ?>           
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>report/penjualanperhari">
                    <i class="fa fa-credit-card"></i> <span>Penjualan Perhari</span> 
                  </a>
                </li>
  
                <li>
                  <a href="<?php echo base_url(); ?>report/penjualanbyinvoice">
                    <i class="fa fa-money"></i> <span>Penjualan By Invoice </span> 
                  </a>
                </li>
                </ul>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('level') == '1' ){ ?>
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Tools</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                <ul class="treeview-menu">
                    <li  >
                      <a href="<?php echo base_url(); ?>User">
                        <i class="fa fa-user"></i> <span>User</span> 
                      </a>
                    </li>
                    <?php if($this->session->userdata('level') == '1' || $this->session->userdata('level') == ''){ ?>
                    
                    <li>
                      <a href="<?php echo base_url(); ?>slideshow">
                        <i class="glyphicon glyphicon-film"></i> <span>Slideshow</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>sosmed">
                        <i class="fa fa-twitter"></i> <span>Sosial media</span> 
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url(); ?>login/logout">
                        <i class="fa fa-sign-out"></i> <span>Keluar</span> 
                      </a>
                    </li>
                    
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
            
            <!--<li>
              <a href="<?php// echo base_url(); ?>login/logout">
                <i class="fa fa-sign-out"></i> <span>Keluar</span> 
              </a>
            </li>-->
            
            <!-- <li><a href="#"><i class="fa fa-circle-o text-danger"></i> Important</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-warning"></i> Warning</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-info"></i> Information</a></li> -->
          </ul>
        </section>