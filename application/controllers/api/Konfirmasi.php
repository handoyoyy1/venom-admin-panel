<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Konfirmasi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('site_helper');
        $this->load->model('api/Konfirmasi_model');
    }
    
    function index_get(){
    	$data = $this->get('data');
        $id = $this->get('iduser');

        switch ($data) {
            case "detail":
                $konfirm = $this->Konfirmasi_model->get_by_id($id);
                $this->response([
                            'kode' => 1,
                            'result'=>$konfirm,
                            
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                break; 
            default:
                    echo "Maaf data anda kosong..!"; 
         }	

    }

    function index_post(){
        //$data = $this->post('data');
       
       $kode_invoice = $this->post('kdinvoice');
       $id_user = $this->post('iduser');
       $atas_nama = $this->post('nama');
       $bank =  $this->post('bank');
       $no_rek = $this->post('norek');
       $tanggal = $this->post('tanggal');
       $image = $this->post('images');
       
      /* $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        
        $dataimage = base64_decode($image);
        $nama = uniqid() . '.png';
        $file = "./assets/upload/konfirm/".$nama;
        
        file_put_contents($file, $data);*/
        
        $decode = base64_decode($image);
        $nama = uniqid() . '.png';
        $path = "./assets/upload/konfirm/".$nama;
        $file = fopen($path, 'wb');
        $is_writen = fwrite($file, $decode);
        fclose($file);

       $data_simpan = array(
                    'kode_invoice' => $kode_invoice,
                    'id_user' => $id_user,
                    'atas_nama' => $atas_nama,
                    'bank' => $bank,
                    'no_rek' => $no_rek,
                    'images' => base_url()."assets/upload/konfirm/".$nama,
                    'tanggal' => $tanggal
                );

	     $this->Konfirmasi_model->insert($data_simpan);
	     $message = array("kode"=>1,"pesan"=>"Konfirmasi berhasil disimpan!");
	     $this->response($message, REST_Controller::HTTP_CREATED);
 	}
}
