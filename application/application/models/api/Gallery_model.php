<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends CI_Model
{
	public $table = 'tb_foto';
    public $id = 'photo_id';
    public $order = 'DESC';

	function __construct()
    {
        parent::__construct();
    }


    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
     function get_all_album()
    {
        return $this->db->get('tb_album')->result();
    }
    
    function get_by_album($id)
    {
        $this->db->select('tb_foto.id ,tb_foto.nama_foto , tb_foto.album_id,tb_foto.status_foto,tb_foto.user_id,count(tb_like.id) as jml_like, count(tb_dislike.id) as jml_dislike');
        $this->db->where('album_id', $id);
        $this->db->where('status_foto', 'publish');
        $this->db->join('tb_like', 'tb_like.foto_id=tb_foto.id','left');
        $this->db->join('tb_dislike', 'tb_dislike.foto_id=tb_foto.id','left');
        $this->db->group_by('id');
        return $this->db->get($this->table)->result();
    }
    
    function insert_like($data)
    {
        $this->db->insert('tb_like', $data);
    }

    function insert_dislike($data)
    {
        $this->db->insert('tb_dislike', $data);
    }
    
    function insert_foto($data)
    {
        $this->db->insert('tb_foto', $data);
    }
    
    function insert_video($data)
    {
        $this->db->insert('tb_video', $data);
    }
    
    public function exist_row_check_dislike($field,$data){
        $this->db->where('foto_id',$field);
        $this->db->where('user_id',$data);
        $this->db->from('tb_dislike');
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function exist_row_check_like($field,$data){
        $this->db->where('foto_id',$field);
        $this->db->where('user_id',$data);
        $this->db->from('tb_like');
        $query = $this->db->get();
        return $query->num_rows();
    }


}