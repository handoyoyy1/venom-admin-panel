<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('site_helper');
		$this->load->model('Model');
		$this->load->model('api/Invoice_model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function notification(){
		if (isset($_GET['kode_invoice'])) {
			$data = array(
				'kode_invoice' => $_GET['kode_invoice'],
				'baca' => 'yes', 
			);
			$this->Model->UpdateInvoice($data);
			header('location:'.base_url('invoice/detail/'.$_GET['kode_invoice']));
		}elseif (isset($_GET['invoice'])) {
			$baca = array('baca' => 'yes', );
		  	$this->Model->UpdateInvoiceByRead($baca);
		  	header('location:'.base_url('invoice'));
		}elseif (isset($_GET['event'])) {
			$baca = array('baca' => 'yes', );
			$this->Model->UpdateEventByRead($baca);
			header('location:'.base_url('event'));
		}elseif (isset($_GET['news'])) {
			$baca = array('baca' => 'yes', );
			$this->Model->UpdateNewsByRead($baca);
			header('location:'.base_url('news'));
		}elseif (isset($_GET['room'])) {
			$baca = array('baca' => 'yes', );
			$this->Model->UpdateChatByRooms($baca);
			header('location:'.base_url('news'));
		}else{
			if (isset($_POST['baca'])) {
				$count_invoice=$this->Model->GetInvoice('where baca = "no" and barang_status!="terkirim"')->num_rows();
				$count_news=$this->Model->GetNews('where baca = "no"')->num_rows();
				$count_event=$this->Model->GetEvent('where baca = "no"')->num_rows();
				$count_barang_terima=$this->Model->GetInvoice('where baca = "no" and barang_status="terkirim"')->num_rows();
				
				$count_room=$this->Model->GetRoom('where baca = "no"')->num_rows();

				$jmlnotif=$count_invoice+$count_news+$count_barang_terima+$count_event+$count_room;
				$output="";
				$output1="";
				$output = '
				<li class="header">You have '.$jmlnotif.' notifications</li>
              	<li>
                	<!-- inner menu: contains the actual data -->
                	<ul class="menu">
                  		<li>
                    		<a href="'.base_url("dashboard/notification?invoice=1").'">
                      			<i class="fa fa-shopping-cart text-aqua"></i> '.$count_invoice.' Pesanan Baru Ditambahkan
                    		</a>
                  		</li>
                  		<li>
                    		<a href="'.base_url("dashboard/notification?invoice=1").'">
                      			<i class="glyphicon glyphicon-calendar text-danger"></i> '.$count_barang_terima.' Barang Telah Diterima Pembeli
                    		</a>
                  		</li>
                  		<li>
                    		<a href="'.base_url("dashboard/notification?news=1").'">
                      			<i class="fa fa-newspaper-o text-warning"></i> '.$count_news.' Berita Baru Ditambahkan
                    		</a>
                  		</li>
                  		<li>
                    		<a href="'.base_url("dashboard/notification?event=1").'">
                      			<i class="glyphicon glyphicon-calendar text-danger"></i> '.$count_event.' Event Baru Ditambahkan
                    		</a>
                  		</li>
                	</ul>
              	</li>	
			   	';

				$row1=$this->Model->GetInvoice('where baca = "no" and barang_status!="terkirim"')->result_array();
				$row_news1=$this->Model->GetNews('where baca = "no"')->result_array();
				$row_event1=$this->Model->GetEvent('where baca = "no"')->result_array();
				$row_barang_diterima1=$this->Model->GetInvoice('where baca = "no" and barang_status="terkirim"')->result_array();
				
				$row_room=$this->Model->GetRoom('where rm.baca = "no"')->result_array();

				foreach ($row1 as $notif1) { 
					$output1.="
					<script>
						$.notify({
							title: '<strong>Pesanan Baru</strong><br>',
							message: '<small><em>Kode Invoice : ".$notif1['kode_invoice']."</em></small><br /><small><em>".$notif1["create_on"]."</em></small>',
							url: '".base_url('dashboard/notification?kode_invoice='.$notif1['kode_invoice'])."',
						},{
							animate: {
								enter: 'animated fadeInUp',
								exit: 'animated fadeOutRight'
							},
							placement: {
								from: 'bottom',
								align: 'right'
							},
							z_index: 1031,
						});
					</script>";
				}

				foreach ($row_news1 as $news1) { 
					$output1.="
					<script>
						$.notify({
							title: '<strong>Berita Baru</strong><br>',
							message: '<small><em>Judul : ".$news1['judul']."</em></small><br /><small><em>".$news1["tanggal"]."</em></small>',
							url: '".base_url('dashboard/notification?news=1')."',
						},{
							type : 'warning',
							animate: {
								enter: 'animated fadeInUp',
								exit: 'animated fadeOutRight'
							},
							placement: {
								from: 'bottom',
								align: 'right'
							},
							z_index: 1031,
						});
					</script>";
				}

				foreach ($row_event1 as $event1) { 
					$output1.="
					<script>
						$.notify({
							title: '<strong>Event Baru</strong><br>',
							message: '<small><em>Nama Event : ".$event1['nama_event']."</em></small><br /><small><em>".$event1["tanggal"]."</em></small>',
							url: '".base_url('dashboard/notification?event=1')."',
						},{
							type : 'danger',
							animate: {
								enter: 'animated fadeInUp',
								exit: 'animated fadeOutRight'
							},
							placement: {
								from: 'bottom',
								align: 'right'
							},
							z_index: 1031,
						});
					</script>";
				}

				foreach ($row_barang_diterima1 as $barang_diterima1) { 
					$output1.="
					<script>
						$.notify({
							title: '<strong>Barang Telah Diterima</strong><br>',
							message: '<small><em>Kode Invoice : ".$barang_diterima1['kode_invoice']."</em></small><br /><small><em>".$barang_diterima1["create_on"]."</em></small>',
							url: '".base_url('dashboard/notification?kode_invoice='.$barang_diterima1['kode_invoice'])."',
						},{
							type : 'success',
							animate: {
								enter: 'animated fadeInUp',
								exit: 'animated fadeOutRight'
							},
							placement: {
								from: 'bottom',
								align: 'right'
							},
							z_index: 1031,
						});
					</script>";
				}
				
				foreach ($row_room as $room) { 
					$output1.="
					<script>
						$.notify({
							title: '<strong>Chatting masuk</strong><br>',
							message: '<small><em>Nama : ".$room['first_name']." ".$room['last_name']."</em></small><br /><small><em>".$room["room"]."</em></small>',
							url: '".base_url('Chatting/custemer/'.$room['id'])."',
						},{
							type : 'success',
							animate: {
								enter: 'animated fadeInUp',
								exit: 'animated fadeOutRight'
							},
							placement: {
								from: 'bottom',
								align: 'right'
							},
							z_index: 1031,
						});
					</script>";
				}

			   	$data = array(
				 	'notification'   => $output,
				  	'notificationpopup'   => $output1,
				  	'unseen_notification' => $jmlnotif
				 );
		        echo json_encode($data);
		    }
		}
	}


	public function index()
	{
	    
		$data = array(
			'total_product' => $this->Model->GetProduk()->num_rows(),
			'product_view' => $this->Model->GetProductView()->result_array(),
			'testimoni' => $this->Model->GetTestimoni()->result_array(),
			'komentar' => $this->Model->GetKomentar()->result_array(),
			'sales' => $this->Model->Getsalestoday()->result_array(),
			'sales_mount' => $this->Model->Getsalesmount()->result_array(),
			'jmltesti' => $this->Model->GetJmlTestimoni()->result_array(),
			'jmlmember' => $this->Model->GetJmlMember()->result_array(),
			'nama' => $this->session->userdata('nama'),	
		);
		
		$this->load->view('index', $data);
	}
	
	public function send(){
	    $this->load->library('email');
		$code_verifikasi = rand(0,1000000);

        $config = send_email();
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $list = array('hamidbanyuwangi@gmail.com','sspanji3@gmail.com','panca990@gmail.com');
        $this->email->to($list);
        //$this->email->from('admin@levanpedia.com','Vibe24jam');
        $this->email->from('vibelsx@gmail.com','Vibe24jam');
        $this->email->subject('verifikasi akun');
        $this->email->message('Kode verifikasi akun anda :'.$code_verifikasi);
        
        //Send email
        $this->email->send();

	}
	
	public function jajal(){
	    $to ="cV_YeVwqyfg:APA91bHHwmkacpo4tx_IZALm7oGmSOOQMhbi5Q1DSK_ewJTijiNdBcbVr2uNmM1YjyYjvSDnCReO1l2WvxLAfAunKqvcEVEy6y2oDbsupV7NIuJpgx7snsC6gV1MUSmS9Kb9tPXJt19i";
	$values = array();
        $values['status'] = "event";
        $values['title'] = "km";
        $values['message'] = "ll";
    
        // Server key from Firebase Console
    define( 'API_ACCESS_KEY', 'AIzaSyDe328lwM1xfdq9OcNzp-YgXkOOWf93Wfs' );
    
    $data = array("to" => $to,
                    "data" => $values,
                 /* "notification" => 
                  array( 
                      "title" => "Shareurcodes.com", 
                      "body" => "A Code Sharing Blog!"
                )*/);                                                                    
    $data_string = json_encode($data); 
    
    echo "The Json Data : ".$data_string; 
    
    $headers = array
    (
         'Authorization: key=' . API_ACCESS_KEY, 
         'Content-Type: application/json'
    ); 
    $ch = curl_init();  
    
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );                                                                  
    curl_setopt( $ch,CURLOPT_POST, true );  
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string); 
    
    $result = curl_exec($ch);
    
    curl_close ($ch);
    
    echo "<p>&nbsp;</p>";
    echo "The Result : ".$result;
	
	}
}

// Developed by Muhammad Ridho
// Email: kenduanything23@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */