<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chatting extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		 
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'admin' => 'admin',
			'data_room' =>$this->Model->GetRooms()->result_array()
		);
		$this->load->view('chatting/chatting', $data);
	}

	public function custemer($id=null)
	{
	    $data = array(
				'chat_room_id' => $id,
				'status' => 'active',
				'baca' => 'yes'
				);

		$this->Model->UpdateRooms($data);
		 
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'admin' => 'admin',
			'user_id' => $this->session->userdata('id_user'),
			'room_id' => $id,
			'data_room' =>$this->Model->GetRooms()->result_array()
		);

		$this->load->view('chatting/chatting_custemer', $data);
	}

	function data_messages($id=null){
		$data=$this->Model->GetMessages($id)->result_array();
		echo json_encode($data);
	}

	function simpan_messages(){
		$data = array(	
			'message'=> $this->input->post('messages'),
			'user_id' => $this->input->post('user_id'),
			'chat_room_id' => $this->input->post('room_id')	
			);
		$result = $this->Model->Simpan('tb_messages', $data);
		echo json_encode($result);
	}
	
	function data_room(){
		$data=$this->Model->GetRooms()->result_array();
		echo json_encode($data);
	}

	public function tambahcontact()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
		);
		$this->load->view('chatting/tambah_contact', $data);
	}
}