<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claim extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('Model');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url());
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),
			'data_claim' => $this->Model->Getclaim("order by id desc")->result_array(),
		);
		$this->load->view('claim/claim', $data);
	}

	
}